<?php

/*
 *  Define Navigation Text
 */
$scroll = 1;
$load_css = array(
        'style.1.0.3',
        'redactor',
		'jquery.fancybox',
        'jquery.fancybox-thumbs',
        'dp',
);

$load_js = array(
        'jquery',
        'jquery.html5uploader',
        'uploader_config_single',
        'uploader_config_more',
		'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'markerclusterer',
        'tCity',
        'city_select_',
        'common',
        'detail',
        'publish',
        'theme',
        'redactor',
        'redactor_zh_cn',
        'redactor_config',
);

$title_text = '创意世界智库';

$navigation_text = array(
        '0' => '<a class="on" href="javascript:;">智库权责</a>',
        '1' => '<a href="javascript:;">智库资讯</a>',
        '2' => '<a href="javascript:;">交流区</a>',
        '3' => '<a href="javascript:;">创意智库</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-thinktank.php');
include('modules/sidebar.php');

include('blocks/thinktank.php');

include('modules/footer.php');
?>