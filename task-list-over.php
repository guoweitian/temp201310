<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
        'style.1.0.3',
        'jquery.fancybox',
);

$load_js = array(
        'jquery',
        'jquery.html5uploader',
        'uploader_config_single',
        'jquery.fancybox.min',
        'common',
);

$title_text = '任务人才比赛';

$navigation_text = array(
        '0' => '<a href="task-list.php">正在寻找人才的任务</a>',
        '1' => '<a class="on" href="task-list-over.php">已经找到人才的任务</a>',
        '2' => '<a href="task-publish.php">发布任务</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-task.php');
include('modules/sidebar.php');

include('blocks/task-list-over.php');

include('modules/footer.php');
?>