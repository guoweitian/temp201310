<?php

/*
 *  Define Navigation Text
 */
$scroll = 1;
$load_css = array(
        'style.1.0.3',
        'redactor',
        'rateit',
        'jquery.fancybox',
        'jquery.fancybox-thumbs',
);

$load_js = array(
        'jquery',
        'redactor.min',
        'redactor_zh_cn',
        'redactor_config',
        'jquery.html5uploader',
        'uploader_config_single',
        'uploader_config_more',
        'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'tabs',
        'jquery.rateit.min',
        'jquery.datepicker',
        'datepicker_lang_CN',
        'publish',
        'common',
        'detail',
        'theme',
);

$title_text = '任务人才比赛';

$navigation_text = array(
        '0' => '<a class="on" href="javascript:;">任务详情</a>',
        '1' => '<a href="javascript:;">参赛人才</a>',
        '3' => '<a href="javascript:;">讨论区</a>',
        '4' => '<a href="javascript:;">竞猜</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-task.php');
include('modules/sidebar.php');

include('blocks/task-over.php');

include('modules/footer.php');
?>