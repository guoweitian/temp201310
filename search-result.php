<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
        'style.1.0.3',
        'jquery.fancybox',
);

$load_js = array(
        'jquery',
        'jquery.html5uploader',
        'uploader_config_single',
        'jquery.fancybox.min',
        'common',
);

$title_text = '搜索';

$navigation_text = array(
        '0' => '<a href="search.php">来自创意世界的内容</a>',
        '1' => '<a class="on" href="javascript:;">来自互联网的内容</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation.php');
include('modules/sidebar.php');

include('blocks/search-result.php');

include('modules/footer.php');
?>