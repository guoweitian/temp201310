<?php

/*
 *  Define Navigation Text
 */
$scroll = 1;
$load_css = array(
        'style.1.0.3',
        'jquery.fancybox',
        'jquery.fancybox-thumbs',
);

$load_js = array(
        'jquery',

        'masonry.min',
        'imagesloaded.min',
        'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'jquery.html5uploader',
        'uploader_config_single',
        'common',
        'detail',
);

$title_text = '个人主页';

$navigation_text = array(
    '0' => '<a class="on" href="javascript:;">关于我</a>',
    '1' => '<a href="javascript:;">我的创意世界大赛</a>',
    '2' => '<a href="javascript:;">我的创意世界交易所</a>',
    '3' => '<a href="javascript:;">我的动态</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation.php');
include('modules/sidebar.php');

include('blocks/profile-self.php');

include('modules/footer.php');
?>