<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
        'style.1.0.3',
        'jquery.fancybox',
        'jquery.fancybox-thumbs'
);

$load_js = array(
        'jquery',
        'masonry.min',
        'jquery.html5uploader',
        'uploader_config_single',
        'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'common',
        'circle',
        'mslider.min',
        'mslider'
);

$title_text = '圈子';

$navigation_text = array(
        '0' => '<a href="find-friend.php">寻找朋友</a>',
        '1' => '<a class="on" href="circle-me.php">圈我的人</a>',
        '2' => '<a href="my-circle.php">我的圈子</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation.php');
include('modules/sidebar.php');

include('blocks/circle-me.php');

include('modules/footer.php');
?>