<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
        'style.1.0.3',
        'redactor',
        'jquery-ui',
        'dp',
        'jquery.fancybox',
);

$load_js = array(
        'jquery',
        'jquery.html5uploader',
        'uploader_config_single',
        'uploader_config_more',
        'jquery.fancybox.min',
        'tabs',
        'jquery.datepicker',
        'datepicker_lang_CN',
        'common',
        'publish',
        'exchange_publish',
        'redactor',
        'redactor_zh_cn',
        'redactor_config',
);

$title_text = '创意世界交易所发布';

$navigation_text = array(
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-trade.php');
include('modules/sidebar.php');

include('blocks/trade-publish.php');

include('modules/footer.php');
?>