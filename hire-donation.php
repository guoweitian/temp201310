<?php

/*
 *  Define Navigation Text
 */
$scroll = 1;
$load_css = array(
        'style.1.0.3',
        'redactor',
        'dp',
        'jquery.fancybox',
        'jquery.fancybox-thumbs'
);

$load_js = array(
        'jquery',
        'jquery.html5uploader',
        'uploader_config_single',
        'uploader_config_more',
        'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'jquery.datepicker',
        'datepicker_lang_CN',
        'jquery.fancybox.min',
        'tabs',
        'detail',
        'publish',
        'theme',
        'common',
        'redactor',
        'redactor_zh_cn',
        'redactor_config',
);

$title_text = '创意世界交易所';

$navigation_text = array(
        '0' => '<a class="on" href="javascript:;">公益详情</a>',
        '1' => '<a href="javascript:;">爱心用户</a>',
        '2' => '<a href="javascript:;">讨论区</a>',
        '3' => '<a href="javascript:;">竞猜</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-hire.php');
include('modules/sidebar.php');

include('blocks/hire-donation.php');

include('modules/footer.php');
?>