<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
        'style.1.0.3',
        'jquery.fancybox',
        'jquery.fancybox-thumbs'
);

$load_js = array(
        'jquery',
        'masonry.min',
        'jquery.html5uploader',
        'uploader_config_single',
        'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'common',
        'circle',
);

$title_text = '圈子';

$navigation_text = array(
        '0' => '<a href="find-friend.php">寻找朋友</a>',
        '1' => '<a href="circle-me.php">圈我的人</a>',
        '2' => '<a class="on" href="my-circle.php">我的圈子</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation.php');
include('modules/sidebar.php');

include('blocks/my-circle-edit.php');

include('modules/footer.php');
?>