<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
        'style.1.0.3',
        'redactor',
        'jquery-ui',
        'dp',
        'jquery.fancybox',
);

$load_js = array(
        'jquery',
        'jquery.html5uploader',
        'uploader_config_single',
        'uploader_config_more',
        'jquery.fancybox.min',
        'tabs',
        'jquery.datepicker',
        'datepicker_lang_CN',
        'common',
        'publish',
        'project_publish',
        'redactor',
        'redactor_zh_cn',
        'redactor_config',
);

$title_text = '投资项目比赛发布';

$navigation_text = array(
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-project.php');
include('modules/sidebar.php');

include('blocks/project-publish.php');

include('modules/footer.php');
?>