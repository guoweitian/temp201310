<?php
$value = array();
if($_POST["email"] == "" || $_POST["user_name"] == "" || $_POST["password"] == "" || $_POST["password_again"] == "" || strtolower($_POST["verify_code"]) != "gwcv" || !isset($_POST["is_agree_protocol"])){
    $value["status"] = 10001;
}else{
    $value["status"] = 0;
}
$value["error"] = array();
if($_POST["email"] == "")$value["error"]["email"] = "请输入正确的邮箱地址";
if($_POST["user_name"] == "")$value["error"]["user_name"] = "请输入正确的用户名";
if($_POST["password"] == "")$value["error"]["password"] = "请输入正确的密码";
if($_POST["password_again"] == "")$value["error"]["password_again"] = "请输入一样的密码";
if(strtolower($_POST["verify_code"]) != "gwcv")$value["error"]["verify_code"] = "请输入正确的验证码";
if(!isset($_POST["is_agree_protocol"]))$value["error"]["is_agree_protocol"] = "请接受注册协议";
echo json_encode($value);
?>