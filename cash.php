<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
        'style.1.0.3',
        'redactor',
        'dp',
        'jquery.fancybox',
        'jquery.fancybox-thumbs'
);

$load_js = array(
        'jquery',
        'jquery.datepicker',
        'jquery.html5uploader',
        'uploader_config_single',
        'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'common',
        'detail',
        'datepicker_lang_CN',
);

$title_text = '现金';

$navigation_text = array(
        '0' => '<a href="creative-index.php">创意指数</a>',
        '1' => '<a href="order-manage.php">订单</a>',
        '2' => '<a class="on" href="cash.php">现金</a>',
        '3' => '<a href="ccb.php">创创币</a>',
        '4' => '<a href="userfile.php">个人资料</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation.php');
include('modules/sidebar.php');

include('blocks/cash.php');

include('modules/footer.php');
?>