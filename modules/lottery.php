<div class="detailPageMid">
        <div class="proContextDiv">
                <div class="titDiv">
                        <span></span>

                        <h2 class="Js_scroll"><?php echo $guessTT; ?></h2>
                </div>

                <div class="quiz">
                        <p class="mt30 des"><span>当前竞猜奖池拥有创创币</span><b>10,000元</b></p>
                        <ul class="mt25 qusItem"  style="color: #5f5f5f;">
                                <li>
                                        <p class="question mb10"><b>01</b>当前竞猜奖池能否获得投资？</p>

                                        <p class="height_5"></p>

                                        <div class="n-agree font14">
            <span class="mr25" data-name="radio1">
            <label class="Js_label_ok radio ok"><input class="mr10" type="radio" name="radio1" checked="checked">可以获得</label>
            </span>
            <span class="Js_radio" data-name="radio1">
            <label class="Js_label_ok radio"><input class="mr10" type="radio" name="radio1">不能获得</label>
            </span>
                                        </div>
                                </li>
                                <li>
                                        <p class="question mb10"><b>02</b>创意项目能否成功交易？</p>

                                        <p class="height_5"></p>

                                        <div class="n-agree font14">
            <span class="mr25" data-name="radio2">
              <label class="Js_label_ok radio ok"><input class="mr10" type="radio" name="radio2" checked="checked">可以成功</label>
            </span>
            <span class="Js_radio" data-name="radio2">
              <label  class="Js_label_ok radio"><input class="mr10" type="radio" name="radio2">不会成功</label>
            </span>
                                        </div>
                                </li>
                                <li class="last">
                                        <p class="question mb10"><b>03</b>最终获得投资的金额</p>

                                        <p class="height_5"></p>

                                        <div class="n-agree font14">
            <span class="mr25" data-name="radio3">
              <label class="Js_label_ok radio ok"><input class="mr10" type="radio" name="radio3" checked="checked"> 多于预期 </label>
            </span>
            <span class="Js_radio" data-name="radio3">
              <label  class="Js_label_ok radio"><input class="mr10" type="radio" name="radio3">小于预期</label>
            </span>
                                        </div>
                                </li>
                        </ul>
						<form name="guress_match">
                        <div class="rowblock clearfix mt30">
                                <div class="pt">投入竞猜的创创币数量</div>
                                <div class="tools text left mr16 Js_cityList">
                                        <s class="mon"></s>

                                        <div class="inputWarp loc">
                                                <input type="text" id="Js_guress_input" name="guress_num" class="Js_enterOnlyNum">
                                        </div>
                                </div>
								<div class="errorMsg left" style="display:none;">错误提示内容</div>
                        </div>
                        <div class="rowblock clearfix mt30">
                                <div class="pt">预计获得竞猜奖金</div>
                                <div class="tools text left mr16 getMoney">
                                        <s class="mon"></s>

                                        <div class="inputWarp loc">
                                                <input type="text" disabled="disabled" name="guress_money">
                                        </div>
                                </div>
                        </div>
						</form>
                        <p class="mt30"><a href="javascript:;" class="blueBtn Js_guress">我要竞猜</a></p>
                </div>
        </div>
</div>

