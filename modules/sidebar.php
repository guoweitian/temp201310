<div class="aside">
    <div class="l-location">
        <a class="link-home" href="#">创意世界大赛∙首页</a><i class="arr"></i>
        <a href="index.php" class="jeiss Js_ipad_click"></a>
    </div>
    <div class="line"></div>
    <div class="height_15"></div>
    <nav class="nav-area">
        <ul>
            <li><a class="ico-project" href="project-list.php">创意投资项目比赛</a></li>
            <li><a class="ico-task" href="task-list.php">创意任务人才比赛</a></li>
            <li><a class="ico-hiring" href="hire-list.php">创意公益明星比赛</a></li>
            <li><a class="ico-exchange" href="trade-list.php">创意世界交易所</a></li>
        </ul>
        <div class="height_15"></div>
    </nav>
    <div class="line"></div>
    <nav class="nav-area">
        <div class="height_15"></div>
        <ul>
            <li><a class="ico-area" href="area.php">创意世界地区</a></li>
            <li><a class="ico-thinktank" href="thinktank.php">创意世界智库</a></li>
        </ul>
        <div class="height_15"></div>
    </nav>
    <div class="line"></div>
<!--    <div class="nav-title">我的创意世界</div>-->
<!--    <nav class="nav-area">-->
<!--        <ul>-->
<!--            <li><a class="ico-circle" href="my-circle.php">朋友圈</a></li>-->
<!--            <li><a class="ico-info" href="messages.php">信息中心</a></li>-->
<!--            <li><a class="ico-homepage" href="profile-self.php">个人主页</a></li>-->
<!--            <li><a class="ico-account" href="creative-index.php">账户管理</a></li>-->
<!--        </ul>-->
<!--        <div class="height_15"></div>-->
<!--    </nav>-->
    <div class="line"></div>
    <div class="nav-title">我的应用</div>
    <div class="app-index-list">
        <div class="height_15"></div>
        <ul>
            <li><a href="#"><img src="assets/images/icon-app1.png" alt=""></a></li>
            <li><a href="#"><img src="assets/images/icon-app4.png" alt=""></a></li>
            <li><a href="#"><img src="assets/images/icon-app3.png" alt=""></a></li>
            <li><a href="#"><img src="assets/images/icon-app4.png" alt=""></a></li>
            <li><a href="#"><img src="assets/images/icon-app5.png" alt=""></a></li>
            <li><a href="#"><img src="assets/images/icon-app6.png" alt=""></a></li>
        </ul>
    </div>
    <div class="sidebar-arrow">
        <a id="Js_silder_bar" class="in"></a>
    </div>
</div>