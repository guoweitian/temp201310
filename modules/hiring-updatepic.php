<div class="detailPageMid">
  <div class="">
    <div class="titDiv clearfix">
      <span></span>

      <h2 class="left">照片展示</h2>

      <div class="s-btnarea right">
        <a class="sub-sbtn Js_tsubpost" href="javascript:;">上传照片</a>
      </div>
      <i class="arr photo"></i>
    </div>
    <div class="js_tsubpost_txt photo post-box">
      <div class="height_15" style="background:#EFEFEB"></div>
      <div class="proContextDiv clearfix" style="background:#FFF">
        <div class="height_20"></div>
        <div class="rowblock">
          <div class="uploadWrap-more dargArea left">
            <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
            <input class="file_uploadbox-more" type="file" ismul="true" multiple="multiple" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">
			<input type="hidden" name="img_way" value="" />
          </div>
          <div class="errorMsg left" style="display:none;">错误提示内容</div>
          <div class="height_10"></div>
          <div id="upload-more" class="clearfix Js_hiring_update_img upload-more"></div>

        </div>

        <div class="height_20"></div>
        <div class="post-subbtn">
          <a class="sub-sbtn gray left mr10 Js_cancle_tsubpost" href="javascript:;">取消上传</a>
          <a class="sub-sbtn left Js_phone_ok" href="javascript:;">确认上传</a>
        </div>
        <div class="height_40"></div>
      </div>
      <div style=" margin:0 auto; padding-left:90px; width:937px;">
        <div class="sp-line" style="top:0">
          <div class="line-left"></div>
          <div class="line-right"></div>
        </div>
      </div>
    </div>


    <div class="proContextDiv clearfix">
      <ul class="clearfix hirImgShow">
        <li>
          <a data-fancybox-group="gallery" class="Js_fancyBox"
             href="./assets/temp/bigpic1.jpg">
            <img src="./assets/temp/tp1.jpg" alt=""/>

            <div class="box Js_likeyou">
              <div class="c-box"></div>
              <div class="heartDiv">
                <s></s>
                <span>25</span>
              </div>
            </div>
          </a>
        </li>
        <li>
          <a data-fancybox-group="gallery" class="Js_fancyBox"
             href="./assets/temp/bigpic2.jpg">
            <img src="./assets/temp/tp2.jpg" alt=""/>

            <div class="box Js_likeyou">
              <div class="c-box"></div>
              <div class="heartDiv">
                <s></s>
                <span>25</span>
              </div>
            </div>
          </a>
        </li>
        <li>
          <a data-fancybox-group="gallery" class="Js_fancyBox"
             href="./assets/temp/bigpic3.jpg">
            <img src="./assets/temp/tp3.jpg" alt=""/>

            <div class="box Js_likeyou">
              <div class="c-box"></div>
              <div class="heartDiv">
                <s></s>
                <span>25</span>
              </div>
            </div>
          </a>
        </li>
        <li>
          <a data-fancybox-group="gallery" class="Js_fancyBox"
             href="./assets/temp/bigpic4.jpg">
            <img src="./assets/temp/tp4.jpg" alt=""/>

            <div class="box Js_likeyou">
              <div class="c-box"></div>
              <div class="heartDiv">
                <s></s>
                <span>25</span>
              </div>
            </div>
          </a>
        </li>
        <li>
          <a data-fancybox-group="gallery" class="Js_fancyBox"
             href="./assets/temp/bigpic1.jpg">
            <img src="./assets/temp/tp1.jpg" alt=""/>

            <div class="box Js_likeyou">
              <div class="c-box"></div>
              <div class="heartDiv">
                <s></s>
                <span>25</span>
              </div>
            </div>
          </a>
        </li>
        <li>
          <a data-fancybox-group="gallery" class="Js_fancyBox"
             href="./assets/temp/bigpic2.jpg">
            <img src="./assets/temp/tp1.jpg" alt=""/>

            <div class="box Js_likeyou">
              <div class="c-box"></div>
              <div class="heartDiv">
                <s></s>
                <span>25</span>
              </div>
            </div>
          </a>
        </li>
      </ul>

    </div>
  </div>
</div>