<div class="detailPageMid">
<div class="proContextDiv">
        <div class="titDiv">
                <span></span>

                <h2 class="Js_scroll">参赛明星排行榜</h2>
        </div>
</div>
<div class="Js_phbList_div">
<div class="phbList clearfix Js_phbList topThree">
        <div class="listNumTopThree"><span style="left:2px;">1</span></div>
        <div class="u_listItem left mr160">
                <a href="#" class="leftArea shadow pr">
                        <img src="./assets/temp/5.png" alt="">
                        <span class="ipad-pm first">1</span>
                </a>

                <div class="rightArea">
                        <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>

                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a
                                    href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                </div>
        </div>
        <div class="otherColumn one Js_otherColumn pr">
                <div class="left Js_user_like">
                        <span class="rColor"><em>4</em><small>位</small></span>
                        <p class="rColor">用户喜欢</p>
                </div>
                <s class="directBox one"></s>
        </div>
        <div class="otherColumn two Js_otherColumn pr">
                <div class="left Js_gift_money">
                        <span class="rColor"><em>5,000</em><small>元</small></span>
                        <p class="rColor">已筹善款金额</p>
                </div>
                <s class="directBox two"></s>
        </div>
        <div class="otherColumn three Js_otherColumn pr">
                <div class="left Js_trade_money">
                        <span class="rColor"><em>30,000</em><small>元</small></span>
                        <p class="rColor">交易成交金额</p>
                </div>
                <s class="directBox two"></s>
        </div>
        <div class="otherColumn last Js_otherColumn pr">
                <div class="left Js_expert_review">
                        <span class="rColor"><em>4</em><small>位</small></span>
                        <p class="rColor">专家点评</p>
                </div>
                <s class="directBox one"></s>
        </div>
        <div class="Js_otherColumn pr" style="float:right;">
            <a href="javascript:;" class="btn_remark Js_t_remarks">邀请专家点评</a>
            <s class="directBox" style="bottom:-55px; left:42px; background: #e6e6e6;"></s>
        </div>
</div>
<div class="" style="display:none;"></div>

<div class="phbList clearfix Js_phbList topThree">
        <div class="listNumTopThree"><span>2</span></div>
        <div class="u_listItem left mr160">
                <a href="#" class="leftArea shadow pr">
                        <img src="./assets/temp/5.png" alt="">
                        <span class="ipad-pm">2</span>
                </a>
                <div class="rightArea">
                        <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                </div>
        </div>
        <div class="otherColumn one Js_otherColumn pr">
            <div class="left Js_user_like">
                <span class="rColor"><em>4</em><small>位</small></span>
                <p class="rColor">用户喜欢</p>
            </div>
            <s class="directBox one"></s>
        </div>
        <div class="otherColumn two Js_otherColumn pr">
            <div class="left Js_gift_money">
                <span class="rColor"><em>5,000</em><small>元</small></span>
                <p class="rColor">已筹善款金额</p>
            </div>
            <s class="directBox two"></s>
        </div>
        <div class="otherColumn three Js_otherColumn pr">
            <div class="left Js_trade_money">
                <span class="rColor"><em>30,000</em><small>元</small></span>
                <p class="rColor">交易成交金额</p>
            </div>
            <s class="directBox two"></s>
        </div>
        <div class="otherColumn last Js_otherColumn pr">
            <div class="left Js_expert_review">
                <span class="rColor"><em>4</em><small>位</small></span>
                <p class="rColor">专家点评</p>
            </div>
            <s class="directBox one"></s>
        </div>
        <div class="Js_otherColumn pr" style="float:right;">
            <a href="javascript:;" class="btn_remark Js_t_remarks">邀请专家点评</a>
            <s class="directBox" style="bottom:-55px; left:42px; background: #e6e6e6;"></s>
        </div>
</div>
<div class="" style="display:none;"></div>

<div class="phbList clearfix Js_phbList topThree">
        <div class="listNumTopThree"><span>3</span></div>
        <div class="u_listItem left mr160">
                <a href="#" class="leftArea shadow pr">
                        <img src="./assets/temp/5.png" alt="">
                        <span class="ipad-pm">3</span>
                </a>
                <div class="rightArea">
                        <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                </div>
        </div>
        <div class="otherColumn one Js_otherColumn pr">
            <div class="left Js_user_like">
                <span class="rColor"><em>4</em><small>位</small></span>
                <p class="rColor">用户喜欢</p>
            </div>
            <s class="directBox one"></s>
        </div>
        <div class="otherColumn two Js_otherColumn pr">
            <div class="left Js_gift_money">
                <span class="rColor"><em>5,000</em><small>元</small></span>
                <p class="rColor">已筹善款金额</p>
            </div>
            <s class="directBox two"></s>
        </div>
        <div class="otherColumn three Js_otherColumn pr">
            <div class="left Js_trade_money">
                <span class="rColor"><em>30,000</em><small>元</small></span>
                <p class="rColor">交易成交金额</p>
            </div>
            <s class="directBox two"></s>
        </div>
        <div class="otherColumn last Js_otherColumn pr">
            <div class="left Js_expert_review">
                <span class="rColor"><em>4</em><small>位</small></span>
                <p class="rColor">专家点评</p>
            </div>
            <s class="directBox one"></s>
        </div>
        <div class="Js_otherColumn pr" style="float:right;">
            <a href="javascript:;" class="btn_remark Js_t_remarks">邀请专家点评</a>
            <s class="directBox" style="bottom:-55px; left:42px; background: #e6e6e6;"></s>
        </div>
</div>
<div class="" style="display:none;"></div>

<div class="phbList clearfix Js_phbList afterThree">
        <div class="listNumAfterThree"><span>4</span></div>
        <div class="u_listItem left mr160">
                <a href="#" class="leftArea shadow pr">
                        <img src="./assets/temp/5.png" alt="">
                        <span class="ipad-pm">4</span>
                </a>

                <div class="rightArea">
                        <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>

                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a
                                    href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                </div>
        </div>
        <div class="otherColumn one Js_otherColumn pr">
            <div class="left Js_user_like">
                <span class="rColor"><em>4</em><small>位</small></span>
                <p class="rColor">用户喜欢</p>
            </div>
            <s class="directBox one"></s>
        </div>
        <div class="otherColumn two Js_otherColumn pr">
            <div class="left Js_gift_money">
                <span class="rColor"><em>5,000</em><small>元</small></span>
                <p class="rColor">已筹善款金额</p>
            </div>
            <s class="directBox two"></s>
        </div>
        <div class="otherColumn three Js_otherColumn pr">
            <div class="left Js_trade_money">
                <span class="rColor"><em>30,000</em><small>元</small></span>
                <p class="rColor">交易成交金额</p>
            </div>
            <s class="directBox two"></s>
        </div>
        <div class="otherColumn last Js_otherColumn pr">
            <div class="left Js_expert_review">
                <span class="rColor"><em>4</em><small>位</small></span>
                <p class="rColor">专家点评</p>
            </div>
            <s class="directBox one"></s>
        </div>
        <div class="Js_otherColumn pr" style="float:right;">
            <a href="javascript:;" class="btn_remark Js_t_remarks">邀请专家点评</a>
            <s class="directBox" style="bottom:-55px; left:42px; background: #e6e6e6;"></s>
        </div>
</div>
<div class="" style="display:none;"></div>

<div class="phbList clearfix Js_phbList afterThree">
        <div class="listNumAfterThree"><span>5</span></div>
        <div class="u_listItem left mr160">
                <a href="#" class="leftArea shadow pr">
                        <img src="./assets/temp/5.png" alt="">
                        <span class="ipad-pm">5</span>
                </a>

                <div class="rightArea">
                        <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>

                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a
                                    href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                </div>
        </div>
        <div class="otherColumn one Js_otherColumn pr">
            <div class="left Js_user_like">
                <span class="rColor"><em>4</em><small>位</small></span>
                <p class="rColor">用户喜欢</p>
            </div>
            <s class="directBox one"></s>
        </div>
        <div class="otherColumn two Js_otherColumn pr">
            <div class="left Js_gift_money">
                <span class="rColor"><em>5,000</em><small>元</small></span>
                <p class="rColor">已筹善款金额</p>
            </div>
            <s class="directBox two"></s>
        </div>
        <div class="otherColumn three Js_otherColumn pr">
            <div class="left Js_trade_money">
                <span class="rColor"><em>30,000</em><small>元</small></span>
                <p class="rColor">交易成交金额</p>
            </div>
            <s class="directBox two"></s>
        </div>
        <div class="otherColumn last Js_otherColumn pr">
            <div class="left Js_expert_review">
                <span class="rColor"><em>4</em><small>位</small></span>
                <p class="rColor">专家点评</p>
            </div>
            <s class="directBox one"></s>
        </div>
        <div class="Js_otherColumn pr" style="float:right;">
            <a href="javascript:;" class="btn_remark Js_t_remarks">邀请专家点评</a>
            <s class="directBox" style="bottom:-55px; left:42px; background: #e6e6e6;"></s>
        </div>
</div>
<div class="" style="display:none;"></div>

<div class="phbList clearfix Js_phbList afterThree">
        <div class="listNumAfterThree"><span>6</span></div>
        <div class="u_listItem left mr160">
                <a href="#" class="leftArea shadow pr">
                        <img src="./assets/temp/5.png" alt="">
                        <span class="ipad-pm">6</span>
                </a>

                <div class="rightArea">
                        <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>

                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a
                                    href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                </div>
        </div>
        <div class="otherColumn one Js_otherColumn pr">
            <div class="left Js_user_like">
                <span class="rColor"><em>4</em><small>位</small></span>
                <p class="rColor">用户喜欢</p>
            </div>
            <s class="directBox one"></s>
        </div>
        <div class="otherColumn two Js_otherColumn pr">
            <div class="left Js_gift_money">
                <span class="rColor"><em>5,000</em><small>元</small></span>
                <p class="rColor">已筹善款金额</p>
            </div>
            <s class="directBox two"></s>
        </div>
        <div class="otherColumn three Js_otherColumn pr">
            <div class="left Js_trade_money">
                <span class="rColor"><em>30,000</em><small>元</small></span>
                <p class="rColor">交易成交金额</p>
            </div>
            <s class="directBox two"></s>
        </div>
        <div class="otherColumn last Js_otherColumn pr">
            <div class="left Js_expert_review">
                <span class="rColor"><em>4</em><small>位</small></span>
                <p class="rColor">专家点评</p>
            </div>
            <s class="directBox one"></s>
        </div>
        <div class="Js_otherColumn pr" style="float:right;">
            <a href="javascript:;" class="btn_remark Js_t_remarks">邀请专家点评</a>
            <s class="directBox" style="bottom:-55px; left:42px; background: #e6e6e6;"></s>
        </div>
</div>
<div class="" style="display:none;"></div>

<div class="phbList clearfix Js_phbList afterThree">
        <div class="listNumAfterThree"><span style="">7</span></div>
        <div class="u_listItem left mr160">
                <a href="#" class="leftArea shadow pr">
                        <img src="./assets/temp/5.png" alt="">
                        <span class="ipad-pm">10</span>
                </a>

                <div class="rightArea">
                        <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>

                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a
                                    href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                </div>
        </div>
        <div class="otherColumn one Js_otherColumn pr">
            <div class="left Js_user_like">
                <span class="rColor"><em>4</em><small>位</small></span>
                <p class="rColor">用户喜欢</p>
            </div>
            <s class="directBox one"></s>
        </div>
        <div class="otherColumn two Js_otherColumn pr">
            <div class="left Js_gift_money">
                <span class="rColor"><em>5,000</em><small>元</small></span>
                <p class="rColor">已筹善款金额</p>
            </div>
            <s class="directBox two"></s>
        </div>
        <div class="otherColumn three Js_otherColumn pr">
            <div class="left Js_trade_money">
                <span class="rColor"><em>30,000</em><small>元</small></span>
                <p class="rColor">交易成交金额</p>
            </div>
            <s class="directBox two"></s>
        </div>
        <div class="otherColumn last Js_otherColumn pr">
            <div class="left Js_expert_review">
                <span class="rColor"><em>4</em><small>位</small></span>
                <p class="rColor">专家点评</p>
            </div>
            <s class="directBox one"></s>
        </div>
        <div class="Js_otherColumn pr" style="float:right;">
            <a href="javascript:;" class="btn_remark Js_t_remarks">邀请专家点评</a>
            <s class="directBox" style="bottom:-55px; left:42px; background: #e6e6e6;"></s>
        </div>
</div>
<div class="" style="display:none;"></div>

</div>
</div>