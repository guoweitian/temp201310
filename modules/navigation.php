<div class="match-tab publish">
    <div class="row" style="max-width:100%">
        <div class="large-3 columns local-tab">
            <div class="location left Js_location">
                <a class="link-home" href="index.php">创意世界大赛∙首页</a><i class="arr"></i>
            </div>
        </div>
        <div class="large-6 columns">
            <div class="tab-list-nav">
                <ul class="match-tab-list clearfix Js_tList">
                    <?php
                    if(isset($special_text))
                    {
                        echo $special_text;
                    }
                    foreach($navigation_text as $text)
                    {
                    ?>
                        <li class="listItem <?php if(isset($scroll)){?>Js_nav_scroll" <?php }else{?>"<?php }?>><?php echo $text; ?></li>
                    <?php
                    }
                    ?>
                    <li class="listItem last">
                        <a class="more-nav Js_navMore" href="#">更多</a>
                        <div class="nav-more Js_navMoreItem">
                            <ul></ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
            <!--地图引用文件-->
            <?php include("./modules/addr.php"); ?>
    </div>
</div>