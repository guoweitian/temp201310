<div class="detailPageMid">
        <div class="titDiv clearfix" style="margin-bottom:8px;">
                <span></span>
                <h2 class="left">
                <?php if(isset($expertTT_NOJS)) {
        echo $expertTT_NOJS;
                }else {  ?>
                        专家点评
                       <?php } ?>
                </h2>
                <div class="right mt20">
                        <a class="sub-sbtn Js_remark">邀请专家点评</a>
                </div>
                <div class="right mt20 mr10">
                        <a class="sub-sbtn Js_invistd">发布专家点评</a>
                </div>
                <s class="p-box b1 Js_invistd_b"></s>
                <s class="p-box b2 Js_remark_b"></s>
        </div>

        <div style="background:#e6e6e6; display:none;" class="Js_dp">
                <!--专家点评弹出层开始-->
                <div class="expertDpDiv Js_invistd_c" style="display:none;">
                        <div class="height_60"></div>
                        <div class="p-warp">
                                <p>专家评价</p>
                                <div class="height_25"></div>
                                <div class="l-edit">
                                        <div class="n-agree">
          <span class="Js_radio mr30" data-name="rad">
            <label>
                    <input type="radio" name="rad">
                    <span class="p-level b73">优秀</span>
            </label>
          </span>
          <span class="Js_radio mr30" data-name="rad">
            <label>
                    <input type="radio" name="rad">
                    <span class="p-level c7ef">良好</span>
            </label>
          </span>
          <span class="Js_radio" data-name="rad">
            <label>
                    <input type="radio" name="rad">
                    <span class="p-level c0">一般</span>
            </label>
          </span>
                                        </div>
                                </div>
                        </div>
                        <div class="height_40"></div>
                        <div class="p-warp">
                                <p>专家评语</p>
                                <div class="height_15"></div>
                                <div class="rowblock l-edit">
                                        <div id="dtb_toolbar" class="toolbar-wrap"></div>
                                        <div class="toolbar-content left">
                                                <textarea id="dtb_content" name="content" placeholder="请输入你要点评的内容"></textarea>
                                        </div>
                                        <div class="errorMsg left" style="display: none;">错误提示</div>
                                </div>
                        </div>
                        <div class="height_50"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="btn h8 mr5 Js_invistd">取消点评</a>
                                <a href="javascript:;" class="btn b73 Js_zj_fbdp">发表专家点评</a>
                        </div>
                        <div class="height_50"></div>
                        <div class="sp-line" style="top:0; left:52px;">
                                <div class="line-left"></div>
                                <div class="line-right"></div>
                        </div>
                </div>
                <!--专家点评弹出层结束-->
                <div style="width:940px; margin:0 auto; display:none;" class="Js_remark_c">
                        <div class="height_40"></div>
                        <div class="p-warp">
                                <p>邀请创意世界专家</p>
                        </div>
                        <div class="height_15"></div>
                        <div class="rowblock">
                                <div class="tools sel left Js_expertList">
                                        <div class="inputWarp downMenuExperts">
                                                <input type="text">
                                                <div class="selDiv expertList">
                                                        <div class="describe">让创创召帮您查找关键词为"<span class="Js_search_txt"></span>"的内容</div>
                                                        <ul class="mt-2">
                                                        </ul>
                                                </div>
                                        </div>
                                </div>
                                <a href="javascript:void(0)" class="blueBtn fl ml10 Js_recommend_expert">推荐站外专家</a>
                        </div>
                        <div class="height_15"></div>
                        <div class="inviteExpert">
                                <h3>已邀请专家</h3>
                                <ul class="Js_invitedexplist">
                                        <li class="clearfix">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">
                                                                <a href="#">刘兆宇</a>
                                                                <i class="bspw">比赛评委</i>
                                                        </p>
                                                        <p class="rightDis"><a href="#">成都电子科技大学</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">
                                                                <a href="#">如果我不是郦道元怎么办</a>
                                                                <i class="bspw">比赛评委</i>
                                                        </p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">创意世界</a><i>，</i><a href="#">创意世界</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix" style="margin-top:31px;">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">
                                                                <a href="#">如果我不是郦道元怎么办</a>

                                                        </p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">创意世界</a><i>，</i><a href="#">创意世界</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix" style="margin-top:31px;">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">站外专家姓名</p>
                                                        <p class="rightDis"><a href="#">站外专家的简介内容</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                </ul>

                        </div>
                        <div class="height_50"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="btn h8 mr5 Js_remark">取消邀请</a>
                                <a href="javascript:;" class="btn b73 Js_yq_ok">确认邀请</a>
                        </div>
                        <div class="height_50"></div>



                        <div class="sp-line" style="top:0; left:52px;">
                                <div class="line-left"></div>
                                <div class="line-right"></div>
                        </div>
                </div>
        </div>
    <div class="proContextDiv">
        <ul class="clearfix u_list h250 vh Js_zjdiddiv">
            <li class="left h224">
                <div class="u_listItem">
                    <a href="#" class="leftArea shadow">
                        <img src="./assets/temp/5.png" alt="">
                    </a>
                    <div class="rightArea mb7">
                        <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                    </div>
                    <span class="reviewLevel">优秀</span>
                    <p class="reviewTxt"><b class="zyh">“</b>它对于数码产品消费者而言，最大的好处就是可以极大减少卡片机的厚度...<b class="yyh">”</b></p>
                </div>
            </li>
            <li class="left h224">
                <div class="u_listItem">
                    <a href="#" class="leftArea shadow">
                        <img src="./assets/temp/5.png" alt="">
                    </a>
                    <div class="rightArea mb7">
                        <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                    </div>
                    <span class="reviewLevel two">良好</span>
                    <p class="reviewTxt"><b class="zyh">“</b>它对于数码产品消费者而言，最大的好处就是可以极大减少卡片机的厚度然后就木有然后...<b class="yyh">”</b></p>
                </div>
            </li>
            <li class="left h224">
                <div class="u_listItem">
                    <a href="#" class="leftArea shadow">
                        <img src="./assets/temp/5.png" alt="">
                    </a>
                    <div class="rightArea mb7">
                        <p class="rightName"><a href="#" class="Js_visitCard">郦道元</a></p>
                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">经过一段时间的观察</a></p>
                    </div>
                    <span class="reviewLevel three">一般</span>
                    <p class="reviewTxt"><b class="zyh">“</b>唉！Fisher打网球只打得过老头儿...<b class="yyh">”</b></p>
                </div>
            </li>
        </ul>
    </div>
</div>