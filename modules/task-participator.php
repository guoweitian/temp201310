<div class="detailPageMid">
<div class="proContextDiv pw100 Js_u_list_item">
<div class="titDiv w937">
        <span style=""></span>
        <h2 class="Js_scroll">任务人才和作品</h2>
</div>
<div class="u_listItem w937 ph100">
        <div class="height_30"></div>
        <a href="#" class="leftArea shadow">
                <img src="./assets/temp/5.png" alt="">
        </a>
        <div class="rightArea pw100 ph100">
                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办（发布者页面）</a></p>
                <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                <div><p class="des">用户还可以在App Store的“更新”栏看到最近刚刚升级过的应用列表。用户可以在这里看到安装升级程序的时间以及升级的内容。 如果App Store还没有进行自动更新，用户也可以选择手动安装最新的更新文件。</p></div>
                <ul class="worklists clearfix">
                        <li>
                                <a data-fancybox-group="gallery" class="Js_fancyBox" href="./assets/temp/bigpic1.jpg">
                                        <img src="./assets/temp/tp1.jpg" alt="">
                                </a>
                        </li>
                        <li>
                                <a data-fancybox-group="gallery" class="Js_fancyBox" href="./assets/temp/bigpic2.jpg">
                                        <img src="./assets/temp/tp2.jpg" alt="">
                                </a>
                        </li>
                        <li>
                                <a data-fancybox-group="gallery" class="Js_fancyBox" href="./assets/temp/bigpic3.jpg">
                                        <img src="./assets/temp/tp3.jpg" alt="">
                                </a>
                        </li>
                        <li>
                                <a data-fancybox-group="gallery" class="Js_fancyBox" href="./assets/temp/bigpic4.jpg">
                                        <img src="./assets/temp/tp4.jpg" alt="">
                                </a>
                        </li>
                </ul>
                <div class="workbottom clearfix">
                        <div class="pub-time left">2013年6月15日 10:03发布</div>
                        <div class="pub-operate right">
                                <div class="pub-score left" style=" margin:6px 20px 0 0">
                                        <div class="rateit"></div>
                                </div>
                                <div class="exp-comments Js_exp_comment left">3 位专家点评</div>
                                <div class="set-winner left Js_t_remark">邀请专家点评</div>
                                <div class="set-winner left Js_winner">选为获胜作品</div>
                                <div class="set-out left Js_eliminate">淘汰该作品</div>
                                <div class="set-eliminate left" style="display: none;">已淘汰</div>
<!--                                <div class="set-cancle left Js_zp_cancel">撤回作品</div>-->

                        </div>
<!--                        <i class="arr Js_arr"></i>-->
<!--                        <i class="arr Js_arr1" style=" border-color: transparent transparent #fff; right:320px;"></i>-->
                </div>
        </div>
</div>
<div class="task-expcomment Js_parents" style="display:none">
        <div class="proContextDiv Js_exp_c" style="display:none;">
                <div class="titDiv clearfix">
                        <span></span>
                        <h2 class="left">专家点评</h2>
<!--                        <div class="right mt20">-->
<!--                            <a class="sub-sbtn Js_t_remark">邀请专家点评</a>-->
<!--                        </div>-->
                        <div class="right mt20 mr10">
<!--                                <a class="sub-sbtn Js_t_invistd">发布专家点评</a>-->
                        </div>
                </div>

                <!--专家点评弹出层开始-->
                <div class="expertDpDiv Js_tInvistd_c" style="display:none;">
                        <div class="height_60"></div>
                        <div class="p-warp">
                                <p>专家评价</p>
                                <div class="height_25"></div>
                                <div class="l-edit">
                                        <div class="n-agree">
                  <span class="Js_radio mr30" data-name="rad">
                    <label>
                            <input type="radio" name="rad">
                            <span class="p-level b73">优秀</span>
                    </label>
                  </span>
                  <span class="Js_radio mr30" data-name="rad">
                    <label>
                            <input type="radio" name="rad">
                            <span class="p-level c7ef">良好</span>
                    </label>
                  </span>
                  <span class="Js_radio" data-name="rad">
                    <label>
                            <input type="radio" name="rad">
                            <span class="p-level c0">一般</span>
                    </label>
                  </span>
                                        </div>
                                </div>
                        </div>
                        <div class="height_40"></div>
                        <div class="p-warp">
                                <p>专家评语</p>
                                <div class="height_15"></div>
                                <div class="rowblock l-edit">
                                        <div id="dtb_toolbar" class="toolbar-wrap"></div>
                                        <div class="toolbar-content left">
                                                <textarea id="dtb_content" name="content" placeholder="请输入你要点评的内容"></textarea>
                                        </div>
                                </div>
                        </div>
                        <div class="height_50"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="btn h8 mr5 Js_tcan_invistd">取消点评</a>
                                <a href="javascript:;" class="btn b73">发表专家点评</a>
                        </div>
                        <div class="height_50"></div>
                        <div class="sp-line" style="top:0; left:52px;">
                                <div class="line-left"></div>
                                <div class="line-right"></div>
                        </div>
                </div>
                <!--专家点评弹出层结束-->

                <div style="width:940px; margin:0 auto; display:none;" class="Js_tRemark_c">
                        <div class="height_10"></div>
                        <div class="p-warp">
                                <p>邀请创意世界专家</p>
                        </div>
                        <div class="height_15"></div>
                        <div class="rowblock">
                                <div class="tools sel left Js_expertList">
                                        <div class="inputWarp downMenuExperts">
                                                <input type="text">
                                                <div class="selDiv expertList">
                                                        <div class="describe">让创创召帮您查找关键词为"<span class="Js_search_txt"></span>"的内容</div>
                                                        <ul class="mt-2">
                                                        </ul>
                                                </div>
                                        </div>
                                </div>
                                <a href="javascript:void(0)" class="blueBtn fl ml10 Js_recommend_expert">推荐站外专家</a>
                        </div>
                        <div class="height_15"></div>
                        <div class="inviteExpert">
                                <h3>已邀请专家</h3>
                                <ul class="Js_invitedexplist">
                                        <li class="clearfix">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">
                                                                <a href="#">刘兆宇</a>
                                                                <i class="bspw">比赛评委</i>
                                                        </p>
                                                        <p class="rightDis"><a href="#">成都电子科技大学</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">
                                                                <a href="#">如果我不是郦道元怎么办</a>
                                                                <i class="bspw">比赛评委</i>
                                                        </p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">创意世界</a><i>，</i><a href="#">创意世界</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix" style="margin-top:31px;">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">
                                                                <a href="#">刘兆宇</a>
                                                                <i class="bspw">比赛评委</i>
                                                        </p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">创意世界</a><i>，</i><a href="#">创意世界</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix" style="margin-top:31px;">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">站外专家姓名123</p>
                                                        <p class="rightDis"><a href="#">站外专家的简介内容</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                </ul>

                        </div>
                        <div class="height_50"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="grayBtn small-grayBtn h8 mr5 Js_tcan_remark">取消邀请</a>
                                <a href="javascript:;" class="blueBtn small-blueBtn b73 Js_task_yqok">确认邀请</a>
                        </div>
                </div>
                <div class="height_54"></div>
                <ul class="clearfix u_list h250 Js_explist vh">
                </ul>
                <div class="height_20"></div>
        </div>

        <div style="" class="winner-works Js_winner_c">
                <div class="proContextDiv workDiv">
                        <div class="titDiv">
                                <span></span>
                                <h2>请确认是否选择当前作品为获胜作品</h2>
                        </div>
                        <div class="height_31"></div>
                        <div class="txt">
                                <p class="tit">当您一旦选择了获胜任务作品，本次比赛将进入任务作品确权交割流程，在该流程中您可以：</p>
                                <p class="nc">1.要求获胜任务作品的发布者对当前作品进行微小的调整；</p>
                                <p class="nc">2.确认当前作品符合您的要求；</p>
                                <p class="nc">3.签订创意任务人才比赛的获胜作品成果转让协议；</p>
                                <p class="bt">只有当双方均确认了成果转让协议之后，创意任务人才比赛的奖金才会发放给比赛获胜人才。</p>
                        </div>
                        <div class="height_40"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="btn h8 mr5 Js_cancle_winner">取消选择</a>
                                <a href="javascript:;" class="btn b73 Js_affirm_win_works">确认选为获胜作品</a>
                        </div>
                        <div class="height_20"></div>
                </div>
        </div>
</div>
<div style=" margin:0 auto; padding-left:90px; width:937px;">
        <div class="sp-line">
                <div class="line-left"></div>
                <div class="line-right"></div>
        </div>
</div>

<div class="u_listItem w937 ph100">
        <div class="height_30"></div>
        <a href="#" class="leftArea shadow">
                <img src="./assets/temp/5.png" alt="">
        </a>
        <div class="rightArea pw100 ph100">
                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办（参赛者页面）</a></p>
                <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                <div><p class="des">用户还可以在App Store的“更新”栏看到最近刚刚升级过的应用列表。用户可以在这里看到安装升级程序的时间以及升级的内容。 如果App Store还没有进行自动更新，用户也可以选择手动安装最新的更新文件。</p></div>
                <ul class="worklists clearfix">
                        <li>
                                <a data-fancybox-group="gallery1" class="Js_fancyBox1" href="./assets/temp/bigpic1.jpg">
                                        <img src="./assets/temp/tp1.jpg" alt="">
                                </a>
                        </li>
                        <li>
                                <a data-fancybox-group="gallery1" class="Js_fancyBox1" href="./assets/temp/bigpic2.jpg">
                                        <img src="./assets/temp/tp2.jpg" alt="">
                                </a>
                        </li>
                        <li>
                                <a data-fancybox-group="gallery1" class="Js_fancyBox1" href="./assets/temp/bigpic3.jpg">
                                        <img src="./assets/temp/tp3.jpg" alt="">
                                </a>
                        </li>
                        <li>
                                <a data-fancybox-group="gallery1" class="Js_fancyBox1" href="./assets/temp/bigpic4.jpg">
                                        <img src="./assets/temp/tp4.jpg" alt="">
                                </a>
                        </li>
                </ul>
                <div class="workbottom clearfix">
                        <div class="pub-time left">2013年6月15日 10:03发布</div>
                        <div class="pub-operate right">
                                <div class="pub-score left" style=" margin:6px 20px 0 0">
                                    <div class="pub-score left">
                                        <div class="rateit"></div>
                                    </div>
                                </div>
                                <div class="exp-comments Js_exp_comment left">3 位专家点评</div>
                                <div class="set-winner left Js_t_remark">邀请专家点评</div>
<!--                                <div class="set-winner left Js_winner">选为获胜作品</div>-->
<!--                                <div class="set-out left Js_eliminate">淘汰该作品</div>-->
<!--                                <div class="set-eliminate left" style="display: none;">已淘汰</div>-->
                                <div class="set-cancle left Js_zp_cancel">撤回该作品</div>
                                <div class="set-eliminate left" style="display: none;">已撤回</div>
                        </div>
<!--                        <i class="arr Js_arr"></i>-->
<!--                        <i class="arr Js_arr1" style=" border-color: transparent transparent #FFF; right:320px;"></i>-->
                </div>
        </div>
</div>
<div class="task-expcomment Js_parents" style="display:none">
        <div class="proContextDiv Js_exp_c" style="display:none;">
                <div class="titDiv clearfix">
                        <span></span>
                        <h2 class="left">专家点评</h2>
                        <div class="right mt20">
<!--                                <a class="sub-sbtn Js_t_remark">邀请专家点评</a>-->
                        </div>
                        <div class="right mt20 mr10">
<!--                                <a class="sub-sbtn Js_t_invistd">发布专家点评</a>-->
                        </div>
                </div>

                <!--专家点评弹出层开始-->
                <div class="expertDpDiv Js_tInvistd_c" style="display:none;">
                        <div class="height_60"></div>
                        <div class="p-warp">
                                <p>专家评价</p>
                                <div class="height_25"></div>
                                <div class="l-edit">
                                        <div class="n-agree">
                  <span class="Js_radio mr30" data-name="rad">
                    <label>
                            <input type="radio" name="rad">
                            <span class="p-level b73">优秀</span>
                    </label>
                  </span>
                  <span class="Js_radio mr30" data-name="rad">
                    <label>
                            <input type="radio" name="rad">
                            <span class="p-level c7ef">良好</span>
                    </label>
                  </span>
                  <span class="Js_radio" data-name="rad">
                    <label>
                            <input type="radio" name="rad">
                            <span class="p-level c0">一般</span>
                    </label>
                  </span>
                                        </div>
                                </div>
                        </div>
                        <div class="height_40"></div>
                        <div class="p-warp">
                                <p>专家评语</p>
                                <div class="height_15"></div>
                                <div class="rowblock l-edit">
                                        <div id="dtb_toolbar1" class="toolbar-wrap"></div>
                                        <div class="toolbar-content left">
                                                <textarea id="dtb_content1" name="content" placeholder="请输入你要点评的内容"></textarea>
                                        </div>
                                </div>
                        </div>
                        <div class="height_50"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="btn h8 mr5 Js_tcan_invistd">取消点评</a>
                                <a href="javascript:;" class="btn b73">发表专家点评</a>
                        </div>
                        <div class="height_50"></div>
                        <div class="sp-line" style="top:0; left:52px;">
                                <div class="line-left"></div>
                                <div class="line-right"></div>
                        </div>
                </div>
                <!--专家点评弹出层结束-->

                <div style="width:940px; margin:0 auto; display:none;" class="Js_tRemark_c">
                        <div class="height_10"></div>
                        <div class="p-warp">
                                <p>邀请创意世界专家</p>
                        </div>
                        <div class="height_15"></div>
                        <div class="rowblock">
                                <div class="tools sel left Js_expertList">
                                        <div class="inputWarp downMenuExperts">
                                                <input type="text">
                                                <div class="selDiv expertList">
                                                        <div class="describe">让创创召帮您查找关键词为"<span class="Js_search_txt"></span>"的内容</div>
                                                        <ul class="mt-2">
                                                        </ul>
                                                </div>
                                        </div>
                                </div>
                                <a href="javascript:void(0)" class="blueBtn fl ml10 Js_recommend_expert">推荐站外专家</a>
                        </div>
                        <div class="height_15"></div>
                        <div class="inviteExpert">
                                <h3>已邀请专家</h3>
                                <ul class="Js_invitedexplist">
                                        <li class="clearfix">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">刘兆宇</p>
                                                        <p class="rightDis"><a href="#">成都电子科技大学</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">如果我不是郦道元怎么办</p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">创意世界</a><i>，</i><a href="#">创意世界</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix" style="margin-top:31px;">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">刘兆宇</p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">创意世界</a><i>，</i><a href="#">创意世界</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix" style="margin-top:31px;">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">站外专家姓名</p>
                                                        <p class="rightDis"><a href="#">站外专家的简介内容</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                </ul>

                        </div>
                        <div class="height_50"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="grayBtn small-grayBtn h8 mr5 Js_tcan_remark">取消邀请</a>
                                <a href="javascript:;" class="blueBtn small-blueBtn b73 Js_task_yqok">确认邀请</a>
                        </div>
                </div>
                <div class="height_54"></div>
                <ul class="clearfix u_list h250 Js_explist vh">
                </ul>
                <div class="height_20"></div>
        </div>

        <div style="" class="winner-works Js_winner_c">
                <div class="proContextDiv workDiv">
                        <div class="titDiv">
                                <span></span>
                                <h2>请确认是否选择当前作品为获胜作品</h2>
                        </div>
                        <div class="height_31"></div>
                        <div class="txt">
                                <p class="tit">当您一旦选择了获胜任务作品，本次比赛将进入任务作品确权交割流程，在该流程中您可以：</p>
                                <p class="nc">1.要求获胜任务作品的发布者对当前作品进行微小的调整；</p>
                                <p class="nc">2.确认当前作品符合您的要求；</p>
                                <p class="nc">3.签订创意任务人才比赛的获胜作品成果转让协议；</p>
                                <p class="bt">只有当双方均确认了成果转让协议之后，创意任务人才比赛的奖金才会发放给比赛获胜人才。</p>
                        </div>
                        <div class="height_40"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="btn h8 mr5 Js_cancle_winner">取消选择</a>
                                <a href="javascript:;" class="btn b73 Js_affirm_win_works">确认选为获胜作品</a>
                        </div>
                        <div class="height_20"></div>
                </div>
        </div>
</div>
<div style=" margin:0 auto; padding-left:90px; width:937px;">
        <div class="sp-line">
                <div class="line-left"></div>
                <div class="line-right"></div>
        </div>
</div>

<div class="u_listItem w937 ph100">
        <div class="height_30"></div>
        <a href="#" class="leftArea shadow">
                <img src="./assets/temp/5.png" alt="">
        </a>
        <div class="rightArea pw100 ph100">
                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办（专家页面）</a></p>
                <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                <div><p class="des">用户还可以在App Store的“更新”栏看到最近刚刚升级过的应用列表。用户可以在这里看到安装升级程序的时间以及升级的内容。 如果App Store还没有进行自动更新，用户也可以选择手动安装最新的更新文件。</p></div>
                <ul class="worklists clearfix">
                        <li>
                                <a data-fancybox-group="gallery2" class="Js_fancyBox2" href="./assets/temp/bigpic1.jpg">
                                        <img src="./assets/temp/tp1.jpg" alt="">
                                </a>
                        </li>
                        <li>
                                <a data-fancybox-group="gallery2" class="Js_fancyBox2" href="./assets/temp/bigpic2.jpg">
                                        <img src="./assets/temp/tp2.jpg" alt="">
                                </a>
                        </li>
                        <li>
                                <a data-fancybox-group="gallery2" class="Js_fancyBox2" href="./assets/temp/bigpic3.jpg">
                                        <img src="./assets/temp/tp3.jpg" alt="">
                                </a>
                        </li>
                        <li>
                                <a data-fancybox-group="gallery2" class="Js_fancyBox2" href="./assets/temp/bigpic4.jpg">
                                        <img src="./assets/temp/tp4.jpg" alt="">
                                </a>
                        </li>
                </ul>
                <div class="workbottom clearfix">
                        <div class="pub-time left">2013年6月15日 10:03发布</div>
                        <div class="pub-operate right">
                                <div class="pub-score left" style=" margin:6px 20px 0 0">
                                    <div class="rateitltd left" style=" margin:6px 20px 0 0">
                                        <div class="rateitltd-range" style="width: 120px; height: 24px;">
                                            <div class="rateitltd-selected" style="height: 24px; width: 96px; display: block;"></div>
                                        </div>
                                </div>
                                <div class="exp-comments Js_exp_comment left">3 位专家点评</div>
                                <div class="set-winner left Js_t_remark">邀请专家点评</div>
<!--                                <div class="set-winner left Js_winner">选为获胜作品</div>-->
<!--                                <div class="set-out left Js_eliminate">淘汰该作品</div>-->
<!--                                <div class="set-eliminate left" style="display: none;">已淘汰</div>-->
<!--                                <div class="set-cancle left Js_zp_cancel">撤回作品</div>-->
                        </div>
<!--                        <i class="arr Js_arr"></i>-->
<!--                        <i class="arr Js_arr1" style=" border-color: transparent transparent #FFF; right:320px;"></i>-->
                </div>
        </div>
</div>
<div class="task-expcomment Js_parents" style="display:none;">
        <div class="proContextDiv Js_exp_c" style="display:none;">
                <div class="titDiv clearfix">
                        <span></span>
                        <h2 class="left">专家点评</h2>
                        <div class="right mt20">
<!--                                <a class="sub-sbtn Js_t_remark">邀请专家点评</a>-->
                        </div>
                        <div class="right mt20 mr10">
                                <a class="sub-sbtn Js_t_invistd">发布专家点评</a>
                        </div>
                </div>

                <!--专家点评弹出层开始-->
                <div class="expertDpDiv Js_tInvistd_c" style="display:none;">
                        <div class="height_60"></div>
                        <div class="p-warp">
                                <p>专家评价</p>
                                <div class="height_25"></div>
                                <div class="l-edit">
                                        <div class="n-agree">
                  <span class="Js_radio mr30" data-name="rad">
                    <label>
                            <input type="radio" name="rad">
                            <span class="p-level b73">优秀</span>
                    </label>
                  </span>
                  <span class="Js_radio mr30" data-name="rad">
                    <label>
                            <input type="radio" name="rad">
                            <span class="p-level c7ef">良好</span>
                    </label>
                  </span>
                  <span class="Js_radio" data-name="rad">
                    <label>
                            <input type="radio" name="rad">
                            <span class="p-level c0">一般</span>
                    </label>
                  </span>
                                        </div>
                                </div>
                        </div>
                        <div class="height_40"></div>
                        <div class="p-warp">
                                <p>专家评语</p>
                                <div class="height_15"></div>
                                <div class="rowblock l-edit">
                                        <div id="dtb_toolbar2" class="toolbar-wrap"></div>
                                        <div class="toolbar-content left">
                                                <textarea id="dtb_content2" name="content" placeholder="请输入你要点评的内容"></textarea>
                                        </div>
                                </div>
                        </div>
                        <div class="height_50"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="btn h8 mr5 Js_tcan_invistd">取消点评</a>
                                <a href="javascript:;" class="btn b73">发表专家点评</a>
                        </div>
                        <div class="height_50"></div>

                </div>
                <!--专家点评弹出层结束-->

                <div style="width:940px; margin:0 auto; display:none;" class="Js_tRemark_c">
                        <div class="height_10"></div>
                        <div class="p-warp">
                                <p>邀请创意世界专家</p>
                        </div>
                        <div class="height_15"></div>
                        <div class="rowblock">
                                <div class="tools sel left Js_expertList">
                                        <div class="inputWarp downMenuExperts">
                                                <input type="text">
                                                <div class="selDiv expertList">
                                                        <div class="describe">让创创召帮您查找关键词为"<span class="Js_search_txt"></span>"的内容</div>
                                                        <ul class="mt-2">
                                                        </ul>
                                                </div>
                                        </div>
                                </div>
                                <a href="javascript:void(0)" class="blueBtn fl ml10 Js_recommend_expert">推荐站外专家</a>
                        </div>
                        <div class="height_15"></div>
                        <div class="inviteExpert">
                                <h3>已邀请专家</h3>
                                <ul class="Js_invitedexplist">
                                        <li class="clearfix">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">刘兆宇</p>
                                                        <p class="rightDis"><a href="#">成都电子科技大学</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">如果我不是郦道元怎么办</p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">创意世界</a><i>，</i><a href="#">创意世界</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix" style="margin-top:31px;">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">刘兆宇</p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">创意世界</a><i>，</i><a href="#">创意世界</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix" style="margin-top:31px;">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">站外专家姓名</p>
                                                        <p class="rightDis"><a href="#">站外专家的简介内容</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                </ul>

                        </div>
                        <div class="height_50"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="grayBtn small-grayBtn h8 mr5 Js_tcan_remark">取消邀请</a>
                                <a href="javascript:;" class="blueBtn small-blueBtn b73 Js_task_yqok">确认邀请</a>
                        </div>
                </div>
                <div class="height_54"></div>
                <ul class="clearfix u_list h250 Js_explist vh">
                </ul>
                <div class="height_20"></div>
        </div>

        <div style="" class="winner-works Js_winner_c">
                <div class="proContextDiv workDiv">
                        <div class="titDiv">
                                <span></span>
                                <h2>请确认是否选择当前作品为获胜作品</h2>
                        </div>
                        <div class="height_31"></div>
                        <div class="txt">
                                <p class="tit">当您一旦选择了获胜任务作品，本次比赛将进入任务作品确权交割流程，在该流程中您可以：</p>
                                <p class="nc">1.要求获胜任务作品的发布者对当前作品进行微小的调整；</p>
                                <p class="nc">2.确认当前作品符合您的要求；</p>
                                <p class="nc">3.签订创意任务人才比赛的获胜作品成果转让协议；</p>
                                <p class="bt">只有当双方均确认了成果转让协议之后，创意任务人才比赛的奖金才会发放给比赛获胜人才。</p>
                        </div>
                        <div class="height_40"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="btn h8 mr5 Js_cancle_winner">取消选择</a>
                                <a href="javascript:;" class="btn b73 Js_affirm_win_works">确认选为获胜作品</a>
                        </div>
                        <div class="height_20"></div>
                </div>
        </div>
</div>
<div class="sp-line" style="top:0; left:52px;">
    <div class="line-left"></div>
    <div class="line-right"></div>
</div>
</div>

<div class="u_listItem w937 ph100">
    <div class="height_30"></div>
    <a href="#" class="leftArea shadow">
        <img src="./assets/temp/5.png" alt="">
    </a>
    <div class="rightArea pw100 ph100">
        <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办（其他用户页面）</a></p>
        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
        <div><p class="des">用户还可以在App Store的“更新”栏看到最近刚刚升级过的应用列表。用户可以在这里看到安装升级程序的时间以及升级的内容。 如果App Store还没有进行自动更新，用户也可以选择手动安装最新的更新文件。</p></div>
        <ul class="worklists clearfix">
            <li>
                <a data-fancybox-group="gallery2" class="Js_fancyBox2" href="./assets/temp/bigpic1.jpg">
                    <img src="./assets/temp/tp1.jpg" alt="">
                </a>
            </li>
            <li>
                <a data-fancybox-group="gallery2" class="Js_fancyBox2" href="./assets/temp/bigpic2.jpg">
                    <img src="./assets/temp/tp2.jpg" alt="">
                </a>
            </li>
            <li>
                <a data-fancybox-group="gallery2" class="Js_fancyBox2" href="./assets/temp/bigpic3.jpg">
                    <img src="./assets/temp/tp3.jpg" alt="">
                </a>
            </li>
            <li>
                <a data-fancybox-group="gallery2" class="Js_fancyBox2" href="./assets/temp/bigpic4.jpg">
                    <img src="./assets/temp/tp4.jpg" alt="">
                </a>
            </li>
        </ul>
        <div class="workbottom clearfix">
            <div class="pub-time left">2013年6月15日 10:03发布</div>
            <div class="pub-operate right">
                <div class="pub-score left" style=" margin:6px 20px 0 0">
                    <div class="rateitltd left" style=" margin:6px 20px 0 0">
                        <div class="rateitltd-range" style="width: 120px; height: 24px;">
                            <div class="rateitltd-selected" style="height: 24px; width: 120px; display: block;"></div>
                        </div>
                </div>
                <div class="exp-comments Js_exp_comment left">3 位专家点评</div>
                <div class="set-winner left Js_t_remark">邀请专家点评</div>
                <!--                                <div class="set-winner left Js_winner">选为获胜作品</div>-->
                <!--                                <div class="set-out left Js_eliminate">淘汰该作品</div>-->
                <!--                                <div class="set-eliminate left" style="display: none;">已淘汰</div>-->
                <!--                                <div class="set-cancle left Js_zp_cancel">撤回作品</div>-->
            </div>
            <!--                        <i class="arr Js_arr"></i>-->
            <!--                        <i class="arr Js_arr1" style=" border-color: transparent transparent #FFF; right:320px;"></i>-->
        </div>
    </div>
</div>
<div class="task-expcomment Js_parents" style="display:none;">
    <div class="proContextDiv Js_exp_c" style="display:none;">
        <div class="titDiv clearfix">
            <span></span>
            <h2 class="left">专家点评</h2>
            <div class="right mt20">
                <!--                                <a class="sub-sbtn Js_t_remark">邀请专家点评</a>-->
            </div>
            <div class="right mt20 mr10">
<!--                <a class="sub-sbtn Js_t_invistd">发布专家点评</a>-->
            </div>
        </div>

        <!--专家点评弹出层开始-->
        <div class="expertDpDiv Js_tInvistd_c" style="display:none;">
            <div class="height_60"></div>
            <div class="p-warp">
                <p>专家评价</p>
                <div class="height_25"></div>
                <div class="l-edit">
                    <div class="n-agree">
                  <span class="Js_radio mr30" data-name="rad">
                    <label>
                        <input type="radio" name="rad">
                        <span class="p-level b73">优秀</span>
                    </label>
                  </span>
                  <span class="Js_radio mr30" data-name="rad">
                    <label>
                        <input type="radio" name="rad">
                        <span class="p-level c7ef">良好</span>
                    </label>
                  </span>
                  <span class="Js_radio" data-name="rad">
                    <label>
                        <input type="radio" name="rad">
                        <span class="p-level c0">一般</span>
                    </label>
                  </span>
                    </div>
                </div>
            </div>
            <div class="height_40"></div>
            <div class="p-warp">
                <p>专家评语</p>
                <div class="height_15"></div>
                <div class="rowblock l-edit">
                    <div id="dtb_toolbar2" class="toolbar-wrap"></div>
                    <div class="toolbar-content left">
                        <textarea id="dtb_content2" name="content" placeholder="请输入你要点评的内容"></textarea>
                    </div>
                </div>
            </div>
            <div class="height_50"></div>
            <div class="p-warp">
                <a href="javascript:;" class="btn h8 mr5 Js_tcan_invistd">取消点评</a>
                <a href="javascript:;" class="btn b73">发表专家点评</a>
            </div>
            <div class="height_50"></div>
            <div class="sp-line" style="top:0; left:52px;">
                <div class="line-left"></div>
                <div class="line-right"></div>
            </div>
        </div>
        <!--专家点评弹出层结束-->

        <div style="width:940px; margin:0 auto; display:none;" class="Js_tRemark_c">
            <div class="height_10"></div>
            <div class="p-warp">
                <p>邀请创意世界专家</p>
            </div>
            <div class="height_15"></div>
            <div class="rowblock">
                <div class="tools sel left Js_expertList">
                    <div class="inputWarp downMenuExperts">
                        <input type="text">
                        <div class="selDiv expertList">
                            <div class="describe">让创创召帮您查找关键词为"<span class="Js_search_txt"></span>"的内容</div>
                            <ul class="mt-2">
                            </ul>
                        </div>
                    </div>
                </div>
                <a href="javascript:void(0)" class="blueBtn fl ml10 Js_recommend_expert">推荐站外专家</a>
            </div>
            <div class="height_15"></div>
            <div class="inviteExpert">
                <h3>已邀请专家</h3>
                <ul class="Js_invitedexplist">
                    <li class="clearfix">
                        <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                        <div>
                            <p class="rightName">刘兆宇</p>
                            <p class="rightDis"><a href="#">成都电子科技大学</a></p>
                        </div>
                        <span class="expert-del Js_exp_del"></span>
                    </li>
                    <li class="clearfix">
                        <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                        <div>
                            <p class="rightName">如果我不是郦道元怎么办</p>
                            <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">创意世界</a><i>，</i><a href="#">创意世界</a></p>
                        </div>
                        <span class="expert-del Js_exp_del"></span>
                    </li>
                    <li class="clearfix" style="margin-top:31px;">
                        <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                        <div>
                            <p class="rightName">刘兆宇</p>
                            <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">创意世界</a><i>，</i><a href="#">创意世界</a></p>
                        </div>
                        <span class="expert-del Js_exp_del"></span>
                    </li>
                    <li class="clearfix" style="margin-top:31px;">
                        <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                        <div>
                            <p class="rightName">站外专家姓名</p>
                            <p class="rightDis"><a href="#">站外专家的简介内容</a></p>
                        </div>
                        <span class="expert-del Js_exp_del"></span>
                    </li>
                </ul>

            </div>
            <div class="height_50"></div>
            <div class="p-warp">
                <a href="javascript:;" class="grayBtn small-grayBtn h8 mr5 Js_tcan_remark">取消邀请</a>
                <a href="javascript:;" class="blueBtn small-blueBtn b73 Js_task_yqok">确认邀请</a>
            </div>
        </div>
        <div class="height_54"></div>
        <ul class="clearfix u_list h250 Js_explist vh">
        </ul>
        <div class="height_20"></div>
    </div>

    <div style="" class="winner-works Js_winner_c">
        <div class="proContextDiv workDiv">
            <div class="titDiv">
                <span></span>
                <h2>请确认是否选择当前作品为获胜作品</h2>
            </div>
            <div class="height_31"></div>
            <div class="txt">
                <p class="tit">当您一旦选择了获胜任务作品，本次比赛将进入任务作品确权交割流程，在该流程中您可以：</p>
                <p class="nc">1.要求获胜任务作品的发布者对当前作品进行微小的调整；</p>
                <p class="nc">2.确认当前作品符合您的要求；</p>
                <p class="nc">3.签订创意任务人才比赛的获胜作品成果转让协议；</p>
                <p class="bt">只有当双方均确认了成果转让协议之后，创意任务人才比赛的奖金才会发放给比赛获胜人才。</p>
            </div>
            <div class="height_40"></div>
            <div class="p-warp">
                <a href="javascript:;" class="btn h8 mr5 Js_cancle_winner">取消选择</a>
                <a href="javascript:;" class="btn b73 Js_affirm_win_works">确认选为获胜作品</a>
            </div>
            <div class="height_20"></div>
        </div>
    </div>
</div>

</div>

</div>