<div class="web-n-header">
    <div class="headerDiv clearfix">
        <div class="logo"><a href="index.php"><img src="assets/images/logo-pc.png" alt=""></a></div>
        <div class="t-search clearfix">
            <div class="clearfix left Js_ccz_left Js_user_img cur Js_user_select" style="display: inline-block">
                <div class="img-div">
                    <a href="#"><img src="assets/images/default.jpg" /></a>
                </div>
                <div class="user-name-ccb">
                    <span class="logout">未登录的游客</span>
                    <p style="display: none;">CC000000</p>
                </div>
                <span class="user-createIndex">0</span>
                <div class="user-dh">
                    <a href="javascript:;" class="Js_livemsg">
                        <b>0</b>
                    </a>
                </div>
            </div>
            <div class="user-search Js_ccz_right">
                <input type="text" class="Js_cczsearch" autocomplete="off" />
                <span class="Js_placeholder">所有我要，召之即来</span>
            </div>
            <!--未登陆提示弹窗-->
            <div class="p-ccbNum clearfix Js_ccbNum">
                   <div class="logined">
                    <ul>
                        <li>
                            <a href="./creative-index.php" class="clearfix">
                                <span class="cyzs">创意指数</span>
                                <i>0&nbsp;点</i>
                            </a>
                        </li>
                        <li>
                            <a href="./cash.php"class="clearfix">
                                <span class="rmbzh">人民币账户</span>
                                <i>0&nbsp;元</i>
                            </a>
                        </li>
                        <li>
                            <a href="./ccb.php"class="clearfix">
                                <span class="ccbzh">创创币帐户</span>
                                <i>0&nbsp;枚</i>
                            </a>
                        </li>
                        <li class="Js_livemsg">
                            <a href="#"class="clearfix pr">
                                <span class="sfdlh">消息提示</span>
                                <em>0</em>
                            </a>
                        </li>
                        <li>
                            <a href="./my-circle.php"class="clearfix">
                                <span class="circles">朋友圈</span>
                            </a>
                        </li>
                        <li>
                            <a href="./profile-edit.php"class="clearfix">
                                <span class="grzy">个人主页</span>
                            </a>
                        </li>
                        <li>
                            <a href="./userfile.php"class="clearfix">
                                <span class="zhsz">帐号设置</span>
                            </a>
                        </li>
                        <li class="last" style="display: none;">
                            <a href="#"class="clearfix">
                                <span class="tc">登出创意世界</span>
                            </a>
                        </li>
                        <li class="last" style="display: block">
                            <a href="login.php" class="n-login" style="margin-top: 10px; color:#fff;">登录</a>
                            <a href="register.php" class="n-regist" style="margin-top: 10px; color:#fff;">注册</a>
                            <p class="displace">马上注册拥有专属创创号并保存创意指数和创创币</p>
                        </li>
                    </ul>
                </div>
            </div>
            <!--未登陆提示弹窗结束-->
            <div class="information" style="display:none"></div>
            <div class="search-result" style="display:none;"></div>
            <div class="ccz-scHead Js_cczScHead" style="display:none;"></div>
            <div class="ccz-shareDiv clearfix Js_shareScHead" style="display:none;">
				<h3 class="share-tit">分享</h3>
				<div class="ccz-enterAreaDiv">
					<a href="#" class="ccz-userImg">
						<img src="./assets/temp/5.png" alt="">
					</a>
					<div class="ccz-area">
						<textarea class="publish_textarea" name="" id="" cols="30" rows="10" placeholder="分享新鲜事..."></textarea>
						<s></s>
					</div>
					<div class="height_10"></div>
					<div class="clearfix">
						<div class="clearfix">
							<label style="line-height:16px;">添加：</label>
							<ul class="clearfix ccz-cz_list Js_ccz_list">
								<li>
									<a class="clearfix" id="Js_share_img">
										<span style="background-position:10px 2px;"></span><s>照片</s>
									</a>
								</li>
								<li>
									<a class="clearfix" id="Js_share_video">
										<span style="background-position:10px -22px;"></span><s>视频</s>
									</a>
								</li>
								<li>
									<a class="clearfix" id="Js_share_src">
										<span style="background-position:10px -49px;"></span><s>链接</s>
									</a>
								</li>
							</ul>
						</div>
						<div class="ml98 mt5 Js_ccblist_item">
							<div class="rowblock Js_cover" style="display:none">
								<div class="uploadWrap-single dargArea left">
									<span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
									<input class="file_uploadbox-single" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">
								</div>
								<div class="height_10"></div>
								<div id="upload-single" class="clearfix Js_cover_div upload-single"></div>
							</div>
							<div class="clearfix" style="display:none">
								<span class="left share-videoAddress">视频地址链接：</span>
								<div class="tools text left" style="width:50%;">
									<div class="inputWarp empty">
										<input type="text" style="color:#999999; font-family:simsun; font-size:12px;">
									</div>
								</div>
								<span class="share-icon-yz Js_video_del"></span>
							</div>
							<div class="clearfix" style="display:none">
								<div class="clearfix">
									<span class="left share-videoAddress">网址：</span>
									<div class="tools text left" style="width:50%;">
										<div class="inputWarp empty">
											<input type="text" style="color:#999999; font-family:simsun; font-size:12px;" value="'+window.location.href" />
										</div>
									</div>
								</div>
								<a href="javascript:;" class="share-presentWeb Js_presentWeb">分享当前页面</a>
							</div>
						</div>
					</div>
					<div class="height_12"></div>
					<div class="clearfix ccz-share_obj">
						<label style=" line-height:38px;">分享对象：</label>
						<div style="margin:0 96px 0 82px;">
							<div class="left ml16" style="width:100%; height:auto;">
								<div class="tools text pr Js_share_search clearfix" style="width:100%; height:auto;">
									<div class="inputWarp opt font12 pr left Js_input_w">
										<input type="text" class="df-text" />
										<span class="df-text defalutTxt Js_defualt_txt"> + 请输入好友姓名或圈子名称</span>
									</div>
								</div>
								<div class="share-menu Js_share_menu">
									<ul>
										<li class="share-gk" data-type="1"><span>公开分享</span></li>
										<li class="share-circle" data-type="2"><span>全部</span>（80）</li>
										<li class="share-circle" data-type="2"><span>熟人</span>（50）</li>
										<li class="share-person" data-type="3"><img src="./assets/temp/c1.png" /><span>朋友</span>（48）</li>
										<li class="share-person" data-type="3"><img src="./assets/temp/c1.png" /><span>同学</span>（16）</li>
										<li class="share-person" data-type="3"><img src="./assets/temp/c1.png" /><span>家人</span>（2）</li>
									</ul>
								</div>
							</div>
						</div>
						<a href="#" class="ccz-add Js_webout_share">站外分享</a>
						<div class="webout-share">
							<s></s>
							<ul>
								<li class="Js_bind_webout">
									<input type="checkbox" />
									<img src="./assets/temp/xl.png">
									<span>新浪微博</span>
								</li>
								<li class="Js_bind_webout">
									<input type="checkbox" />
									<img src="./assets/temp/rrw.png">
									<span>人人网</span>
								</li>
								<li class="Js_bind_webout">
									<input type="checkbox" />
									<img src="./assets/temp/txw.png">
									<span>腾讯微博</span>
								</li>
								<li class="Js_bind_webout">
									<input type="checkbox" />
									<img src="./assets/temp/qqkj.png">
									<span>QQ空间</span>
								</li>
							</ul>
						</div>
					</div>
					<div class="height_25"></div>
					<div class="ccz-btnDiv">
						<a href="#" class="mr5 bshy Js_share_cancel">取消</a>
						<a href="#" class="bqh Js_share_ok">分享</a>
					</div>
				</div>
			</div>
            <div class="ccz-ydDiv Js_ccz_ydDiv" style="display:none;"></div>
            <div class="ccz-yjfkDiv Js_yjfk_div" style="display:none;"></div>

        </div>
    </div>
</div>