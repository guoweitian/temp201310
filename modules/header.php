<!Doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <?php if(isset($title_text) AND $title_text != ''){ ?>
    <title><?php echo $title_text;?> - 创意世界：所有我要，召之即来 - CSOOW.com</title>
    <?php }else{ ?>
    <title>创意世界：所有我要，召之即来 - CSOOW.com</title>
    <?php } ?>

    <?php foreach($load_css as $css_file_name){ ?>
    <link rel="stylesheet" href="assets/css/<?php echo $css_file_name;?>.css" />
    <?php } ?>
    <?php foreach($load_js as $js_file_name){ ?>
    <script type="text/javascript" src="assets/js/<?php echo $js_file_name;?>.js"></script>
    <?php } ?>

</head>
<body>
<div class="wrapper">