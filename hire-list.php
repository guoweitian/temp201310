<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
        'style.1.0.3',
        'jquery.fancybox',
        'jquery.fancybox-thumbs'
);

$load_js = array(
        'jquery',
        'jquery.html5uploader',
        'uploader_config_single',
        'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'common',
        'detail'
);

$title_text = '创意公益明星比赛';

$navigation_text = array(
        '0' => '<a class="on" href="hire-list.php">正在寻找明星的比赛</a>',
        '1' => '<a href="hire-list-over.php">已经找到明星的比赛</a>',
        '2' => '<a href="hire-publish.php">发布比赛</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-hire.php');
include('modules/sidebar.php');

include('blocks/hire-list.php');

include('modules/footer.php');
?>