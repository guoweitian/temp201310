<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
        'style.1.0.3',
        'jquery.fancybox',
);

$load_js = array(
        'jquery',
        'jquery.html5uploader',
        'uploader_config_single',
        'jquery.fancybox.min',
        'common',
);

$title_text = '投资项目比赛';

$navigation_text = array(
        '0' => '<a href="project-list.php">正在寻找投资的项目</a>',
        '1' => '<a class="on" href="project-list-over.php">已经接受投资的项目</a>',
        '2' => '<a href="project-publish.php">发布项目</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-project.php');
include('modules/sidebar.php');

include('blocks/project-list-over.php');

include('modules/footer.php');
?>