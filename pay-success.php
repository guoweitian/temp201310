<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
        'style.1.0.3',
        'redactor',
        'jquery.fancybox',
        'jquery.fancybox-thumbs'
);

$load_js = array(
        'jquery',
        'jquery.html5uploader',
        'uploader_config_single',
        'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'common',
        'detail',
);

$title_text = '订单支付成功';

$navigation_text = array(
    '0' => '<a href="creative-index.php">创意指数</a>',
    '1' => '<a class="on" href="order-manage.php">订单</a>',
    '2' => '<a href="cash.php">现金</a>',
    '3' => '<a href="ccb.php">创创币</a>',
    '4' => '<a href="userfile.php">个人资料</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation.php');
include('modules/sidebar.php');

include('blocks/pay-success.php');

include('modules/footer.php');
?>