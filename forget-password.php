<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
    'style.1.0.3',
    'jquery.fancybox',
    'jquery.fancybox-thumbs'
);

$load_js = array(
    'jquery',
    'jquery.html5uploader',
    'uploader_config_single',
    'jquery.fancybox.min',
    'jquery.fancybox-thumbs',
    'common',
    'detail'
);

$title_text = '找回密码';

$navigation_text = array(

);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation.php');
include('modules/sidebar.php');

include('blocks/forget-password.php');

include('modules/footer.php');
?>

