<div class="content">
<div class="publish-wrap">
<form method="post" id="form-send-trade" action="">
<div id="tabs-tab" class="publish-form publish w765">
<div class="row" style="overflow:hidden;">
    <div class="large-12 columns">
        <div class="publish-progress">
            <div class="height_40"></div>
            <div class="issueStrip pr">
                <div class="gray-bg"></div>
                <ul id="tab-t" class="clearfix strip strip3">
                    <!--当前current/没填notFill/最后一步没填notFill last-->
                    <li class="one current">
                        <span class="fousLine w-first"></span>
                        <a class="selected" href="#step-one">
                            <b>1</b>
                            <span class="st-one">基本信息</span>
                        </a>
                    </li>
                    <li class="notFill two ml">
                        <s></s>
                        <span class="fousLine wh"></span>
                        <a href="#step-two"><b>2</b><span class="st-two" style="margin-left: -27px">描述交易内容</span></a>
                    </li>
                    <li class="notFill three ml">
                        <s></s>
                        <span class="fousLine wh"></span>
                        <span class="fousLine w-last"></span>
                        <a href="#step-three"><b>3</b><span class="st-three"
                                                            style="margin-left: -27px">确认发布协议</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="tabs-items" class="Js_tabs_items clearfix">
    <div class="row tabs-c" style="display: block">
        <div class="large-12 columns">
            <div class="rowblock">
                <div class="pt">填写交易名称</div>
                <div class="tools text left">
                    <div class="inputWarp empty">
                        <input type="text" id="Js_task_name_input">
                    </div>
                </div>
                <div class="errorMsg left" style="display:none;">错误提示内容</div>
            </div>
            <div class="height_25"></div>
            <div class="rowblock">
                <div class="pt">交易所在地</div>
                <div class="tools text left Js_cityList">
                    <s class="locBg"></s>

                    <div class="inputWarp downMenuCity">
                        <input type="text" class="Js_loc_input" id="Js_loc_input">

                        <div class="selDiv cityList gloc">
                            <ul>
                                <li class="s1">可以根据城市名，街道名，国家名等搜索</li>
                                <li class="s2 Js_loc_ad"><a href="javascript:;">自动定位当前位置</a></li>
                                <li class="s3 Js_world_ad"><a href="javascript:;">全世界</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="errorMsg left" style="display:none;">错误提示内容</div>
            </div>
            <div class="height_25"></div>
            <div class="rowblock">
                <div class="pt">选择交易模式</div>
                <div class="l-edit">
                    <p class="n-agree mt10 left">
                        <span class="Js_radio mr25" data-name="rad">
                            <label class="Js_label_ok ok"><input type="radio" name="rad" checked="checked" id="Js_check_jp">竞拍价格</label>  <!--选择后文中颜色-->
                        </span>
                        <span class="Js_radio" data-name="rad">
                            <label class="Js_label_ok"><input type="radio" name="rad" id="Js_check_gd">固定价格</label>
                        </span>
                    </p>
                </div>
            </div>
            <div class="height_25"></div>
            <!--设置竞价显示层-->
            <div id="fields_auction" style="display: block;">
                <div class="Js_setting_jp">
                    <div class="rowblock">
                        <div class="pt">设置竞价起拍价格</div>
                        <div class="tools text left">
                            <div class="inputWarp enterNum">
                                <input type="text" class="Js_input_text">
                            </div>
                            <div class="numFormat">.00</div>
                        </div>
                        <div class="errorMsg left" style="display:none;">错误提示内容</div>
                    </div>
                    <div class="height_25"></div>
                    <div class="rowblock">
                        <div class="pt">设置竞价拍卖的截止日期</div>
                        <div class="tools text left">
                            <input class="datebox Js_input_date" type="text" id="date1" readonly="readonly">
                        </div>
                        <div class="errorMsg left" style="display:none;">请选择日期</div>
                    </div>
                </div>
                <div class="Js_setting_gd" style="display:none;">
                    <div class="rowblock">
                        <div class="pt">设置交易固定价格</div>
                        <div class="tools text left">
                            <div class="inputWarp enterNum">
                                <input type="text" class="Js_input_text" name="buy_now_price">
                            </div>
                            <div class="numFormat">.00</div>
                        </div>
                        <div class="errorMsg left" style="display:none">错误提示内容</div>
                    </div>
                    <div class="height_25"></div>
                    <div class="rowblock">
                        <div class="pt">设置交易数量</div>
                        <div class="tools text left">
                            <input class="datebox Js_input_text" type="text" name="num_for_trade">
                        </div>
                        <div class="errorMsg left" style="display:none">错误提示内容</div>
                    </div>
                </div>
            </div>
            <div class="height_25"></div>
            <div class="rowblock">
                <div class="pt">上传交易封面图片</div>
                <div class="uploadWrap-single dargArea left">
                    <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
                    <input class="file_uploadbox-single" type="file"
                           accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">
                </div>
                <div class="errorMsg left" style="display:none">请选上传封面图片</div>
                <div class="height_10"></div>
                <div id="upload-single" class="clearfix Js_fm_cover_div upload-single"></div>
            </div>
            <div class="height_50"></div>
            <a href="#step-two" class="blueBtn Js_s_one" style="margin-bottom: 60px;">下一步</a>
        </div>
        <!--设置固定价格显示层-->
        <div id="fields_buyout" style="display: none;">
            <div class="rowblock">
                <div class="pt">设置交易固定价格</div>
                <div class="tools text left">
                    <div class="inputWarp enterNum">
                        <input type="text" class="Js_input_text">
                    </div>
                    <div class="numFormat">.00</div>
                </div>
                <div class="errorMsg left" style="display:none">错误提示内容</div>
            </div>
            <div class="height_25"></div>
            <div class="rowblock">
                <div class="pt">设置交易数量</div>
                <div class="tools text left">
                    <div class="inputWarp empty">
                        <input type="text" class="Js_input_text">
                    </div>
                </div>
                <div class="errorMsg left" style="display:none">错误提示内容</div>
            </div>
        </div>
    </div>
    <div class="height_60"></div>
</div>

<div class="row tabs-c">
    <div class="large-12 columns">
        <div class="rowblock l-edit">
            <div class="pt">描述交易的详细内容</div>
            <p class="pdes">请尽可能详细的描述您的交易内容，以便更多的人参与交易</p>

            <div class="height_15"></div>
            <div id="js_toolbar" class="toolbar-wrap"></div>
            <div class="toolbar-content left">
                <textarea id="js_content" name="content"></textarea>
            </div>
            <div class="errorMsg left" style="display:none">请选上传封面图片</div>
        </div>

        <div class="height_50"></div>
        <a href="#step-one" class="grayBtn mr16 Js_s_two_pre">上一步</a>
        <a href="#step-three" class="blueBtn Js_s_two_next" style="margin-bottom: 60px;">下一步</a>
    </div>
    <div class="height_60"></div>
</div>

<div class="row tabs-c">
    <div class="large-12 columns">
        <div class="rowblock l-edit">
            <div class="pt">确认交易发布协议</div>
            <p class="pdes">请您阅读并确认创意世界交易所发布协议</p>

            <div class="height_15"></div>
            <div class="tools textArea">
                <textarea style="font-size: 15px; line-height: 1.5" id="Js_release_agreement" readonly>
                    确认发布交易协议
                    1 简介
                    创意世界交易所是创意世界网站中用户之间（创意交易发布者和创意交易购买者）交易创意的版块。
                    您作为创意交易发布者：您可以在创意世界交易所展示您的创意，您也可以通过设置竞价模式或者一口价购买模式来出售您的创意。
                    您作为创意交易购买者：您可以通过创意世界交易所寻找您需要的任何创意，并且您可以通过参加某个创意交易发布者提供的交易方式（竞价或一口价）来获得其出售的创意。
                    创意世界：并不是创意交易发布者和创意交易购买者中的任何一方。所有创意交易仅存在于用户和用户之间。
                    2 行为规则
                    创意交易出售者：
                    您必须在法律允许的范围内出售您的创意。
                    您必须在创意世界交易所对您所出售的创意或物品，以及它们的所有出售条件作出文字说明。
                    您不得发布任何含有欺诈行为的信息。如创意世界（通过定罪、和解、保险或其它调查或创意世界自行酌情决定的其它方式）怀疑您进行欺诈活动，创意世界可全权酌情决定中止或终止您的帐户。
                    您不得出售任何侵犯知识产权，第三方的著作权、商标权或其他知识产权的创意交易。知识产权权利人可通过联系创意世界客服申请删除侵犯其知识产权的物品。
                    创意交易购买者：
                    为了维护创意世界交易所的交易秩序，防止创意交易购买者恶拍，尽量避免并妥善处理买卖双方之间的交易纠纷，您参与竞拍时冻结的保证金将为本次竞拍提供保障。如您未在竞拍成功之日起三日内按照成交价格支付拍品货款，您同意创意世界交易有权将您在竞拍流程中最终冻结的竞拍保证金赔偿给卖家。
                    3 知识产权的转让：
                    在创意交易出售者和创意交易购买者完成交易资金交接后，该创意的知识产权即转让给交易购买者所有。
                    4 创意世界的责任范围
                    创意世界有权随时以任何理由拒绝、取消、中断、删除或暂停该任务。创意世界不因该行为承担任何赔偿。创意世界的政策并非评论此类行为的理由。
                    创意世界不承担任何创意或使用服务产生的损失或亏损。创意世界无义务介入任何用户之间的纠纷，或用户与其他第三方就服务使用方面产生的纠纷。包括但不限于货物及服务的交付，其他条款、条件、保证或与网站活动相关联的有关陈述。创意世界不负责监督任务的实现与严格执行。您可授权创意世界、其工作人员、职员、代理人及对损失索赔权的继任者所有已知或未知、公开或秘密的解决争议的方法和服务。

                </textarea>
            </div>
            <div class="errorMsg left" style="display:none">
                错误提示内容
            </div>
        </div>
        <div class="height_10"></div>
        <div class="l-edit">
            <p class="n-agree left">
                <span class="Js_checkBox" data-name="chk">
                 <label class="Js_label_ok ok">
                     <input type="checkbox" name="chk" checked="checked" id="Js_readed_agreement">
                     我已经阅读并同意创意世界交易发布协议</label> <!--选择后文中颜色-->
                </span>
            </p>

            <div class="errorMsg left" style="margin-top: -10px; display:none;">请确认同意协议</div>
        </div>

        <div class="height_40"></div>
        <div class="rowblock l-edit">
            <div class="pt left">
                <div class="sign-bg">
                    <input class="signature" id="Js_signature">
                </div>
            </div>
            <div class="errorMsg left mt8" style="display:none">
                错误提示内容
            </div>
        </div>
        <div class="height_50"></div>
        <a href="#step-two" class="grayBtn mr16 Js_s_three_pre">上一步</a>
        <a href="#step-one" class="blueBtn Js_send_project" style="margin-bottom: 60px;">发布交易</a><!--跳转到第一步，其他class为 Js_goto_one/Js_goto_two/Js_goto_three/Js_goto_four，同时设置一下a标签的锚点到对应位置 -->
    </div>
    <div class="height_60"></div>
</div>

</div>
</form>
</div>
</div>
</div>
<script type="text/javascript">
    $(function () {
        tabs('tabs-tab', 'tab-t', 1, 'click');
        tabs('tabs-tab', 'tab-w', 1, 'click');
        dataControl('date1');
        dataControl('date2');
    })
</script>