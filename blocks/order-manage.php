<div class="content">
<div class="detailPageMid">
<div class="proContextDiv">
        <div class="height_38"></div>
        <div class="titDiv">
                <span></span>
                <h2>订单管理</h2>
        </div>
        <div class="height_40"></div>
        <div class="pr">
                <h3 class="n-tit">帐户余额</h3>
                <div class="clearfix n-mon">
                        <span>1,123,456,789</span><s class="left">元</s>
                        <a href="#" class="l-m_btn left Js_recharge">账户充值</a>
                        <a href="pay-withdraw.php" class="s_btn left ml10">账户提现</a>
                </div>
<!--                <s class="p-box b1 Js_recharge_b" style="display:none; left:333px; bottom:-18px;"></s>-->
        </div>
        <div class="height_18"></div>
</div>
<div style="background:#e6e6e6; display:none;" class="Js_recharge_c">
        <div class="proContextDiv">
                <div class="height_18"></div>
                <div class="titDiv">
                        <span></span>
                        <h2>账户充值</h2>
                </div>
                <div class="height_33"></div>
                <div class="ft-style">
                        <div class="n-agree">
						<span class="Js_radio Js_labelck_ok" data-name="rad">
              <label class="radio">
                      <input type="radio" name="rad">
              </label>
              <span class="p-level b73" style="font-size:16px;">&nbsp;100&nbsp;&nbsp;元</span>
            </span>
                        </div>
                        <div class="height_27"></div>
                        <div class="n-agree">
            <span class="Js_radio Js_labelck_ok" data-name="rad">
              <label class="radio">
                      <input type="radio" name="rad">
              </label>
                <span class="p-level b73" style="font-size:16px;">&nbsp;200&nbsp;&nbsp;元</span>

            </span>
                        </div>
                        <div class="height_27"></div>
                        <div class="n-agree">
           	<span class="Js_radio Js_labelck_ok" data-name="rad">
              <label class="radio">
                      <input type="radio" name="rad">
              </label>
                <span class="p-level b73" style="font-size:16px;">&nbsp;500&nbsp;&nbsp;元</span>
            </span>
                        </div>
                        <div class="height_27"></div>
                        <div class="n-agree">
           	<span class="Js_radio Js_labelck_ok" data-name="rad">
              <label class="radio">
                      <input type="radio" name="rad">
              </label>
                 <span class="p-level b73" style="font-size:16px;"><input type="text" class="w108 ml10 Js_input_text">&nbsp;&nbsp;元</span>
            </span>
                        </div>
                </div>
                <div class="height_45"></div>
                <div class="p-warp">
                        <a href="javascript:;" class="btn h8 mr5 Js_cancle_buy">取消充值</a>
                        <a href="javascript:;" class="btn b73">确认充值</a>
                </div>
                <div class="height_50"></div>
                <div class="sp-line" style="top:0; margin:0;">
                        <div class="line-left"></div>
                        <div class="line-right"></div>
                </div>
        </div>
</div>
<div class="proContextDiv">
        <div class="height_30"></div>
        <div>
                <label class="l-start">起止时间：</label>
                <div class="tools text left" style="width:148px;">
                        <input class="datebox bbit-dp-input" type="text" id="dateone" readonly="readonly" style="width:144px !important;">
                </div>
                <s class="time-fh"></s>
                <div class="tools text left" style="width:148px;">
                        <input class="datebox bbit-dp-input" type="text" id="datetwo" readonly="readonly" style="width:144px !important;">
                </div>
                <ul class="clearfix l-areaNewsTit userfile">
                                <li><a href="javascript:;" class="Js_time_filter" data-type="2013/10/27">今天</a></li>
                                <li><a href="javascript:;" class="Js_time_filter current" data-type="2013/9/27">最近1个月</a></li>
                                <li><a href="javascript:;" class="Js_time_filter" data-type="2013/7/27">3个月</a></li>
                                <li><a href="javascript:;" class="Js_time_filter" data-type="2012/10/27">1年</a></li>
                                <li class="last"><a href="javascript:;" class="Js_time_filter" data-type="2012/10/27">1年前</a></li>
                        </ul>
        </div>
        <div class="height_38"></div>
        <div>
                <div class="clearfix">
                        <ul class="clearfix l-areaNewsTit userfile left" style="line-height:normal;">
                                <li><a href="#" class="current">全部</a></li>
                                <li><a href="#">未付款</a></li>
                                <li><a href="#">进行中</a></li>
                                <li class="last"><a href="#">已结束</a></li>
                        </ul>
                        <div class="pageItemDiv right">
                                <a href="javascript:;" class="fyBtn prev Js_page_list_prev Js_net_order_account"></a>
						<span class="pageItem cfs19 Js_page_list_itemNum">
							<a href="javascript:;" class="current Js_net_order_account">1</a>
							<a href="javascript:;" class="Js_net_order_account">2</a>
							<a href="javascript:;" class="Js_net_order_account">3</a>
							<a href="javascript:;" class="Js_net_order_account">4</a>
							<a href="javascript:;" class="Js_net_order_account">5</a>
						</span>
                                <a href="javascript:;" class="fyBtn next Js_page_list_next Js_net_order_account"></a>
                        </div>
                </div>

                <div class="height_27"></div>
                <div>
                        <!--<div class="clearfix t-title">
                            <span class="time t-man">时间</span>
                            <span class="deal">交易方</span>
                            <span class="dis d-man">名称</span>
                            <span class="status">状态</span>
                            <span class="sum s-man">金额</span>
                            <span class="cz">操作</span>
                        </div>-->
                        <div>
                                <table width="940" border="0" cellpadding="0" cellspacing="0" class="t-detail Js_order_account_list">
                                        <tr style="background:#deded9">
                                                <td class="td-time t-time">时间</td>
                                                <td class="td-deal">交易方</td>
                                                <td class="td-dis t-dis">名称</td>
                                                <td class="td-status">状态</td>
                                                <td class="td-sum t-sum" style="font-size:14px; color:#5F5F5F; font-weight:normal;">金额</td>
                                                <td class="td-cz t-cz">操作</td>
                                        </tr>
                                        <tr>
                                                <td class="td-time t-time">2013/01/25 22:20:22</td>
                                                <td class="td-deal">我不是郦道元</td>
                                                <td class="td-dis t-dis">淘宝购物-聚壹之家牛津百纳箱聚壹之家牛津60L+30L二件套玩具收纳物箱</td>
                                                <td class="td-status">交易关闭</td>
                                                <td class="td-sum t-sum">11198.00</td>
                                                <td class="td-cz t-cz"><a href="pay-order-detail.php">详情</a></td>
                                        </tr>
                                        <tr>
                                                <td class="td-time t-time">2013/01/25 21:20:22</td>
                                                <td class="td-deal">用户A</td>
                                                <td class="td-dis t-dis">ThinkPad E430C14英寸笔记本电脑</td>
                                                <td class="td-status">等待发货</td>
                                                <td class="td-sum t-sum">98.00</td>
                                                <td class="td-cz t-cz"><a href="pay-order-detail.php">确认发货</a></td>
                                        </tr>
                                        <tr>
                                                <td class="td-time t-time">2013/01/25 20:20:22</td>
                                                <td class="td-deal">用户一</td>
                                                <td class="td-dis t-dis">神奇的台灯</td>
                                                <td class="td-status">等待收货</td>
                                                <td class="td-sum t-sum">298.00</td>
                                                <td class="td-cz t-cz"><a href="pay-order-detail.php">确认收货</a></td>
                                        </tr>
                                        <tr>
                                                <td class="td-time t-time">2013/01/25 19:20:22</td>
                                                <td class="td-deal">用户二</td>
                                                <td class="td-dis t-dis">ThinkPad E430C14英寸笔记本电脑</td>
                                                <td class="td-status">交易成功</td>
                                                <td class="td-sum t-sum">3498.00</td>
                                                <td class="td-cz t-cz"><a href="pay-order-detail.php">详情</a></td>
                                        </tr>
                                        <tr>
                                                <td class="td-time t-time">2013/01/25 18:20:22</td>
                                                <td class="td-deal">刘兆宇</td>
                                                <td class="td-dis t-dis">百诺（BENRO）C2681TB1 可拆卸独脚架</td>
                                                <td class="td-status">交易退款</td>
                                                <td class="td-sum t-sum">98.00</td>
                                                <td class="td-cz t-cz"><a href="pay-order-detail.php">详情</a></td>
                                        </tr>
                                        <tr>
                                                <td class="td-time t-time">2013/01/25 17:20:22</td>
                                                <td class="td-deal">李永刚</td>
                                                <td class="td-dis t-dis">百诺（BENRO）C2681TB1 可拆卸</td>
                                                <td class="td-status">等待付款</td>
                                                <td class="td-sum t-sum">98.00</td>
                                                <td class="td-cz t-cz">
                                                        <div class="payDiv">
                                                                <a href="#" class="pay Js_pay">支付订单<s></s></a>
                                                                <ul class="Js_pay_menu">
                                                                        <li><a href="pay-order.php">支付订单</a><s></s></li>
                                                                        <li><a href="#">取消订单</a></li>
                                                                </ul>
                                                        </div>
                                                </td>
                                        </tr>
                                </table>
                        </div>
                        <div class="height_40"></div>
                        <div class="pageItemDiv right mb25">
                                <a href="javascript:;" class="fyBtn prev Js_page_list_prev Js_net_order_account"></a>
						<span class="pageItem cfs19 Js_page_list_itemNum">
							<a href="javascript:;" class="current Js_order_rmb_account">1</a>
							<a href="javascript:;" class="Js_net_order_account">2</a>
							<a href="javascript:;" class="Js_net_order_account">3</a>
							<a href="javascript:;" class="Js_net_order_account">4</a>
							<a href="javascript:;" class="Js_net_order_account">5</a>
						</span>
                                <a href="javascript:;" class="fyBtn next Js_page_list_next Js_net_order_account"></a>
                        </div>
                </div>
        </div>
</div>
</div>
</div>
<script type="text/javascript">
var d = new Date();
var dy = d.getFullYear();
var dm = d.getMonth()+1;
var dd = d.getDate();
$('#dateone').val(dy + '/' + (dm-1) + '/' + dd);
$('#datetwo').val(dy + '/' + dm + '/' + dd);
dataControl('dateone','datetwo');
</script>