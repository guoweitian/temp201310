<div class="content">
        <div class="detail-wrap task">
                <div class="detail-imgbg">
                        <div class="toppattern"></div>
                        <div class="detailTopDiv pew">
                                <h3>这里是比赛名称，如果有很多字，这里也只能显示一行标题。这里是比赛名称，如果有很多字，这里也只能显示一行标题。</h3>
                                <p class="disDiv">
                                        <span class="loc">成都市青羊区小南街</span>
                                        <span class="time">已结束</span>
                                        <span class="m_num">M2013052400021</span>
                                </p>
                                <div>
                                        <div class="u_listItem left">
                                                <a href="#" class="leftArea shadow">
                                                        <img src="./assets/temp/5.png" alt="">
                                                </a>
                                                <div class="rightArea w600">
                                                        <p class="rightName"><a class="Js_visitCard cfff" href="#">刘兆宇</a></p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                                </div>
                                        </div>
                                        <div class="tz_moneyDiv right">
                                                <div class="p-ok">
                                                <p style="padding-top: 2px">任务比赛奖金:<b>10,000</b>元</p>
                                                <p style="padding:4px 0 8px 1px">获奖人才:这个杀手不太冷</p>
<!--                                                <a href="#" class="bgimgBtn_over right over" onclick="return false">已结束</a>-->
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>

                <div class="detailPageMidDiv">
                    <!--获胜任务人才和作品 开始-->
                    <div class="detailPageMid">
                        <div class="proContextDiv">
                            <div class="titDiv">
                                <span></span>
                                <h2>获胜任务人才和作品</h2>
                            </div>
                            <!--头像、ID和作品信息-->
                            <div class="u_listItem w937 ph100">
                                <div class="height_40"></div>
                                <a href="#" class="leftArea shadow">
                                    <img src="./assets/temp/5.png" alt="">
                                </a>
                                <div class="rightArea pw100 ph100">
                                    <p class="rightName"><a href="#" class="Js_visitCard">这个杀手不太冷</a></p>
                                    <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                    <p class="des">用户还可以在App Store的"更新"栏看到最近刚刚升级过的应用列表。用户可以在这里看到安装升级程序的时间以及升级的内容。 如果App Store还没有进行自动更新，用户也可以选择手动安装最新的更新文件。</p>
                                    <ul class="worklists clearfix">
                                        <li>
                                            <a data-fancybox-group="gallery3" class="Js_fancyBox3" href="./assets/temp/tp1.jpg">
                                                <img src="./assets/temp/tp1.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a data-fancybox-group="gallery3" class="Js_fancyBox3" href="./assets/temp/tp3.jpg">
                                                <img src="./assets/temp/tp2.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a data-fancybox-group="gallery3" class="Js_fancyBox3" href="./assets/temp/tp4.jpg">
                                                <img src="./assets/temp/tp3.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a data-fancybox-group="gallery3" class="Js_fancyBox3" href="./assets/temp/tp4.jpg">
                                                <img src="./assets/temp/tp4.jpg" alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
<!--                            <div class="height_45"></div>-->

                        </div>
                    </div>
                    <!--获胜任务人才和作品 结束-->

                    <!--                        <div class="detailPageMid">-->
<!--                                <div class="proContextDiv">-->
<!--                                        <div class="titDiv">-->
<!--                                                <span></span>-->
<!--                                                <h2>比赛已经结束</h2>-->
<!--                                        </div>-->
<!---->
<!--                                        <div class="tOverDiv">-->
<!--                                                <p class="top">您发布的创意任务人才比赛已经结束。</p>-->
<!--                                                <p class="iList">在比赛期间，您未能选择到满意的任务人才作品；</p>-->
<!--                                                <p class="iList">您可以更新您的创意任务需求，以便让更多的创意任务人才理解您的需求，并且提交您可能满意的作品；</p>-->
<!--                                                <p class="iList">您可以邀请更多的创意世界智库专家点评比赛中的任务人才作品，为您的选拔提供专业的意见；</p>-->
<!--                                                <p class="iList">为了方便您再次发布本创意任务人才比赛，您可以使用下方的"再次发布"按钮。</p>-->
<!--                                        </div>-->
<!--                                        <div class="height_40"></div>-->
<!--                                        <a href="./task-publish.php" class="nsBtn">再次发布</a>-->
<!--                                </div>-->
<!--                        </div>-->
                        <div class="height_18"></div>
                        <div class="detailPageMid">
                                <div class="proContextDiv">
                                        <div class="titDiv">
                                                <span></span>
                                                <h2 class="Js_scroll">任务信息</h2>
                                        </div>
                                        <div class="context">苹果用户每天手动安装应用更新的日子已经成为过去时了，由于iOS 7中的App Store增强了功能，发现新软件将变得比以前更容易。虽然App Store中无处不在的红色升级标记令用户们感到非常迷惑，但是当iOS 7在今年秋季正式发布的时候，用户们就会体会到它的好处了。如果启动自动更新的功能，iOS 7中的App Store就可以在后台自动检查和安装应用更新。
                                                用户还可以在App Store的“更新”栏看到最近刚刚升级过的应用列表。用户可以在这里看到安装升级程序的时间以及升级的内容。 如果App Store还没有进行自动更新，用户也可以选择手动安装最新的更新文件。

                                                “更新”栏中还将包括一个指向已购买内容的链接，用户们可以重新下载他们以前在另一台iOS设备上已经安装过的应用程序。

                                                用户可以在“iOS设置”应用的“iTunes App Store”部分控制自动更新功能，在“自动下载”部分根据需要开启或关闭“更新”、“应用”和“音乐”等选项。 与在以前版本的iOS系统中一样，用户们还可以决定是否允许自动下载功能使用蜂窝网络数据。</div>
                                </div>
                        </div>
                        <?php include("./modules/task-participator.php"); ?>

                        <!--todo 社区论坛引用文件-->
                        <?php $forumTT = "讨论区"; ?>
                        <?php include("./modules/discuss.php"); ?>

                        <!--todo 竞猜引用文件-->
                        <?php $guessTT = "竞猜"; ?>
                        <?php include("./modules/lottery.php"); ?>
                </div>
        </div>
</div>
<script type="text/javascript">
        $(function(){
                tabs('tabs-tab','tab-t', 1, 'click');
                tabs('tabs-tab','tab-w', 1, 'click');

                $('.Js_fancyBox,.Js_fancyBox1,.Js_fancyBox2,.Js_fancyBox3').fancybox({
                        prevEffect : 'none',
                        nextEffect : 'none',

                        closeBtn  : true,
                        arrows    : true,
                        nextClick : true,
                        tpl : {
                                closeBtn : '<a title="Close" style="background-image: url(assets/images/fancybox/fancybox_sprite.png); width:36px; height:36px; top:-18px; right:-18px; position:absolute"></a>'
                        },
                        helpers : {
                                thumbs : {
                                        width  : 50,
                                        height : 50
                                }
                        }
                })
        });
</script>