<div class="content" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html"
     xmlns="http://www.w3.org/1999/html">
<div class="publish-wrap">

<form method="post" id="form-send-project" action="">
<div id="tabs-tab" class="publish-form publish w765">
<div class="row" style="overflow:hidden;">
        <div class="large-12 columns">
                <div class="publish-progress">
                        <div class="height_40"></div>
                        <div class="issueStrip pr">
                                <div class="gray-bg"></div>
                                <ul id="tab-t" class="clearfix strip strip4">
                                        <!--当前current/没填notFill/最后一步没填notFill last-->
                                        <li class="one">
                                                <span class="fousLine w-first"></span>
                                                <a class="selected" href="#step-one">
                                                        <b>1</b>
                                                        <span class="st-one">基本信息</span>
                                                </a>
                                        </li>
                                        <li class="notFill two ml">
                                                <s></s>
                                                <span class="fousLine wh"></span>
                                                <a href="#step-two"><b>2</b><span class="st-two">描述内容</span></a>
                                        </li>
                                        <li class="notFill three ml">
                                                <s></s>
                                                <span class="fousLine wh"></span>
                                                <a href="#step-three"><b>3</b><span class="st-three">详细介绍</span></a>
                                        </li>
                                        <li class="notFill four ml">
                                                <s></s>
                                                <span class="fousLine wh"></span>
                                                <span class="fousLine w-last"></span>
                                                <a href="#step-four"><b>4</b><span class="st-fore" style="margin-left: -27px">确认发布协议</span></a>
                                        </li>
                                </ul>
                        </div>
                </div>
        </div>
</div>
<div id="tabs-items" class="Js_tabs_items clearfix">
        <div class="row tabs-c" style="display: block">
                <div class="large-12 columns">
                        <div class="rowblock">
                                <div class="pt">填写项目名称</div>
                                <div class="tools text left">
                                        <div class="inputWarp empty">
                                                <input type="text" id="Js_project_name_input">
                                        </div>
                                </div>
                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        </div>
                        <div class="height_25"></div>
                        <div class="rowblock">
                                <div class="pt">项目所在地</div>
                                <div class="tools text left Js_cityList">
                                        <s class="locBg"></s>
                                        <div class="inputWarp downMenuCity">
                                                <input type="text" class="Js_loc_input" id="Js_loc_input">
                                                <div class="selDiv cityList gloc">
                                                        <ul>
                                                                <li class="s1">可以根据城市名，街道名，国家名等搜索</li>
                                                                <li class="s2 Js_loc_ad"><a href="javascript:;">自动定位当前位置</a></li>
                                                                <li class="s3 Js_world_ad"><a href="javascript:;">全世界</a></li>
                                                        </ul>
                                                </div>
                                        </div>
                                </div>
                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        </div>

                        <div class="height_25"></div>
                        <div class="rowblock">
                                <div class="pt">寻找项目投资时间</div>
                                <div class="l-edit">
                                        <p class="n-agree mt10 left">
                                            <span class="Js_radio mr25 Js_forever" data-name="rad">
                                                <label class="Js_label_ok ok"><input type="radio" name="invest_time_end_type" value="1" checked>长期</label> <!--选择后文中颜色-->
                                            </span>
                                            <span class="Js_radio Js_limit_time" data-name="rad">
                                                <label class="Js_label_ok"> <input type="radio" name="invest_time_end_type" value="2"> 限制时间</label>
                                            </span>
                                        </p>
                                </div>
                        </div>
                        <div class="height_25"></div>
                        <div class="rowblock Js_limit_content" style="display:none;">
                                <div class="pt">设置项目投资的截止日期</div>
                                <div class="tools text error left">
                                        <input class="datebox Js_input_date" type="text" id="date1" readonly="readonly" >
                                </div>
                                <div class="errorMsg left" style="display:none;">请选择日期</div>
                        </div>
                        <div class="height_15"></div>
                        <div class="rowblock">
                                <div class="pt">寻求项目投资的金额</div>
                                <div class="tools text left">
                                        <div class="inputWarp enterNum">
                                                <input  class="Js_enterOnlyNum" type="text" id="Js_find_money"/>
                                        </div>
                                        <div class="numFormat">.00</div>
                                </div>
                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        </div>
                        <div class="height_25"></div>
                        <div class="rowblock">
                                <div class="pt">上传项目封面图片</div>
                                <div class="uploadWrap-single dargArea left">
                                        <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
                                        <input class="file_uploadbox-single" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">
                                </div>
								
                                <div class="errorMsg left" style="display:none">请上传封面图片</div>
                                <div class="height_10"></div>
                                <div id="upload-single" class="clearfix Js_fm_cover_div upload-single"></div>
                        </div>
                        <div class="height_50"></div>
                        <a href="#step-two" class="blueBtn Js_s_one" style="margin-bottom: 60px;">下一步</a>
                </div>
                <div class="height_60"></div>
        </div>
</div>

<div class="row tabs-c" style="display:none;">
        <div class="large-12 columns">
                <div class="rowblock l-edit">
                        <div class="pt">项目投资回报内容</div>
                        <p class="pdes">投资回报内容是指您能够给予投资方享受的权益。</p>
                        <div class="height_10"></div>
                        <div id="tz_toolbar" class="toolbar-wrap"></div>
                        <div class="toolbar-content left">
                                <textarea id="tz_content" name="content"></textarea>
                        </div>
                        <div class="errorMsg left" style="display:none;">请填写投资回报内容</div>
                </div>
                <div class="height_25"></div>
                <div class="rowblock">
                        <div class="pt">投资回报兑现时间</div>
                        <div class="tools text left">
                                <input class="datebox bbit-dp-input" type="text" id="date2" readonly="readonly">
                        </div>
                        <div class="errorMsg left" style="display:none;">错误提示内容</div>
                </div>
                <div class="height_25"></div>

                <div class="rowblock">
                        <div class="pt">上传投资回报图片</div>
                        <div class="uploadWrap-more dargArea left">
                                <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
                                <input class="file_uploadbox-more" type="file" ismul="true" multiple="multiple" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">

                        </div>
                        <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        <div class="height_10"></div>
                        <div id="upload-more" class="clearfix Js_upload_more upload-more"></div>

                </div>
                <div class="height_50"></div>
                <a href="#step-one" class="grayBtn mr16 Js_s_two_pre">上一步</a>
                <a href="#step-three" class="blueBtn Js_s_two_next" style="margin-bottom: 60px;">下一步</a>
        </div>
        <div class="height_60"></div>
</div>

<div class="row tabs-c" style="display:none;">
        <div class="large-12 columns">
                <div class="rowblock l-edit">
                        <div class="pt">项目详细介绍</div>
                        <p class="pdes">填写项目详细介绍</p>
                        <div class="height_10"></div>
                        <div id="js_toolbar" class="toolbar-wrap"></div>
                        <div class="toolbar-content">
                                <textarea id="js_content" name="content" ></textarea>
                        </div>
                        <div class="errorMsg" style="display:none;">错误提示内容</div>
                </div>

                <div class="height_50"></div>
                <a href="#step-two" class="grayBtn mr16 Js_s_three_pre">上一步</a>
                <a href="#step-four" class="blueBtn Js_s_three_next" style="margin-bottom: 60px;">下一步</a>
        </div>
        <div class="height_60"></div>
</div>

<div class="row tabs-c">
        <div class="large-12 columns">
                <div class="rowblock l-edit">
                        <div class="pt">确认发布协议</div>
                        <p class="pdes">请您阅读并确认创意投资项目比赛的发布协议</p>
                        <div class="height_10"></div>
                        <div class="tools textArea">
                                <textarea style="font-size: 15px; line-height: 1.5" id="Js_release_agreement" readonly>
确认发布创意投资项目比赛协议
1.1 简介
        您只有在完全同意下列服务条款，并完成注册后，才能成为而且立即成为四川创意世界科技有限公司创意投资项目比赛的用户并使用创意投资项目比赛所提供的服务。一旦您浏览或开始使用创意投资项目比赛的服务，即视为您已经完全了解并同意下列条款，包括创意投资项目比赛对条款做的修改。
创意投资项目比赛是创意世界提供的一个允许用户(即“项目发布者”)通过展示项目或创意，并承诺分享项目成果，以便寻找项目投资者共同实现项目或创意的服务。
        您作为项目发布者：项目投资者可以与您订立合同，为您的项目或创意提供资金的投资，而您则在接受投资后努力实现项目或创意，并兑现您给项目投资者做出的承诺。
        您作为项目投资者：可以接受项目发布者对您做出的的项目成果分享的承诺，以资金的方式投资项目发布者的项目或创意。
        创意世界并不是项目发布者和项目投资者中的任何一方。所有交易仅存在于用户和用户之间。
1.2 行为规则
        通过创意世界投资项目，您须同意并遵守以下协议，包括如下条款：
	项目投资者同意接受在其承诺投资某项目时提供付款授权信息 。
	项目投资者同意创意世界及其合作伙伴授权或保留收费的权利。
	在项目进行中，只要项目投资者还没有点击“确认收到项目成果”按钮，项目投资者都可以任意取消投资。
	投资回报的预期发放日期并非约定实现的期限，它仅为项目发布者希望实现的日期。
	为建立良好的信用和名声，项目发布者会尽力依照预期完成日期实现项目。
	对于所有项目，创意世界将提供项目投资者的姓名、联系方式和邮寄地址等信息给于项目发布者。
	项目发布者可以在项目成功后直接向项目投资者要求额外信息。为了顺利获得投资回报，项目投资者须同意在合理期限内提供给项目发布者相关信息。
1.3 创意世界的责任范围
        创意世界保留随时以任何理由取消项目的权利。
        创意世界有权随时以任何理由拒绝、取消、中断、删除或暂停该项目。创意世界不因该行为承担任何赔偿。创意世界的政策并非评论此类行为的理由。
        在项目成功和获得款项之间可能存在延迟。
        创意世界不承担任何相关回报或使用服务产生的损失或亏损。创意世界无义务介入任何用户之间的纠纷，或用户与其他第三方就服务使用方面产生的纠纷。包括但不限于货物及服务的交付，其他条款、条件、保证或与网站活动相关联的有关陈述。创意世界不负责监督项目的实现与严格执行。您可授权创意世界、其工作人员、职员、代理人及对损失索赔权的继任者所有已知或未知、公开或秘密的解决争议的方法和服务。
                                </textarea>
                        </div>
                        <div class="errorMsg left" style="display:none;">请填写比赛协议</div>
                </div>
                <div class="height_10"></div>
                <div class="l-edit">
                        <p class="n-agree left">
                <span class="Js_checkBox" data-name="chk">
                <label class="ok">
                        <input type="checkbox" name="chk" checked="checked" id="Js_readed_agreement" class="Js_label_ok">
                        我已经阅读并同意创意投资项目比赛的发布协议</label> <!--选择后文中颜色-->
                </span>
                        </p>
                        <div class="errorMsg left" style="margin-top: -10px; display:none;">请确认同意协议</div>
                </div>
                <div class="height_40"></div>
                <div class="rowblock l-edit">
                        <div class="pt left">
                                <div class="sign-bg">
                                        <input class="signature" id="Js_signature">
                                </div>
                        </div>
                        <div class="errorMsg left mt8" style="display:none;">错误提示内容</div>
                </div>
                <div class="height_50"></div>
                <a href="#step-three" class="grayBtn mr16 Js_s_four_pre">上一步</a>
                <a href="javascript:;" class="blueBtn Js_send_project" style="margin-bottom: 60px;">发布项目</a><!--跳转到第一步，其他class为 Js_goto_one/Js_goto_two/Js_goto_three/Js_goto_four，同时设置一下a标签的锚点到对应位置 -->
        </div>
        <div class="height_60"></div>
</div>

</div>
</form>

</div>
</div>
</div>
<script type="text/javascript">
        $(function(){
                tabs('tabs-tab','tab-t', 1, 'click');
                tabs('tabs-tab','tab-w', 1, 'click');
                dataControl('date1');
                dataControl('date2');
        })
</script>