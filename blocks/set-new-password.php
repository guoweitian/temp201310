<div class="content">
        <div class="login-box" style="width: 660px;">
                <form id="set_new_password">
                <div class="l-login-form">
                        <div class="height_18"></div>
                        <div class="box-title">
                                <h4 class="ml32">设置新密码</h4>
                        </div>
                        <div class="form-item">
                                <p class="ptext left">新密码:</p>
                                <div class="tools text left" style="width: 256px;">
                                        <div class="inputWarp mr20">
                                                <input class="Js_password ipt_password_r" name="password" type="password" placeholder="请输入您的新密码">
                                        </div>
                                </div>
                                <div class="errorMsg left"></div>
                                <div class="height_5"></div>
                                <div class="strength" style="margin-left: 146px;">
                                        <span>密码强度：<span class="f1">弱</span></span>
                                        <div class="bk"></div>
                                        <div class="over">
                                                <span class="level level_1"></span>
                                                <span class="level level_2"></span>
                                                <span class="level level_3"></span>
                                        </div>
                                        <span class="f2">强</span>
                                        <input type="hidden" class="Js_strengthv_alue">
                                </div>
                        </div>
                        <div class="height_10"></div>

                        <div class="form-item">
                                <p class="ptext left">确认新密码:</p>
                                <div class="tools text left" style="width: 256px;">
                                        <div class="inputWarp mr20">
                                                <input type="password" placeholder="请再次输入您的新密码">
                                        </div>
                                </div>
                                <div class="errorMsg left"></div>
                        </div>
                </div>
                </form>
                <div class="height_20"></div>
                <div class="login-btnbar" style="margin-left: 122px;">
                        <a href="set-new-password-success.php" class="blueBtn login-btn">确认设置</a>
                </div>

        </div>
</div>