<div class="content">
    <form id="form_login">
        <div class="login-box l-shadow" style="width: 360px;">
            <div class="login-form">
                <div class="box-title"><h4>登录账号</h4></div>
                <div class="form-item rowblock" id="login_name">
                    <div class="tools text">

                        <div class="inputWarp mr20" style="margin-left: 10px;">
                            <input type="text" placeholder="请输入您的用户名/邮箱/创创号" id="Js_user_name" name="login_name">
                        </div>
                    </div>
                    <div class="errorMsg" style="display:none;margin-left: 0px;"></div>
                </div>
                <div class="height_12"></div>
                <div class="form-item rowblock" id="password">
                    <div class="tools text">

                        <div class="inputWarp mr20" style="margin-left: 10px;">
                            <input type="password" placeholder="请输入您的密码" id="Js_user_pwd" name="password">
                        </div>
                    </div>
                    <div class="errorMsg" style="display:none;margin-left: 0px;"></div>
                </div>
            </div>
            <div class="login-btnbar">
                <a href="javascript:;" class="blueBtn" id="button_login">登录</a><a href="forget-password.php" class="link-s ml10">忘记密码?</a>
            </div>
            <div class="ifnotregistered">
                <p>如果您还没有创意世界账号，请点击<a href="register.php">注册</a></p>
            </div>
        </div>
    </form>
</div>