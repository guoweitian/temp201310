<div class="content">
<div class="height_20"></div>
<div class="search-bar w940">
        <div class="search-input-text left">搜索关键字</div>
        <div class="search-logo">
                <div class="i-google"><a href="#"></a></div>
                <div class="i-baidu"><a href="#"></a></div>
        </div>
</div>
<div class="height_20"></div>
<div class="posts-wrap w940">
<ul class="posts-wraplist" >
<li class="posts-item">
        <div class="publish-cbbox clearfix">
                <div class="textwrap">
                        <textarea class="publish_textarea" name="" id="" cols="30" rows="10" placeholder="分享新鲜事..."></textarea>
                </div>
                <div class="publish-cbtype Js_cbtype clearfix">
                        <div class="type-word left current"><i class="dot"></i><span></span>文字</div>
                        <div class="type-pic left"><i class="dot"></i><span></span>照片</div>
                        <div class="type-link left"><i class="dot"></i><span></span>链接</div>
                        <div class="type-video left"><i class="dot"></i><span></span>视频</div>
                </div>
        </div>
</li>

<li class="posts-item">
        <div class="posts-box clearfix ">
                <div class="post-newsbox">
                        <nav class="nav-list">
                                <ul>
                                        <li><a href="#">热门</a></li>
                                        <li><span class="d-line"></span></li>
                                        <li><a href="#">时事</a></li>
                                        <li><span class="d-line"></span></li>
                                        <li><a href="#">政治</a></li>
                                        <li><span class="d-line"></span></li>
                                        <li><a href="#">经济</a></li>
                                        <li><span class="d-line"></span></li>
                                        <li><a href="#">创意</a></li>
                                        <li><span class="d-line"></span></li>
                                        <li><a href="#">军事</a></li>
                                </ul>
                        </nav>
                        <div class="height_10"></div>
                        <div class="toppic clear">
                                <a href="#">
                                        <img class="topimg" src="./assets/temp/t1.png" alt="">
                                        <div class="toptitle">重庆暴雨成灾 居民遭围困</div>
                                </a>
                        </div>
                        <div class="height_20"></div>
                        <div class="t-h">
                                <h3><a href="#">新疆：提供重要恐怖犯罪线索奖5至10万元</a></h3>
                                <p><a href="#">外交部:"东突"勾结国际势力破坏新疆 环球时报:斗争需5个"不怕"</a></p>
                        </div>
                        <div class="height_20"></div>
                        <div class="t-h">
                                <h3><a href="#">中国首例精神赡养案无锡落锤 法官判定期</a></h3>
                                <p>[<a href="#">专题</a>][<a href="#">强降水将持续至今晚</a>][<a href="#">寻遗失车牌车主</a>][<a href="#">直播</a>]</p>
                        </div>
                        <div class="height_15"></div>
                        <ul class="top-newslist">
                                <li><a href="#">强热带风暴“温比亚”登陆湛江 洪灾致重庆90万人受灾</a></li>
                                <li><a href="#">宋庆龄雕像未完工即拆除 河南宋基会被调查</a></li>
                                <li><a href="#">深圳：8月1日起被救助人诬告救助人或被追刑责</a></li>
                                <li><a href="#">北京市长：下定决心打一场大气污染治理攻坚战</a></li>
                                <li><a href="#">法官谈“常回家看看”：执行困难但有积极一面</a></li>
                                <li><a href="#">英首相卡梅伦访问哈萨克斯坦 签署10亿美元协议</a></li>
                                <li><a href="#">俄罗斯总统普京再次指责欧盟能源政策</a></li>
                                <li><a href="#">孟加拉女孩被埋17天生还被指系造假</a></li>
                        </ul>
                </div>
        </div>
</li>

<li class="posts-item" data-type="0">
        <div class="posts-box clearfix">
                <div class="tit-bq bfs">美女</div>
                <div class="post-user-info clearfix">
                        <a href="#"><img class="u-avatar left" src="./assets/temp/9.png" alt=""></a>
                        <p class="u-name left">
                                <a class="name Js_visitCard" href="#">如果我不是郦道元怎么办</a>
                                <span class="time">下午12:53</span>
                        </p>
                </div>
                <div class="post-content">
                        <a>
                                由JueLab发起的项目[成为一位皮革匠人第四期]上线了。
                        </a>
                </div>
                <div class="psot-like clearfix">
                        <a class="like-box zan Js_zan" href="javascript:;">464</a>
                        <a class="like-box zhuan" href="javascript:;">600</a>
                </div>
                <div class="post-comment">
                        <div class="post-c-con">
                                <div class="p-c-title"><b>4</b> 条评论</div>
                                <ul class="Js_itemlist clearfix">
                                        <li>
                                                <div class="p-c-list">
                                                        <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                        <div class="p-c-con">
                                                                <p class="top"><a class="cname Js_visitCard" href="#">刘兆宇</a><span class="ctime">9月9日&nbsp;&nbsp;2:27PM</span></p>
                                                                <p class="con">这东西不错呀，真心很好的这东西不错呀，真心很好的这东西不错呀，真心很好的</p>
                                                        </div>
                                                </div>
                                        </li>
                                        <li>
                                                <div class="p-c-list">
                                                        <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                        <div class="p-c-con">
                                                                <p class="top"><a class="cname Js_visitCard" href="#">刘兆宇</a><span class="ctime">9月9日&nbsp;&nbsp;2:27PM</span></p>
                                                                <p class="con">这东西不错呀，真心很好的这东西不错呀，真心很好的这东西不错呀，真心很好的</p>
                                                        </div>
                                                </div>
                                        </li>
                                        <li>
                                                <div class="p-c-list">
                                                        <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                        <div class="p-c-con">
                                                                <p class="top"><a class="cname Js_visitCard" href="#">刘兆宇</a><span class="ctime">9月9日&nbsp;&nbsp;2:27PM</span></p>
                                                                <p class="con">这东西不错呀，真心很好的这东西不错呀，真心很好的这东西不错呀，真心很好的</p>
                                                        </div>
                                                </div>
                                        </li>
                                        <li>
                                                <div class="p-c-list">
                                                        <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                        <div class="p-c-con">
                                                                <p class="top"><a class="cname Js_visitCard" href="#">刘兆宇</a><span class="ctime">9月9日&nbsp;&nbsp;2:27PM</span></p>
                                                                <p class="con">这东西不错呀，真心很好的这东西不错呀，真心很好的这东西不错呀，真心很好的</p>
                                                        </div>
                                                </div>
                                        </li>
                                </ul>
                        </div>
                        <div class="post-repeat-wrap">
                                <div class="post-repeat-text">
                                        <input class="Js_hinput" type="text" placeholder="添加您的评论...">
                                </div>
                                <div class="post-repeat-box Js_subtextarea">
                                        <div class="p-c-list ">
                                                <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                <div class="p-c-con">
                                                        <textarea class="sub-text" name="" cols="30" rows="10"></textarea>
                                                        <div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</li>

<li class="posts-item" data-type="1">
        <div class="posts-box clearfix">
                <div class="tit-bq blys">创意</div>
                <div class="post-user-info clearfix">
                        <a href="#"><img class="u-avatar left" src="./assets/temp/9.png" alt=""></a>
                        <p class="u-name left">
                                <a class="name Js_visitCard" href="#">如果我不是郦道元怎么办</a>
                                <span class="time">下午12:53</span>
                        </p>
                </div>
                <div class="post-pic img4 clearfix">
                        <div class="left-img">
                                <a data-fancybox-group="gallery" class="Js_fancyBox" href="./assets/temp/p6.jpg">
                                        <img src="./assets/temp/p6.jpg" alt="">
                                </a>
                        </div>
                        <div class="right-img">
                                <a data-fancybox-group="gallery" class="Js_fancyBox" href="./assets/temp/p7.jpg">
                                        <img src="./assets/temp/p7.jpg" alt="">
                                </a>
                                <a data-fancybox-group="gallery" class="Js_fancyBox" href="./assets/temp/p8.jpg">
                                        <img src="./assets/temp/p8.jpg" alt="">
                                </a>
                                <a data-fancybox-group="gallery" class="Js_fancyBox" href="./assets/temp/p9.jpg">
                                        <img src="./assets/temp/p9.jpg" alt="">
                                </a>

                        </div>

                </div>
                <div class="psot-like clearfix">
                        <a class="like-box zan Js_zan" href="javascript:;">赞</a>
                        <a class="like-box zhuan" href="javascript:;">600</a>
                        <input class="Js_noneinput" type="text" placeholder="添加您的评论..." />
                </div>
                <div class="post-comment" style="display:none;">
                        <div class="post-c-con">
                                <div class="p-c-title"><b>0</b> 条评论</div>
                                <ul class="Js_itemlist clearfix">
                                </ul>
                        </div>
                        <div class="post-repeat-wrap">
                                <div class="post-repeat-text">
                                        <input class="Js_hinput" type="text" placeholder="添加您的评论...">
                                </div>
                                <div class="post-repeat-box Js_subtextarea">
                                        <div class="p-c-list ">
                                                <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                <div class="p-c-con">
                                                        <textarea class="sub-text" name="" cols="30" rows="10"></textarea>
                                                        <div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</li>

<li class="posts-item" data-type="3">
        <div class="posts-box clearfix">
                <div class="tit-bq blys">创意</div>
                <div class="post-user-info clearfix">
                        <a href="#"><img class="u-avatar left" src="./assets/temp/9.png" alt=""></a>
                        <p class="u-name left">
                                <a class="name Js_visitCard" href="#">如果我不是郦道元怎么办</a>
                                <span class="time">下午12:53</span>
                        </p>
                </div>
                <div class="post-pic">
                        <div class="l-video">
                                <a href="javascript:;" class="js_player"></a>
                                <video id="video1" width="465">
                                        <source src="./assets/temp/mov.ogg" type="video/ogg">
                                        <source src="./assets/temp/mov.mp4" type="video/mp4">
                                        Your browser does not support the video tag.
                                </video>
                        </div>
                </div>
                <div class="psot-like clearfix">
                        <a class="like-box zan Js_zan" href="javascript:;">赞</a>
                        <a class="like-box zhuan" href="javascript:;">600</a>
                        <input class="Js_noneinput" type="text" placeholder="添加您的评论..." />
                </div>
                <div class="post-comment" style="display:none;">
                        <div class="post-c-con">
                                <div class="p-c-title"><b>0</b> 条评论</div>
                                <ul class="Js_itemlist clearfix">
                                </ul>
                        </div>
                        <div class="post-repeat-wrap">
                                <div class="post-repeat-text">
                                        <input class="Js_hinput" type="text" placeholder="添加您的评论...">
                                </div>
                                <div class="post-repeat-box Js_subtextarea">
                                        <div class="p-c-list ">
                                                <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                <div class="p-c-con">
                                                        <textarea class="sub-text" name="" cols="30" rows="10"></textarea>
                                                        <div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</li>

<li class="posts-item" data-type="0">
        <div class="posts-box clearfix">
                <div class="tit-bq blys">创意</div>
                <div class="post-user-info clearfix">
                        <a href="#"><img class="u-avatar left" src="./assets/temp/9.png" alt=""></a>
                        <p class="u-name left">
                                <a class="name Js_visitCard" href="#">如果我不是郦道元怎么办</a>
                                <span class="time">下午12:53</span>
                        </p>
                </div>
                <div class="post-pic img2 clearfix">
                        <div class="left-img">
                                <a data-fancybox-group="gallery2" class="Js_fancyBox2" href="./assets/temp/p13.jpg">
                                        <img src="./assets/temp/p13.jpg" alt="">
                                </a>
                        </div>
                        <div class="right-img">
                                <a data-fancybox-group="gallery2" class="Js_fancyBox2" href="./assets/temp/p14.jpg">
                                        <img src="./assets/temp/p14.jpg" alt="">
                                </a>
                        </div>

                </div>
                <div class="psot-like clearfix">
                        <a class="like-box zan Js_zan" href="javascript:;">赞</a>
                        <a class="like-box zhuan" href="javascript:;">600</a>
                        <input class="Js_noneinput" type="text" placeholder="添加您的评论..." />
                </div>
                <div class="post-comment" style="display:none;">
                        <div class="post-c-con">
                                <div class="p-c-title"><b>0</b> 条评论</div>
                                <ul class="Js_itemlist clearfix">
                                </ul>
                        </div>
                        <div class="post-repeat-wrap">
                                <div class="post-repeat-text">
                                        <input class="Js_hinput" type="text" placeholder="添加您的评论...">
                                </div>
                                <div class="post-repeat-box Js_subtextarea">
                                        <div class="p-c-list ">
                                                <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                <div class="p-c-con">
                                                        <textarea class="sub-text" name="" cols="30" rows="10"></textarea>
                                                        <div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</li>

<li class="posts-item" data-type="3">
        <div class="posts-box clearfix">
                <div class="tit-bq blys">创意</div>
                <div class="post-user-info clearfix">
                        <a href="#"><img class="u-avatar left" src="./assets/temp/9.png" alt=""></a>
                        <p class="u-name left">
                                <a class="name Js_visitCard" href="#">如果我不是郦道元怎么办</a>
                                <span class="time">下午12:53</span>
                        </p>
                </div>
                <div class="post-pic img3 clearfix">
                        <div class="left-img">
                                <a data-fancybox-group="gallery1" class="Js_fancyBox1" href="./assets/temp/p10.jpg">
                                        <img src="./assets/temp/p10.jpg" alt="">
                                </a>
                        </div>
                        <div class="right-img">
                                <a data-fancybox-group="gallery1" class="Js_fancyBox1" href="./assets/temp/p11.jpg">
                                        <img src="./assets/temp/p11.jpg" alt="">
                                </a>
                                <a data-fancybox-group="gallery1" class="Js_fancyBox1 last" href="./assets/temp/p11.jpg">
                                        <img src="./assets/temp/p12.jpg" alt="">
                                </a>

                        </div>

                </div>
                <div class="psot-like clearfix">
                        <a class="like-box zan Js_zan" href="javascript:;">赞</a>
                        <a class="like-box zhuan" href="javascript:;">600</a>
                        <input class="Js_noneinput" type="text" placeholder="添加您的评论..." />
                </div>
                <div class="post-comment" style="display:none;">
                        <div class="post-c-con">
                                <div class="p-c-title"><b>0</b> 条评论</div>
                                <ul class="Js_itemlist clearfix">
                                </ul>
                        </div>
                        <div class="post-repeat-wrap">
                                <div class="post-repeat-text">
                                        <input class="Js_hinput" type="text" placeholder="添加您的评论...">
                                </div>
                                <div class="post-repeat-box Js_subtextarea">
                                        <div class="p-c-list ">
                                                <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                <div class="p-c-con">
                                                        <textarea class="sub-text" name="" cols="30" rows="10"></textarea>
                                                        <div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</li>
</ul>
</div>
</div>
<script type="text/javascript">
        var container = $('.posts-wraplist');
        container.masonry({
                columnWidth: 448,
                gutter: 25,
                itemSelector: '.posts-item'
        });
        container.imagesLoaded( function() {
                container.masonry();
        });
        $('.Js_fancyBox,.Js_fancyBox1,.Js_fancyBox2,.Js_fancyBox3').fancybox({
                prevEffect : 'none',
                nextEffect : 'none',

                closeBtn  : true,
                arrows    : true,
                nextClick : true,
                tpl : {
                        closeBtn : '<a title="Close" style="background-image: url(../../images/fancybox/fancybox_sprite.png); width:36px; height:36px; top:-18px; right:-18px; position:absolute"></a>'
                },
                helpers : {
                        thumbs : {
                                width  : 50,
                                height : 50
                        }
                }
        });
        //视频播放
        var a = $("#video1");
        var b = $(".js_player");
        b.css({"top":Math.floor((a.height()-b.height())/2)+"px","left":Math.floor((a.width()-b.width())/2)+"px"})
        var myVideo= a.get(0);
        b.click(function(){
                myVideo.play();
                $(this).fadeOut();
        });
        myVideo.addEventListener("ended",function(){b.fadeIn()})
</script>