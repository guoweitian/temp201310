<div class="content">
<div class="detailPageMid">
<div class="proContextDiv">
        <ul class="clearfix areaNewsTit">
                <li><a href="#" class="current Js_userfile_item">个人资料</a></li>
                <li><a href="#" class="Js_userfile_item">密码管理</a></li>
                <li><a href="#" class="Js_userfile_item">地址管理</a></li>
                <li><a href="#" class="Js_userfile_item">身份验证</a></li>
                <li class="last"><a href="#" class="Js_userfile_item">银行卡管理</a></li>
        </ul>
<!--个人资料 开始-->
		<form name="user_flie">
        	<div class="u_listItem list_no_w Js_u_listitem">
                <a href="#" class="leftArea" style="width:66px; height:66px; border-radius:33px; margin-right:0px; margin-top:12px;">
                        <img src="./assets/temp/5.png" alt="" style="width:60px; height:60px; border-radius:30px; margin:3px;" id="Js_change_userphoto">
                </a>
                <div class="rightArea no_w" style="padding-left:80px;">
                        <p class="rightName"><a href="#" style="font-size:20px; font-family:'微软雅黑',sans-serif;">刘兆宇</a></p>
                        <p class="rightDis hs"><a href="#">csoow@csoow.com</a></p>
<!--                        <p style="font-size:12px; font-family:simsun; color:#5f5f5f; margin-top:10px;">密码安全等级：-->
<!--						<span style="display:inline-block; width:126px; height:12px; border:1px solid #c7c7c7; position:relative; top:2px;">-->
<!--							<span style="width:40px; height:8px; background:#c7c7c7; position:absolute; top:1px; left:1px;"></span>-->
<!--							<span style="width:40px; height:8px; background:#a0a0a0; position:absolute; top:1px; left:42px;"></span>-->
<!--							<span style="width:40px; height:8px; background:#898989; position:absolute; top:1px; left:83px;"></span>-->
<!--						</span>-->
<!--                                <b style="font-size:12px; font-family:simsun; color:#3b86c4; margin-left:8px;">高</b>-->
<!--                        </p>-->
                        <div class="height_35"></div>
                        <div class="t_warp clearfix">
                                <label class="lab lh28">头&nbsp;&nbsp;像：</label>
                                <a href="javascript:;" class="m_btn left" id="uploadbox">编辑图像</a>
                                <a href="javascript:;" class="m_btn left Js_uploadbox" style="display:none;"></a>
                                <input class="file_uploadbox" id="file_b" type="file" style="display:none">
                                <input type="hidden" id="x" name="x">
                                <input type="hidden" id="y" name="y">
                                <input type="hidden" id="w" name="w">
                                <input type="hidden" id="h" name="h">
                                <input type="hidden" id="file_path" name="file_path">
                        </div>
						<div class="user-file-d Js_userfile_box">
							<div class="user-file-t clearfix">
								<div class="user-file Js_user_head">
									<!--<div class="Js_file_loading">图片加载中...</div>-->
								</div>
							</div>
						</div>
                        <div class="height_15"></div>
                        <div class="t_warp clearfix">
                                <label class="lab">生&nbsp;&nbsp;日：</label>
                                <div class="tools text left" style="background:#efefeb; width:148px;">
                                        <input class="datebox bbit-dp-input" type="text" id="date1" readonly="readonly" style="background:#efefeb; width:146px !important; font-family:Arial, Helvetica, sans-serif; font-size:16px;">
                                </div>
                        </div>
                        <div class="height_20"></div>
                        <div class="t_warp clearfix n-agree">
                                <label class="lab lh28">性&nbsp;&nbsp;别：</label>
                                <div style="line-height:32px;">
                                        <label class="Js_label_ok"><input type="radio" name="sex" />&nbsp;&nbsp;男</label>
                                        <label class="Js_label_ok"><input type="radio" name="sex" style="margin-left:30px;" />&nbsp;&nbsp;女 </label>
                                </div>
                        </div>
                        <div class="height_20"></div>
                        <div class="t_warp clearfix">
                                <label class="lab">三&nbsp;&nbsp;围：</label>
                                <div class="optDiv Js_state left mr16" style="width:148px;">
                                        <div class="tools text pr" style="width:148px; background:#efefeb">
                                                <div class="inputWarp opt left font14">
                                                        <input type="text" style="text-align:left; font-size:14px; width:68px; background:#efefeb" placeholder="胸围" class="Js_input_text">
                                                </div>
                                                <div class="numFormat">cm</div>
                                        </div>
                                </div>
                                <div class="optDiv Js_state left mr16" style="width:148px;">
                                        <div class="tools text pr" style="width:148px; background:#efefeb">
                                                <div class="inputWarp opt left font14">
                                                        <input type="text" style="text-align:left; font-size:14px; width:68px; background:#efefeb" placeholder="腰围" class="Js_input_text">
                                                </div>
                                                <div class="numFormat">cm</div>
                                        </div>
                                </div>
                                <div class="optDiv Js_state left mr16" style="width:148px;">
                                        <div class="tools text pr" style="width:148px; background:#efefeb">
                                                <div class="inputWarp opt left font14">
                                                        <input type="text" style="text-align:left; font-size:14px; width:68px; background:#efefeb" placeholder="臀围" class="Js_input_text">
                                                </div>
                                                <div class="numFormat">cm</div>
                                        </div>
                                </div>
                        </div>
                        <div class="height_20"></div>
                        <div class="t_warp clearfix">
                                <label class="lab">体&nbsp;&nbsp;重：</label>
                                <div class="optDiv Js_state left" style="width:148px;">
                                        <div class="tools text pr" style="width:148px; background:#efefeb">
                                                <div class="inputWarp opt left font14">
                                                        <input type="text" style="text-align:left; font-size:14px; width:68px; background:#efefeb" placeholder="体重" class="Js_input_text">
                                                </div>
                                                <div class="numFormat">kg</div>
                                        </div>
                                </div>
                        </div>
                        <div class="height_20"></div>
                        <div class="t_warp clearfix">
                                <label class="lab">所在地：</label>
                                <div class="clearfix left">
                                        <div class="optDiv Js_state left mr10" style="width:120px;">
                                                <div class="tools text pr" style="width:120px; background:#efefeb">
                                                        <div class="inputWarp opt">
                                                                <span></span>
                                                                <a href="javascript:void(0)" class="showMenu"></a>
                                                        </div>
                                                </div>
                                                <ul id="Js_t1"></ul>
                                                <select id="t1" style="display:none;">
                                                        <option>数据加载中...</option>
                                                </select>
                                        </div>
                                        <div class="optDiv Js_state left mr10" style="width:130px;" id="Js_t2">
                                                <div class="tools text pr" style="width:130px; background:#efefeb">
                                                        <div class="inputWarp opt">
                                                                <span></span>
                                                                <a href="javascript:void(0)" class="showMenu"></a>
                                                        </div>
                                                </div>
                                                <ul></ul>
                                                <select id="t2" style="display:none;">
                                                        <option>数据加载中...</option>
                                                </select>
                                        </div>
                                        <div class="optDiv Js_state left" style="width:150px;" id="Js_t3">
                                                <div class="tools text pr" style="width:150px; background:#efefeb">
                                                        <div class="inputWarp opt">
                                                                <span></span>
                                                                <a href="javascript:void(0)" class="showMenu"></a>
                                                        </div>
                                                </div>
                                                <ul></ul>
                                                <select id="t3" style="display:none;" isflag='1'>
                                                        <option>数据加载中...</option>
                                                </select>
                                        </div>
                                </div>
                        </div>
                        <div class="height_20"></div>
                        <div class="t_warp clearfix">
                                <label class="lab"></label>
                                <a href="javascript:void(0)" class="blueBtn Js_update_userfile">更新资料</a>
                        </div>
                </div>

        </div>
		</form>
<!--个人资料 结束-->
</div>
<div class="detailPageMid">

<!--密码管理 开始-->
<div class="proContextDiv">
		<form name="pwd_manager">
        	<div class="u_listItem list_no_w Js_u_listitem" style="display:none;">
                <a href="#" class="leftArea" style="width:66px; height:66px; border-radius:33px; margin-right:0px; margin-top:12px;">
                        <img src="./assets/temp/5.png" alt="" style="width:60px; height:60px; border-radius:30px; margin:3px;">
                </a>
                <div class="rightArea no_w" style="padding-left:80px;">
                        <p class="rightName"><a href="#" style="font-size:20px; font-family:'微软雅黑',sans-serif;">刘兆宇</a></p>
                        <p class="rightDis hs"><a href="#">csoow@csoow.com</a></p>
                        <p style="font-size:12px; font-family:simsun, sans-serif; color:#5f5f5f; margin-top:10px;">密码安全等级：
						<span style="display:inline-block; width:126px; height:12px; border:1px solid #c7c7c7; position:relative; top:2px; margin-left: 10px">
							<span style="width:40px; height:8px; background:#c7c7c7; position:absolute; top:1px; left:1px;"></span>
							<span style="width:40px; height:8px; background:#a0a0a0; position:absolute; top:1px; left:42px;"></span>
							<span style="width:40px; height:8px; background:#898989; position:absolute; top:1px; left:83px;"></span>
						</span>
                                <b style="font-size:12px; font-family:simsun; color:#3b86c4; margin-left:8px;">高</b>
                        </p>
                        <div class="height_20"></div>
                        <!--<div class="t_warp clearfix">
                            <label class="lab w112">密码安全：</label>
                            <div class="strength ml40 clearfix" style="line-height:34px;">
                                <div class="bk" style="height:32px; margin:0 40px 0 0;"></div>
                                <div class="over" style="left:2px; top:10px;">
                                    <span class="level level_1"></span>
                                    <span class="level level_2"></span>
                                    <span class="level level_3"></span>
                                </div>
                                <span class="f2" style="color:#000">一般</span>
                                <input type="hidden" class="Js_strengthv_alue">
                            </div>
                        </div>-->
                        <!--<div class="height_5"></div>
                        <div class="t_warp clearfix">
                            <label class="lab w112"></label>
                            <a href="#" class="m_btn left">更新密码</a>
                        </div>-->
                        <div class="t_warp clearfix">
                                <label class="lab" style="width:80px;">原始密码：</label>
                                <div class="tools text left" style="background:#efefeb; margin-left: 20px">
                                        <div class="inputWarp empty">
                                                <input type="password" style="background:#efefeb">
                                        </div>
                                </div>
                        </div>
                        <div class="height_20"></div>
                        <div class="t_warp clearfix">
                                <label class="lab" style="width:80px;">新的密码：</label>
                                <div class="tools text left" style="background:#efefeb; margin-left: 20px">
                                        <div class="inputWarp empty">
                                                <input type="password" class="Js_password ipt_password_r" style="background:#efefeb">
                                        </div>
                                </div>
                        </div>
                        <div class="height_20"></div>
                        <div class="t_warp clearfix">
                                <label class="lab" style="width:80px;">确认密码：</label>
                                <div class="tools text left" style="background:#efefeb; margin-left: 20px">
                                        <div class="inputWarp empty">
                                                <input type="password" style="background:#efefeb">
                                        </div>
                                </div>
                        </div>
                        <div class="height_20"></div>
                        <div class="t_warp clearfix">
                                <label class="lab" style="width:100px;"></label>
                                <a href="javascript:void(0)" class="blueBtn">确认修改</a>
                        </div>
                </div>
        </div>
		</form>
</div>
<!--密码管理 结束-->

<div class="detailPageMid">

<!--地址管理 开始-->
<div class="proContextDiv">
        <div class="u_listItem list_no_w Js_u_listitem" style="display:none;">
                <a href="#" class="leftArea" style="width:66px; height:66px; border-radius:33px; margin-right:0px; margin-top:12px;">
                        <img src="./assets/temp/5.png" alt="" style="width:60px; height:60px; border-radius:30px; margin:3px;">
                </a>
                <div class="rightArea no_w" style="padding-left:80px;">
                        <p class="rightName"><a href="#" style="font-size:20px; font-family:'微软雅黑', sans-serif">刘兆宇</a></p>
                        <p class="rightDis hs"><a href="#">csoow@csoow.com</a></p>
<!--                        <p style="font-size:12px; font-family:simsun; color:#5f5f5f; margin-top:10px;">密码安全等级：-->
<!--						<span style="display:inline-block; width:126px; height:12px; border:1px solid #c7c7c7; position:relative; top:2px;">-->
<!--							<span style="width:40px; height:8px; background:#c7c7c7; position:absolute; top:1px; left:1px;"></span>-->
<!--							<span style="width:40px; height:8px; background:#a0a0a0; position:absolute; top:1px; left:42px;"></span>-->
<!--							<span style="width:40px; height:8px; background:#898989; position:absolute; top:1px; left:83px;"></span>-->
<!--						</span>-->
<!--                                <b style="font-size:12px; font-family:simsun; color:#3b86c4; margin-left:8px;">高</b>-->
<!--                        </p>-->
                        <div class="height_30"></div>
                        <div style="width:497px; height:99px; border:2px dashed #cccccc" class="Js_add_dress_btn">
                                <div class="clearfix" style="margin-top:29px; margin-left:21px;">
                                        <a href="javascript:;" class="new-btn Js_add_newaddress" style="float:left; margin-right:10px;">添加地址</a>
                                        <span style="float:left; font-size:14px; color:#888888; line-height:20px;">您还没有添加你的地址<br />为了能使商品准确达到你的手中，请添加收货地址。</span>
                                </div>
                        </div>
                        <div class="height_10"></div>
                        <div>
                                <div class="t_warp clearfix Js_add_addresslist" style="display:none;">
                                        <div class="height_20"></div>
                                        <a href="javascript:;" class="c-bank-btn left">添加新地址</a>
                                </div>
                        </div>
                        <!--<div class="t_warp clearfix">
                            <label class="lab lh0 w112">个人地址：</label>
                            <ul class="address">
                                <li class="clearfix">
                                    <div class="addressItemDiv">
                                        <span>四川省成都市青羊区</span>
                                        <p class="a-name">
                                            <span>小南街123号冠城花园二期檀香阁635室</span>&nbsp;
                                            <span>610000</span>
                                        </p>
                                    </div>
                                    <div class="u-name">李永刚</div>
                                    <div class="z-code">810000</div>
                                    <div class="edit">
                                        <a href="#" class="a-default">默认地址</a>
                                        <a href="#" class="b-edit">修改</a>
                                        <a href="#" class="b-del">删除</a>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <div class="addressItemDiv">
                                        <span>四川省成都市青羊区</span>
                                        <p class="a-name">
                                            <span>小南街123号冠城花园二期檀香阁635室</span>&nbsp;
                                            <span>610000</span>
                                        </p>
                                    </div>
                                    <div class="u-name">李永刚</div>
                                    <div class="z-code">810000</div>
                                    <div class="edit">
                                        <a href="#" class="a-edit">设为默认</a>
                                        <a href="#" class="b-edit">修改</a>
                                        <a href="#" class="b-del">删除</a>
                                    </div>
                                </li>
                                <li class="clearfix last">
                                    <div class="addressItemDiv">
                                        <span>四川省成都市青羊区</span>
                                        <p class="a-name">
                                            <span>小南街123号冠城花园二期檀香阁635室</span>&nbsp;
                                            <span>610000</span>
                                        </p>
                                    </div>
                                    <div class="u-name">李永刚</div>
                                    <div class="z-code">810000</div>
                                    <div class="edit">
                                        <a href="#" class="a-edit">设为默认</a>
                                        <a href="#" class="b-edit">修改</a>
                                        <a href="#" class="b-del">删除</a>
                                    </div>
                                </li>
                            </ul>
                        </div>-->
                        <div class="height_15"></div>
						<form name="addr_manager">
                        	<div class="t_warp clearfix Js_add_form" style="display:none;">
                                <div class="left">
                                        <div class="t_warp clearfix">
                                                <label class="lab w128">收货人姓名：</label>
                                                <div class="tools text left" style="width:278px; background:#efefeb;font-family:simsun;">
                                                        <div class="inputWarp empty">
                                                                <input type="text" style="background:#efefeb" class="Js_add_ngname">
                                                        </div>
                                                </div>
                                                <div class="errorMsg left" style="display:none">错误提示内容</div>
                                        </div>
                                        <div class="height_18"></div>
                                        <div class="t_warp clearfix">
                                                <label class="lab w128">所在地：</label>

                                                <div class="clearfix left">
                                                        <div class="optDiv Js_state left mr10" style="width:120px;" id="Js_c1">
                                                                <div class="tools text pr" style="width:120px; background:#efefeb">
                                                                        <div class="inputWarp opt">
                                                                                <span></span>
                                                                                <a href="javascript:void(0)" class="showMenu"></a>
                                                                        </div>
                                                                </div>
                                                                <ul></ul>
                                                                <select id="c1" style="display:none;">
                                                                        <option>数据加载中...</option>
                                                                </select>
                                                        </div>
                                                        <div class="optDiv Js_state left mr10" style="width:130px;" id="Js_c2">
                                                                <div class="tools text pr" style="width:130px; background:#efefeb">
                                                                        <div class="inputWarp opt">
                                                                                <span></span>
                                                                                <a href="javascript:void(0)" class="showMenu"></a>
                                                                        </div>
                                                                </div>
                                                                <ul></ul>
                                                                <select id="c2" style="display:none;">
                                                                        <option>数据加载中...</option>
                                                                </select>
                                                        </div>
                                                        <div class="optDiv Js_state left" style="width:150px;" id="Js_c3">
                                                                <div class="tools text pr" style="width:150px; background:#efefeb;">
                                                                        <div class="inputWarp opt">
                                                                                <span></span>
                                                                                <a href="javascript:void(0)" class="showMenu"></a>
                                                                        </div>
                                                                </div>
                                                                <ul></ul>
                                                                <select id="c3" style="display:none;">
                                                                        <option>数据加载中...</option>
                                                                </select>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="height_22"></div>
                                        <div class="t_warp clearfix">
                                                <label class="lab w128">街道地址：</label>
                                                <div class="tools text left" style="width:278px; background:#efefeb; font-family:simsun;">
                                                        <div class="inputWarp empty">
                                                                <input type="text" style="background:#efefeb" class="Js_jd_address">
                                                        </div>
                                                </div>
                                                <div class="errorMsg left" style="display: none">错误提示内容</div>
                                        </div>
                                        <div class="height_22"></div>
                                        <div class="t_warp clearfix">
                                                <label class="lab w128">邮政编码：</label>
                                                <div class="tools text left" style="width:278px; background:#efefeb; font-family:simsun;">
                                                        <div class="inputWarp empty">
                                                                <input type="text" style="background:#efefeb" class="Js_add_ngzipcode Js_enterOnlyNum">
                                                        </div>
                                                </div>
                                                <div class="errorMsg left" style="display:none">错误提示内容</div>
                                        </div>
                                        <div class="height_22"></div>
                                        <div class="t_warp clearfix">
                                                <label class="lab w128">电话号码：</label>
                                                <div class="tools text left" style="width:278px; background:#efefeb; font-family:simsun;">
                                                        <div class="inputWarp empty">
                                                                <input type="text" style="background:#efefeb" class="Js_add_ngphone Js_enterOnlyNum">
                                                        </div>
                                                </div>
                                                <div class="errorMsg left" style="display: none">错误提示内容</div>
                                        </div>
                                        <div class="height_20"></div>
                                        <div class="t_warp clearfix">
                                                <label class="lab w128"></label>
                                                <a href="javascript:void(0)" class="grayBtn Js_add_dress_cancle" style="margin-right:20px; font-family:simsun">取消添加</a>
                                                <a href="javascript:void(0)" class="blueBtn Js_add_dress_ok" style="font-family:simsun">添加地址</a>
                                        </div>
                                </div>
                        </div>
						</form>
                </div>
        </div>
</div>
<!--地址管理 结束-->

<div class="detailPageMid">

<!--身份验证 开始-->
<div class="proContextDiv">
        <div class="u_listItem list_no_w Js_u_listitem" style="display:none;">
                        <a href="#" class="leftArea" style="width:66px; height:66px; border-radius:33px; margin-right:0px; margin-top:12px;">
                                <img src="./assets/temp/5.png" alt="" style="width:60px; height:60px; border-radius:30px; margin:3px;">
                        </a>
                        <div class="rightArea no_w" style="padding-left:80px;">
                                <p class="rightName"><a href="#" style="font-size:20px; font-family:'微软雅黑', sans-serif">刘兆宇</a></p>
                                <p class="rightDis hs"><a href="#">csoow@csoow.com</a></p>
                                <p class="" style="display: none; margin-top: 60px; color: #cf6e6e; font-family: simsum, sans-serif; font-size: 14px;">客服正在审核您提交的资料，请耐心等待</p>
<!--                                <p style="font-size:12px; font-family: simsun, sans-serif; color:#5f5f5f; margin-top:10px;">密码安全等级：-->
<!--						        <span style="display:inline-block; width:126px; height:12px; border:1px solid #c7c7c7; position:relative; top:2px;">-->
<!--							        <span style="width:40px; height:8px; background:#c7c7c7; position:absolute; top:1px; left:1px;"></span>-->
<!--							        <span style="width:40px; height:8px; background:#a0a0a0; position:absolute; top:1px; left:42px;"></span>-->
<!--							        <span style="width:40px; height:8px; background:#898989; position:absolute; top:1px; left:83px;"></span>-->
<!--						        </span>-->
<!--                                        <b style="font-size:12px; font-family:simsun; color:#3b86c4; margin-left:8px;">高</b>-->
<!--                                </p>-->

<!--                                <div class="height_20"></div>-->
<!--                                <div class="t_warp clearfix">-->
<!--                                        <label class="lab">真实姓名：</label>-->
<!--                                        <p class="yz" style="font-size:14px; color:#555555">刘烨</p>-->
<!--                                </div>-->
                                <div class="height_20"></div>
<!--                                <div class="t_warp clearfix">-->
<!--                                        <label class="lab" >身份验证：</label>-->
<!--                                        <p class="yz" style="font-size:14px; display:none;">已通过验证</p>-->
<!--                                        <p class="yz" style="font-size:14px; display: none; color:#cf6e6e; font-weight:bold;">未通过认证</p>-->
<!--                                </div>-->
                            <div style="display: none">
                                <div class="t_warp clearfix">
                                    <label class="lab" style="width:90px;">真实姓名：</label>
                                    <div class="tools text left" style="background:#efefeb; margin-left: 10px">
                                        <div class="inputWarp empty">
                                        <input type="text" style="background:#efefeb" class="Js_idvname">
                                        </div>
                                    </div>
                                <div class="errorMsg left" style="display:none">错误提示内容</div>
                                </div>
                                <div class="height_20"></div>
                                <div class="t_warp clearfix">
                                    <label class="lab" style="width:90px;">身份证号码：</label>
                                    <div class="tools text left" style="background:#efefeb; margin-left: 10px">
                                        <div class="inputWarp empty">
                                        <input type="text" style="background:#efefeb;" maxlength="18" class="Js_input_text Js_idvnum">
                                        </div>
                                    </div>
                                <div class="errorMsg left" style="display:none">错误提示内容</div>
                                </div>
                                <div class="height_20"></div>
                                <div class="t_warp clearfix">
                                    <label class="lab" style="width:90px;">手机号：</label>
                                    <div class="tools text left" style="background:#efefeb; margin-left: 10px">
                                        <div class="inputWarp empty">
                                        <input type="text" style="background:#efefeb;" maxlength="11" class="Js_input_text Js_idvphone">
                                        </div>
                                    </div>
                                <div class="errorMsg left" style="display:none">错误提示内容</div>
                                </div>
                                <div class="height_20"></div>
                                <div class="t_warp clearfix">
                                    <label class="lab" style="width:90px;">邮箱：</label>
                                    <div class="tools text left" style="background:#efefeb; margin-left: 10px">
                                        <div class="inputWarp empty">
                                        <input type="text" style="background:#efefeb" class="Js_idvemail">
                                        </div>
                                    </div>
                                <div class="errorMsg left" style="display:none">错误提示内容</div>
                                </div>
                                <div class="rowblock" style="margin-left: 100px; margin-top: 10px">
                                    <div class="pt">上传身份证正反面图片</div>
                                    <div class="uploadWrap-single dargArea left">
                                        <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
                                        <input class="file_uploadbox-single" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">
                                    </div>
                                    <div class="errorMsg left" style="display:none;">错误提示内容</div>
                                    <div class="height_10"></div>
                                    <div id="upload-single" class="clearfix Js_fm_cover_div upload-single"></div>
                                </div>
                                <div class="height_30"></div>
                                <div class="t_warp clearfix">
                                    <label class="lab" style="width:100px;"></label>
                                <a href="javascript:void(0)" class="blueBtn Js_idverif_ok">提交</a>
                                </div>
                            </div>
                            <div style="display: block">
                                <p style="margin-top: 140px; margin-bottom: 100px; padding-right: 60px; font-family: simhei, sans-serif; font-size: 26px; font-weight: bold; text-align: center;">您已成功提交身份验证，请耐心等待工作人员回复</p>
                            </div>
                        </div>
                </div>
</div>
<!--身份验证 结束-->

<div class="detailPageMid">
<!--银行卡管理 开始-->
<div class="proContextDiv">
                        <div class="u_listItem list_no_w Js_u_listitem" style="display:none;">
                                <a href="#" class="leftArea" style="width:66px; height:66px; border-radius:33px; margin-right:0px; margin-top:12px;">
                                        <img src="./assets/temp/5.png" alt="" style="width:60px; height:60px; border-radius:30px; margin:3px;">
                                </a>
                                <div class="rightArea no_w" style="padding-left:80px;">
                                        <p class="rightName"><a href="#" style="font-size:20px; font-family:'微软雅黑'">刘兆宇</a></p>
                                        <p class="rightDis hs"><a href="#">csoow@csoow.com</a></p>
<!--                                        <p style="font-size:12px; font-family:simsun; color:#5f5f5f; margin-top:10px;">密码安全等级：-->
<!--						                <span style="display:inline-block; width:126px; height:12px; border:1px solid #c7c7c7; position:relative; top:2px;">-->
<!--							                <span style="width:40px; height:8px; background:#c7c7c7; position:absolute; top:1px; left:1px;"></span>-->
<!--							                <span style="width:40px; height:8px; background:#a0a0a0; position:absolute; top:1px; left:42px;"></span>-->
<!--							                <span style="width:40px; height:8px; background:#898989; position:absolute; top:1px; left:83px;"></span>-->
<!--						                </span>-->
<!--                                                <b style="font-size:12px; font-family:simsun; color:#3b86c4; margin-left:8px;">高</b>-->
<!--                                        </p>-->
                                        <div class="height_30"></div>
                                        <div style="width:497px; height:99px; border:2px dashed #cccccc" class="Js_add_bank_btn">
                                                <div class="clearfix" style="margin-top:29px; margin-left:21px;">
                                                        <a href="javascript:;" class="new-btn Js_add_newbank" style="float:left; margin-right:10px;">添加银行卡</a>
                                                        <span style="float:left; font-size:14px; color:#888888; line-height:20px;">您还没有添加您的银行卡<br />为了更好的服务您，请添加您的银行卡。</span>
                                                </div>
                                        </div>
                                        <div class="height_10"></div>
                                        <div>
                                                <div class="t_warp clearfix Js_add_banklist" style="display:none;">
                                                        <div class="height_20"></div>
                                                        <a href="javascript:;" class="c-bank-btn left">添加新的银行卡</a>
                                                </div>
                                        </div>
                                        <!--<div class="t_warp clearfix">
                                            <label class="lab lh0 w112">银行卡号：</label>
                                            <ul class="bank">
                                                <li class="clearfix">
                                                    <div class="bankItemDiv">招商银行</div>
                                                    <div class="c-num">尾号：1799</div>
                                                    <div class="edit nolh">
                                                        <a href="#" class="a-default">默认地址</a>
                                                        <a href="#" class="b-edit">修改</a>
                                                        <a href="#" class="b-del">删除</a>
                                                    </div>
                                                </li>
                                                <li class="clearfix">
                                                    <div class="bankItemDiv">招商银行</div>
                                                    <div class="c-num">尾号：1799</div>
                                                    <div class="edit nolh">
                                                        <a href="#" class="a-edit">设为默认</a>
                                                        <a href="#" class="b-edit">修改</a>
                                                        <a href="#" class="b-del">删除</a>
                                                    </div>
                                                </li>
                                                <li class="clearfix last">
                                                    <div class="bankItemDiv">招商银行</div>
                                                    <div class="c-num">尾号：1799</div>
                                                    <div class="edit nolh">
                                                        <a href="#" class="a-edit">设为默认</a>
                                                        <a href="#" class="b-edit">修改</a>
                                                        <a href="#" class="b-del">删除</a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>-->
                                        <div class="height_15"></div>
										<form name="card_manager">
                                        	<div class="t_warp clearfix Js_addbank_form" style="display:none;">
                                                <div class="left">
                                                        <div class="t_warp clearfix">
                                                                <label class="lab w183">银行账户类型：</label>
                                                                <span class="yz" style="font-size:14px;">银行卡</span>
                                                        </div>
                                                        <div class="height_20"></div>
                                                        <div class="t_warp clearfix">
                                                                <label class="lab w183">开户人真实姓名：</label>
                                                                <span class="yz" style="font-size:14px;">刘烨</span>
                                                        </div>
                                                        <div class="height_25"></div>
                                                        <div class="t_warp clearfix">
                                                                <label class="lab w183">开户银行所在城市：</label>
                                                                <div class="clearfix left">
                                                                        <div class="optDiv Js_state left mr10" style="width:120px;" id="Js_s1">
                                                                                <div class="tools text pr" style="width:120px; background:#efefeb">
                                                                                        <div class="inputWarp opt">
                                                                                                <span></span>
                                                                                                <a href="javascript:void(0)" class="showMenu"></a>
                                                                                        </div>
                                                                                </div>
                                                                                <ul></ul>
                                                                                <select id="s1" style="display:none;">
                                                                                        <option>数据加载中...</option>
                                                                                </select>
                                                                        </div>
                                                                        <div class="optDiv Js_state left mr10" style="width:130px;" id="Js_s2">
                                                                                <div class="tools text pr" style="width:130px; background:#efefeb">
                                                                                        <div class="inputWarp opt">
                                                                                                <span></span>
                                                                                                <a href="javascript:void(0)" class="showMenu"></a>
                                                                                        </div>
                                                                                </div>
                                                                                <ul></ul>
                                                                                <select id="s2" style="display:none;">
                                                                                        <option>数据加载中...</option>
                                                                                </select>
                                                                        </div>
                                                                        <div class="optDiv Js_state left mr10" style="width:150px;" id="Js_s3">
                                                                                <div class="tools text pr" style="width:150px; background:#efefeb">
                                                                                        <div class="inputWarp opt">
                                                                                                <span></span>
                                                                                                <a href="javascript:void(0)" class="showMenu"></a>
                                                                                        </div>
                                                                                </div>
                                                                                <ul></ul>
                                                                                <select id="s3" style="display:none;">
                                                                                        <option>数据加载中...</option>
                                                                                </select>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="height_22"></div>
                                                        <div class="t_warp clearfix">
                                                                <label class="lab w183">选择银行：</label>
                                                                <div class="optDiv Js_state left mr10" style="width:250px;">
                                                                        <div class="tools text pr" style="width:250px; background:#efefeb">
                                                                                <div class="inputWarp opt">
                                                                                        <span class="Js_bankname"></span>
                                                                                        <a href="javascript:void(0)" class="showMenu"></a>
                                                                                </div>
                                                                        </div>
                                                                        <ul id="Js_t1"><li><a href="javascript:;">中国建设银行</a></li></ul>
                                                                        <div class="errorMsg left" style="display:none">错误提示内容</div>
                                                                </div>
                                                        </div>
                                                        <div class="height_22"></div>
                                                        <div class="t_warp clearfix">
                                                                <label class="lab w183">银行卡号：</label>
                                                                <div class="tools text left" style="width:382px; background:#efefeb">
                                                                        <div class="inputWarp empty">
                                                                                <input type="text" style="background:#efefeb; font-family:simsun;" maxlength="16" class="Js_add_ngbanknum Js_input_text">
                                                                        </div>
                                                                </div>
                                                                <div class="errorMsg left" style="display:none">错误提示内容</div>
                                                        </div>
                                                        <div class="t_warp clearfix">
                                                                <label class="lab w183"></label>
                                                                <span class="w-td">银行卡的户名必须为<b>“李原早”</b>否则会提现失败</span>
                                                        </div>
                                                        <div class="height_20"></div>
                                                        <div class="t_warp clearfix">
                                                                <label class="lab w183"></label>
                                                                <a href="javascript:void(0)" class="grayBtn Js_add_bank_cancle" style="margin-right:20px; font-family:simsun">取消添加</a>
                                                                <a href="javascript:void(0)" class="blueBtn Js_add_bank_ok" style="font-family:simsun">确认添加</a>
                                                        </div>
                                                        <div class="height_20"></div>
                                                </div>
                                        </div>
										</form>
                                </div>
                        </div>
                </div>
<!--银行卡管理 结束-->

        </div>
</div>
</div>
</div>
</div>
</div>