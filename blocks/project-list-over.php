<div class="content">
<div class="w940">
        <div class="large-12">
                <div class="rank-list">
                        <h2>创意投资项目比赛排行榜</h2>
                        <div class="listDiv">
                                <ul>
                                        <li class="tit clearfix">
                                                <span class="left ml40">比赛名称</span>
                                                <span class="right mr20">创意指数</span>
                                        </li>
                                        <li class="clearfix">
                                                <a href="project-going.php">
                                                        <s class="topThree">1</s>
							<span class="namearea">
								<img src="./assets/temp/m1.png" />
								<span class="name">侏罗纪的召唤-蚀刻暴龙拼装模型</span>
							</span>
                                                        <span class="createnum">1,000 点</span>
                                                </a>
                                        </li>
                                        <li class="clearfix">
                                                <a href="project-going.php">
                                                        <s class="topThree">2</s>
							<span class="namearea">
								<img src="./assets/temp/m2.png" />
								<span class="name">原创作品：穿着婚纱去旅行Ⅱ</span>
							</span>
                                                        <span class="createnum">900 点</span>
                                                </a>
                                        </li>
                                        <li class="clearfix">
                                                <a href="project-going.php">
                                                        <s class="topThree">3</s>
							<span class="namearea">
								<img src="./assets/temp/m3.png" />
								<span class="name">理想是未来，更是生活的当下-独立杂志《晚安书》</span>
							</span>
                                                        <span class="createnum">800 点</span>
                                                </a>
                                        </li>
                                        <li class="clearfix">
                                                <a href="project-going.php">
                                                        <s>4</s>
							<span class="namearea">
								<img src="./assets/temp/m4.png" />
								<span class="name">竹木结合，温润拾香——"L-ONE "手工台灯</span>
							</span>
                                                        <span class="createnum">700 点</span>
                                                </a>
                                        </li>
                                        <li class="clearfix">
                                                <a href="project-going.php">
                                                        <s>5</s>
							<span class="namearea">
								<img src="./assets/temp/m5.png" />
								<span class="name">末那之梦中景系列第7弹【Norlan】独角兽雪枭仓鸮手办预订</span>
							</span>
                                                        <span class="createnum">600 点</span>
                                                </a>
                                        </li>
                                </ul>
                        </div>
                </div>
        </div>

</div>

<div class="match-list l-list-wrap clearfix">

<div  class="js_container row ml-3 Js_page_list_item">

<div class="large-4 small-6 columns left">
        <a href="project-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox mark"></div>
                                <s class="mark"></s>
                                <div class="bottomBox"></div>
                                <img src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰111思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">已经确认投资金额</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">用户<i class="Js_visitCard">斯威夫特·泰勒</i>投资了本项目</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns left">
        <a href="project-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox mark"></div>
                                <s class="mark"></s>
                                <div class="bottomBox"></div>
                                <img src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰111思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">已经确认投资金额</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">用户<i class="Js_visitCard">斯威夫特·泰勒</i>投资了本项目</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns left">
        <a href="project-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox mark"></div>
                                <s class="mark"></s>
                                <div class="bottomBox"></div>
                                <img src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰111思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">已经确认投资金额</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">用户<i class="Js_visitCard">斯威夫特·泰勒</i>投资了本项目</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns left">
        <a href="project-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox mark"></div>
                                <s class="mark"></s>
                                <div class="bottomBox"></div>
                                <img src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰111思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">已经确认投资金额</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">用户<i class="Js_visitCard">斯威夫特·泰勒</i>投资了本项目</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns">
        <a href="project-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox mark"></div>
                                <s class="mark"></s>
                                <div class="bottomBox"></div>
                                <img class="thumb" src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">已经确认投资金额</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">用户<i class="Js_visitCard">斯威夫特·泰勒</i>确认投资了本项目</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns">
        <a href="project-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox mark"></div>
                                <s class="mark"></s>
                                <div class="bottomBox"></div>
                                <img src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">已经确认投资金额</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">用户<i class="Js_visitCard">斯威夫特·泰勒</i>确认投资了本项目</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns left">
        <a href="project-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox mark"></div>
                                <s class="mark"></s>
                                <div class="bottomBox"></div>
                                <img src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰111思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">已经确认投资金额</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">用户<i class="Js_visitCard">斯威夫特·泰勒</i>投资了本项目</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns">
        <a href="project-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox mark"></div>
                                <s class="mark"></s>
                                <div class="bottomBox"></div>
                                <img class="thumb" src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">确认投资金额</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">用户<i class="Js_visitCard">斯威夫特·泰勒</i>确认投资了本项目</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns left">
        <a href="project-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox mark"></div>
                                <s class="mark"></s>
                                <div class="bottomBox"></div>
                                <img src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰111思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">已经确认投资金额</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">用户<i class="Js_visitCard">斯威夫特·泰勒</i>投资了本项目</span>
                        </div>
                </div>
        </a>
</div>
<div style="text-align:center; display:none;" class="Js_page_list_btn">
        <div class="pageItemDiv" style="display:inline-block">
                <a href="javascript:;" class="fyBtn prev Js_page_list_prev"></a>
					<span class="pageItem">
						<a href="javascript:;" class="current">1</a>
						<a href="javascript:;">2</a>
						<a href="javascript:;">3</a>
						<a href="javascript:;">4</a>
						<a href="javascript:;">5</a>
					</span>
                <a href="javascript:;" class="fyBtn next Js_page_list_next"></a>
        </div>
</div>
</div>
</div>
</div>