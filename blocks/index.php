<div class="content">
<div class="height_25"></div>
<div class="posts-wrap w940 pr">
<!--幻灯片-->
<div class="detailPageMid mb10">
        <div class="clearfix pr">
                <div class="sy-div left pr">
                        <div id="Js_img_show1" style="overflow:hidden; width:940px;">
                                <div style="width:4700px;" class="clearfix">
                                        <a href="project-list.php" class="left"><img src="./assets/temp/tnt1.jpg"  class="pr"></a>
                                        <a href="task-list.php" class="left"><img src="./assets/temp/tnt2.jpg" /></a>
                                        <a href="hire-list.php" class="left"><img src="./assets/temp/tnt3.jpg" /></a>
                                        <a href="area.php" class="left"><img src="./assets/temp/tnt4.jpg" /></a>
                                        <a href="thinktank.php" class="left"><img src="./assets/temp/tnt5.jpg" /></a>
                                </div>
                        </div>
                        <p class="pageBtn">
                                <a href="javascript:;" class="current Js_imgShow_item1"></a>
                                <a href="javascript:;" class="Js_imgShow_item1"></a>
                                <a href="javascript:;" class="Js_imgShow_item1"></a>
                                <a href="javascript:;" class="Js_imgShow_item1"></a>
                                <a href="javascript:;" class="Js_imgShow_item1"></a>
                        </p>
                </div>

                <div class="slide-mask"></div>
                <div class="left yd-box">
                        <p class="tit">创意世界</p>
                        <p class="con">有无数的方法帮助您实现需求</p>
                        <p class="con">马上使用创创召</p>
                        <a href="#" class="yd-btn Js_ydBtn"></a>
                </div>
        </div>
</div>
<!--新闻-->
<div class="detailPageMid mb10">
<div class="clearfix l-wrap Js_scroll">
<div class="left l-news">
<div class="title pr">
        热门资讯
        <ul id="Js_lNews_tab" class="clearfix">
                <li><a class="ok" href="javascript:;">热门</a> </li>
                <li><a href="javascript:;">创意</a> </li>
                <li><a href="javascript:;">政治</a> </li>
                <li><a href="javascript:;">经济</a> </li>
                <li><a href="javascript:;">体育</a> </li>
                <li><a href="javascript:;">军事</a> </li>
                <li><a href="javascript:;">科技</a> </li>
        </ul>
</div>
<!--资讯1热门-->
<div class="news-box clearfix mt20 Js_news_box">
        <div class="f-photo left pr">
                <img src="./assets/temp/lnews-1.jpg">
                <div class="photo-tit">
                        <a href="news.php">班禅额尔德尼举行火供仪式</a>
                </div>
        </div>
        <div class="left ml20">
                <div class="list-n">
                        <img  src="./assets/temp/lnews-2.jpg">
                        <div class="tit-box NewsBox">
                                <a href="news.php">2001年在上海，中国曾成功举办了APEC第九次领导人非正式会员。</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-4.jpg">
                        <div class="tit-box">
                                <a  href="news.php">北京大兴摔童案主犯韩磊确定上诉，韩磊称想进一步澄清案情</a>
                        </div>
                </div>
                <div class="list-n">
                        <img src="./assets/temp/lnews-3.jpg">
                        <div class="tit-box">
                                <a  href="news.php">在南京河西江东北路上，也有一座造型奇特的大楼..</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-5.jpg">
                        <div class="tit-box">
                                <a  href="news.php">俄罗斯总统普京表示：“日俄之间在经济等各领域的合作正在..</a>
                        </div>
                </div>
        </div>
</div>
<!--资讯2创意-->
<div class="news-box clearfix mt20 Js_news_box" style="display: none;">
        <div class="f-photo left pr">
                <img src="./assets/temp/lnews-31.jpg">
                <div class="photo-tit">
                        <a href="news.php">2013年度中国工业设计十佳获奖名单公示</a>
                </div>
        </div>
        <div class="left ml20">
                <div class="list-n">
                        <img  src="./assets/temp/lnews-32.jpg">
                        <div class="tit-box">
                                <a href="news.php">为期五天的广州琶洲国际会展中心举行的“第二届亚洲动漫..</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-33.jpg">
                        <div class="tit-box">
                                <a  href="news.php">一诺百年：2012诺文学奖得住莫言领奖回顾</a>
                        </div>
                </div>
                <div class="list-n">
                        <img src="./assets/temp/lnews-34.jpg">
                        <div class="tit-box">
                                <a  href="news.php">2013年诺奖系列之四：东欧作家只有符合西方眼光才能得奖</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-35.jpg">
                        <div class="tit-box">
                                <a  href="news.php">美国军方确认存在钢铁侠武器套装</a>
                        </div>
                </div>
        </div>
</div>
<!--资讯3政治-->
<div class="news-box clearfix mt20 Js_news_box" style="display: none;">
        <div class="f-photo left pr">
                <img src="./assets/temp/lnews-41.jpg">
                <div class="photo-tit">
                        <a href="news.php">李克强出席第16次中国-东盟领导人会议</a>
                </div>
        </div>
        <div class="left ml20">
                <div class="list-n">
                        <img  src="./assets/temp/lnews-42.jpg">
                        <div class="tit-box">
                                <a href="news.php">遏制“房腐”须完善官员财产申报制度</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-43.jpg">
                        <div class="tit-box">
                                <a  href="news.php">洋地名频现 该不该叫停</a>
                        </div>
                </div>
                <div class="list-n">
                        <img src="./assets/temp/lnews-44.jpg">
                        <div class="tit-box">
                                <a  href="news.php">甘肃纪念习仲勋诞辰100周年</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-45.jpg">
                        <div class="tit-box">
                                <a  href="news.php">养老金改革顶层方案出炉</a>
                        </div>
                </div>
        </div>
</div>
<!--资讯4经济-->
<div class="news-box clearfix mt20 Js_news_box" style="display: none;">
        <div class="f-photo left pr">
                <img src="./assets/temp/lnews-21.jpg">
                <div class="photo-tit">
                        <a href="news.php">盘点那些只赚不赔的收藏佳品</a>
                </div>
        </div>
        <div class="left ml20">
                <div class="list-n">
                        <img  src="./assets/temp/lnews-22.jpg">
                        <div class="tit-box">
                                <a href="news.php">耶伦获奥巴马提名 或成史上第一位女性美联储主席</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-23.jpg">
                        <div class="tit-box">
                                <a  href="news.php">山西煤企掀煤化工投资潮 前景难测</a>
                        </div>
                </div>
                <div class="list-n">
                        <img src="./assets/temp/lnews-24.jpg">
                        <div class="tit-box">
                                <a  href="news.php">开发商缺地不缺钱 土地市场持续高烧难退</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-25.jpg">
                        <div class="tit-box">
                                <a  href="news.php">2013胡润投资富豪榜发布 刘永好问鼎投资大王</a>
                        </div>
                </div>
        </div>
</div>
<!--资讯5体育-->
<div class="news-box clearfix mt20 Js_news_box" style="display: none;">
        <div class="f-photo left pr">
                <img src="./assets/temp/lnews-21.jpg">
                <div class="photo-tit">
                        <a href="news.php">盘点那些只赚不赔的收藏佳品</a>
                </div>
        </div>
        <div class="left ml20">
                <div class="list-n">
                        <img  src="./assets/temp/lnews-22.jpg">
                        <div class="tit-box">
                                <a href="news.php">耶伦获奥巴马提名 或成史上第一位女性美联储主席</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-23.jpg">
                        <div class="tit-box">
                                <a  href="news.php">山西煤企掀煤化工投资潮 前景难测</a>
                        </div>
                </div>
                <div class="list-n">
                        <img src="./assets/temp/lnews-24.jpg">
                        <div class="tit-box">
                                <a  href="news.php">开发商缺地不缺钱 土地市场持续高烧难退</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-25.jpg">
                        <div class="tit-box">
                                <a  href="news.php">2013胡润投资富豪榜发布 刘永好问鼎投资大王</a>
                        </div>
                </div>
        </div>
</div>
<!--资讯6军事-->
<div class="news-box clearfix mt20 Js_news_box" style="display: none;">
        <div class="f-photo left pr">
                <img src="./assets/temp/lnews-21.jpg">
                <div class="photo-tit">
                        <a href="news.php">盘点那些只赚不赔的收藏佳品</a>
                </div>
        </div>
        <div class="left ml20">
                <div class="list-n">
                        <img  src="./assets/temp/lnews-22.jpg">
                        <div class="tit-box">
                                <a href="news.php">耶伦获奥巴马提名 或成史上第一位女性美联储主席</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-23.jpg">
                        <div class="tit-box">
                                <a  href="news.php">山西煤企掀煤化工投资潮 前景难测</a>
                        </div>
                </div>
                <div class="list-n">
                        <img src="./assets/temp/lnews-24.jpg">
                        <div class="tit-box">
                                <a  href="news.php">开发商缺地不缺钱 土地市场持续高烧难退</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-25.jpg">
                        <div class="tit-box">
                                <a  href="news.php">2013胡润投资富豪榜发布 刘永好问鼎投资大王</a>
                        </div>
                </div>
        </div>
</div>
<!--资讯7科技-->
<div class="news-box clearfix mt20 Js_news_box" style="display: none;">
        <div class="f-photo left pr">
                <img src="./assets/temp/lnews-21.jpg">
                <div class="photo-tit">
                        <a href="news.php">盘点那些只赚不赔的收藏佳品</a>
                </div>
        </div>
        <div class="left ml20">
                <div class="list-n">
                        <img  src="./assets/temp/lnews-22.jpg">
                        <div class="tit-box">
                                <a href="news.php">耶伦获奥巴马提名 或成史上第一位女性美联储主席</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-23.jpg">
                        <div class="tit-box">
                                <a  href="news.php">山西煤企掀煤化工投资潮 前景难测</a>
                        </div>
                </div>
                <div class="list-n">
                        <img src="./assets/temp/lnews-24.jpg">
                        <div class="tit-box">
                                <a  href="news.php">开发商缺地不缺钱 土地市场持续高烧难退</a>
                        </div>
                </div>
                <div class="list-n">
                        <img  src="./assets/temp/lnews-25.jpg">
                        <div class="tit-box">
                                <a  href="news.php">2013胡润投资富豪榜发布 刘永好问鼎投资大王</a>
                        </div>
                </div>
        </div>
</div>
</div>
<div class="left l-app">
        <div class="title pr">应用中心
                <ul class="clearfix Js_app_list">
                        <li><a class="ok" href="#">热门</a> </li>
                        <li><a href="#">购物</a> </li>
                        <li><a href="#">资讯</a> </li>
                        <li><a href="#">工具</a> </li>
                </ul>
        </div>
        <!--app 标签1-->
        <div class="app-box Js_app_item" style="display: block;">
                <div class="app">
                        <a target="_blank" href="http://www.163.com"><img src="./assets/temp/163.png">
                                网易
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.weibo.com"><img src="./assets/temp/xlwb_.png">
                                新浪微博
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.taobao.com"><img src="./assets/temp/tb_.png">
                                淘宝
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.jd.com"><img src="./assets/temp/jd1.png">
                                京东
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.qunaer.com"><img src="./assets/temp/lapp-5.png">
                                去哪里
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.xinhuanet.com"><img src="./assets/temp/lapp-6.png">
                                新华网
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.tianya.com"><img src="./assets/temp/lapp-2.png">
                                天涯
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.jiayuan.com"><img src="./assets/temp/lapp-3.png">
                                世纪佳缘
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.nba.com"><img src="./assets/temp/lapp-4.png">
                                NBA
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.ifeng.com"><img src="./assets/temp/ffw.png">
                                凤凰网
                        </a>
                </div>
                <div class="app">
                        <a href="http://www.baidu.com"><img src="./assets/temp/lapp-7.png">
                                百度
                        </a>
                </div>
                <div class="app">
                        <a href="http://www.cmbchina.com"><img src="./assets/temp/lapp-1.png">
                                招商银行
                        </a>
                </div>
        </div>
        <!--app 标签2 购物-->
        <div class="app-box Js_app_item" style="display: none;">
                <div class="app">
                        <a target="_blank" href="http://www.taobao.com"><img src="./assets/temp/tb_.png">
                                淘宝
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.jd.com"><img src="./assets/temp/jd1.png">
                                京东
                        </a>
                </div>
        </div>
        <!--app 标签3 资讯-->
        <div class="app-box Js_app_item" style="display: none;">
                <div class="app">
                        <a target="_blank" href="http://www.ifeng.com"><img src="./assets/temp/ffw.png">
                                凤凰网
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.xinhuanet.com"><img src="./assets/temp/lapp-6.png">
                                新华网
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.58.com"><img src="./assets/temp/58.png">
                                58
                        </a>
                </div>
        </div>
        <!--app 标签4 工具-->
        <div class="app-box Js_app_item" style="display: none;">
                <div class="app">
                        <a target="_blank" href="http://www.papa.com"><img src="./assets/temp/pp.png">
                                啪啪
                        </a>
                </div>
                <div class="app">
                        <a target="_blank" href="http://www.weibo.com"><img src="./assets/temp/xlwb_.png">
                                新浪微博
                        </a>
                </div>
        </div>
</div>
</div>
</div>
<!--智库-->
<div class="detailPageMid mb10">
        <div class="clearfix Js_scroll" >
                <div class="l-img3 left">
                        <a href="thinktank.php"><img src="./assets/temp/index-03.jpg"></a>
                </div>
                <div class="l-text3 left">
                        <div class="title">
                                创意世界智库
                        </div>
                        <p>世界级行业创意领袖的智慧宝库全球十万名创意领袖的创新平台与全世界最有思想的人在一起为民造福时间</p>
                        <a href="thinktank.php">
                                现在就申请加入/推荐成为创意世界智库候选人
                        </a>
                </div>
        </div>
</div>
<!--地区-->
<div class="detailPageMid mb10">
        <div class="yd-area clearfix Js_scroll">
                <div class="btn-area">
                        <div class="clearfix mb15">
                                <label>城市</label>
                                <div class="city-area clearfix">
                                        <div class="city-div Js_state ">
                                                <div class="city-txt">
                                                        <span>北京</span>
                                                        <a href="javascript:;"></a>
                                                </div>
                                                <ul id="Js_m1"></ul>
                                                <select id="m1" style="display:none;">
                                                        <option>数据加载中...</option>
                                                </select>
                                        </div>
                                        <div class="city-div Js_state ">
                                                <div class="city-txt">
                                                        <span>北京</span>
                                                        <a href="javascript:;"></a>
                                                </div>
                                                <ul id="Js_m2"></ul>
                                                <select id="m2" style="display:none;">
                                                        <option>数据加载中...</option>
                                                </select>
                                        </div>
                                        <div class="city-div Js_state">
                                                <div class="city-txt">
                                                        <span>北京</span>
                                                        <a href="javascript:;"></a>
                                                </div>
                                                <ul id="Js_m3"></ul>
                                                <select id="m3" style="display:none;">
                                                        <option>数据加载中...</option>
                                                </select>
                                        </div>
                                </div>
                                <!--<div class="optDiv Js_state left mr10" style="width:150px;">
                                  <div class="tools text pr" style="width:150px;">
                                    <div class="inputWarp opt">
                                      <span></span>
                                      <a href="javascript:void(0)" class="showMenu"></a>
                                    </div>
                                  </div>
                                  <ul id="Js_t1"></ul>
                                  <select id="t1" style="display:none;">
                                    <option>数据加载中...</option>
                                  </select>
                                </div>-->
                        </div>
                        <div class="clearfix mb15">
                                <label>学校</label>
                                <div class="btn-div">
                                        <input type="text" class="Js_db_area_input" disabled="disabled" placeholder="即将开放，敬请期待..." />
                                        <ul class="Js_db_area"></ul>
                                </div>
                        </div>
                        <div class="clearfix mb15">
                                <label>企业</label>
                                <div class="btn-div">
                                        <input type="text" class="Js_db_area_input" disabled="disabled" placeholder="即将开放，敬请期待..." />
                                        <ul class="Js_db_area"></ul>
                                </div>
                        </div>
                </div>
                <div class="createIndex-list">
                        <ul class="list-ul clearfix">
                                <li class="current Js_yd_createIndex_list">城市</li>
                                <li class="Js_yd_createIndex_list xx">学校</li>
                                <li class="Js_yd_createIndex_list qy">企业</li>
                        </ul>
                        <div class="Js_createIndex_item">
                                <div class="list-tit clearfix">
                                        <span class="pm">排名</span>
                                        <span class="mc">城市名称</span>
                                        <span class="zs">创意指数</span>
                                </div>
                                <ul class="yd-pm-list Js_city_rank">
                                        <li class="clearfix">
                                                <span class="y-pm bqh">1</span>
                                                <span class="y-dq">北京</span>
                                                <span class="y-zs">1000点</span>
                                        </li>
                                        <li class="clearfix">
                                                <span class="y-pm bqh">2</span>
                                                <span class="y-dq">上海</span>
                                                <span class="y-zs">1000点</span>
                                        </li>
                                        <li class="clearfix">
                                                <span class="y-pm bqh">3</span>
                                                <span class="y-dq">成都</span>
                                                <span class="y-zs">900点</span>
                                        </li>
                                        <li class="clearfix">
                                                <span class="y-pm btqh">4</span>
                                                <span class="y-dq">天津</span>
                                                <span class="y-zs">800点</span>
                                        </li>
                                        <li class="clearfix">
                                                <span class="y-pm btqh">5</span>
                                                <span class="y-dq">广西</span>
                                                <span class="y-zs">600点</span>
                                        </li>
                                </ul>
                                <div class="gxnumtxt">您总共对<b>成都市</b>贡献了</div>
                                <p class="gxnum"><b>40</b><span>&nbsp;&nbsp;创意指数</span></p>
                                <p class="gxnum-today">今日贡献为<em>0</em></p>
                        </div>
                        <div class="Js_createIndex_item" style="display:none;">
                                <div class="list-tit clearfix">
                                        <span class="pm">排名</span>
                                        <span class="mc">学校名称</span>
                                        <span class="zs">创意指数</span>
                                </div>
                                <ul class="yd-pm-list">
                                        <li class="clearfix">
                                                <span class="y-pm bqh">1</span>
                                                <span class="y-dq">四川大学</span>
                                                <span class="y-zs">1000点</span>
                                        </li>
                                        <li class="clearfix">
                                                <span class="y-pm bqh">2</span>
                                                <span class="y-dq">四川师范大学</span>
                                                <span class="y-zs">870点</span>
                                        </li>
                                        <li class="clearfix">
                                                <span class="y-pm bqh">3</span>
                                                <span class="y-dq">西南财大</span>
                                                <span class="y-zs">640点</span>
                                        </li>
                                        <li class="clearfix">
                                                <span class="y-pm btqh">4</span>
                                                <span class="y-dq">西南交大</span>
                                                <span class="y-zs">640点</span>
                                        </li>
                                        <li class="clearfix">
                                                <span class="y-pm btqh">5</span>
                                                <span class="y-dq">电子科大</span>
                                                <span class="y-zs">640点</span>
                                        </li>
                                </ul>
                                <div class="gxnumtxt">您总共对<b>四川大学</b>贡献了</div>
                                <p class="gxnum"><b>40</b>&nbsp;&nbsp;<span>创意指数</span></p>
                                <p class="gxnum-today">今日贡献为<em>0</em></p>
                        </div>
                        <div class="Js_createIndex_item" style="display:none;">
                                <div class="list-tit clearfix">
                                        <span class="pm">排名</span>
                                        <span class="mc">企业名称</span>
                                        <span class="zs">创意指数</span>
                                </div>
                                <ul class="yd-pm-list">
                                        <li class="clearfix">
                                                <span class="y-pm bqh">1</span>
                                                <span class="y-dq">苹果</span>
                                                <span class="y-zs">1000点</span>
                                        </li>
                                        <li class="clearfix">
                                                <span class="y-pm bqh">2</span>
                                                <span class="y-dq">戴尔</span>
                                                <span class="y-zs">870点</span>
                                        </li>
                                        <li class="clearfix">
                                                <span class="y-pm bqh">3</span>
                                                <span class="y-dq">索尼</span>
                                                <span class="y-zs">640点</span>
                                        </li>
                                        <li class="clearfix">
                                                <span class="y-pm btqh">4</span>
                                                <span class="y-dq">三星</span>
                                                <span class="y-zs">300点</span>
                                        </li>
                                        <li class="clearfix">
                                                <span class="y-pm btqh">5</span>
                                                <span class="y-dq">佳能</span>
                                                <span class="y-zs">300点</span>
                                        </li>
                                </ul>
                                <div class="gxnumtxt">您总共对<b>三星</b>贡献了</div>
                                <p class="gxnum"><b>40</b>&nbsp;&nbsp;<span>创意指数</span></p>
                                <p class="gxnum-today">今日贡献为<em>0</em></p>
                        </div>
                        <div class="enter"><a class="blueBtn" href="area.php">进入创意世界地区</a></div>
                </div>
        </div>
</div>
<!--排行榜-->
<div class="detailPageMid mb10">
<div class="l-friend-top">
<div class="title">
        朋友圈创意排行榜
</div>
<div style="padding-left: 30px;">
<table>
<thead>
<tr>
        <td colspan="3" width="170">排名</td>
        <td width="118">创意等级</td>
        <td width="118">创意指数</td>
        <td width="118">发布创意世界大赛</td>
        <td width="118">发布创意交易</td>
        <td width="118">获得创创币</td>
        <td width="100">送出创创币</td>
</tr>
</thead>
<tbody>
<tr>
        <td width="30">
                <a href="../user/home.php">
                        <i class="topThree">1</i>
                </a>
        </td>
        <td width="40">
                <a href="../user/home.php">
                        <img src="./assets/temp/zc1.png">
                </a>
        </td>
        <td width="170">
                <a href="profile-other.php">
                        <span>Angela</span>
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        <em>Lv.1024</em>
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        99,999,000 点
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        123,456 场
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        567,890,000 次
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        567,890,000 枚
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        890,000 枚
                </a>
        </td>
</tr>
<tr>
        <td width="30">
                <i class="topThree">2</i>
        </td>
        <td width="40">
                <a href="profile-other.php">
                        <img src="./assets/temp/zc2.png">
                </a>
        </td>
        <td width="170">
                <a href="profile-other.php">
                        <span>姚仙</span>
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        <em>Lv.512</em>
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        9,999,000 点
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        123,456 场
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        567,890,000 次
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        567,890,000 枚
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        890,000 枚
                </a>
        </td>
</tr>
<tr>
        <td width="30">
                <i class="topThree">3</i>
        </td>
        <td width="40">
                <a href="profile-other.php">
                        <img src="./assets/temp/zc3.png">
                </a>
        </td>
        <td width="170">
                <a href="profile-other.php">
                        <span>老郦的名字很短</span>
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        <em>Lv.256</em>
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        800,000 点
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        123,456 场
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        567,890,000 次
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        567,890,000 枚
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        890,000 枚
                </a>
        </td>
</tr>
<tr>
        <td width="30">
                <i>4</i>
        </td>
        <td width="40">
                <a href="profile-other.php">
                        <img src="./assets/temp/zc1.png">
                </a>
        </td>
        <td width="170">
                <a href="profile-other.php">
                        <span>刘找鱼的名字很长很长很长</span>
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        <em>Lv.128</em>
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        800,000 点
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        123,456 场
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        567,890,000 次
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        567,890,000 枚
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        890,000 枚
                </a>
        </td>
</tr>
<tr>
        <td width="30">
                <i>5</i>
        </td>
        <td width="40">
                <a href="profile-other.php">
                        <img src="./assets/temp/zc2.png">
                </a>
        </td>
        <td width="170">
                <a href="profile-other.php">
                        <span>Owen_Liu</span>
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        <em>Lv.64</em>
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        10 点
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        123,456 场
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        567,890,000 次
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        567,890,000 枚
                </a>
        </td>
        <td>
                <a href="../user/home.php">
                        890,000 枚
                </a>
        </td>
</tr>
<!--</tbody>-->
</table>
</div>
</div>
</div>
<!--朋友圈-->
<div class="detailPageMid Js_scroll" >
<div class="clearfix">
        <dl class="yd-areatype-list clearfix Js_friendArea_filter">
                <dt>朋友圈动态</dt>
                <dd class="first"><a href="javascript:;">熟人</a></dd>
                <dd><a href="javascript:;">朋友</a></dd>
                <dd><a href="javascript:;">同学</a></dd>
                <dd><a href="javascript:;">家人</a></dd>
        </dl>
        <a href="javascript:;" class="yd-share-btn Js_share_btn"></a>
</div>
<div class="pr">
<ul class="posts-wraplist Js_wrapList" >

<li class="posts-item" data-type="2">
        <div class="posts-box clearfix">
                <div class="tit-bq bfs">美女</div>
                <div class="post-user-info clearfix">
                        <a href="#"><img class="u-avatar left" src="./assets/temp/9.png" alt=""></a>
                        <p class="u-name left">
                                <a class="name Js_visitCard" href="#">如果我不是郦道元怎么办</a>
                                <span class="time">下午12:53</span>
                        </p>
                </div>
                <div class="post-pic"><img src="./assets/temp/p5.jpg" alt=""></div>
                <div class="post-content">
                        <a>
                                由JueLab发起的项目[成为一位皮革匠人第四期]上线了。
                        </a>
                </div>
                <div class="psot-like clearfix">
                        <a class="like-box zan Js_zan" href="javascript:;">464</a>
                        <a class="like-box zhuan" href="javascript:;">600</a>
                        <input class="Js_noneinput" type="text" placeholder="添加您的评论..." />
                </div>
                <div class="post-comment" style="display:none;">
                        <div class="post-c-con">
                                <div class="p-c-title"><b>0</b> 条评论</div>
                                <ul class="Js_itemlist clearfix">
                                </ul>
                        </div>
                        <div class="post-repeat-wrap">
                                <div class="post-repeat-text">
                                        <input class="Js_hinput" type="text" placeholder="添加您的评论...">
                                </div>
                                <div class="post-repeat-box Js_subtextarea">
                                        <div class="p-c-list ">
                                                <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                <div class="p-c-con">
                                                        <textarea class="sub-text" name="" cols="30" rows="10"></textarea>
                                                        <div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</li>

<li class="posts-item">
        <div class="n-sfdl">
                <h3>寻找您的朋友</h3>
                <div class="l-list"><a href="javascript:;">通过QQ寻找朋友</a></div>
                <div class="l-list n-sina"><a href="javascript:;">通过新浪微博寻找朋友</a></div>
                <div class="l-list n-baidu"><a href="javascript:;">通过人人帐号寻找朋友</a></div>
                <div class="l-list n-douban"><a href="javascript:;">通过豆瓣寻找朋友</a></div>
        </div>
</li>

<li class="posts-item" data-type="0">
        <div class="posts-box clearfix">
                <div class="tit-bq bfs">美女</div>
                <div class="post-user-info clearfix">
                        <a href="#"><img class="u-avatar left" src="./assets/temp/9.png" alt=""></a>
                        <p class="u-name left">
                                <a class="name Js_visitCard" href="#">如果我不是郦道元怎么办</a>
                                <span class="time">下午12:53</span>
                        </p>
                </div>
                <div class="post-content">
                        <a>
                                由JueLab发起的项目[成为一位皮革匠人第四期]上线了。
                        </a>
                </div>
                <div class="psot-like clearfix">
                        <a class="like-box zan Js_zan" href="javascript:;">464</a>
                        <a class="like-box zhuan" href="javascript:;">600</a>
                </div>
                <div class="post-comment">
                        <div class="post-c-con">
                                <div class="p-c-title"><b>4</b> 条评论</div>
                                <ul class="Js_itemlist clearfix">
                                        <li>
                                                <div class="p-c-list">
                                                        <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                        <div class="p-c-con">
                                                                <p class="top"><a class="cname Js_visitCard" href="#">刘兆宇</a><span class="ctime">9月9日&nbsp;&nbsp;2:27PM</span></p>
                                                                <p class="con">这东西不错呀，真心很好的这东西不错呀，真心很好的这东西不错呀，真心很好的</p>
                                                        </div>
                                                </div>
                                        </li>
                                        <li>
                                                <div class="p-c-list">
                                                        <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                        <div class="p-c-con">
                                                                <p class="top"><a class="cname Js_visitCard" href="#">刘兆宇</a><span class="ctime">9月9日&nbsp;&nbsp;2:27PM</span></p>
                                                                <p class="con">这东西不错呀，真心很好的这东西不错呀，真心很好的这东西不错呀，真心很好的</p>
                                                        </div>
                                                </div>
                                        </li>
                                        <li>
                                                <div class="p-c-list">
                                                        <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                        <div class="p-c-con">
                                                                <p class="top"><a class="cname Js_visitCard" href="#">刘兆宇</a><span class="ctime">9月9日&nbsp;&nbsp;2:27PM</span></p>
                                                                <p class="con">这东西不错呀，真心很好的这东西不错呀，真心很好的这东西不错呀，真心很好的</p>
                                                        </div>
                                                </div>
                                        </li>
                                        <li>
                                                <div class="p-c-list">
                                                        <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                        <div class="p-c-con">
                                                                <p class="top"><a class="cname Js_visitCard" href="#">刘兆宇</a><span class="ctime">9月9日&nbsp;&nbsp;2:27PM</span></p>
                                                                <p class="con">这东西不错呀，真心很好的这东西不错呀，真心很好的这东西不错呀，真心很好的</p>
                                                        </div>
                                                </div>
                                        </li>
                                </ul>
                        </div>
                        <div class="post-repeat-wrap">
                                <div class="post-repeat-text">
                                        <input class="Js_hinput" type="text" placeholder="添加您的评论...">
                                </div>
                                <div class="post-repeat-box Js_subtextarea">
                                        <div class="p-c-list ">
                                                <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                <div class="p-c-con">
                                                        <textarea class="sub-text" name="" cols="30" rows="10"></textarea>
                                                        <div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</li>

<li class="posts-item" data-type="1">
        <div class="posts-box clearfix">
                <div class="tit-bq blys">创意</div>
                <div class="post-user-info clearfix">
                        <a href="#"><img class="u-avatar left" src="./assets/temp/9.png" alt=""></a>
                        <p class="u-name left">
                                <a class="name Js_visitCard" href="#">如果我不是郦道元怎么办</a>
                                <span class="time">下午12:53</span>
                        </p>
                </div>
                <div class="post-pic img4 clearfix">
                        <div class="left-img">
                                <a data-fancybox-group="gallery" class="Js_fancyBox" href="./assets/temp/p6.jpg">
                                        <img src="./assets/temp/p6.jpg" alt="">
                                </a>
                        </div>
                        <div class="right-img">
                                <a data-fancybox-group="gallery" class="Js_fancyBox" href="./assets/temp/p7.jpg">
                                        <img src="./assets/temp/p7.jpg" alt="">
                                </a>
                                <a data-fancybox-group="gallery" class="Js_fancyBox" href="./assets/temp/p8.jpg">
                                        <img src="./assets/temp/p8.jpg" alt="">
                                </a>
                                <a data-fancybox-group="gallery" class="Js_fancyBox" href="./assets/temp/p9.jpg">
                                        <img src="./assets/temp/p9.jpg" alt="">
                                </a>

                        </div>

                </div>
                <div class="psot-like clearfix">
                        <a class="like-box zan Js_zan" href="javascript:;">赞</a>
                        <a class="like-box zhuan" href="javascript:;">600</a>
                        <input class="Js_noneinput" type="text" placeholder="添加您的评论..." />
                </div>
                <div class="post-comment" style="display:none;">
                        <div class="post-c-con">
                                <div class="p-c-title"><b>0</b> 条评论</div>
                                <ul class="Js_itemlist clearfix">
                                </ul>
                        </div>
                        <div class="post-repeat-wrap">
                                <div class="post-repeat-text">
                                        <input class="Js_hinput" type="text" placeholder="添加您的评论...">
                                </div>
                                <div class="post-repeat-box Js_subtextarea">
                                        <div class="p-c-list ">
                                                <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                <div class="p-c-con">
                                                        <textarea class="sub-text" name="" cols="30" rows="10"></textarea>
                                                        <div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</li>

<li class="posts-item" data-type="3">
        <div class="posts-box clearfix">
                <div class="tit-bq blys">创意</div>
                <div class="post-user-info clearfix">
                        <a href="#"><img class="u-avatar left" src="./assets/temp/9.png" alt=""></a>
                        <p class="u-name left">
                                <a class="name Js_visitCard" href="#">如果我不是郦道元怎么办</a>
                                <span class="time">下午12:53</span>
                        </p>
                </div>
                <div class="post-pic">
                        <div class="l-video">
                                <a href="javascript:;" class="js_player"></a>
                                <video id="video1" width="465">
                                        <source src="./assets/temp/mov.ogg" type="video/ogg">
                                        <source src="./assets/temp/mov.mp4" type="video/mp4">
                                        Your browser does not support the video tag.
                                </video>
                        </div>

                </div>
                <div class="psot-like clearfix">
                        <a class="like-box zan Js_zan" href="javascript:;">赞</a>
                        <a class="like-box zhuan" href="javascript:;">600</a>
                        <input class="Js_noneinput" type="text" placeholder="添加您的评论..." />
                </div>
                <div class="post-comment" style="display:none;">
                        <div class="post-c-con">
                                <div class="p-c-title"><b>0</b> 条评论</div>
                                <ul class="Js_itemlist clearfix">
                                </ul>
                        </div>
                        <div class="post-repeat-wrap">
                                <div class="post-repeat-text">
                                        <input class="Js_hinput" type="text" placeholder="添加您的评论...">
                                </div>
                                <div class="post-repeat-box Js_subtextarea">
                                        <div class="p-c-list ">
                                                <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                <div class="p-c-con">
                                                        <textarea class="sub-text" name="" cols="30" rows="10"></textarea>
                                                        <div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</li>

<li class="posts-item" data-type="0">
        <div class="posts-box clearfix">
                <div class="tit-bq blys">创意</div>
                <div class="post-user-info clearfix">
                        <a href="#"><img class="u-avatar left" src="./assets/temp/9.png" alt=""></a>
                        <p class="u-name left">
                                <a class="name Js_visitCard" href="#">如果我不是郦道元怎么办</a>
                                <span class="time">下午12:53</span>
                        </p>
                </div>
                <div class="post-pic img2 clearfix">
                        <div class="left-img">
                                <a data-fancybox-group="gallery2" class="Js_fancyBox2" href="./assets/temp/p13.jpg">
                                        <img src="./assets/temp/p13.jpg" alt="">
                                </a>
                        </div>
                        <div class="right-img">
                                <a data-fancybox-group="gallery2" class="Js_fancyBox2" href="./assets/temp/p14.jpg">
                                        <img src="./assets/temp/p14.jpg" alt="">
                                </a>
                        </div>

                </div>
                <div class="psot-like clearfix">
                        <a class="like-box zan Js_zan" href="javascript:;">赞</a>
                        <a class="like-box zhuan" href="javascript:;">600</a>
                        <input class="Js_noneinput" type="text" placeholder="添加您的评论..." />
                </div>
                <div class="post-comment" style="display:none;">
                        <div class="post-c-con">
                                <div class="p-c-title"><b>0</b> 条评论</div>
                                <ul class="Js_itemlist clearfix">
                                </ul>
                        </div>
                        <div class="post-repeat-wrap">
                                <div class="post-repeat-text">
                                        <input class="Js_hinput" type="text" placeholder="添加您的评论...">
                                </div>
                                <div class="post-repeat-box Js_subtextarea">
                                        <div class="p-c-list ">
                                                <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                <div class="p-c-con">
                                                        <textarea class="sub-text" name="" cols="30" rows="10"></textarea>
                                                        <div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</li>

<li class="posts-item" data-type="3">
        <div class="posts-box clearfix">
                <div class="tit-bq blys">创意</div>
                <div class="post-user-info clearfix">
                        <a href="#"><img class="u-avatar left" src="./assets/temp/9.png" alt=""></a>
                        <p class="u-name left">
                                <a class="name Js_visitCard" href="#">如果我不是郦道元怎么办</a>
                                <span class="time">下午12:53</span>
                        </p>
                </div>
                <div class="post-pic img3 clearfix">
                        <div class="left-img">
                                <a data-fancybox-group="gallery1" class="Js_fancyBox1" href="./assets/temp/p10.jpg">
                                        <img src="./assets/temp/p10.jpg" alt="">
                                </a>
                        </div>
                        <div class="right-img">
                                <a data-fancybox-group="gallery1" class="Js_fancyBox1" href="./assets/temp/p11.jpg">
                                        <img src="./assets/temp/p11.jpg" alt="">
                                </a>
                                <a data-fancybox-group="gallery1" class="Js_fancyBox1 last" href="./assets/temp/p11.jpg">
                                        <img src="./assets/temp/p12.jpg" alt="">
                                </a>

                        </div>

                </div>
                <div class="psot-like clearfix">
                        <a class="like-box zan Js_zan" href="javascript:;">赞</a>
                        <a class="like-box zhuan" href="javascript:;">600</a>
                        <input class="Js_noneinput" type="text" placeholder="添加您的评论..." />
                </div>
                <div class="post-comment" style="display:none;">
                        <div class="post-c-con">
                                <div class="p-c-title"><b>0</b> 条评论</div>
                                <ul class="Js_itemlist clearfix">
                                </ul>
                        </div>
                        <div class="post-repeat-wrap">
                                <div class="post-repeat-text">
                                        <input class="Js_hinput" type="text" placeholder="添加您的评论...">
                                </div>
                                <div class="post-repeat-box Js_subtextarea">
                                        <div class="p-c-list ">
                                                <img class="p-c-avatar" src="./assets/temp/7.png" alt="">
                                                <div class="p-c-con">
                                                        <textarea class="sub-text" name="" cols="30" rows="10"></textarea>
                                                        <div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</li>
</ul>
</div>
</div>

</div>
</div>
<script type="text/javascript" src="assets/js/tCity_index.js"></script>
<script type="text/javascript" src="assets/js/city_select_index.js"></script>
<!---->
<script type="text/javascript">
    var c = cityIndexSelect('m1','m2','m3',callbackIndex);
    c.init();
    function callbackIndex(ev){
        var res = {
            content:[
                {id:0,name:"广州",creative_index:100},
                {id:2,name:"广州1",creative_index:100},
                {id:3,name:"广州2",creative_index:100},
                {id:4,name:"广州3",creative_index:100},
                {id:5,name:"广州4",creative_index:100},
                {id:6,name:"广州5",creative_index:100},
                {id:7,name:"广州6",creative_index:100},
                {id:1,name:"广州7",creative_index:100},
                {id:9,name:"广州8",creative_index:100},
                {id:22,name:"广州9",creative_index:100},
                {id:23,name:"广州10",creative_index:100},
                {id:24,name:"广州11",creative_index:100}
            ]
        }
        $('.Js_city_rank').html('');
        for (var i=0; i<res.content.length; i++) {
            var t_style = i < 3 ? 'bqh' : 'btqh';
            var focus = '';
            if (ev.cityId.indexOf(res.content[i].id) == 0) {
                focus = ' data-focus=1 style="background:#ebebeb;"';
            }
            var chtml = '<li class="clearfix"'+focus+'>'
                +'		<span class="y-pm '+t_style+'">'+(i+1)+'</span>'
                +'		<span class="y-dq">'+res.content[i].name+'</span>'
                +'		<span class="y-zs">'+res.content[i].creative_index+'</span>'
                +'	</li>'
            $('.Js_city_rank').append(chtml);
        }
        var city_bank_s = $('.Js_city_rank').children('[data-focus=1]').position().top;
        var h = city_bank_s-48;
        $('.Js_city_rank').scrollTop(h);
    }

    var container = $('.posts-wraplist');
        container.masonry({
                columnWidth: 448,
                gutter: 25,
                itemSelector: '.posts-item'
        });
        container.imagesLoaded( function() {
                $('.Js_itemlist').each(function(){
                        $('.Js_itemlist li:gt(2)').hide();
                })
                container.masonry();
        });
        $('.Js_fancyBox,.Js_fancyBox1,.Js_fancyBox2,.Js_fancyBox3').fancybox({
                prevEffect : 'none',
                nextEffect : 'none',

                closeBtn  : true,
                arrows    : true,
                nextClick : true,
                tpl : {
                        closeBtn : '<a title="Close" style="background-image: url(assets/images/fancybox/fancybox_sprite.png); width:36px; height:36px; top:-18px; right:-18px; position:absolute"></a>'
                },
                helpers : {
                        thumbs : {
                                width  : 50,
                                height : 50
                        }
                }
        });
        //视频播放
        var a = $("#video1");
        var b = $(".js_player");
        b.css({"top":Math.floor((a.height()-b.height())/2)+"px","left":Math.floor((a.width()-b.width())/2)+"px"})
        var myVideo= a.get(0);
        b.click(function(){
                myVideo.play();
                $(this).fadeOut();
        });
        myVideo.addEventListener("ended",function(){b.fadeIn()})
</script>