<div class="content">

<div class="detail-wrap">
<div id="googlemap">
        <div id="map">
        </div>
</div>

<div class="detailPageMidDiv">
<!--地区信息-->
<!--<div class="detailPageMid">-->
<!--        <div class="proContextDiv">-->
<!--                <div class="titDiv">-->
<!--                        <span></span>-->
<!--                        <h2>地区 信息</h2>-->
<!--                </div>-->
<!--                <div class="context">成都，简称"蓉"，四川省省会，西南地区教育、科技、商贸、金融、文化、军事中心，通信、交通枢纽，2012年，经济总量名列省会城市第二（仅次于广州）[1]，城市GDP中国大陆第八位，2011年国务院批复《成渝经济区区域规划》，把成都定位为充分国际化的大都市。<br /><br />-->
<!---->
<!--                        成都幅员面积12390k㎡（截至2013年4月30日）；管辖9个区、6个县，代管4个县级市，仅次于哈尔滨，名列副省级城市第二；常住人口14047625人（其中农业人口800万），名列副省级城市第一。2007年6月7日，成都市全国统筹城乡综合配套改革试验区正式获得国务院批准。<br /><br />-->
<!---->
<!--                        成都位于四川盆地西部，别称"蓉城"、"锦城"，简称"蓉"、"成"。成都物产丰富，自古享有"天府之国"美誉。成都自古为西南重镇，是我国第一批历史文化名城。成都是九朝古都，蜀、成家、蜀汉、成汉、谯蜀、前蜀、后蜀、李蜀、大西等政权相继在此建都，唐朝和民国时曾作为全国临时首都而存在。[2]<br /><br />-->
<!---->
<!--                        2001年2月8日发现的金沙遗址，已经将成都建城历史从公元前311年提前到了公元前611年，超过了苏州，成为中国未变遗址最长久的城市。2012年4月9日，《财富》杂志总编辑苏安迪在北京宣布，第十二届财富全球论坛将于2013年6月6日──2013年6月8日在成都举办。</div>-->
<!--        </div>-->
<!--</div>-->
<!--新闻-->
<div class="detailPageMid">
        <div class="proContextDiv">
                <div class="titDiv">
                        <span></span>
                        <h2 class="Js_scroll">成都 资讯</h2>
                </div>
                <div class="height_25"></div>
                <ul class="clearfix l-areaNewsTit">
                        <li><a href="#">全部新闻</a></li>
                        <li><a href="#">专家专栏</a></li>
                        <li class="last"><a href="#">专家观点</a></li>
                </ul>

                <div class="clearfix">
                        <!--小图轮播开始-->
                        <div class="wheelShow">
                                <div class="imgShow">
                                        <div id="Js_img_show" style="overflow:hidden">
                                                <div class="clearfix" style="width:1800px;">
                                                        <a href="#"><img src="./assets/temp/13.png" alt="" style="float:left; position:relative;" /></a>
                                                        <a href="#"><img src="./assets/temp/13.png" alt="" style="float:left;" /></a>
                                                        <a href="#"><img src="./assets/temp/13.png" alt="" style="float:left;" /></a>
                                                        <a href="#"><img src="./assets/temp/13.png" alt="" style="float:left;" /></a>
                                                        <a href="#"><img src="./assets/temp/13.png" alt="" style="float:left;" /></a>
                                                </div>
                                        </div>
                                        <div class="tmBox"></div>
                                        <span class="txt current Js_show_text">地震之后四川依然美丽</span>
                                        <span class="txt Js_show_text">地震之后四川依然美丽1</span>
                                        <span class="txt Js_show_text">地震之后四川依然美丽2</span>
                                        <span class="txt Js_show_text">地震之后四川依然美丽3</span>
                                        <span class="txt Js_show_text">地震之后四川依然美丽4</span>
                                </div>
                                <p class="eventImgShow">
                                        <a href="#" class="current Js_imgShow_item"></a>
                                        <a href="#" class="Js_imgShow_item"></a>
                                        <a href="#" class="Js_imgShow_item"></a>
                                        <a href="#" class="Js_imgShow_item"></a>
                                        <a href="#" class="Js_imgShow_item"></a>
                                </p>
                        </div> <!--小图轮播结束-->
                        <div class="midNews">
                                <div class="m_news">
                                        <h2><a href="news.php">成都公积金贷款新政：首套房认定扩至全域</a></h2>
                                        <p class="clearfix">
                                                [<a href="news.php">7月10日起执行</a>]
                                                [<a href="news.php">成都全域内再买房都算二套</a>]
                                                [<a href="news.php">新规详情</a>]
                                        </p>
                                </div>
                                <div class="m_news">
                                        <h2><a href="news.php">四川多地暴雨 成都今晨"看海"</a></h2>
                                        <p class="clearfix">
                                                [<a href="news.php">专题</a>]
                                                [<a href="news.php">强降水将持续至今晚</a>]
                                                [<a href="news.php">寻遗失车牌车主</a>]
                                                [<a href="news.php">直播</a>]
                                        </p>
                                </div>
                                <ul>
                                        <li>
                                                <dl class="clearfix">
                                                        <dt><a href="news.php">雨情</a></dt>
                                                        <dd><a href="news.php">近期多市仍有大雨</a></dd>
                                                        <dd><a href="news.php">今晨成都发生雷电11064次</a></dd>
                                                </dl>
                                        </li>
                                        <li>
                                                <dl class="clearfix">
                                                        <dt><a href="news.php">现场</a></dt>
                                                        <dd><a href="news.php">消防官兵搭梯渡人</a></dd>
                                                        <dd><a href="news.php">背老人和学生"过河"(图)</a></dd>
                                                </dl>
                                        </li>
                                        <li>
                                                <dl class="clearfix">
                                                        <dt><a href="news.php">交通</a></dt>
                                                        <dd><a href="news.php">双流机场大量航班延误</a></dd>
                                                        <dd><a href="news.php">宝成线5趟列车晚点</a></dd>
                                                </dl>
                                        </li>
                                        <li>
                                                <dl class="clearfix">
                                                        <dt><a href="news.php">灾情</a></dt>
                                                        <dd><a href="news.php">北川遭暴雨多处塌方</a></dd>
                                                        <dd><a href="news.php">泥石流卷走六旬老人(图)</a></dd>
                                                </dl>
                                        </li>
                                </ul>
                        </div>

                        <div class="rightNews">
                                <div class="clearfix m_news height_96">
                                        <h2><a href="news.php">成都人民公园新地下停车场建成(图)</a></h2>
                                        <div class="tw_composing">
                                                <a href="news.php"><img src="./assets/temp/14.png" alt="" /></a>
                                                <p>人民公园地下停车场共248个车位，可停车换乘地铁2号线…</p>
                                        </div>
                                </div>
                                <ul class="newsList">
                                        <li><a href="news.php">网曝宜宾一客运车辆严重超载(图)</a></li>
                                        <li><a href="news.php">网友称乐山大佛风华严重 呼吁保护</a></li>
                                        <li><a href="news.php">交通部：未来只有3%公路里程收费</a></li>
                                        <li><a href="news.php">蜀绣被授予国家地理标志保护产品</a></li>
                                </ul>
                        </div>
                </div>



        </div>
</div>
<!--创意世界智库-->
<div class="detailPageMid">
        <div class="proContextDiv">
                <div class="titDiv">
                        <span></span>
                        <h2 class="Js_scroll">成都 创意世界智库</h2>
                </div>
                <div class="height_31"></div>
                <div class="thinkExpert">
                        <dl class="clearfix classify">
                                <dt>按职务分类</dt>
                                <dd><a href="javascript:;" class="Js_thinktank_filter">创意世界智库主席团</a></dd>
                                <dd><a href="javascript:;" class="Js_thinktank_filter">创意世界智库理事会</a></dd>
                                <dd><a href="javascript:;" class="Js_thinktank_filter">创意世界智库专家顾问团</a></dd>
                        </dl>
                        <div class="height_10"></div>
                        <div class="clearfix">
                                <dl class="clearfix classify trade">
                                        <dt>按行业分类</dt>
                                        <dd><a href="javascript:;" class="Js_thinktank_filter">全部行业</a></dd>
                                        <dd><a href="javascript:;" class="Js_thinktank_filter">科学家</a></dd>
                                        <dd><a href="javascript:;" class="Js_thinktank_filter">艺术家</a></dd>
                                        <dd><a href="javascript:;" class="Js_thinktank_filter">企业家</a></dd>
                                        <dd><a href="javascript:;" class="Js_thinktank_filter">慈善家</a></dd>
                                        <dd><a href="javascript:;" class="Js_thinktank_filter">投资家</a></dd>
                                </dl>
                                <a href="javascript:;" class="lookAll Js_thinktank_all">查看全部</a>
                        </div>
                        <div>
                                <ul class="clearfix expertList Js_thinktank_zj">
                                        <li>
                                                <a href="#">
                                                        <img src="./assets/temp/tp5.jpg" alt="" />
                                                        <dl>
                                                                <dt class="Js_visitCard">孙正义</dt>
                                                                <dd>新闻集团核心大股东，董事长兼行政总裁</dd>
                                                        </dl>
                                                </a>
                                        </li>
                                        <li>
                                                <a href="#">
                                                        <img src="./assets/temp/tp6.jpg" alt="" />
                                                        <dl>
                                                                <dt class="Js_visitCard">孙正义</dt>
                                                                <dd>新闻集团核心大股东，董事长兼行政总裁</dd>
                                                        </dl>
                                                </a>
                                        </li>
                                        <li>
                                                <a href="#">
                                                        <img src="./assets/temp/tp7.jpg" alt="" />
                                                        <dl>
                                                                <dt class="Js_visitCard">孙正义</dt>
                                                                <dd>新闻集团核心大股东，董事长兼行政总裁</dd>
                                                        </dl>
                                                </a>
                                        </li>
                                        <li>
                                                <a href="#">
                                                        <img src="./assets/temp/tp8.jpg" alt="" />
                                                        <dl>
                                                                <dt class="Js_visitCard">孙正义</dt>
                                                                <dd>新闻集团核心大股东，董事长兼行政总裁</dd>
                                                        </dl>
                                                </a>
                                        </li>
                                        <li>
                                                <a href="#">
                                                        <img src="./assets/temp/tp5.jpg" alt="" />
                                                        <dl>
                                                                <dt class="Js_visitCard">孙正义</dt>
                                                                <dd>新闻集团核心大股东，董事长兼行政总裁</dd>
                                                        </dl>
                                                </a>
                                        </li>
                                        <li>
                                                <a href="#">
                                                        <img src="./assets/temp/tp6.jpg" alt="" />
                                                        <dl>
                                                                <dt class="Js_visitCard">孙正义</dt>
                                                                <dd>新闻集团核心大股东，董事长兼行政总裁</dd>
                                                        </dl>
                                                </a>
                                        </li>
                                        <li>
                                                <a href="#">
                                                        <img src="./assets/temp/tp7.jpg" alt="" />
                                                        <dl>
                                                                <dt class="Js_visitCard">孙正义</dt>
                                                                <dd>新闻集团核心大股东，董事长兼行政总裁</dd>
                                                        </dl>
                                                </a>
                                        </li>
                                        <li>
                                                <a href="#">
                                                        <img src="./assets/temp/tp8.jpg" alt="" />
                                                        <dl>
                                                                <dt class="Js_visitCard">孙正义</dt>
                                                                <dd>新闻集团核心大股东，董事长兼行政总裁</dd>
                                                        </dl>
                                                </a>
                                        </li>
                                </ul>
                        </div>
                </div>
        </div>
</div>

<!--世界大赛-->
<div class="detailPageMid">
        <div class="proContextDiv">
                <div class="titDiv">
                        <span></span>
                        <h2 class="Js_scroll">成都 创意世界大赛</h2>
                </div>
                <div class="height_20"></div>
                <div class="clearfix">
                        <ul class="clearfix l-areaNewsTit fl">
                                <li><a href="javascript:;" class="Js_match_filter">热门比赛</a></li>
                                <li><a href="javascript:;" class="Js_match_filter">创意投资项目比赛</a></li>
                                <li><a href="javascript:;" class="Js_match_filter">创意任务人才比赛</a></li>
                                <li class="last"><a href="javascript:;" class="Js_match_filter">创意公益明星比赛</a></li>
                        </ul>
                        <a href="javascript:;" class="lookAll Js_match_filter_all">查看全部</a>
                </div>
                <div class="height_10"></div>
                <div class="clearfix">
                        <div id="match-product" class="areaMatch" style="width:940px;">
                                <div class="pagearea"></div>
                                <div class="itemListDiv Js_pageChange" style="overflow: hidden; margin-left:-8px;">
                                        <ul class="clearfix">
                                                <!--列表数目务必保证是3的倍数-->
                                                <li class="p-match" data-type="0">
                                                        <div class="match-box">
                                                                <div class="listItemImg">
                                                                        <div class="rightBox mark"></div>
                                                                        <s class="mark"></s>
                                                                        <div class="bottomBox"></div>
                                                                        <img src="./assets/temp/p1.png" alt="">
                                                                </div>
                                                                <div class="contextDiv">
                                                                        <h3>幻世纪大作再现-蚀刻暴龙拼装模型1 -杰思模型</h3>
                                                                        <div class="progress"></div>
                                                                        <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                                                                        <p class="promulgatorMoney">10,000元</p>
                                                                        <span class="status">所需投资金额</span>
                                                                </div>
                                                                <div class="bottomDiv clearfix">
                                                                        <span class="left">长期</span>
                                                                        <span class="right rr">有3位用户参与投资</span>
                                                                </div>
                                                        </div>
                                                </li>
                                                <li class="p-match" data-type="0">
                                                        <div class="match-box">
                                                                <div class="listItemImg">
                                                                        <div class="rightBox mark"></div>
                                                                        <s class="mark"></s>
                                                                        <div class="bottomBox"></div>
                                                                        <img src="./assets/temp/p1.png" alt="">
                                                                </div>
                                                                <div class="contextDiv">
                                                                        <h3>侏罗纪的召唤-蚀刻暴龙拼装模型2 -杰思模型</h3>
                                                                        <div class="progress"></div>
                                                                        <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                                                                        <p class="promulgatorMoney">10,000元</p>
                                                                        <span class="status">所需投资金额</span>
                                                                </div>
                                                                <div class="bottomDiv clearfix">
                                                                        <span class="left">长期</span>
                                                                        <span class="right rr">有3位用户参与投资</span>
                                                                </div>
                                                        </div>
                                                </li>
                                                <li class="p-match" data-type="1">
                                                        <div class="match-box">
                                                                <div class="listItemImg">
                                                                        <div class="rightBox mark"></div>
                                                                        <s class="mark"></s>
                                                                        <div class="bottomBox"></div>
                                                                        <img src="./assets/temp/p1.png" alt="">
                                                                </div>
                                                                <div class="contextDiv">
                                                                        <h3>侏罗纪的召唤-蚀刻暴龙拼装模型3 -杰思模型</h3>
                                                                        <div class="progress"></div>
                                                                        <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                                                                        <p class="promulgatorMoney">10,000元</p>
                                                                        <span class="status">所需投资金额</span>
                                                                </div>
                                                                <div class="bottomDiv clearfix">
                                                                        <span class="left">长期</span>
                                                                        <span class="right rr">有3位用户参与投资</span>
                                                                </div>
                                                        </div>
                                                </li>
                                                <li class="p-match" data-type="3">
                                                        <div class="match-box">
                                                                <div class="listItemImg">
                                                                        <div class="rightBox mark"></div>
                                                                        <s class="mark"></s>
                                                                        <div class="bottomBox"></div>
                                                                        <img src="./assets/temp/p1.png" alt="">
                                                                </div>
                                                                <div class="contextDiv">
                                                                        <h3>侏罗纪的召唤-蚀刻暴龙拼装模型4 -杰思模型</h3>
                                                                        <div class="progress"></div>
                                                                        <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                                                                        <p class="promulgatorMoney">10,000元</p>
                                                                        <span class="status">所需投资金额</span>
                                                                </div>
                                                                <div class="bottomDiv clearfix">
                                                                        <span class="left">长期</span>
                                                                        <span class="right tk">有3位用户参与投资</span>
                                                                </div>
                                                        </div>
                                                </li>
                                                <li class="p-match" data-type="3">
                                                        <div class="match-box">
                                                                <div class="listItemImg">
                                                                        <div class="rightBox mark"></div>
                                                                        <s class="mark"></s>
                                                                        <div class="bottomBox"></div>
                                                                        <img src="./assets/temp/p1.png" alt="">
                                                                </div>
                                                                <div class="contextDiv">
                                                                        <h3>侏罗纪的召唤-蚀刻暴龙拼装模型5 -杰思模型</h3>
                                                                        <div class="progress"></div>
                                                                        <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                                                                        <p class="promulgatorMoney">10,000元</p>
                                                                        <span class="status">所需投资金额</span>
                                                                </div>
                                                                <div class="bottomDiv clearfix">
                                                                        <span class="left">长期</span>
                                                                        <span class="right tk">有3位用户参与投资</span>
                                                                </div>
                                                        </div>
                                                </li>
                                        </ul>
                                </div>
                        </div>
                </div>
        </div>
</div>

<!--世界交易所-->
<div class="detailPageMid">
    <div class="proContextDiv">
        <div class="titDiv">
            <span></span>
            <h2 class="Js_scroll">成都 创意世界交易所</h2>
        </div>
        <div class="height_20"></div>
        <div class="clearfix">
            <ul class="clearfix l-areaNewsTit fl">
                <li><a href="javascript:;" class="Js_match_filter">热门交易</a></li>
<!--                <li><a href="javascript:;" class="Js_match_filter">创意投资项目比赛</a></li>-->
<!--                <li><a href="javascript:;" class="Js_match_filter">创意任务人才比赛</a></li>-->
<!--                <li class="last"><a href="javascript:;" class="Js_match_filter">创意公益明星比赛</a></li>-->
            </ul>
            <a href="javascript:;" class="lookAll Js_match_filter_all">查看全部</a>
        </div>
        <div class="height_10"></div>
        <div class="clearfix">
            <div id="match-product" class="areaMatch" style="width:940px;">
                <div class="pagearea"></div>
                <div class="itemListDiv Js_pageChange" style="overflow: hidden; margin-left:-8px;">
                    <ul class="clearfix">
                        <!--列表数目务必保证是3的倍数-->
                        <li class="p-match" data-type="0">
                            <div class="match-box">
                                <div class="listItemImg">
                                    <div class="rightBox mark"></div>
                                    <s class="mark"></s>
                                    <div class="bottomBox"></div>
                                    <img src="./assets/temp/item.jpg" alt="">
                                </div>
                                <div class="contextDiv">
                                    <h3>幻世纪大作再现-蚀刻暴龙拼装模型1 -杰思模型</h3>
                                    <div class="progress"></div>
                                    <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                                    <p class="promulgatorMoney">10,000元</p>
                                    <span class="status">直接购买价</span>
                                    <span class="number">T2013092900001</span>
                                </div>
                                <div class="bottomDiv clearfix">
                                    <span class="left">剩余30天</span>
                                    <span class="right rr">有3位用户购买</span>
                                </div>
                            </div>
                        </li>
                        <li class="p-match" data-type="0">
                            <div class="match-box">
                                <div class="listItemImg">
                                    <div class="rightBox mark"></div>
                                    <s class="mark"></s>
                                    <div class="bottomBox"></div>
                                    <img src="./assets/temp/item.jpg" alt="">
                                </div>
                                <div class="contextDiv">
                                    <h3>侏罗纪的召唤-蚀刻暴龙拼装模型2 -杰思模型</h3>
                                    <div class="progress"></div>
                                    <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                                    <p class="promulgatorMoney">3,000元</p>
                                    <span class="status">当前竞拍价</span>
                                    <span class="number">T2013092900002</span>
                                </div>
                                <div class="bottomDiv clearfix">
                                    <span class="left">长期</span>
                                    <span class="right rr">有30位用户参与竞价</span>
                                </div>
                            </div>
                        </li>
                        <li class="p-match" data-type="1">
                            <div class="match-box">
                                <div class="listItemImg">
                                    <div class="rightBox mark"></div>
                                    <s class="mark"></s>
                                    <div class="bottomBox"></div>
                                    <img src="./assets/temp/item.jpg" alt="">
                                </div>
                                <div class="contextDiv">
                                    <h3>侏罗纪的召唤-蚀刻暴龙拼装模型3 -杰思模型</h3>
                                    <div class="progress"></div>
                                    <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                                    <p class="promulgatorMoney">9,000元</p>
                                    <span class="status">最终成交价</span>
                                    <span class="number">T2013092900003</span>
                                </div>
                                <div class="bottomDiv clearfix">
                                    <span class="left">长期</span>
                                    <span class="final">用户<i class="Js_visitCard">斯威夫特</i>在竞拍中获胜</span>
                                </div>
                            </div>
                        </li>
                        <li class="p-match" data-type="3">
                            <div class="match-box">
                                <div class="listItemImg">
                                    <div class="rightBox mark"></div>
                                    <s class="mark"></s>
                                    <div class="bottomBox"></div>
                                    <img src="./assets/temp/item.jpg" alt="">
                                </div>
                                <div class="contextDiv">
                                    <h3>侏罗纪的召唤-蚀刻暴龙拼装模型4 -杰思模型</h3>
                                    <div class="progress"></div>
                                    <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                                    <p class="promulgatorMoney">1,000,000元</p>
                                    <span class="status">当前竞拍价</span>
                                    <span class="number">T2013092900004</span>
                                </div>
                                <div class="bottomDiv clearfix">
                                    <span class="left">长期</span>
                                    <span class="right tk">有3位用户参与竞价</span>
                                </div>
                            </div>
                        </li>
                        <li class="p-match" data-type="3">
                            <div class="match-box">
                                <div class="listItemImg">
                                    <div class="rightBox mark"></div>
                                    <s class="mark"></s>
                                    <div class="bottomBox"></div>
                                    <img src="./assets/temp/item.jpg" alt="">
                                </div>
                                <div class="contextDiv">
                                    <h3>侏罗纪的召唤-蚀刻暴龙拼装模型5 -杰思模型</h3>
                                    <div class="progress"></div>
                                    <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                                    <p class="promulgatorMoney">400,000元</p>
                                    <span class="status">直接购买价</span>
                                    <span class="number">T2013092900005</span>
                                </div>
                                <div class="bottomDiv clearfix">
                                    <span class="left">已下架</span>
                                    <span class="right tk">有3位用户参与购买</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--社区论坛开始-->
<?php $forumTT = "成都 讨论区" ; //社区论坛名称变量 ?>
<?php include("./modules/discuss.php"); ?>
<!--社区论坛结束-->




</div>

</div>
</div>