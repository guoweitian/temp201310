<div class="content">
        <div class="login-box" style="width:415px;">
                <div class="login-form">
                        <div class="box-title"><h4>设置新密码成功</h4><span class="s-line"></span></div>
                        <div class="form-item">
                                <div class="height_10"></div>
                                <p class="destitle">您的新密码已经生效</p>
                                <div class="height_15"></div>
                                <ul class="prolist">
                                        <li>您需要重新以新密码登录创意世界。</li>
                                        <li>为了方便您更好的使用创意世界，请牢记您的新密码。</li>
                                </ul>
                        </div>
                </div>
                <div class="login-btnbar">
                        <a href="login.php" class="blueBtn login-btn">登录创意世界</a>
                </div>
        </div>
</div>