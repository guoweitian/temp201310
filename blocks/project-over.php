<div class="content">
<div class="detail-wrap project">
<div class="detail-imgbg">
        <div class="toppattern"></div>
        <div class="detailTopDiv pew">
                <h3>这里是比赛名称，如果有很多字，这里也只能显示一行标题。这里是比赛名称，如果有很多字，这里也只能显示一行标题。</h3>
                <p class="disDiv">
                        <span class="loc">成都市青羊区小南街</span>
                        <span class="time">剩余3天</span>
                        <span class="m_num">P2013052400021</span>
                </p>
                <div>
                        <div class="u_listItem left">
                                <a href="#" class="leftArea shadow">
                                        <img src="./assets/temp/5.png" alt="">
                                </a>
                                <div class="rightArea w600">
                                        <p class="rightName"><a class="Js_visitCard cfff" href="#">刘兆宇</a></p>
                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                </div>
                        </div>
                        <div class="tz_moneyDiv right">
                                <p>所需投资金额:<b>10,000元</b></p>
                                <a href="#" class="bgimgBtn_over over right" onclick="return false">已结束</a>
                        </div>
                </div>
        </div>
</div>
<div class="detailPageMidDiv">
<div class="detailPageMid">
        <div class="proContextDiv">
                <div class="titDiv">
                        <span></span>
                        <h2>比赛已经结束</h2>
                </div>

                <div class="tOverDiv">
                        <p class="top">您发布的创意投资项目比赛已经结束。</p>
                        <p class="iList">在比赛期间，您未能和投资方达成项目投资协议；</p>
                        <p class="iList">您可以更新一部份的创意投资项目比赛信息，以便让更多的潜在投资方参与投资；</p>
                        <p class="iList">您可以邀请更多的创意世界智库专家点评您的项目，提升投资方对项目的认可程度；</p>
                        <p class="iList">为了方便您再次发布本创意投资项目比赛，您可以使用下方的"再次发布"按钮。</p>
                </div>
                <div class="height_40"></div>
                <a href="./project-publish.php" class="nsBtn">再次发布</a>
        </div>
</div>


<div class="detailPageMidDiv">
        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="titDiv">
                                <span></span>
                                <h2 class="Js_scroll">项目信息</h2>
                        </div>
                        <div class="context">〖商机〗2006—2008年，因奥运会而产生的各类商机不断出现，每一个商机背后都意味着巨大的财富，这期间，如果把握好其中一项商机，一生的命运可能都会改变。对于个人而言，怎么样把握这个难得的商机？〖发现〗其实奥运会上最有值钱的东西就是5个吉祥物了。现在很多的人已经开始打它的主意，相关的吉祥物产品，诸如卡通像、剪纸等等     也将不断出现。对于个人在这方面而言，获利可能只是经销产品而已，而经营的人多，竟争也会激烈！但是，却有一门被忽略的冷门绝活手艺，将它应到吉祥物的制作上，所制作的吉祥物产品不仅形象出众、与众不同，而且很有内涵，极其新颖独特！绝不是我们可见到的普通吉祥物产品。如果面世，一定会深受迎！</div>
                </div>
        </div>
        <div class="detailPageMid">
                <div class="proContextDiv" id="pro_detail" name="pro_detail">
                        <div class="titDiv">
                                <span></span>
                                <h2 class="Js_scroll">创意项目回报</h2>
                        </div>
                        <div class="context">〖获利〗用这个冷门绝活手艺制作吉祥物，非常适合个人操作。全部投资有几百元即可。两个月就可熟练掌握。既可自己制作销售，同时还可在当地进行技术培训，可小量制作，也可批量生产。从现在一直到奥运会到来之前的两年多时间，如果能抓好这个商机，掌握这门制作手工艺，抢先经营，一定会大获其利，赚到大钱的。我们详细揭秘这个手艺项目，转让全套技术及方法，包括赠送7张操作光盘。通过这些光盘可完全学会。全部内容收入了本套创意方案资料中。</div>
                </div>
        </div>
        <div class="detailPageMid">
                <div class="titDiv clearfix" style="margin-bottom:15px;">
                        <span></span>
                        <h2 class="left">项目更新</h2>
                        <div class="right mt20">
                                <a class="sub-sbtn Js_pro_upadate">发布项目更新</a>
                        </div>
                        <s class="p-box b2 Js_pro_b" style="bottom:-15px;"></s>
                </div>


                <div style="background:#e6e6e6; display:none;" class="js_pro_c">
                        <div class="expertDpDiv">
                                <div style="width:940px; margin:0 auto;">
                                        <div class="height_27"></div>
                                        <div class="rowblock clearfix">
                                                <div class="pt">标题</div>
                                                <div class="tools text left">
                                                        <div class="inputWarp empty">
                                                                <input type="text" id="Js_update_tit">
                                                        </div>
                                                </div>
												<div class="errorMsg left" style="display:none;">错误提示内容</div>
                                        </div>

                                        <div class="height_18"></div>
                                        <div class="rowblock clearfix">
                                                <div class="pt">更新内容</div>
                                                <div class="tools textArea left" style="width:540px; height:168px;">
                                                        <textarea style="font-size: 15px; line-height: 1.5" id="Js_text_con"></textarea>
                                                </div>
												<div class="errorMsg left" style="display:none;">错误提示内容</div>
                                        </div>
                                        <div class="height_15"></div>
                                        <div class="rowblock">
                                                <div class="pt">上传项目更新图片</div>
                                                <div class="uploadWrap-more dargArea left">
                                                        <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
                                                        <input class="file_uploadbox-more" type="file" ismul="true" multiple="multiple" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">
                                                </div>
                                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                                                <div class="height_10"></div>
                                                <div id="upload-more" class="clearfix Js_update_img upload-more"></div>

                                        </div>

                                        <div class="height_40"></div>
                                        <div class="p-warp">
                                                <a href="javascript:;" class="btn h8 mr5 Js_pro_upadate">取消更新</a>
                                                <a href="javascript:;" class="btn b73 Js_submit_update">提交更新</a>
                                        </div>
                                        <div class="height_50"></div>
                                        <div class="sp-line" style="top:0; left:52px;">
                                                <div class="line-left"></div>
                                                <div class="line-right"></div>
                                        </div>
                                </div>
                        </div>
                </div>

                <div class="proContextDiv">
                        <div class="pro_update">
                                <dl>
                                        <dt><a href="#">重回频率大战 AMD推出FX-9590 首次触及5GHz</a></dt>
                                        <dd class="date">2012年10月3日</dd>
                                        <dd class="txt">21世纪初处理器的频率大战在多年前止步于4GHz，而到了2013年，随着技术的进步这场战役似乎又要开始了。AMD今天公布了第一款5GHz处理器FX-9590，它八个核心都可以稳定运行在4.7GHz，而通过"Max Turbo"的短暂自动超频，它的频率最高可达到5GHz。<br />
                                                FX-9590: Eight "Piledriver" cores, 5 GHz Max Turbo<br />
                                                FX-9370: Eight "Piledriver" cores, 4.7 GHz Max Turbo<br />
                                                这款产品将随着另一型号FX-9370在今年夏天发布，值得注意的是它的TDP高达220W，相对不极端一点的FX-8350运行在4.2GHz下时的TDP为125W。<br />
                                                FX-9590和FX-9370芯片将在今年夏季晚些时候开始上市。但是，这些芯片首先仅向生产配置这些芯片的设备PC OEM厂商提供。其中一家厂商是Maingear。这家厂商已经宣布计划在其Shift台式电脑产品线中使用AMD的这些新的芯片。
                                                虽然主频速度曾经是确定一个处理器的性能的最佳途径，但是，多核处理器的新时代已经改变了这种情况。有趣的是，AMD现在用新的5GHz处理器再一次挑起"主频速度大战"。</dd>
                                </dl>
                                <dl class="last">
                                        <dt><a href="#">重回频率大战 AMD推出FX-9590 首次触及5GHz</a></dt>
                                        <dd class="date">2012年10月3日</dd>
                                        <dd class="txt">相对于以往笨重的数码相机而言，现在的卡片机轻薄便携，但如果真要冠以"卡片"的名义，它就非得安装Plastic Logic刚研发出的传感器不可。这是一款40×40毫米传感器，包含一个透射背板和ISORG有机光电探测器材料，它可以形成一个有弹性可弯曲，轻巧无比的相机模块，可以用来取代传统的图像传感器。但目前为止分辨率只能实现到94x95像素，像素大小175微米见方，间距200微米。<br />
                                                但公司有信心制作出大面积图像传感器应用。这种柔性传感器可以被用在医疗、工业、安全控制等行业，同时还可以实现指纹扫描以及用在健康传感器上当然，它对数码产品消费者而言，最大的好处就是可以极大减少卡片机的厚度以及智能手机、平板电脑的厚度，同时也可以当做可塑性输入表面所用</dd>
                                </dl>
                        </div>
                </div>
        </div>

        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="titDiv">
                                <span></span>
                                <h2 class="Js_scroll">洽谈投资者</h2>
                        </div>
                        <ul class="clearfix u_list">
                                <li class="left">
                                        <div class="u_listItem">
                                                <a href="#" class="leftArea shadow">
                                                        <img src="./assets/temp/5.png" alt="">
                                                </a>
                                                <div class="rightArea">
                                                        <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                                                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                                </div>
                                        </div>
                                </li>
                                <li class="left">
                                        <div class="u_listItem">
                                                <a href="#" class="leftArea shadow">
                                                        <img src="./assets/temp/5.png" alt="">
                                                </a>
                                                <div class="rightArea">
                                                        <p class="rightName"><a href="#" class="Js_visitCard">刘兆宇</a></p>
                                                        <p class="rightList"><a href="#">成都电子科技大学</a></p>
                                                </div>
                                        </div>
                                </li>
                                <li class="left">
                                        <div class="u_listItem">
                                                <a href="#" class="leftArea shadow">
                                                        <img src="./assets/temp/5.png" alt="">
                                                </a>
                                                <div class="rightArea">
                                                        <p class="rightName"><a href="#" class="Js_visitCard">刘兆宇</a></p>
                                                        <p class="rightList"><a href="#">创意世界</a></p>
                                                </div>
                                        </div>
                                </li>
                                <li class="left">
                                        <div class="u_listItem">
                                                <a href="#" class="leftArea shadow">
                                                        <img src="./assets/temp/5.png" alt="">
                                                </a>
                                                <div class="rightArea">
                                                        <p class="rightName"><a href="#" class="Js_visitCard">刘兆宇</a></p>
                                                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                                </div>
                                        </div>
                                </li>
                                <li class="left">
                                        <div class="u_listItem">
                                                <a href="#" class="leftArea shadow">
                                                        <img src="./assets/temp/5.png" alt="">
                                                </a>
                                                <div class="rightArea">
                                                        <p class="rightName"><a href="#" class="Js_visitCard">刘兆宇</a></p>
                                                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                                </div>
                                        </div>
                                </li>
                                <li class="left">
                                        <div class="u_listItem">
                                                <a href="#" class="leftArea shadow">
                                                        <img src="./assets/temp/5.png" alt="">
                                                </a>
                                                <div class="rightArea">
                                                        <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                                                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                                </div>
                                        </div>
                                </li>
                        </ul>
                </div>
        </div>
        <!--todo 引用文件专家点评开始-->
        <?php include("./modules/expert-comment.php"); ?>
        <!--todo 竞猜引用文件-->
        <?php $guessTT = "项目竞猜"; ?>
        <?php include("./modules/lottery.php"); ?>
        <!--todo 社区论坛引用文件-->
        <?php $forumTT = "讨论区"; //论坛社区名称变量?>
        <?php include("./modules/discuss.php"); ?>
</div>
</div>
</div>
</div>