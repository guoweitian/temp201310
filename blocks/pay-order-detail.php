<div class="content">
        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="height_18"></div>
                        <div class="titDiv">
                                <span></span>
                                <h2>订单详情</h2>
                        </div>
                        <div class="height_35"></div>
                        <div class="pay-order-detail">
                            <div class="detail-group">
                                <span class="detail-1">订单编号：<b>D0000001234567</b></span>
                                <span class="detail-2">支付编号：<b>T0000001234567</b></span>
                            </div>
                            <div class="detail-group">
                                <span class="detail-1">订单状态：<b>等待付款/交易成功/已付款，等待发货/...</b></span>
                                <span class="detail-2">支付时间：<b>2013/02/26</b><b>10:26:22</b></span>
                            </div>
                            <div class="detail-group">
                                <span class="detail-1">创建时间：<b>2013/02/26</b><b>10:26:22</b></span>
                            </div>
                            <ul class="order-num">
                                <li class="clearfix">
                                </li>
                            </ul>
                            <div class="height_20"></div>
                            <div class="pay-order-detail">
                                <div class="detail-group">
                                    <span class="detail-1">买方信息：<b>尾田荣一郎</b></span>
                                </div>
                                <div class="detail-group">
                                    <span class="detail-1">卖方信息：<b>鸟山明</b></span>
                                </div>
                                <div class="detail-group">
                                    <span class="detail-2">收货地址：<b class="addr">四川省成都市青羊区小南街123号冠城花园二期檀香阁605室</b><b class="zcode">610000</b><b class="nam">李原早</b><b class="tel">18615796713</b></span>
                                </div>
                                <ul class="order-num">
                                    <li class="clearfix">
                                    </li>
                                </ul>
                        </div>
                        <div class="height_40"></div>
                        <div class="confirm-order-detail">
                                <span class="order-lab">订单内容</span>
                                <div class="height_20"></div>
                                <div class="clearfix order-detail">
                                        <span class="left">名称</span>
                                        <span class="right">金额</span>
                                </div>
                                <ul class="order-num">
                                        <li class="clearfix">
                                                <span class="name">淘宝购物-聚壹之家牛津百纳箱60L+30L二件套玩具收纳物箱</span>
                                                <span class="money">1,234.00<small style="margin-left: 6px">元</small></span>
                                        </li>
                                </ul>
                        </div>

                        <div class="height_20"></div>
                        <div style="width:890px;">
                                <div class="height_22"></div>
                                <a href="#" class="blueBtn">确认支付</a>
                                <a href="#" class="grayBtn" style="margin-left: 6px">返回</a>
                        </div>
                        <div class="height_40"></div>
                </div>
        </div>
</div>