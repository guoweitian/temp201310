<div class="content">
        <div class="login-box" style="width:760px;">
                <div class="l-login-form">
                        <div class="height_18"></div>
                        <div class="box-title">
                                <h4 class="ml32">发送邮件成功</h4>
                        </div>
                        <div class="form-item ml95">
                                <p class="font12" style="color: #888;">系统已经给您的邮箱发送了一封邮件</p>
                                <div class="height_15"></div>
                                <ul class="prolist">
                                        <li>通过邮件上的地址，您可以重新设置新的创意世界登录密码。</li>
                                        <li>如果在收件箱中未能找到找回密码的邮件，还烦请您查阅您的垃圾箱。</li>
                                        <li>创意世界邮件系统采用业内领先的邮件发送技术，99%以上的邮件都可以在收件箱中直接查阅到。</li>
                                </ul>
                        </div>
                </div>
                <div class="height_18"></div>
                <div class="login-btnbar" style="margin-left: 72px;">
                        <a href="javascript:;" class="blueBtn login-btn">查看邮件</a>
                </div>

        </div>
</div>