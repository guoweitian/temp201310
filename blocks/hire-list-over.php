<div class="content">
<div class="w940">
        <div class="large-12">
                <div class="rank-list">
                        <h2>创意公益明星排行榜</h2>
                        <div class="listDiv">
                                <ul>
                                        <li class="tit clearfix">
                                                <span class="left ml40">公益明星</span>
                                                <span class="right mr20">已获创币</span>
                                                <span class="right mr80">创意指数</span>
                                        </li>
                                        <li class="clearfix">
                                                <a href="../user/home.php">
                                                        <s class="topThree">1</s>
							<span class="namearea">
								<img src="./assets/temp/m1.png" />
								<span class="name">我的名字有好长</span>
							</span>
                                                        <span class="createnum">1,000.00</span>
                                                        <span class="getmoney">1,000.00</span>
                                                </a>
                                        </li>
                                        <li class="clearfix">
                                                <a href="../user/home.php">
                                                        <s class="topThree">2</s>
							<span class="namearea">
								<img src="./assets/temp/m2.png" />
								<span class="name">宁财神</span>
							</span>
                                                        <span class="createnum">900.00</span>
                                                        <span class="getmoney">900.00</span>
                                                </a>
                                        </li>
                                        <li class="clearfix">
                                                <a href="../user/home.php">
                                                        <s class="topThree">3</s>
							<span class="namearea">
								<img src="./assets/temp/m3.png" />
								<span class="name">马可斯波基可</span>
							</span>
                                                        <span class="createnum">800.00</span>
                                                        <span class="getmoney">800.00</span>
                                                </a>
                                        </li>
                                        <li class="clearfix">
                                                <a href="../user/home.php">
                                                        <s>4</s>
							<span class="namearea">
								<img src="./assets/temp/m4.png" />
								<span class="name">李伯清</span>
							</span>
                                                        <span class="createnum">700.00</span>
                                                        <span class="getmoney">700.00</span>
                                                </a>
                                        </li>
                                        <li class="clearfix">
                                                <a href="../user/home.php">
                                                        <s>5</s>
							<span class="namearea">
								<img src="./assets/temp/m5.png" />
								<span class="name">KKDESIGN</span>
							</span>
                                                        <span class="createnum">600.00</span>
                                                        <span class="getmoney">600.00</span>
                                                </a>
                                        </li>
                                </ul>
                        </div>
                </div>
        </div>
</div>
<div class="match-list l-list-wrap clearfix">

<div  class="js_container row ml-3 Js_page_list_item">

<div class="large-4 small-6 columns">
        <a href="hire-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox heart"></div>
                                <s class="heart"></s>
                                <div class="bottomBox"></div>
                                <img class="thumb" src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney"></p>
                                <span class="status">非现金奖励</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">明星<i class="Js_visitCard">泰勒·斯威夫特</i>和其他9位明星获奖</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns">
        <a href="hire-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox heart"></div>
                                <s class="heart"></s>
                                <div class="bottomBox"></div>
                                <img class="thumb" src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney"></p>
                                <span class="status">非现金奖励</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">明星<i class="Js_visitCard">泰勒·斯威夫特</i>和其他9位明星获奖</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns">
        <a href="hire-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox heart"></div>
                                <s class="heart"></s>
                                <div class="bottomBox"></div>
                                <img class="thumb" src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney"></p>
                                <span class="status">非现金奖励</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">明星<i class="Js_visitCard">泰勒·斯威夫特</i>和其他9位明星获奖</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns">
        <a href="hire-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox heart"></div>
                                <s class="heart"></s>
                                <div class="bottomBox"></div>
                                <img class="thumb" src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney"></p>
                                <span class="status">非现金奖励</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">明星<i class="Js_visitCard">泰勒·斯威夫特</i>和其他9位明星获奖</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns">
        <a href="hire-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox heart"></div>
                                <s class="heart"></s>
                                <div class="bottomBox"></div>
                                <img class="thumb" src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney"></p>
                                <span class="status">非现金奖励</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">明星<i class="Js_visitCard">泰勒·斯威夫特</i>和其他9位明星获奖</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns">
        <a href="hire-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox heart"></div>
                                <s class="heart"></s>
                                <div class="bottomBox"></div>
                                <img src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">比赛总奖金</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">明星<i class="Js_visitCard">泰勒·斯威夫特</i>和其他9位明星获奖</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns">
        <a href="hire-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox heart"></div>
                                <s class="heart"></s>
                                <div class="bottomBox"></div>
                                <img src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">比赛总奖金</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">明星<i class="Js_visitCard">泰勒·斯威夫特</i>和其他9位明星获奖</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns">
        <a href="hire-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox heart"></div>
                                <s class="heart"></s>
                                <div class="bottomBox"></div>
                                <img src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">比赛总奖金</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">明星<i class="Js_visitCard">泰勒·斯威夫特</i>和其他9位明星获奖</span>
                        </div>
                </div>
        </a>
</div>

<div class="large-4 small-6 columns">
        <a href="hire-going.php">
                <div class="match-box">
                        <div class="listItemImg">
                                <div class="rightBox heart"></div>
                                <s class="heart"></s>
                                <div class="bottomBox"></div>
                                <img src="./assets/temp/item.jpg" alt="">
                        </div>
                        <div class="contextDiv">
                                <h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>
                                <div class="progress"></div>
                                <p class="promulgatorName Js_visitCard">发布者名字</p>
                                <p class="promulgatorMoney">10,000元</p>
                                <span class="status">比赛总奖金</span>
                                <span class="number">M2013092900001</span>
                        </div>
                        <div class="bottomDiv clearfix">
                                <span class="final">明星<i class="Js_visitCard">泰勒·斯威夫特</i>和其他9位明星获奖</span>
                        </div>
                </div>
        </a>
</div>

</div>
<div style="text-align:center; display:none;" class="Js_page_list_btn">
        <div class="pageItemDiv" style="display:inline-block">
                <a href="javascript:;" class="fyBtn prev Js_page_list_prev"></a>
					<span class="pageItem">
						<a href="javascript:;" class="current">1</a>
						<a href="javascript:;">2</a>
						<a href="javascript:;">3</a>
						<a href="javascript:;">4</a>
						<a href="javascript:;">5</a>
					</span>
                <a href="javascript:;" class="fyBtn next Js_page_list_next"></a>
        </div>
</div>
</div>
</div>