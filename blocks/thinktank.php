<div class="content">
<div class="detail-wrap thinktank">
<div class="detail-imgbg">
        <div style="width:100%; height:186px; opacity:0.67; background:#000; position:absolute; bottom:0;"></div>
        <div class="toppattern"></div>
        <div class="detailTopDiv pew">
                <h3>世界级行业创意领袖的智慧宝库  全球十万名创意领袖的创新平台</h3>
                <p class="disDiv thinktank-bannerTxt">
                        世界创意精英与国际社会分享交流的创新源泉
                </p>
                <div>
                        <div class="clearfix hiringBannerImg">
                                <a href="#"><img src="./assets/temp/18.png" alt="" /></a>
                                <a href="#"><img src="./assets/temp/19.png" alt="" /></a>
                                <a href="#"><img src="./assets/temp/20.png" alt="" /></a>
                                <a href="#"><img src="./assets/temp/21.png" alt="" /></a>
                                <a href="#"><img src="./assets/temp/22.png" alt="" /></a>
                        </div>
                        <div class="tz_thinktank">
                                <p>一个好的创意<br />创造的社会经济效益超过百千人的埋头苦干<br />与全世界最有思想的人在一起为民造福，风行世界</p>
                                <a href="#" class="bgimgBtnJoin right js_detailPublish ">申请/邀请加入创意世界智库候选人</a>
                        </div>
                </div>
        </div>
</div>

<div class="form-wrap js_detailForm" style="background:#e6e6e6">
		<form name="apply_candidate">
        	<div class="proContextDiv">
                <div class="height_15"></div>
                <div class="titDiv">
                        <span></span>
                        <h2>申请成为创意世界智库候选专家</h2>
                </div>
                <div class="height_18"></div>
                <div class="rowblock clearfix">
                        <div class="pt">您的姓名</div>
                        <div class="tools text left">
                                <div class="inputWarp empty">
                                        <input type="text" id="Js_your_name">
                                </div>
                        </div>
						<div class="errorMsg left" style="display:none;">错误提示内容</div>
                </div>
                <div class="height_18"></div>
                <div class="rowblock l-edit">
                        <div class="pt">您的个人简介</div>
                        <div class="height_10"></div>
                        <div id="js_toolbar" class="toolbar-wrap"></div>
                        <div class="toolbar-content left">
                                <textarea id="js_content" name="content Js_your_intro"></textarea>
                        </div>
                        <div class="errorMsg left" style="display:none;">错误提示内容</div>
                </div>
                <div class="height_25"></div>
                <div class="rowblock clearfix">
                        <div class="pt">上传证明材料</div>
                        <div class="height_8"></div>
                        <div class="uploadWrap-single left clearfix">
                                <div class="upfileDiv">
                                        <ul>
                                                <li>
                                                        <label class="lv">PNG</label>
                                                        <span>证明材料.jpg</span>
                                                        <a href="javascript:;" class="Js_upfile_del"></a>
                                                </li>
                                                <li>
                                                        <label class="lan">ZIP</label>
                                                        <span>证明材料.jpg</span>
                                                        <a href="javascript:;" class="Js_upfile_del"></a>
                                                </li>
                                                <li>
                                                        <label class="fen">DOC</label>
                                                        <span>证明材料.jpg</span>
                                                        <a href="javascript:;" class="Js_upfile_del"></a>
                                                </li>
                                        </ul>
                                        <div class="height_12"></div>

                                </div>
                                <input class="file_uploadbox-single" type="file">
                        </div>
                        <div id="upload-single" class="clearfix"></div>
                        <div class="clearfix">
                                <a href="javascript:;" class="upBtn tuploadbox left">上传文件</a>
                        </div>
                </div>
                <div class="height_38"></div>
                <div class="rowblock clearfix">
                        <div class="pt">选择您所在的行业</div>
                        <div class="optDiv Js_state left">
                                <div class="tools text pr">
                                        <div class="inputWarp opt">
                                                <span id="Js_your_work_type">请您选择推荐专家行业</span>
                                                <a href="javascript:void(0)" class="showMenu"></a>
                                        </div>
                                </div>
                                <ul>
                                        <li><a href="javascript:void(0)">领导人</a></li>
                                        <li><a href="javascript:void(0)">科学家</a></li>
                                </ul>
                                <select style="display:none;">
                                        <option>领导人</option>
                                        <option>科学家</option>
                                </select>
                        </div>
                        <div class="errorMsg mr10 left" style="display:none;">错误提示内容</div>
                </div>

                <div class="height_27"></div>
                <div class="rowblock clearfix">
                        <div class="pt">选择您申请加入的组织</div>
                        <div class="optDiv Js_state left">
                                <div class="tools text pr">
                                        <div class="inputWarp opt">
                                                <span id="Js_your_zz_type">选择您申请加入的组织</span>
                                                <a href="javascript:void(0)" class="showMenu"></a>
                                        </div>
                                </div>
                                <ul>
                                        <li><a href="javascript:void(0)">组织一</a></li>
                                        <li><a href="javascript:void(0)">组织二</a></li>
                                </ul>
                                <select style="display:none;">
                                        <option>组织一</option>
                                        <option>组织二</option>
                                </select>
                        </div>
                        <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        <div class="optDiv Js_state left ml32">
                                <div class="tools text pr">
                                        <div class="inputWarp opt">
                                                <span id="Js_your_zw_type">选择申请职务</span>
                                                <a href="javascript:void(0)" class="showMenu"></a>
                                        </div>
                                </div>
                                <ul>
                                        <li><a href="javascript:void(0)">职务一</a></li>
                                        <li><a href="javascript:void(0)">职务二</a></li>
                                </ul>
                                <select style="display:none;">
                                        <option>职务一</option>
                                        <option>职务二</option>
                                </select>
                        </div>
                        <div class="errorMsg left" style="display:none;">错误提示内容</div>
                </div>
                <div class="height_27"></div>
                <div class="rowblock clearfix">
                        <div class="pt">您的邮箱地址</div>
                        <div class="tools text left">
                                <div class="inputWarp empty">
                                        <input type="text" id="Js_your_email">
                                </div>
                        </div>
						<div class="errorMsg left" style="display:none;">错误提示内容</div>
                </div>
                <div class="height_20"></div>
                <div class="rowblock clearfix">
                        <div class="pt">您的电话号码</div>
                        <div class="tools text left">
                                <div class="inputWarp empty">
                                        <input type="text" id="Js_your_phone" class="Js_enterOnlyNum">
                                </div>
                        </div>
						<div class="errorMsg left" style="display:none;">错误提示内容</div>
                </div>
                <div class="height_18"></div>
                <div class="rowblock clearfix">
                        <div class="pt">您的联系方式</div>
                        <div class="tools text left">
                                <div class="inputWarp empty">
                                        <input type="text" id="Js_your_content">
                                </div>
                        </div>
						<div class="errorMsg left" style="display:none;">错误提示内容</div>
                </div>


                <div class="rowblock clearfix">
                    <span class="Js_checkBox" data-name="chk" style="display: inline-block; margin-top: 16px; margin-bottom: 16px;">
                        <label class="Js_label_ok ok" style="float: left">
                            <input type="checkbox" name="chk" id="Js_register_chk" checked="checked">
                            我已经阅读并确认
                        </label>
                            <a class="l-tag Js_agreement_check" href="javascript:;" style="float: left">申请/邀请加入创意世界智库候选人协议</a>
                            <span class="errorMsg errorMsg_short left" style="display:none;">请确认同意协议</span>
                    </span>
<!--                    <div class="pt">确认申请/邀请加入创意世界智库候选人协议</div>-->
<!--                    <p class="pdes">请您阅读并确认申请/邀请加入创意世界智库候选人协议</p>-->
                    <div class="height_10"></div>
<!--                    <div class="tools textArea ">-->
<!--                        <textarea style="font-size: 15px; line-height: 1.5" id="Js_release_agreement" readonly>-->
<!--                            申请/邀请加入创意世界智库候选人协议-->
<!--                            申请/邀请成为创意世界智库候选人，您（包括提出申请人，发出邀请人，被邀请人）应时刻遵守创意世界注册协议，并应仔细阅读并确认同意申请/邀请加入创意世界智库候选人协议（即本协议，以下简称智库候选人协议）的各项约定，且保证遵照执行：-->
<!--                            1 合作背景-->
<!--                            创意世界网站是一个可以通过各种创意，创新的方法帮助人们实现任何需求的创意平台，创意世界智库作为平台的核心组成部分，通过为用户（创意人士，创新人士，企业，其他社会团体）提供专家点评，推荐等专业权威的服务，帮助用户更快更好的实现需求。-->
<!--                            创意世界将定期审核创意世界智库候选人资格，创意世界将根据审核结果，保留创意世界智库候选人资格或提升为创意世界智库专家。-->
<!--                            2 合作内容-->
<!--                            创意世界智库将为创意世界智库专家提供一个专业，权威，高端的展示平台，拥有专家身份的用户在创意世界网站各个版块拥有特殊的身份标识，众多的展示机会，拥有专属的专家空间，在创意世界智库拥有仅限专家范围的专属交流区。其他的创意世界用户可以通过付出邀请奖励，向专家发起项目，比赛，任务，交易点评的邀请，  专家也可以主动针对用户的项目，比赛，任务，交易发起点评和推荐。-->
<!--                            创意世界将在今后的运营过程中，增加更多的合作方式以及更多的专家可享受的权益。-->
<!--                            3 合作双方的权利和义务-->
<!--                            创意世界智库为专家提供了与用户互动的平台，同时专家在创意世界网站上要保证高质量地回答网友们提出的相关问题。双方的权利和义务如下：-->
<!--                            专家：-->
<!--                            	创意世界智库专家应保证在专家点评和专家交流区中不得散布任何形式的广告，一经发现，创意世界智库有权直接清理广告内容，情节严重者，创意世界智库将会电话通知专家，并有权取消专家资格。-->
<!--                            	创意世界智库专家的权利、义务，参见站内的“创意世界智库简介”页面内容。创意世界智库有权随时对此页面内容进行调整及修改，创意世界智库专家需同意遵循调整或修改后的页面内容，如若不接受应当立即停止使用创意世界智库的服务，您继续使用创意世界的服务，即表示您接受经修订的协议和规则。-->
<!--                            	创意世界智库专家必须提供相应的身份资料（详见专家申请流程页面，此流程页面内容构成本协议的组成部分，与本协议具有同等法律效力。创意世界智库有权对此进行修改，专家应遵循创意世界智库修改后的要求）；创意世界智库专家应依创意世界智库的需要补充提供相应资料。-->
<!--                            	专家应保证向创意世界智库提供的资料及时、最新、准确、完整并有效。如专家提供的资料信息有任何变更的，应及时通知创意世界智库。若专家提供的资料违反本款规定的，专家应承担一切责任，如给创意世界智库造成损失的，专家并应负责赔偿。-->
<!--                            	创意世界智库专家保证其有权或有合法授权向创意世界智库提供并授权创意世界智库使用本协议项下创意世界智库要求之资料、文件或企业商标标识、企业logo、专家肖像及专家在创意世界智库内的言论等。若违反此款规定的，创意世界智库专家应承担由此给创意世界智库带来的一切损失，包括由此给创意世界智库导致的第三方诉讼或赔偿要求等。-->
<!--                            	创意世界智库有权对创意世界智库专家提供的各项信息在公开发布前进行审核，若发现有违法、违规的情形或不符合创意世界智库的要求的，有权要求专家对信息进行补充或修改。-->
<!--                            	创意世界智库专家无权更改提供给创意世界智库的各项资料，如需变更或应创意世界智库要求进行更新时，专家须通过电话方式联系创意世界智库，由创意世界智库审核后并由管理员进行后台进行修改。-->
<!--                            	创意世界智库专家应保证对项目，比赛，任务，交易的点评和推荐等须是与专题密切相关的、具有相应的专家知识及观点的内容。-->
<!--                            	创意世界智库专家对项目，比赛，任务，交易的点评和推荐等各种言论不得含有违法、违反创意世界智库规则或规定、含有不良内容、或恶意攻击的言论，并不得给创意世界智库造成任何的不良影响。违反此款规定的，创意世界智库有权终止专家资质并终止合作；因此给创意世界智库造成损失的，创意世界智库专家应承担赔偿的责任。-->
<!--                            	未经创意世界智库事先书面审核同意，创意世界智库专家不得转让其专家身份，不得将专家账号提供给其他人使用。创意世界智库专家保证对其在创意世界智库中的一切言论独立对创意世界智库用户承担一切责任，若因其言论给创意世界智库或创意世界智库用户造成损失的，专家应承担全部赔偿责任。-->
<!--                            	创意世界智库对产品进行调整时，专家应配合创意世界智库的调整。-->
<!--                            	创意世界智库专家在此同意并确认，本协议项下合作终止后，创意世界智库仍可将专家已经在创意世界智库上使用及发布的专家信息内容继续保留在创  意世界网站页面中。-->
<!--                            创意世界智库：-->
<!--                            	创意世界智库可定期在创意世界智库首页中推荐专家，显示专家名称、头像；推荐的时间、期间及具体位置可由创意世界智库自行决定。专家应严格遵循“创意世界智库简介”及其他一切创意世界智库对于创意世界智库专家的规则及规定（创意世界智库并有权随时发布、更改或取消适用于创意世界智库专家的规则及规定并以邮件方式通知或在创意世界智库网上进行公示，专家应予遵守）；若专家违反此款规定，创意世界智库对此类违规的专家可直接进行停权操作。若专家的违规操作给创意世界智库造成损失的，专家应承担全部赔偿责任，包括但不限于因此给创意世界智库其他用户造成的损失。-->
<!--                            	创意世界智库有权对专家提交的各项资料（包括更改的资料）进行审核。创意世界智库并有权（但非创意世界智库义务）向专家单位或原单位核实专家提供的资料及各项情况。-->
<!--                            	专家在需要修改资料时，需向创意世界智库进行申请，创意世界智库有权审核合作专家更改或更新的资料，并进行相应的修改操作。-->
<!--                            	对于专家提供的一切资料（包括但不限于专家提供的一切文字、图形、文档、专家企业信息、专家的真实姓名、专家的照片、合作专家企业logo、及专家言论等），创意世界智库有权在且仅在创意世界智库内以创意世界智库需要的方式进行展示（创意世界智库有权自行决定以上资料的展现方式）。-->
<!--                            	创意世界智库有权依其产品或业务需要自行决定中止或终止提供专家服务。在此情形下，创意世界智库应通知专家该服务的中止或终止，合作专家应予遵循。-->
<!--                            4 保密原则-->
<!--                            专家对从创意世界智库得知的创意世界智库的保密信息负有保密义务，不得向任何第三方披露。 保密条款不因本协议的无效、解除、提前终止或不具操作性而失效。-->
<!--                            5 违约及协议终止-->
<!--                            专家违反本协议约定，导致因对第三人的侵权、伤害或损害而引起对创意世界智库提出的赔偿请求、诉讼或其他程序，专家应负担费用保护、保障及赔偿创意世界智库不受损害。-->
<!--                            专家在未获创意世界智库的事先书面同意前不得与第三人达成任何和解或协议。专家同意支付创意世界智库因此而发生的包括律师费在内的所有合理费用；创意世界智库并有权以邮件通知专家的形式终止本协议项下的合作。-->
<!--                            一旦专家违约，创意世界智库有权选择限期要求专家纠正违约行为或立即终止本协议，并要求专家承担违约赔偿责任。-->
<!--                            专家如果在创意世界智库中发布任何违法、违规内容，创意世界智库有权停止专家在创意世界智库内的一切活动，并要求专家赔偿相应损失。-->
<!--                            除依本协议其他规定外，创意世界智库有权依其产品或业务需要经书面通知专家而中止或终止本协议项下专家服务。-->
<!--                            本协议项下合作终止后，创意世界智库仍可将专家已经在创意世界智库上使用及发布的-->
<!--                            专家的信息内容继续保留在创意世界智库网站页面中。-->
<!--                            6 其他-->
<!--                            本协议一经专家向创意世界智库提交其申请即为有效，至创意世界智库依本协议约定终止本协议项下合作时止。-->
<!--                            创意世界智库得以向您发送通知，以对本协议进行修改。通知方式可为通过邮箱，普通信件或从创意世界智库网站上发布公告。-->
<!--                            本协议，包括创意世界服务条款、创意世界智库使用规范及其他被整合进本协议的其他条款，构成全部专家与创意世界智库关于本协议项下专家服务的全部协议。如果本协议中的条款与创意世界服务条款、创意世界智库使用规范及其他被整合进本协议的其他条款相矛盾，以本协议中的约定为准，除非创意世界服务条款、创意世界智库使用规范及其他被整合进本协议的其他条款对专家的行为施加了额外的限制及义务。-->
<!--                            本协议及专家与创意世界智库间的法律关系应当受中华人民共和国法律管辖。创意世界智库专家与创意世界智库同意将因本协议产生的一切争议提交创意世界智库运营方创意世界科技有限公司所在地的人民法院管辖。创意世界智库未行使本协议中规定的权利或相关条款并不视为创意世界智库对此权利或条款的放弃。如果本协议中的任何条款被有管辖权的法院认定为无效，专家与创意世界智库仍然希望法院执行双方在协议中表明的意图，本协议中的其他条款及“创意世界服务条款”仍将有效。-->
<!--                        </textarea>-->
<!--                    </div>-->

                </div>

                <div class="p-warp">
                        <a href="javascript:;" class="grayBtn mr5 js_detailPublish" style="fl">取消申请</a>
                        <a href="javascript:;" class="blueBtn b73 Js_apply_ok" id="Js_apply_thinktank_candidate">确认申请</a>
                </div>
                <div class="height_48"></div>
                <div class="sp-line" style="top:0; margin:0;">
                        <div class="line-left"></div>
                        <div class="line-right"></div>
                </div>
        </div>
		</form>
</div>

<div class="detailPageMidDiv">
<div class="detailPageMid">
        <div class="proContextDiv">
                <div class="titDiv">
                        <span></span>
                        <h2 class="Js_scroll">创意世界智库简介</h2>
                </div>
                <div class="height_30"></div>

                <div class="clearfix whyJoinThinktank">
                        <dl>
                                <dt>社会价值</dt>
                                <dd>为全世界创意领袖创造了一个交易创意、分享创意、激发创意的创新平台。为全世界领导者提供了一个在创意世界中学习与分享的平等舞台。</dd>
                        </dl>
                        <dl>
                                <dt>任务使命</dt>
                                <dd>为全世界创意领袖创造了一个交易创意、分享创意、激发创意的创新平台。为全世界领导者提供了一个在创意世界中学习与分享的平等舞台。</dd>
                        </dl>
                        <dl>
                                <dt>智库交流</dt>
                                <dd>为全世界创意领袖创造了一个交易创意、分享创意、激发创意的创新平台。为全世界领导者提供了一个在创意世界中学习与分享的平等舞台。</dd>
                        </dl>
                        <dl>
                                <dt>领袖专栏</dt>
                                <dd>为全世界创意领袖创造了一个交易创意、分享创意、激发创意的创新平台。为全世界领导者提供了一个在创意世界中学习与分享的平等舞台。</dd>
                        </dl>
                        <dl>
                                <dt>比赛点评</dt>
                                <dd>为全世界创意领袖创造了一个交易创意、分享创意、激发创意的创新平台。为全世界领导者提供了一个在创意世界中学习与分享的平等舞台。</dd>
                        </dl>
                </div>
        </div>
</div>

<!--智库资讯-->
<div class="detailPageMid">
        <div class="proContextDiv">
            <div class="rowblock">

                <dl class="clearfix classify left">
<!--                    <dt style="line-height:36px; font-family: ">按区域分类</dt>-->
                </dl>
                <div class="clearfix left">
                    <div class="optDiv Js_state left mr10" style="width:110px;">
                        <div class="tools text pr" style="width:110px;">
                            <div class="inputWarp opt">
                                <span style="font-size:14px;" class="shs">全球</span>
                                <a href="javascript:void(0)" class="showMenu"></a>
                            </div>
                        </div>
                        <ul id="Js_area_global">
                            <li><a href="javascript:void(0)">全球</a></li>
                            <li><a href="javascript:void(0)">中国</a></li>
                        </ul>
                    </div>
                    <div class="optDiv Js_state left mr10" style="width:110px; display:none;">
                        <div class="tools text pr" style="width:110px;">
                            <div class="inputWarp opt">
                                <span style="font-size:14px;" class="shs">所在省</span>
                                <a href="javascript:void(0)" class="showMenu"></a>
                            </div>
                        </div>
                        <ul></ul>
                        <select id="b1" style="display:none;">
                            <option>数据加载中...</option>
                        </select>
                    </div>
                    <div class="optDiv Js_state left mr10" style="width:110px; display:none" id="Js_b2">
                        <div class="tools text pr" style="width:110px;">
                            <div class="inputWarp opt">
                                <span style="font-size:14px;" class="shs">所在市</span>
                                <a href="javascript:void(0)" class="showMenu"></a>
                            </div>
                        </div>
                        <ul></ul>
                        <select id="b2" style="display:none;">
                            <option>数据加载中...</option>
                        </select>
                    </div>
                    <div class="optDiv Js_state left" style=" min-width:110px; width:auto; display:none;" id="Js_b3">
                        <div class="tools text pr" style="width:auto;">
                            <div class="inputWarp opt">
                                <span style="font-size:14px;" class="shs">所在区/县</span>
                                <a href="javascript:void(0)" class="showMenu"></a>
                            </div>
                        </div>
                        <ul></ul>
                        <select id="b3" style="display:none;">
                            <option>数据加载中...</option>
                        </select>
                    </div>
                </div>
                <div class="height_20"></div>

            </div>
                <div class="titDiv">
                        <span></span>
                        <h2 class="Js_scroll">成都 创意世界智库资讯</h2>
                </div>
                <div class="height_22"></div>
                <ul class="clearfix l-areaNewsTit">
                        <li><a href="#">全部新闻</a></li>
                        <li><a href="#">专家专栏</a></li>
                        <li class="last"><a href="#">专家观点</a></li>
                </ul>

                <div class="clearfix">
                        <div class="wheelShow">
                                <div class="imgShow">
                                        <div id="Js_img_show" style="overflow:hidden">
                                                <div class="clearfix" style="width:1800px;">
                                                        <a href="#"><img src="./assets/temp/13.png" alt="" style="float:left; position:relative;" /></a>
                                                        <a href="#"><img src="./assets/temp/13.png" alt="" style="float:left;" /></a>
                                                        <a href="#"><img src="./assets/temp/13.png" alt="" style="float:left;" /></a>
                                                        <a href="#"><img src="./assets/temp/13.png" alt="" style="float:left;" /></a>
                                                        <a href="#"><img src="./assets/temp/13.png" alt="" style="float:left;" /></a>
                                                </div>
                                        </div>
                                        <div class="tmBox"></div>
                                        <span class="txt current Js_show_text">地震之后四川依然美丽</span>
                                        <span class="txt Js_show_text">地震之后四川依然美丽1</span>
                                        <span class="txt Js_show_text">地震之后四川依然美丽2</span>
                                        <span class="txt Js_show_text">地震之后四川依然美丽3</span>
                                        <span class="txt Js_show_text">地震之后四川依然美丽4</span>
                                </div>
                                <p class="eventImgShow">
                                        <a href="#" class="current Js_imgShow_item"></a>
                                        <a href="#" class="Js_imgShow_item"></a>
                                        <a href="#" class="Js_imgShow_item"></a>
                                        <a href="#" class="Js_imgShow_item"></a>
                                        <a href="#" class="Js_imgShow_item"></a>
                                </p>
                        </div>
                        <div class="midNews thinktank">
                                <div class="m_news">
                                        <h2><a href="news.php">成都公积金贷款新政：首套房认定</a></h2>
                                        <p class="clearfix">
                                                [<a href="news.php">7月10日起执行</a>]
                                                [<a href="news.php">成都全域内再买房都算二套</a>]
                                        </p>
                                </div>
                                <div class="m_news">
                                        <h2><a href="news.php">四川多地暴雨 成都今晨"看海"</a></h2>
                                        <p class="clearfix">
                                                [<a href="news.php">专题</a>]
                                                [<a href="news.php">强降水将持续至今晚</a>]
                                                [<a href="news.php">寻遗失车牌车主</a>]
                                        </p>
                                </div>
                                <ul>
                                        <li>
                                                <dl class="clearfix">
                                                        <dt><a href="news.php">雨情</a></dt>
                                                        <dd><a href="news.php">今晨成都发生雷电11064次</a></dd>
                                                </dl>
                                        </li>
                                        <li>
                                                <dl class="clearfix">
                                                        <dt><a href="news.php">现场</a></dt>
                                                        <dd><a href="news.php">背老人和学生"过河"(图)</a></dd>
                                                </dl>
                                        </li>
                                        <li>
                                                <dl class="clearfix">
                                                        <dt><a href="news.php">交通</a></dt>
                                                        <dd><a href="news.php">宝成线5趟列车晚点</a></dd>
                                                </dl>
                                        </li>
                                        <li>
                                                <dl class="clearfix">
                                                        <dt><a href="news.php">灾情</a></dt>
                                                        <dd><a href="news.php">泥石流卷走六旬老人(图)</a></dd>
                                                </dl>
                                        </li>
                                </ul>
                        </div>

                        <div class="rightNews thinktankRN">
                                <div class="clearfix m_news height_96">
                                        <h2><a href="#" style="font-size:16px;">成都人民公园新地下停车场建成(图)</a></h2>
                                        <div class="tw_composing thinktankTxt">
                                                <a href="news.php"><img src="./assets/temp/14.png" alt="" /></a>
                                                <p>省政府20日印发《关于进一步加强最低生活保障工作的实施意见》提出，研究制定全省统一的最低生活。</p>
                                        </div>
                                </div>
                                <ul class="newsList">
                                        <li><a href="news.php">网曝宜宾一客运车辆严重超载(图)</a></li>
                                        <li><a href="news.php">网友称乐山大佛风华严重 呼吁保护</a></li>
                                        <li><a href="news.php">交通部：未来只有3%公路里程收费</a></li>
                                        <li><a href="news.php">蜀绣被授予国家地理标志保护产品</a></li>
                                </ul>
                        </div>
                </div>
                <ul class="clearfix thinktankNewsList">
                        <li class="one">
                                <p class="newsTit"><a href="news.php">改革跑赢危机的行动路线</a></p>
                                <div class="clearfix newsContext">
                                        <a href="#"><img src="./assets/temp/15.png" alt="" /></a>
                                        <p>释放人口城镇化的最大潜力，关键在于以市场化改革为最大红利，形成以拉动消费支撑7%-8%中速增长的体制格局释放人口城镇释放人口城镇化的最大潜力。</p>
                                </div>
                        </li>
                        <li class="two">
                                <p class="newsTit"><a href="#">双转型中的中国外交</a></p>
                                <div class="clearfix newsContext">
                                        <a href="#"><img src="./assets/temp/16.png" alt="" /></a>
                                        <p>双转型的中国外交过程注定会充满不确定性：世界在调整对中国的期待；中国在适应自己的新角色。对双方来是巨大的，在外部。</p>
                                </div>
                        </li>
                        <li class="three">
                                <p class="newsTit"><a href="#">欧洲需更成熟的干预主义</a></p>
                                <div class="clearfix newsContext">
                                        <a href="#"><img src="./assets/temp/17.png" alt="" /></a>
                                        <p>欧洲目前需要的是更为成熟的国家干预主义，即在国家干预上将审慎性与灵活性结合，既不对国家干预持迷信态度，又能够根据经济发展的不同状。</p>
                                </div>
                        </li>
                </ul>
        </div>
</div>
<!--比赛点评-->
<?php //$expertTT_NOJS = "比赛点评";?>
<?php //include("./modules/expert-comment-nojs.php"); ?>
<!--社区论坛开始-->
<?php $forumTT = "成都 创意世界智库交流区"; //论坛标题名称变量 ?>
<?php include("./modules/discuss.php"); ?>
<!--社区论坛结束-->

<!--智库专家-->
<div class="detailPageMid" style="padding-bottom:0;">
    <div class="proContextDiv">
        <div class="titDiv">
            <span></span>
            <h2 class="Js_scroll">成都 创意世界智库专家</h2>
        </div>
        <div class="height_31"></div>
        <div class="thinkExpert">
<!--            <dl class="clearfix classify left">-->
<!--                <dt style="line-height:36px;">按区域分类</dt>-->
<!--            </dl>-->
<!--            <div class="clearfix left">-->
<!--                <div class="optDiv Js_state left mr10" style="width:110px;">-->
<!--                    <div class="tools text pr" style="width:110px;">-->
<!--                        <div class="inputWarp opt">-->
<!--                            <span style="font-size:14px;" class="shs">全球</span>-->
<!--                            <a href="javascript:void(0)" class="showMenu"></a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <ul id="Js_area_global">-->
<!--                        <li><a href="javascript:void(0)">全球</a></li>-->
<!--                        <li><a href="javascript:void(0)">中国</a></li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--                <div class="optDiv Js_state left mr10" style="width:110px; display:none;">-->
<!--                    <div class="tools text pr" style="width:110px;">-->
<!--                        <div class="inputWarp opt">-->
<!--                            <span style="font-size:14px;" class="shs">所在省</span>-->
<!--                            <a href="javascript:void(0)" class="showMenu"></a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <ul></ul>-->
<!--                    <select id="t1" style="display:none;">-->
<!--                        <option>数据加载中...</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--                <div class="optDiv Js_state left mr10" style="width:110px; display:none" id="Js_t2">-->
<!--                    <div class="tools text pr" style="width:110px;">-->
<!--                        <div class="inputWarp opt">-->
<!--                            <span style="font-size:14px;" class="shs">所在市</span>-->
<!--                            <a href="javascript:void(0)" class="showMenu"></a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <ul></ul>-->
<!--                    <select id="t2" style="display:none;">-->
<!--                        <option>数据加载中...</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--                <div class="optDiv Js_state left" style=" min-width:110px; width:auto; display:none;" id="Js_t3">-->
<!--                    <div class="tools text pr" style="width:auto;">-->
<!--                        <div class="inputWarp opt">-->
<!--                            <span style="font-size:14px;" class="shs">所在区/县</span>-->
<!--                            <a href="javascript:void(0)" class="showMenu"></a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <ul></ul>-->
<!--                    <select id="t3" style="display:none;">-->
<!--                        <option>数据加载中...</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="height_20"></div>-->
            <dl class="clearfix classify">
                <dt>按职务分类</dt>
                <dd><a href="javascript:;" class="Js_thinktank_filter">创意世界智库主席团</a></dd>
                <dd><a href="javascript:;" class="Js_thinktank_filter">创意世界智库理事会</a></dd>
                <dd><a href="javascript:;" class="Js_thinktank_filter">创意世界智库专家顾问团</a></dd>
            </dl>
            <div class="height_10"></div>
            <div class="clearfix">
                <dl class="clearfix classify trade">
                    <dt>按行业分类</dt>
                    <dd><a href="javascript:;" class="Js_thinktank_filter">全部行业</a></dd>
                    <dd><a href="javascript:;" class="Js_thinktank_filter">科学家</a></dd>
                    <dd><a href="javascript:;" class="Js_thinktank_filter">艺术家</a></dd>
                    <dd><a href="javascript:;" class="Js_thinktank_filter">企业家</a></dd>
                    <dd><a href="javascript:;" class="Js_thinktank_filter">慈善家</a></dd>
                    <dd><a href="javascript:;" class="Js_thinktank_filter">投资家</a></dd>
                </dl>
                <a href="javascript:;" class="lookAll Js_thinktank_all">查看全部</a>
            </div>
            <div>
                <ul class="clearfix expertList Js_thinktank_zj">
                    <li>
                        <a href="javascript:;"><img src="./assets/temp/tp5.jpg" alt="" /></a>
                        <dl>
                            <dt><a href="javascript:;" class="Js_visitCard">孙正义</a></dt>
                            <dd><a href="javascript:;">新闻集团核心大股东，董事长兼行政总裁</a></dd>
                        </dl>
                    </li>
                    <li>
                        <a href="javascript:;"><img src="./assets/temp/tp6.jpg" alt="" /></a>
                        <dl>
                            <dt><a href="javascript:;" class="Js_visitCard">孙正义</a></dt>
                            <dd><a href="javascript:;">新闻集团核心大股东，董事长兼行政总裁</a></dd>
                        </dl>
                    </li>
                    <li>
                        <a href="javascript:;"><img src="./assets/temp/tp7.jpg" alt="" /></a>
                        <dl>
                            <dt><a href="javascript:;" class="Js_visitCard">孙正义</a></dt>
                            <dd><a href="javascript:;">新闻集团核心大股东，董事长兼行政总裁</a></dd>
                        </dl>
                    </li>
                    <li>
                        <a href="javascript:;"><img src="./assets/temp/tp8.jpg" alt="" /></a>
                        <dl>
                            <dt><a href="javascript:;" class="Js_visitCard">孙正义</a></dt>
                            <dd><a href="javascript:;">新闻集团核心大股东，董事长兼行政总裁</a></dd>
                        </dl>
                    </li>
                    <li>
                        <a href="javascript:;"><img src="./assets/temp/tp5.jpg" alt="" /></a>
                        <dl>
                            <dt><a href="javascript:;" class="Js_visitCard">孙正义</a></dt>
                            <dd><a href="javascript:;">新闻集团核心大股东，董事长兼行政总裁</a></dd>
                        </dl>
                    </li>
                    <li>
                        <a href="javascript:;"><img src="./assets/temp/tp6.jpg" alt="" /></a>
                        <dl>
                            <dt><a href="javascript:;" class="Js_visitCard">孙正义</a></dt>
                            <dd><a href="javascript:;">新闻集团核心大股东，董事长兼行政总裁</a></dd>
                        </dl>
                    </li>
                    <li>
                        <a href="javascript:;"><img src="./assets/temp/tp7.jpg" alt="" /></a>
                        <dl>
                            <dt><a href="javascript:;" class="Js_visitCard">孙正义</a></dt>
                            <dd><a href="javascript:;">新闻集团核心大股东，董事长兼行政总裁</a></dd>
                        </dl>
                    </li>
                    <li>
                        <a href="javascript:;"><img src="./assets/temp/tp8.jpg" alt="" /></a>
                        <dl>
                            <dt><a href="javascript:;" class="Js_visitCard">孙正义</a></dt>
                            <dd><a href="javascript:;">新闻集团核心大股东，董事长兼行政总裁</a></dd>
                        </dl>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

</div>

</div>

</div>
<script type="text/javascript">
    city_select_.id = '090000'
    city_select_.init('b1','b2','b3',callback);//城市联动
    function callback(ev){};
    $('#Js_area_global li').click(function(){
        var defaultarr = ['所在省','所在市','所在区/县'];
        $('#Js_area_global').hide();
        var o = $(this).parents('.Js_state').nextAll().find('.tools');
        if($(this).text()=="全球"){
            o.each(function(a,b){
                $(this).hide();
            })
        }else{
            o.each(function(a,b){
                $(this).parents('.Js_state').show();
                $(this).show();
            })
            city_select_.id = '090000'
            city_select_.init('b1','b2','b3',callback);//城市联动
        }
        return false;
    })
</script>