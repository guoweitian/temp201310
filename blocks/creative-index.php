<div class="content">
        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="height_38"></div>
                        <div class="titDiv">
                                <span></span>
                                <h2>创意指数</h2>
                        </div>
                        <div class="height_56"></div>
                        <div>
                                <div class="clearfix">
                                        <div style="width:610px; height:184px; float:left; position:relative; border-bottom:2px solid #abae8f">
                                                <img src="./assets/temp/tz.jpg" alt="" />
                                        </div>
                                        <div class="creative clearfix">
                                                <div class="creativeDiv clearfix">
                                                        <span>1,234,567,890<small>点</small></span>
                                                        <label>创意指数</label>
                                                </div>
                                                <div class="creativeDiv clearfix mb22">
                                                        <span>LV.99</span>
                                                        <label style="padding-top: 1px">创意等级</label>
                                                </div>
                                                <div class="w-acz clearfix">
                                                        <span class="w-num">未领取的创意指数奖励</span>
                                                        <a href="javascript:;" class="l-m_btn right Js_receive_create">立即领取</a>
                                                </div>
                                                <div class="creativeDiv clearfix">
                                                        <span>1,234,567,890<small>枚</small></span>
                                                        <s>创创币</s>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="height_50"></div>
                        <div>
                                <label class="l-start">起止时间：</label>
                                <div class="tools text left" style="width:148px;">
                                        <input class="datebox bbit-dp-input" type="text" id="dateone" readonly="readonly" style="width:144px !important;" data-type="history">
                                </div>
                                <s class="time-fh"></s>
                                <div class="tools text left" style="width:148px;">
                                        <input class="datebox bbit-dp-input" type="text" id="datetwo" readonly="readonly" style="width:144px !important;" data-type="history">
                                </div>
                                <ul class="clearfix l-areaNewsTit userfile">
                                        <li><a href="javascript:;" class="Js_time_create_filter" data-type="2013/10/27">今天</a></li>
                                        <li><a href="javascript:;" class="current Js_time_create_filter" data-type="2013/9/27">最近1个月</a></li>
                                        <li><a href="javascript:;" class="Js_time_create_filter" data-type="2013/7/27">3个月</a></li>
                                        <li><a href="javascript:;" class="Js_time_create_filter" data-type="2012/10/27">1年</a></li>
                                        <li class="last"><a href="javascript:;" class="Js_time_create_filter" data-type="2012/10/27">1年前</a></li>
                                </ul>
                        </div>
                        <div class="height_38"></div>
                        <div>
                                <div class="clearfix">
                                        <ul class="clearfix l-areaNewsTit userfile left" style="line-height:normal;">
                                                <li><a href="javascript:;" class="current Js_zs_create_filter">全部</a></li>
                                                <li><a href="javascript:;" class="Js_zs_create_filter">增加指数</a></li>
                                                <li class="last"><a href="javascript:;" class="Js_zs_create_filter">减少指数</a></li>
                                        </ul>
                                        <div class="pageItemDiv right">
                                                <a href="javascript:;" class="fyBtn prev Js_page_list_prev Js_createindex_account"></a>
						<span class="pageItem cfs19 Js_page_list_itemNum">
							<a href="javascript:;" class="current Js_createindex_account">1</a>
							<a href="javascript:;" class="Js_createindex_account">2</a>
							<a href="javascript:;" class="Js_createindex_account">3</a>
							<a href="javascript:;" class="Js_createindex_account">4</a>
							<a href="javascript:;" class="Js_createindex_account">5</a>
						</span>
                                                <a href="javascript:;" class="fyBtn next Js_page_list_next Js_createindex_account"></a>
                                        </div>
                                </div>

                                <div class="height_27"></div>
                                <div>
                                        <!--<div class="clearfix t-title">
                                            <span class="time t-creative">时间</span>
                                            <span class="creative">内容</span>
                                            <span class="c-change">指数变化</span>
                                            <span>创意指数</span>
                                        </div>-->
                                        <div>
                                                <table width="940" border="0" cellpadding="0" cellspacing="0" class="t-detail Js_createIndex_account_list">
                                                        <tr style="background:#deded9">
                                                                <td class="td-time c-time">时间</td>
                                                                <td class="td-creative">内容</td>
                                                                <td class="td-change green" style="font-size:14px; color:#5F5F5F">指数变化</td>
                                                                <td class="td-index" style="font-size:14px; color:#5F5F5F">创意指数</td>
                                                        </tr>
                                                        <tr>
                                                                <td class="td-time c-time">2013/01/25 22:20:22</td>
                                                                <td class="td-creative">竞猜中奖</td>
                                                                <td class="td-change green">+130</td>
                                                                <td class="td-index">1,234,580</td>
                                                        </tr>
                                                        <tr>
                                                                <td class="td-time c-time">2013/01/25 21:20:22</td>
                                                                <td class="td-creative">竞猜中奖</td>
                                                                <td class="td-change green">+2230</td>
                                                                <td class="td-index">1,234,550</td>
                                                        </tr>
                                                        <tr>
                                                                <td class="td-time c-time">2013/01/25 20:20:22</td>
                                                                <td class="td-creative">竞猜中奖</td>
                                                                <td class="td-change green">+2330</td>
                                                                <td class="td-index">1,234,520</td>
                                                        </tr>
                                                        <tr>
                                                                <td class="td-time c-time">2013/01/25 19:20:22</td>
                                                                <td class="td-creative">竞猜</td>
                                                                <td class="td-change green">+230</td>
                                                                <td class="td-index">1,234,490</td>
                                                        </tr>
                                                        <tr>
                                                                <td class="td-time c-time">2013/01/25 18:20:22</td>
                                                                <td class="td-creative">竞猜中奖</td>
                                                                <td class="td-change red">-140</td>
                                                                <td class="td-index">1,234,460</td>
                                                        </tr>
                                                        <tr>
                                                                <td class="td-time c-time">2013/01/25 17:20:22</td>
                                                                <td class="td-creative">竞猜中奖</td>
                                                                <td class="td-change green">+30</td>
                                                                <td class="td-index">1,234,500</td>
                                                        </tr>
                                                </table>
                                        </div>
                                        <div class="height_40"></div>
                                        <div class="pageItemDiv right">
                                                <a href="javascript:;" class="fyBtn prev Js_page_list_prev Js_createindex_account"></a>
						<span class="pageItem cfs19 Js_page_list_itemNum">
							<a href="javascript:;" class="current Js_createindex_account">1</a>
							<a href="javascript:;" class="Js_createindex_account">2</a>
							<a href="javascript:;" class="Js_createindex_account">3</a>
							<a href="javascript:;" class="Js_createindex_account">4</a>
							<a href="javascript:;" class="Js_createindex_account">5</a>
						</span>
                                                <a href="javascript:;" class="fyBtn next Js_page_list_next Js_createindex_account"></a>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
<script type="text/javascript">
(function(){
	var d = new Date();
	var dy = d.getFullYear();
	var dm = d.getMonth()+1;
	var dd = d.getDate();
	$('#dateone').val(dy + '/' + (dm-1) + '/' + dd);
	$('#datetwo').val(dy + '/' + dm + '/' + dd);
	dataControl('dateone','datetwo');
})()
</script>