<div class="content">
<div class="detail-wrap hiring">
<div class="detail-imgbg">
        <div class="toppattern"></div>
        <div class="detailTopDiv pew">
                <h3>这里是比赛名称，如果有很多字，这里也只能显示一行标题。这里是比赛名称，如果有很多字，这里也只能显示一行标题。</h3>
                <p class="disDiv">
                        <span class="loc">成都市青羊区小南街</span>
                        <span class="time">已结束</span>
                        <span class="m_num">C2013052400021</span>
                </p>
                <div>
                        <div class="u_listItem left">
                                <a href="../user/home.php" class="leftArea shadow">
                                        <img src="./assets/temp/5.png" alt="">
                                </a>
                                <div class="rightArea w600">
                                        <p class="rightName">
                                                <a class="Js_visitCard cfff" href="../user/home.php">刘兆宇</a>
                                        </p>
                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                </div>
                        </div>
                    <div class="tz_moneyDiv right">
                        <div class="p-ok"> <!--成功是p-ok 结束是p-no-->
                            <p style="padding-top:2px;">比赛总奖金:<b>10,000</b>元</p>
                            <p style="padding-bottom:7px;">共<b style="margin-left: 6px;">4</b>位获奖明星</p>
                        </div>
                    </div>
                </div>
        </div>
</div>

<div class="detailPageMidDiv">
<!--获胜公益明星（新） 开始-->
<div class="detailPageMid" style="padding-bottom: 0;">
    <div class="proContextDiv">
        <div class="titDiv">
            <span></span>
            <h2>比赛奖励</h2>
        </div>
        <div class="hire-awards" style="padding-top: 18px">
            <span><b>头衔</b><b>奖金</b><b>获奖者</b></span>

        </div>
    </div>

</div>

<div class="detailPageMid Js_select_winner_listItem" style="padding-top:0; padding-bottom: 10px;">
    <div class="proContextDiv">
        <div class="hire-awards">
            <div class="hire-awards-tittle"><span>最上镜创意明星</span><span><em>10000</em><i>元</i></span>
                <div class="hire-awards-listItem">
                    <a href="#" class="leftArea shadow">
                        <img src="./assets/temp/5.png" alt="">
                    </a>
                    <div class="rightArea">
                        <div class="rightName"><a href="#" class="jury_id Js_visitCard">边路推土机</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="detailPageMid Js_select_winner_listItem" style="padding-top:0; padding-bottom: 10px;">
    <div class="proContextDiv">
        <div class="hire-awards">
            <div class="hire-awards-tittle"><span>最佳才艺创意明星</span><span><em>9000</em><i>元</i></span>
                <div class="hire-awards-listItem">
                    <a href="#" class="leftArea shadow">
                        <img src="./assets/temp/4.png" alt="">
                    </a>
                    <div class="rightArea">
                        <div class="rightName"><a href="#" class="jury_id Js_visitCard">我的名字有好长好长</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="detailPageMid Js_select_winner_listItem" style="padding-top:0; padding-bottom: 10px;">
    <div class="proContextDiv">
        <div class="hire-awards">
            <div class="hire-awards-tittle"><span>最佳人气创意明星</span><span><em>8000</em><i>元</i></span>
                <div class="hire-awards-listItem">
                    <a href="#" class="leftArea shadow">
                        <img src="./assets/temp/5.png" alt="">
                    </a>
                    <div class="rightArea">
                        <div class="rightName"><a href="#" class="jury_id Js_visitCard">边路推土机</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="detailPageMid Js_select_winner_listItem" style="padding-top:0; padding-bottom: 10px;">
    <div class="proContextDiv">
        <div class="hire-awards">
            <div class="hire-awards-tittle"><span>最佳博爱创意明星</span><span><em>7000</em><i>元</i></span>
                <div class="hire-awards-listItem">
                    <a href="#" class="leftArea shadow">
                        <img src="./assets/temp/4.png" alt="">
                    </a>
                    <div class="rightArea">
                        <div class="rightName"><a href="#" class="jury_id Js_visitCard">我的名字有好长好长</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="detailPageMid" style="padding-top: 0; padding-bottom: 10px;">
    <div class="proContextDiv">
        <div class="context">其他明星奖励/非物质奖励</div>
    </div>
</div>
<!--获胜公益明星（新） 结束-->

<!--获胜公益明星（旧） 开始-->
<!--<div class="detailPageMid">-->
<!--        <div class="proContextDiv">-->
<!--                <div class="titDiv">-->
<!--                        <span></span>-->
<!--                        <h2>获胜公益明星</h2>-->
<!--                </div>-->
<!--                <ul class="clearfix u_list">-->
<!--                        <li class="left">-->
<!--                                <div class="u_listItem">-->
<!--                                        <a href="#" class="leftArea shadow">-->
<!--                                                <img src="./assets/temp/5.png" alt="">-->
<!--                                        </a>-->
<!--                                        <div class="rightArea h125">-->
<!--                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>-->
<!--                                                <p class="rightList"><a href="#">中国四川成都</a></p>-->
<!--                                                <p class="hWinner">最佳上镜明星</p>-->
<!--                                        </div>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                        <li class="left">-->
<!--                                <div class="u_listItem">-->
<!--                                        <a href="#" class="leftArea shadow">-->
<!--                                                <img src="./assets/temp/5.png" alt="">-->
<!--                                        </a>-->
<!--                                        <div class="rightArea h125">-->
<!--                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>-->
<!--                                                <p class="rightList"><a href="#">中国四川成都</a></p>-->
<!--                                                <p class="hWinner">最佳仪态明星</p>-->
<!--                                        </div>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                        <li class="left">-->
<!--                                <div class="u_listItem">-->
<!--                                        <a href="#" class="leftArea shadow">-->
<!--                                                <img src="./assets/temp/5.png" alt="">-->
<!--                                        </a>-->
<!--                                        <div class="rightArea h125">-->
<!--                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>-->
<!--                                                <p class="rightList"><a href="#">中国四川成都</a></p>-->
<!--                                                <p class="hWinner">最佳上镜明星</p>-->
<!--                                        </div>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                        <li class="left">-->
<!--                                <div class="u_listItem">-->
<!--                                        <a href="#" class="leftArea shadow">-->
<!--                                                <img src="./assets/temp/5.png" alt="">-->
<!--                                        </a>-->
<!--                                        <div class="rightArea h125">-->
<!--                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>-->
<!--                                                <p class="rightList"><a href="#">中国四川成都</a></p>-->
<!--                                                <p class="hWinner">最佳才艺明星</p>-->
<!--                                        </div>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                        <li class="left">-->
<!--                                <div class="u_listItem">-->
<!--                                        <a href="#" class="leftArea shadow">-->
<!--                                                <img src="./assets/temp/5.png" alt="">-->
<!--                                        </a>-->
<!--                                        <div class="rightArea h125">-->
<!--                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>-->
<!--                                                <p class="rightList"><a href="#">中国四川成都</a></p>-->
<!--                                                <p class="hWinner">最佳气质明星</p>-->
<!--                                        </div>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                        <li class="left">-->
<!--                                <div class="u_listItem">-->
<!--                                        <a href="#" class="leftArea shadow">-->
<!--                                                <img src="./assets/temp/5.png" alt="">-->
<!--                                        </a>-->
<!--                                        <div class="rightArea h125">-->
<!--                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>-->
<!--                                                <p class="rightList"><a href="#">中国四川成都</a></p>-->
<!--                                                <p class="hWinner">最佳上镜明星</p>-->
<!--                                        </div>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                </ul>-->
<!--        </div>-->
<!--</div>-->
<!--获胜公益明星（旧） 结束-->

<div class="detailPageMidDiv">

<!--比赛详情 开始-->
<div class="detailPageMid">
        <div class="proContextDiv">
                <div class="titDiv">
                        <span></span>
                        <h2 class="Js_scroll">比赛详情</h2>
                </div>
                <div class="context">苹果用户每天手动安装应用更新的日子已经成为过去时了，由于iOS 7中的App Store增强了功能，发现新软件将变得比以前更容易。虽然App Store中无处不在的红色升级标记令用户们感到非常迷惑，但是当iOS 7在今年秋季正式发布的时候，用户们就会体会到它的好处了。如果启动自动更新的功能，iOS 7中的App Store就可以在后台自动检查和安装应用更新。<br  /><br />

                        用户还可以在App Store的"更新"栏看到最近刚刚升级过的应用列表。用户可以在这里看到安装升级程序的时间以及升级的内容。 如果App Store还没有进行自动更新，用户也可以选择手动安装最新的更新文件。<br  /><br />

                        "更新"栏中还将包括一个指向已购买内容的链接，用户们可以重新下载他们以前在另一台iOS设备上已经安装过的应用程序。<br  /><br />

                        用户可以在"iOS设置"应用的"iTunes App Store"部分控制自动更新功能，在"自动下载"部分根据需要开启或关闭"更新"、"应用"和"音乐"等选项。 与在以前版本的iOS系统中一样，用户们还可以决定是否允许自动下载功能使用蜂窝网络数据。</div>
        </div>
</div>

<!--参赛明星排行榜开始-->
<?php include("./modules/hire-participator.php"); ?>
<!--参赛明星排行榜结束-->

<!--比赛奖励（旧） 开始-->
<div class="detailPageMid">
        <div class="proContextDiv">
                <div class="titDiv">
                        <span style=""></span>
                        <h2>比赛奖励</h2>
                </div>
                <div class="context">
                        冠军：20万元（人民币）<br  />
                        亚军：10万元（人民币）<br  />
                        季军：5万元（人民币）<br  />
                        单项奖：2万元（人民币）<br  /><br />

                        1、决赛十二强选手获颁"扬州旅游形象大使"<br  />
                        2、获奖选手参加新丝路（中国）模特大赛决赛，有机会从此踏上星途<br  />
                        3、部分获奖选手签约扬州广播电视总台，担任主持人。<br  />
                </div>
        </div>
</div>
<!--比赛奖励（旧） 结束-->

<!--比赛评委 开始-->
<div class="detailPageMid">
        <div class="proContextDiv">
                <div class="titDiv">
                        <span></span>
                        <h2 class="Js_scroll">比赛评委</h2>
                </div>
                <ul class="clearfix u_list">
                        <li class="left">
                                <div class="u_listItem">
                                        <a href="#" class="leftArea shadow">
                                                <img src="./assets/temp/5.png" alt="">
                                        </a>
                                        <div class="rightArea">
                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                                                <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                        </div>
                                </div>
                        </li>
                        <li class="left">
                                <div class="u_listItem">
                                        <a href="#" class="leftArea shadow">
                                                <img src="./assets/temp/5.png" alt="">
                                        </a>
                                        <div class="rightArea">
                                                <p class="rightName"><a href="#" class="Js_visitCard">刘兆宇</a></p>
                                                <p class="rightList"><a href="#">成都电子科技大学</a></p>
                                        </div>
                                </div>
                        </li>
                        <li class="left">
                                <div class="u_listItem">
                                        <a href="#" class="leftArea shadow">
                                                <img src="./assets/temp/5.png" alt="">
                                        </a>
                                        <div class="rightArea">
                                                <p class="rightName"><a href="#" class="Js_visitCard">刘兆宇</a></p>
                                                <p class="rightList"><a href="#">创意世界</a></p>
                                        </div>
                                </div>
                        </li>
                        <li class="left">
                                <div class="u_listItem">
                                        <a href="#" class="leftArea shadow">
                                                <img src="./assets/temp/5.png" alt="">
                                        </a>
                                        <div class="rightArea">
                                                <p class="rightName"><a href="#" class="Js_visitCard">刘兆宇</a></p>
                                                <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                        </div>
                                </div>
                        </li>
                        <li class="left">
                                <div class="u_listItem">
                                        <a href="#" class="leftArea shadow">
                                                <img src="./assets/temp/5.png" alt="">
                                        </a>
                                        <div class="rightArea">
                                                <p class="rightName"><a href="#" class="Js_visitCard">刘兆宇</a></p>
                                                <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                        </div>
                                </div>
                        </li>
                        <li class="left">
                                <div class="u_listItem">
                                        <a href="#" class="leftArea shadow">
                                                <img src="./assets/temp/5.png" alt="">
                                        </a>
                                        <div class="rightArea">
                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                                                <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                        </div>
                                </div>
                        </li>
                </ul>
        </div>
</div>
<!--比赛评委 结束-->

<!--todo 图片上传明星引用文件-->
<?php include("./modules/hiring-updatepic.php"); ?>

<div class="height8"></div>

<!--公益参与 开始-->
<div class="detailPageMid">
<div class="titDiv clearfix" style="margin-bottom:15px;">
    <span></span>

    <h2 class="left">公益活动</h2>

    <div class="right mt20">
        <a class="sub-sbtn Js_charity">发布公益活动</a>
    </div>
    <s class="p-box b2 Js_charity_b" style="bottom:-15px;"></s>
</div>
<!--公益活动弹层开始-->
<div style="background:#e6e6e6; display:none;" class="Js_charity_c">
<div class="expertDpDiv">
<div id="tabs-etab" class="publish-form publish w765">
<div class="row" style="overflow:hidden;">
    <div class="large-12 columns">
        <div class="publish-progress">
            <div class="height_40"></div>
            <div class="issueStrip pr">
                <div class="gray-bg"></div>
                <ul id="tab-t" class="clearfix strip strip3">
                    <!--当前current/没填notFill/最后一步没填notFill last-->
                    <li class="one current" style="background-color: rgb(230, 230, 230)">
                        <span class="fousLine w-first"></span>
                        <a class="selected" href="#step-one">
                            <b>1</b>
                            <span class="st-one">基本信息</span>
                        </a>
                    </li>
                    <li class="notFill two ml" style="background-color: rgb(230, 230, 230)">
                        <s></s>
                        <span class="fousLine wh"></span>
                        <a href="#step-two"><b>2</b><span class="st-two" style="margin-left: -43px">描述公益活动内容</span></a>
                    </li>
                    <li class="notFill three ml" style="background-color: rgb(230, 230, 230)">
                        <s></s>
                        <span class="fousLine wh"></span>
                        <span class="fousLine w-last"></span>
                        <a href="#step-three"><b>3</b><span class="st-three" style="margin-left: -27px">确认发布协议</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="tabs-items" class="Js_tabs_items clearfix">
    <div class="row tabs-c">
        <div class="large-12 columns">
            <div class="rowblock">
                <div class="pt">填写公益活动内容名称</div>
                <div class="tools text left">
                    <div class="inputWarp empty">
                        <input type="text" id="Js_activity_name">
                    </div>
                </div>
                <div class="errorMsg left" style="display:none;">错误提示内容</div>
            </div>
            <div class="height_25"></div>
            <div class="rowblock">
                <div class="pt">公益活动所在地</div>
                <div class="tools text left Js_cityList">
                    <s class="locBg"></s>

                    <div class="inputWarp downMenuCity">
                        <input type="text" id="Js_activity_loc">

                        <div class="selDiv cityList">
                            <div class="locAddress"><a href="javascript:void(0)">获取当前地理位置</a></div>
                        </div>
                    </div>
                </div>
                <div class="errorMsg left" style="display:none;">错误提示内容</div>
            </div>

            <div class="height_25"></div>

            <div class="rowblock">
                <div class="pt">上传公益活动封面图片</div>
                <div class="uploadWrap-single dargArea left">
                    <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
                    <input class="file_uploadbox-single" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">
                    <input type="hidden" name="img_way" value="" />
                </div>
                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                <div class="height_10"></div>
                <div class="clearfix Js_activity_upimg upload-single"></div>
            </div>

            <div class="height_50"></div>
            <a href="#step-two" class="blueBtn Js_s_one">下一步</a>
        </div>
        <div class="height_60"></div>
    </div>

    <div class="row tabs-c">
        <div class="large-12 columns">
            <div class="height_15"></div>
            <div class="rowblock l-edit">
                <div class="pt">描述公益活动的详细内容</div>
                <p class="pdes">请尽可能详细的描述您的公益活动内容，以便让更多的人参与活动。</p>

                <div class="height_15"></div>
                <div id="xb_toolbar" class="toolbar-wrap"></div>
                <div class="toolbar-content left">
                    <textarea id="xb_content" name="content"></textarea>
                </div>
                <div class="errorMsg left" style="display:none;">错误提示内容</div>
            </div>

            <div class="height_50"></div>
            <a href="#step-one" class="grayBtn mr16 Js_s_two_pre">上一步</a>
            <a href="#step-three" class="blueBtn Js_s_two_next">下一步</a>
        </div>
        <div class="height_60"></div>
    </div>

    <div class="row tabs-c">
        <div class="large-12 columns">
            <div class="rowblock l-edit">
                <div class="pt">确认公益活动发布协议</div>
                <p class="pdes">请您阅读并确认创意世界公益活动活动发布协议。</p>

                <div class="height_15"></div>
                <!--                                                        <div id="bz_toolbar" class="toolbar-wrap"></div>-->
                <div class="toolbar-content left">
                    <textarea id="bz_content" name="content">
                        确认发布公益活动协议

                        1	简介

                        四川创意世界科技有限公司同意按照本协议的规定及其不时发布的操作规则提供创意世界公益活动服务（以下简称“公益活动”），为使用该服务，用户应当同意本协议的全部条款并按照页面上的提示完成参与公益活动程序。用户一旦发布或参与公益活动即表示用户完全接受本协议项下的全部条款。
                        用户应当对以其创意世界账户进行的所有公益活动和事件负法律责任，不得违反国家现行法律法规。

                        2	公益活动发起人定义

                        通过创意世界公益活动发起公益项目，包括但不限于个人求助、捐助等，以达到为求助人募集善款、物品、或者招募志愿者的目的，是整个创意世界公益活动求助流程中的核心用户。创意世界测试阶段，发起人仅限创意公益明星比赛参赛明星。

                        3	公益活动发起人的权利

                        发起人有权利申请使用创意世界公益活动服务，包括但不限于个人求助、拍卖等服务；并将在了解和保证项目真实性的基础上，作为项目重要参与人员全程跟进公益活动进展，使整个活动公开、透明的进行。

                        4	公益活动发起人的义务

                        发起人使用创意世界公益活动服务时，必须向创意世界提供准确、真实的个人资料、项目基本信息及照片图片，如资料及信息有任何变动，必须及时更新；如因资料及信息有任何不准确造成项目失败或引起质疑，由用户自行承担全部风险和责任；与创意世界无涉。

                        发起人需保证求助项目的公益性质,符合“公益活动”这个平台适用原则,不得夸张或者渲染事件,更不基于商业目的进行求助。

                        发起人正式发布公益项目后，无论是否达到求助目标，都需及时在线上通过公益活动反馈救助情况及所获救助资源消耗情况；反馈信息需准确、真实。

                        5	公益活动发起人出现以下任一情况即失去资格：

                        利用发起人身份谋求不正当的利益；

                        禁止发起人以创意世界公益活动身份之便利，收受任何形式的财物，一经发现核实即失去资格。情节严重者将追究其法律责任。

                        隐瞒或虚报个人信息；未提供真实个人信息。

                        发起人失去资格同时将面临包括但不限于全站公示、禁止再次使用创意世界公益活动、关闭创意世界账号等站方处理。

                        6	公益活动参与人定义

                        通过微公益平台参与公益活动的创意世界用户，实现方式为捐款、捐赠物资、报名志愿者活动、关注、分享等；参与人需保证所捐赠物资为个人合法所得，并对其捐赠钱款、物品有处分权。

                        7	公益活动参与人的权利

                        参与人有权利申请使用创意世界公益活动服务，包括但不限于个人求助、拍卖等产品。

                        参与人在公益项目参与成功后有权向公益活动发起人索要捐款金额相应发票，需参与人在捐赠同时提供发票单位、个人、联系方式及地址。

                        创意世界公益活动为所有参与人公开所有信息，参与人可线上查询所有参与过的公益活动、公益行为包括不限于支持、收藏、关注、捐款、捐物、报名志愿者、拍卖、个人求助。

                        后期将推出的其它激励措施。

                        8	公益活动参与人的义务

                        参与人使用创意世界公益活动服务时，建立在信任该项目中的公益支持（方）的前提下，信任求助人、发起人自主发布的个人的施助信息，并自愿支持求助人。

                        参与人捐赠成功即为确认捐赠善款无误，捐款成功后由相关公益组织进行管理和使用，创意世界公益活动募捐项目款项一旦由用户捐赠成功即不可退回；如出现诈捐情况，创意世界公益活动敦促公益支持（方）确认项目情况，并协调公益组织退款，除此情况外捐赠款项不可退回。

                        参与人同意，如遇自然因素及其他不可抗力导致公益活动结束，参与人所捐物资转做同类公益项目使用，创意世界公益活动需在其公益活动服务的相关页面进行公示，不再另行通知参与人。

                        参与人需知悉创意世界公益活动为创意世界提供的支持公益事业的专业产品，在此产品上进行的任何操作默认为参与人本人对个人创意世界账号进行操作，包含捐款账户。

                        参与人如需开具相应捐赠发票，可向受捐公益组织提交申请。

                        企业用户作为参与人使用创意世界公益活动产品时，需按承诺捐赠所约定款项及实物。

                        9	其他

                        用户因微博行为引发的法律纠纷，与新浪公司无关。

                        微公益可依照互联网发展的不同阶段，随着公益产品管理经验的不断丰富，出于维护微博秩序的目的，不断完善本协议。
                    </textarea>
                </div>
            </div>
            <div class="height_10"></div>
            <div class="l-edit">
                <p class="n-agree left">
                    <label class="Js_label_ok ok">
                 <span class="Js_checkBox" data-name="chk">
                <input type="checkbox" name="chk" checked="checked" id="Js_activity_chk1">
                我已经阅读并同意创意世界公益活动发布协议
                </span></label>
                </p>

                <div class="errorMsg left" style="margin-top: -10px; display:none;">请确认同意协议</div>
            </div>
            <div class="height_40"></div>
            <div class="rowblock l-edit">
                <div class="pt left">
                    <div class="sign-bg">
                        <input class="signature" id="Js_activity_qmname"></div>
                </div>
                <div class="errorMsg left mt8" style="display:none;">错误提示内容</div>
            </div>
            <div class="height_50"></div>
            <a href="#step-two" class="grayBtn mr16 Js_s_three_pre">上一步</a>
            <a href="#step-one" class="blueBtn Js_s_three_next" id="Js_submit_activity">发布公益活动</a>
        </div>
        <div class="height_60"></div>
    </div>

</div>
</div>
</div>
</div>
<!--公益活动弹层结束-->

<div class="proContextDiv">
    <ul class="list clearfix mt30 n_cList">
        <li class="p-match" style="margin: 0 1px 0 23px;">
            <div class="match-box" style="margin-bottom: 0;">
                <div class="listItemImg">
                    <div class="rightBox mark"></div>
                    <s class="mark"></s>

                    <div class="bottomBox"></div>
                    <img src="./assets/temp/p1.png" alt="" width="297" height="235"></div>
                <div class="contextDiv"><h3>幻世纪大作再现-蚀刻暴龙拼装模型 - 杰思模型</h3>

                    <div class="progress"></div>
                    <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>

                    <div class="clearfix" style="margin-top:28px;">
                        <div style="float:left;">
                            <p class="promulgatorMoney" style="margin-top:0;"><em>10,000</em>
                                <small>元</small>
                            </p>
                            <span class="status">已筹善款</span></div>
                        <a href="./hire-donation.php" class="smallblueBtn right" style="margin-top: 3px;">我要参与</a></div>
                </div>
                <div class="bottomDiv clearfix"><span class="left">长期</span> <span class="right rr">有3位用户奉献爱心</span></div>
            </div>
        </li>
        <li class="p-match" style="margin: 0 1px 0 23px;">
            <div class="match-box" style="margin-bottom: 0;">
                <div class="listItemImg">
                    <div class="rightBox mark"></div>
                    <s class="mark"></s>

                    <div class="bottomBox"></div>
                    <img src="./assets/temp/p1.png" alt="" width="297" height="235"></div>
                <div class="contextDiv"><h3>幻世纪大作再现-蚀刻暴龙拼装模型 - 杰思模型</h3>

                    <div class="progress"></div>
                    <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>

                    <div class="clearfix" style="margin-top:28px;">
                        <div style="float:left;">
                            <p class="promulgatorMoney" style="margin-top:0;"><em>10,000</em>
                                <small>元</small>
                            </p>
                            <span class="status">已筹善款</span></div>
                        <a href="./hire-donation.php" class="smallblueBtn right" style="margin-top: 3px;">我要参与</a></div>
                </div>
                <div class="bottomDiv clearfix"><span class="left">长期</span> <span class="right rr">有3位用户奉献爱心</span></div>
            </div>
        </li>
        <li class="p-match" style="margin: 0 1px 0 23px;">
            <div class="match-box" style="margin-bottom: 0;">
                <div class="listItemImg">
                    <div class="rightBox mark"></div>
                    <s class="mark"></s>

                    <div class="bottomBox"></div>
                    <img src="./assets/temp/p1.png" alt="" width="297" height="235"></div>
                <div class="contextDiv"><h3>幻世纪大作再现-蚀刻暴龙拼装模型 - 杰思模型</h3>

                    <div class="progress"></div>
                    <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>

                    <div class="clearfix" style="margin-top:28px;">
                        <div style="float:left;">
                            <p class="promulgatorMoney" style="margin-top:0;"><em>10,000</em>
                                <small>元</small>
                            </p>
                            <span class="status">已筹善款</span></div>
                        <a href="./hire-donation.php" class="smallblueBtn right" style="margin-top: 3px;">我要参与</a></div>
                </div>
                <div class="bottomDiv clearfix"><span class="left">长期</span> <span class="right rr">有3位用户奉献爱心</span></div>
            </div>
        </li>
    </ul>
</div>
</div>
<!--公益参与 结束-->

<div class="height8"></div>

<!--明星交易 开始-->
<div class="detailPageMid">
<div class="titDiv clearfix" style="margin-bottom:15px;">
    <span></span>

    <h2 class="left">明星交易</h2>

    <div class="right mt20">
        <a class="sub-sbtn Js_starEx">发布明星交易</a>
    </div>
    <s class="p-box b2 Js_starEx_b" style="bottom: -15px;"></s>
</div>
<!--明星交易弹出层开始-->
<div style="background:#e6e6e6; display:none;" class="Js_starEx_c">
    <div class="expertDpDiv">
        <div id="tabs-ttab" class="publish-form publish w765">
            <div class="row" style="overflow:hidden;">
                <div class="large-12 columns">
                    <div class="publish-progress">
                        <div class="height_40"></div>
                        <div class="issueStrip pr">
                            <div class="gray-bg"></div>
                            <ul id="tab-t" class="clearfix strip strip3">
                                <!--当前current/没填notFill/最后一步没填notFill last-->
                                <li class="one current" style="background-color: rgb(230, 230, 230)">
                                    <span class="fousLine w-first"></span>
                                    <a class="selected" href="#step-one">
                                        <b>1</b>
                                        <span class="st-one">基本信息</span>
                                    </a>
                                </li>
                                <li class="notFill two ml" style="background-color: rgb(230, 230, 230)">
                                    <s></s>
                                    <span class="fousLine wh"></span>
                                    <a href="#step-two"><b>2</b><span class="st-two" style="margin-left: -27px">描述交易内容</span></a>
                                </li>
                                <li class="notFill three ml" style="background-color: rgb(230, 230, 230)">
                                    <s></s>
                                    <span class="fousLine wh"></span>
                                    <span class="fousLine w-last"></span>
                                    <a href="#step-three"><b>3</b><span class="st-three" style="margin-left: -27px">确认发布协议</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tabs-items" class="Js_tabs_items clearfix">
                <div class="row tabs-c">
                    <div class="large-12 columns">
                        <div class="rowblock">
                            <div class="pt">填写交易名称</div>
                            <div class="tools text left">
                                <div class="inputWarp empty">
                                    <input type="text" id="Js_activity_names">
                                </div>
                            </div>
                            <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        </div>
                        <div class="height_25"></div>
                        <div class="rowblock">
                            <div class="pt">交易所在地</div>
                            <div class="tools text left Js_cityList">
                                <s class="locBg"></s>

                                <div class="inputWarp downMenuCity">
                                    <input type="text" id="Js_activity_locs">

                                    <div class="selDiv cityList">
                                        <div class="locAddress"><a href="javascript:void(0)">获取当前地理位置</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        </div>
                        <div class="height_25"></div>
                        <div class="rowblock">
                            <div class="pt" style="line-height:1.8">选择交易模式</div>
                            <div class="l-edit">
                                <p class="n-agree mt10 left"> <span class="Js_radio mr25 Js_forever" data-name="rad">
                     <label class="Js_label_ok ok">
                         <input type="radio" name="rad" checked>长期</label> <!--选择后文中颜色-->

                  </span>
                  <span class="Js_radio Js_limit_time" data-name="rad">
                    <label class="Js_label_ok"> <input type="radio" name="rad"> 限制时间</label>

                  </span>
                                </p>
                            </div>
                        </div>

                        <div class="height_25"></div>
                        <div class="rowblock">
                            <div class="pt">设置竞价起拍价格</div>
                            <div class="tools text left">
                                <div class="inputWarp enterNum">
                                    <input type="text" class="Js_input_text" id="Js_enterInput">
                                </div>
                                <div class="numFormat">.00</div>
                            </div>
                            <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        </div>

                        <div class="height_25"></div>
                        <div class="rowblock">
                            <div class="pt">设置竞价拍卖的截止时间</div>
                            <div class="tools text left">
                                <input class="datebox Js_input_date" type="text" id="date1" readonly="readonly" value="" >
                            </div>
                            <div class="errorMsg left" style="display:none;">请选择日期</div>
                        </div>


                        <div class="height_25"></div>
                        <div class="rowblock">
                            <div class="pt">上传交易封面图片</div>
                            <div class="uploadWrap-single dargArea left">
                                <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
                                <input class="file_uploadbox-single" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">
                                <input type="hidden" name="img_way" value="" />
                            </div>
                            <div class="errorMsg left" style="display:none;">错误提示内容</div>
                            <div class="height_10"></div>
                            <div class="clearfix upload-single Js_trade_img"></div>
                        </div>

                        <div class="height_50"></div>
                        <a href="#step-two" class="blueBtn Js_s_one">下一步</a>
                    </div>
                    <div class="height_60"></div>
                </div>

                <div class="row tabs-c">
                    <div class="large-12 columns">
                        <div class="height_15"></div>
                        <div class="rowblock l-edit">
                            <div class="pt">描述交易的详细内容</div>
                            <p class="pdes">请尽可能详细的描述您的交易内容，以便让更多的人参与交易。</p>

                            <div class="height_15"></div>
                            <div id="ctb_toolbar" class="toolbar-wrap"></div>
                            <div class="toolbar-content">
                                <textarea id="ctb_content" name="content"></textarea>
                            </div>
                            <div class="errorMsg" style="display: none">错误提示内容</div>
                        </div>

                        <div class="height_50"></div>
                        <a href="#step-one" class="grayBtn mr16 Js_s_two_pre">上一步</a>
                        <a href="#step-three" class="blueBtn Js_s_two_next">下一步</a>
                    </div>
                    <div class="height_60"></div>
                </div>

                <div class="row tabs-c">
                    <div class="large-12 columns">
                        <div class="rowblock l-edit">
                            <div class="pt">确认交易发布协议</div>
                            <p class="pdes">请您阅读并确认创意世界明星交易发布协议。</p>

                            <div class="height_15"></div>
                            <!--                                                        <div id="dtb_toolbar" class="toolbar-wrap"></div>-->
                            <div class="toolbar-content left">
                                <textarea id="dtb_content" name="content"></textarea>
                            </div>
                        </div>
                        <div class="height_10"></div>
                        <div class="l-edit">
                            <p class="n-agree left"><label class="Js_label_ok ok">
                <span class="Js_checkBox" data-name="chk">
                <input type="checkbox" name="chk" checked="checked" id="Js_activity_chk">
                我已经阅读并同意创意世界交易发布协议
                </span></label>
                            </p>

                            <div class="errorMsg left" style="margin-top: -10px; display:none;">请确认同意协议</div>
                        </div>
                        <div class="height_40"></div>
                        <div class="rowblock l-edit">
                            <div class="pt left">
                                <div class="sign-bg">
                                    <input class="signature" id="Js_activity_qmname1"></div>
                            </div>
                            <div class="errorMsg left mt8" style="display:none;">错误提示内容</div>
                        </div>
                        <div class="height_50"></div>
                        <a href="#step-two" class="grayBtn mr16 Js_s_three_pre">上一步</a>
                        <a href="#step-four" class="blueBtn Js_s_three_next" id="Js_submit_trade">发布交易</a>
                    </div>
                    <div class="height_60"></div>
                </div>


            </div>
        </div>
    </div>
</div>
<!--明星交易弹出层结束-->

<div class="proContextDiv">
    <ul class="list clearfix mt30 n_cList">
        <li class="p-match" style="margin: 0 1px 0 23px;">
            <div class="match-box" style="margin-bottom: 0;">
                <div class="listItemImg">
                    <div class="rightBox bookmark"></div>
                    <s class="bookmark"></s>

                    <div class="bottomBox"></div>
                    <img src="./assets/temp/p1.png" alt="" width="297" height="235"></div>
                <div class="contextDiv"><h3>幻世纪大作再现-蚀刻暴龙拼装模型 - 杰思模型</h3>

                    <div class="progress"></div>
                    <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>

                    <div class="clearfix" style="margin-top:28px;">
                        <div style="float:left;">
                            <p class="promulgatorMoney" style="margin-top:0;"><em>10,000</em>
                                <small>元</small>
                            </p>
                            <span class="status">当前竞拍价</span></div>
                        <a href="./trade-bid.php" class="smallblueBtn right" style="margin-top: 3px;">我要竞拍</a></div>
                </div>
                <div class="bottomDiv clearfix"><span class="left">长期</span> <span class="right rr">有3位用户参与竞拍</span></div>
            </div>
        </li>
        <li class="p-match" style="margin: 0 1px 0 23px;">
            <div class="match-box" style="margin-bottom: 0;">
                <div class="listItemImg">
                    <div class="rightBox bookmark"></div>
                    <s class="bookmark"></s>

                    <div class="bottomBox"></div>
                    <img src="./assets/temp/p1.png" alt="" width="297" height="235"></div>
                <div class="contextDiv"><h3>幻世纪大作再现-蚀刻暴龙拼装模型 - 杰思模型</h3>

                    <div class="progress"></div>
                    <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>

                    <div class="clearfix" style="margin-top:28px;">
                        <div style="float:left;">
                            <p class="promulgatorMoney" style="margin-top:0;"><em>10,000</em>
                                <small>元</small>
                            </p>
                            <span class="status">直接购买价</span></div>
                        <a href="./trade-buy.php" class="smallblueBtn right" style="margin-top: 3px;">我要购买</a></div>
                </div>
                <div class="bottomDiv clearfix"><span class="left">长期</span> <span class="right rr">有3位用户参与购买</span></div>
            </div>
        </li>
        <li class="p-match" style="margin: 0 1px 0 23px;">
            <div class="match-box" style="margin-bottom: 0;">
                <div class="listItemImg">
                    <div class="rightBox bookmark"></div>
                    <s class="bookmark"></s>

                    <div class="bottomBox"></div>
                    <img src="./assets/temp/p1.png" alt="" width="297" height="235"></div>
                <div class="contextDiv"><h3>幻世纪大作再现-蚀刻暴龙拼装模型 - 杰思模型</h3>

                    <div class="progress"></div>
                    <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>

                    <div class="clearfix" style="margin-top:28px;">
                        <div style="float:left;">
                            <p class="promulgatorMoney" style="margin-top:0;"><em>10,000</em>
                                <small>元</small>
                            </p>
                            <span class="status">当前竞拍价</span></div>
                        <a href="./trade-bid.php" class="smallblueBtn right" style="margin-top: 3px;">我要竞拍</a></div>
                </div>
                <div class="bottomDiv clearfix"><span class="left">长期</span> <span class="right rr">有3位用户参与竞拍</span></div>
            </div>
        </li>
    </ul>
</div>
</div>
<!--明星交易 结束-->

<div class="height8"></div>

<!--社区论坛引用文件-->
<?php $forumTT = "讨论区"; //社区论坛名称变量 ?>
<?php include("./modules/discuss.php"); ?>

<!--竞猜引用文件-->
<?php $guessTT = "竞猜"; ?>
<?php include("./modules/lottery.php"); ?>
</div>
</div>
</div>
</div>
<script type="text/javascript">
        $(function(){
                tabs('tabs-tab','tab-t', 1, 'click');
                tabs('tabs-tab','tab-w', 1, 'click');
                tabs('tabs-ttab','tab-t', 1, 'click');
                tabs('tabs-ttab','tab-w', 1, 'click');
                tabs('tabs-etab','tab-t', 1, 'click');
                tabs('tabs-etab','tab-w', 1, 'click');
                tDataControl('date2');

                $('.Js_fancyBox,.Js_fancyBox1,.Js_fancyBox2,.Js_fancyBox3').fancybox({
                        prevEffect : 'none',
                        nextEffect : 'none',

                        closeBtn  : true,
                        arrows    : true,
                        nextClick : true,
                        tpl : {
                                closeBtn : '<a title="Close" style="background-image: url(assets/images/fancybox/fancybox_sprite.png); width:36px; height:36px; top:-18px; right:-18px; position:absolute"></a>'
                        },
                        helpers : {
                                thumbs : {
                                        width  : 50,
                                        height : 50
                                }
                        }
                })
        });
</script>