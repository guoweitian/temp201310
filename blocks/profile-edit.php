<div class="content">
<div class="detail-wrap exchange">
<div class="detail-imgbg">
        <div class="toppattern"></div>
        <div class="detailTopDiv pew">
                <div style="height:348px;"></div>
                <div>
                        <div class="u_listItem left">
                                <a href="#" class="leftArea  shadow">
                                        <img src="./assets/temp/5.png" alt="">
                                </a>
                                <div class="rightArea w600">
                                        <p class="rightName "><a class="Js_visitCard cfff" href="#">刘兆宇</a></p>
                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                        <p class="rightfans-friends"><a class="fans">粉丝 <i>10000</i></a>&nbsp&nbsp&nbsp&nbsp<a class="frinends">好友 <i>10000</i></a></p>
                                </div>
                        </div>
                        <div class="tz_moneyDiv right" style=" padding-top: 40px;">
                                <a href="javascript:" class="attenBtn ml10 likeBtn right">123,585</a>
                                <a href="javascript:" class="attenBtn right" id="Js_add_attention">添加关注</a>
                        </div>
                </div>
        </div>
</div>
<div class="detailPageMidDiv">
<div class="detailPageMid" >
<div class="proContextDiv" style="width: 880px;">
<div class="titDiv">
        <span></span>
        <h2 class="Js_scroll">关于我</h2>
</div>
<div class="clearfix">
        <div class="sub-tit info left">个人介绍</div>
        <div class="right mt35">
                <a class="sub-sbtn Js_user_intro">编辑个人介绍</a>
        </div>
</div>
<div class="pro-wrap">
        <div class="Js_userfile_txt" style="display:none;">
                <div class="tools textArea" style="width:100%; height:150px;">
                        <textarea style="font-size: 14px; line-height: 22px; color:#5f5f5f; padding:12px 15px; height:150px;"></textarea>
                </div>
                <div class="height_40"></div>
                <div class="p-warp">
                        <a href="javascript:;" class="grayBtn mr5 Js_userfile_cancle font14">取消修改</a>
                        <a href="javascript:;" class="blueBtn Js_userfile_update font14">确认修改</a>
                </div>
        </div>
        <p class="con Js_userfile_ok">中国著名企业家，生于浙江杭州，祖籍绍兴，阿里巴巴集团主要创始人之一。现任阿里巴巴集团董事局主席，他是《福布斯》杂志创办50多年来成为封面人物的首位大陆企业家，曾获选为未来全球领袖。除此之外，马云还担任中国雅虎董事局主席、杭州师范大学阿里巴巴商学院院长、华谊兄弟传媒集团董事、艺术品中国商业顾问等职务。2012年11月，阿里巴巴在网上的交易额突破一万亿大关，马云由此被扣以“万亿侯”的称号。2013年5月10日，马云正式卸任阿里巴巴CEO。2013年5月28日，马云联合阿里巴巴集团、银泰集团、复星集团、富春集团、顺丰集团、中通、圆通、申通、韵达等多家民营快递企业，组建物流网络平台“菜鸟网络科技有限公司”。马云出任董事长。
</div>
<div class="height_40"></div>
<div class="clearfix">
        <div class="sub-tit experience left">个人经历</div>
        <div class="right mt35">
                <a class="sub-sbtn Js_add_experience">添加新的经历</a>
        </div>
</div>
<div class="pro-wrap Js_file_add" style="display:none;">
        <div class="height_12"></div>
        <div class="rowblock clearfix">
                <div class="pt">公司名称</div>
                <div class="tools text left">
                        <div class="inputWarp empty">
                                <input type="text" id="Js_company_name">
                        </div>
                </div>
				<div class="errorMsg left" style="display:none;">错误提示内容</div>
        </div>
        <div class="height_20"></div>
        <div class="rowblock clearfix">
                <div class="pt">职务名称</div>
                <div class="tools text left">
                        <div class="inputWarp empty">
                                <input type="text" id="Js_job_name">
                        </div>
                </div>
				<div class="errorMsg left" style="display:none;">错误提示内容</div>
        </div>
        <div class="height_18"></div>
        <div class="rowblock clearfix">
                <div class="pt">工作地点</div>
                <div class="tools text left">
                        <div class="inputWarp empty">
                                <input type="text" id="Js_work_place">
                        </div>
                </div>
				<div class="errorMsg left" style="display:none;">错误提示内容</div>
        </div>
        <div class="height_18"></div>
        <div class="rowblock clearfix">
                <div class="pt">时间周期</div>
				<div class="left Js_user_sel_div">
					<div class="optDiv Js_state left mr10" style="width:148px;">
							<div class="tools text pr" style="width:auto">
									<div class="inputWarp opt">
											<span id="Js_oldYear"></span>
											<a href="javascript:void(0)" class="showMenu"></a>
									</div>
							</div>
							<ul>
									<li><a href="javascript:void(0)">2010年</a></li>
									<li><a href="javascript:void(0)">2011年</a></li>
							</ul>
							<select style="display:none;">
									<option>2010年</option>
									<option>2011年</option>
							</select>
					</div>
					<div class="optDiv Js_state left" style="width:148px;">
							<div class="tools text pr" style="width:auto">
									<div class="inputWarp opt">
											<span id="Js_oldMonth"></span>
											<a href="javascript:void(0)" class="showMenu"></a>
									</div>
							</div>
							<ul>
									<li><a href="javascript:void(0)">1月</a></li>
									<li><a href="javascript:void(0)">2月</a></li>
							</ul>
							<select style="display:none;">
									<option>1月</option>
									<option>2月</option>
							</select>
					</div>
					<s class="time-fh"></s>
					<div class="optDiv Js_state left mr10" style="width:148px;">
							<div class="tools text pr" style="width:auto">
									<div class="inputWarp opt">
											<span id="Js_newYear"></span>
											<a href="javascript:void(0)" class="showMenu"></a>
									</div>
							</div>
							<ul>
									<li><a href="javascript:void(0)">2010年</a></li>
									<li><a href="javascript:void(0)">2011年</a></li>
							</ul>
							<select style="display:none;">
									<option>2010年</option>
									<option>2011年</option>
							</select>
					</div>
					<div class="optDiv Js_state left" style="width:148px;">
							<div class="tools text pr" style="width:auto">
									<div class="inputWarp opt">
											<span id="Js_newMonth"></span>
											<a href="javascript:void(0)" class="showMenu"></a>
									</div>
							</div>
							<ul>
									<li><a href="javascript:void(0)">1月</a></li>
									<li><a href="javascript:void(0)">2月</a></li>
							</ul>
							<select style="display:none;">
									<option>1月</option>
									<option>2月</option>
							</select>
					</div>
				</div>
				<div class="errorMsg left" style="display:none;">错误提示内容</div>
        </div>
        <div class="height_5"></div>

        <div class="height_38"></div>
        <div class="rowblock clearfix">
                <div class="pt" style="line-height:12px;">描述</div>
                <div class="height_15"></div>
                <div class="tools textArea left" style="width:640px; height:196px;">
                        <textarea style="font-size: 15px; line-height: 1.5; height:181px;" id="Js_disText"></textarea>
                </div>
				<div class="errorMsg left" style="display:none;">错误提示内容</div>
        </div>
        <div class="height_54"></div>
        <div class="p-warp">
                <a href="javascript:;" class="grayBtn mr5 font14 Js_cancle_add">取消添加</a>
                <a href="javascript:;" class="blueBtn font14 Js_confirm_add">确认添加</a>
        </div>
        <div class="height_70"></div>
</div>

<div class="Js_file_add_context">
        <div class="pro-wrap">
                <p class="post clearfix">
                        <span class="left mr40 Js_job_rname">客户支持部经理</span>
								<span class="editorBtnList">
									<a href="javascript:;" class="editor bqh Js_editor_btn">编辑资料</a>
									<a href="javascript:;" class="editor bfs Js_file_del">删除</a>
									<a href="javascript:;" class="editor bfs Js_del_ok" style="display:none;">确认删除</a>
									<a href="javascript:;" class="editor bshy Js_file_cancel" style="display:none;">取消删除</a>
								</span>
                </p>
                <p class="company mt6 Js_company_rname">联想集团</p>
                <p class="time-area"><span class="Js_roldYear">2011年</span><span class="Js_roldMonth">1月</span></span><span class="spacing">-</span><span class="Js_rnewYear">2013年</span><span class="Js_rnewMonth">6月</span><span class="Js_cMonth">（2年5个月）</span><span class="spacing">|</span> <span class="Js_work_rpalce">中国成都</span></p>
                <p class="des Js_rdisText">成立了联想集团客户支持部，处理客户售后问题。</p>
                <div class="Js_editor_file" style="display:none;">
                        <div class="height_22"></div>
                        <div class="rowblock clearfix">
                                <div class="pt">公司名称</div>
                                <div class="tools text left">
                                        <div class="inputWarp empty">
                                                <input type="text" class="Js_company_name">
                                        </div>
                                </div>
                        </div>
                        <div class="height_18"></div>
                        <div class="rowblock clearfix">
                                <div class="pt">职务名称</div>
                                <div class="tools text left">
                                        <div class="inputWarp empty">
                                                <input type="text" class="Js_job_name">
                                        </div>
                                </div>
                        </div>
                        <div class="height_18"></div>
                        <div class="rowblock clearfix">
                                <div class="pt">工作地点</div>
                                <div class="tools text left">
                                        <div class="inputWarp empty">
                                                <input type="text" class="Js_work_place">
                                        </div>
                                </div>
                        </div>
                        <div class="height_18"></div>
                        <div class="rowblock clearfix">
                                <div class="pt">时间周期</div>
                                <div class="optDiv Js_state left mr10" style="width:148px;">
                                        <div class="tools text pr" style="width:auto">
                                                <div class="inputWarp opt">
                                                        <span class="Js_oldYear"></span>
                                                        <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                        </div>
                                        <ul>
                                                <li><a href="javascript:void(0)">2010年</a></li>
                                                <li><a href="javascript:void(0)">2011年</a></li>
                                        </ul>
                                        <select style="display:none;">
                                                <option>2010年</option>
                                                <option>2011年</option>
                                        </select>
                                </div>
                                <div class="optDiv Js_state left" style="width:148px;">
                                        <div class="tools text pr" style="width:auto">
                                                <div class="inputWarp opt">
                                                        <span class="Js_oldMonth"></span>
                                                        <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                        </div>
                                        <ul>
                                                <li><a href="javascript:void(0)">1月</a></li>
                                                <li><a href="javascript:void(0)">2月</a></li>
                                        </ul>
                                        <select style="display:none;">
                                                <option>1月</option>
                                                <option>2月</option>
                                        </select>
                                </div>
                                <s class="time-fh"></s>
                                <div class="optDiv Js_state left mr10" style="width:148px;">
                                        <div class="tools text pr" style="width:auto">
                                                <div class="inputWarp opt">
                                                        <span class="Js_newYear"></span>
                                                        <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                        </div>
                                        <ul>
                                                <li><a href="javascript:void(0)">2010年</a></li>
                                                <li><a href="javascript:void(0)">2011年</a></li>
                                        </ul>
                                        <select style="display:none;">
                                                <option>2010年</option>
                                                <option>2011年</option>
                                        </select>
                                </div>
                                <div class="optDiv Js_state left" style="width:148px;">
                                        <div class="tools text pr" style="width:auto">
                                                <div class="inputWarp opt">
                                                        <span class="Js_newMonth"></span>
                                                        <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                        </div>
                                        <ul>
                                                <li><a href="javascript:void(0)">1月</a></li>
                                                <li><a href="javascript:void(0)">2月</a></li>
                                        </ul>
                                        <select style="display:none;">
                                                <option>1月</option>
                                                <option>2月</option>
                                        </select>
                                </div>
                        </div>
                        <div class="height_5"></div>
                        <div class="height_38"></div>
                        <div class="rowblock">
                                <div class="pt" style="line-height:12px;">描述</div>
                                <div class="height_15"></div>
                                <div class="tools textArea" style="width:640px; height:130px;">
                                        <textarea style="font-size: 15px; line-height: 1.5; height:115px;" class="Js_disText"></textarea>
                                </div>
                        </div>
                        <div class="height_40"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="grayBtn mr5 font14 Js_cancle_revise">取消修改</a>
                                <a href="javascript:;" class="blueBtn font14 Js_ok_revise">确认修改</a>
                        </div>
                </div>
        </div>
        <div class="height_50"></div>
        <div class="pro-wrap">
                <p class="post">Head - Business Relations</p>
                <p class="company">Equionox</p>
                <p class="time-area">2008年1月<span class="spacing">-</span>现在（3年11个月）<span class="spacing">|</span>中国成都</p>
                <p class="des">In Equinox Labs I Head the Business Relations Team. Interacting with Key Clients, Government and Media on matters of FSSAI Compliance, Food Quality Monitoring, Air Quality Monitoring, etc.	</p>
        </div>
</div>
<div class="height_40"></div>
<div class="clearfix">
        <div class="sub-tit edu left">教育经历</div>
        <div class="right mt35">
                <a class="sub-sbtn Js_add_edu">添加新的经历</a>
        </div>
</div>
<div class="pro-wrap Js_edu_add" style="display:none;">
        <div class="height_12"></div>
        <div class="rowblock clearfix">
                <div class="pt">学校类型</div>
                <div class="optDiv Js_state left">
                        <div class="tools text pr">
                                <div class="inputWarp opt">
                                        <span id="Js_school_type"></span>
                                        <a href="javascript:void(0)" class="showMenu"></a>
                                </div>
                        </div>
                        <ul>
                                <li><a href="javascript:void(0)">大学</a></li>
                                <li><a href="javascript:void(0)">中学</a></li>
                                <li><a href="javascript:void(0)">小学</a></li>
                        </ul>
                        <select style="display:none;">
                                <option>学校类型一</option>
                                <option>学校类型二</option>
                        </select>
                </div>
				<div class="errorMsg left" style="display:none;">错误提示内容</div>
        </div>
        <div class="height_20"></div>
        <div class="rowblock clearfix">
                <div class="pt">学校名称</div>
                <div class="tools text left">
                        <div class="inputWarp empty">
                                <input type="text" id="Js_school_name">
                        </div>
                </div>
				<div class="errorMsg left" style="display:none;">错误提示内容</div>
        </div>
        <div class="height_18"></div>
        <div class="rowblock clearfix">
                <div class="pt">院系</div>
                <div class="tools text left">
                        <div class="inputWarp empty">
                                <input type="text" id="Js_school_yx">
                        </div>
                </div>
				<div class="errorMsg left" style="display:none;">错误提示内容</div>
        </div>
        <div class="height_18"></div>
        <div class="rowblock clearfix">
                <div class="pt">入学年份</div>
				<div class="left Js_user_school_div">
					<div class="optDiv Js_state left mr10" style="width:148px;">
							<div class="tools text pr" style="width:auto">
									<div class="inputWarp opt">
											<span id="Js_s_oldYear"></span>
											<a href="javascript:void(0)" class="showMenu"></a>
									</div>
							</div>
							<ul>
									<li><a href="javascript:void(0)">2010年</a></li>
									<li><a href="javascript:void(0)">2011年</a></li>
							</ul>
							<select style="display:none;">
									<option>2010年</option>
									<option>2011年</option>
							</select>
					</div>
					<div class="optDiv Js_state left" style="width:148px;">
							<div class="tools text pr" style="width:auto">
									<div class="inputWarp opt">
											<span id="Js_s_oldMonth"></span>
											<a href="javascript:void(0)" class="showMenu"></a>
									</div>
							</div>
							<ul>
									<li><a href="javascript:void(0)">1月</a></li>
									<li><a href="javascript:void(0)">2月</a></li>
							</ul>
							<select style="display:none;">
									<option>1月</option>
									<option>2月</option>
							</select>
					</div>
                	<s class="time-fh"></s>
					<div class="optDiv Js_state left mr10" style="width:148px;">
							<div class="tools text pr" style="width:auto">
									<div class="inputWarp opt">
											<span id="Js_s_newYear"></span>
											<a href="javascript:void(0)" class="showMenu"></a>
									</div>
							</div>
							<ul>
									<li><a href="javascript:void(0)">2010年</a></li>
									<li><a href="javascript:void(0)">2011年</a></li>
							</ul>
							<select style="display:none;">
									<option>2010年</option>
									<option>2011年</option>
							</select>
					</div>
					<div class="optDiv Js_state left" style="width:148px;">
							<div class="tools text pr" style="width:auto">
									<div class="inputWarp opt">
											<span id="Js_s_newMonth"></span>
											<a href="javascript:void(0)" class="showMenu"></a>
									</div>
							</div>
							<ul>
									<li><a href="javascript:void(0)">1月</a></li>
									<li><a href="javascript:void(0)">2月</a></li>
							</ul>
							<select style="display:none;">
									<option>1月</option>
									<option>2月</option>
							</select>
					</div>
				</div>
				<div class="errorMsg left" style="display:none;">错误提示内容</div>
        </div>
        <div class="height_50"></div>
        <div class="p-warp">
                <a href="javascript:;" class="grayBtn mr5 font14 Js_cancle_edu">取消添加</a>
                <a href="javascript:;" class="blueBtn font14 Js_confirm_edu">确认添加</a>
        </div>
        <div class="height_50"></div>
</div>
<div class="Js_edu">
        <div class="pro-wrap">
                <p class="post clearfix">
                        <span class="left mr40 Js_company_rname">Indian institute of Management,Ahmedabad</span>
									<span class="editorBtnList">
										<a href="javascript:;" class="editor bqh Js_editor_btn">编辑资料</a>
										<a href="javascript:;" class="editor bfs Js_file_del">删除</a>
										<a href="javascript:;" class="editor bfs Js_del_ok" style="display:none;">确认删除</a>
										<a href="javascript:;" class="editor bshy Js_file_cancel" style="display:none;">取消删除</a>
									</span>
                </p>
                <p class="company Js_job_rname">Enhancing Sales Force Performance, Sales,Marketing,Management</p>
                <p class="time-area"><span class="Js_roldYear">2010年</span><span class="Js_roldMonth">1月</span><span class="spacing">-</span><span class="Js_rnewYear">2012年</span><span class="Js_rnewMonth">1月</span></p>
                <div class="Js_editor_file" style="display:none;">
                        <div class="height_22"></div>
                        <div class="rowblock clearfix">
                                <div class="pt">学校类型</div>
                                <div class="optDiv Js_state left" style="width:108px;">
                                        <div class="tools text pr" style="width:auto">
                                                <div class="inputWarp opt">
                                                        <span></span>
                                                        <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                        </div>
                                        <ul>
                                                <li><a href="javascript:void(0)">类型一</a></li>
                                                <li><a href="javascript:void(0)">类型二</a></li>
                                        </ul>
                                        <select style="display:none;">
                                                <option>类型一</option>
                                                <option>类型二</option>
                                        </select>
                                </div>
                        </div>
                        <div class="height_18"></div>
                        <div class="rowblock clearfix">
                                <div class="pt">学校名称</div>
                                <div class="tools text left">
                                        <div class="inputWarp empty">
                                                <input type="text" class="Js_company_name">
                                        </div>
                                </div>
                        </div>
                        <div class="height_18"></div>
                        <div class="rowblock clearfix">
                                <div class="pt">院系</div>
                                <div class="tools text left">
                                        <div class="inputWarp empty">
                                                <input type="text" class="Js_job_name">
                                        </div>
                                </div>
                        </div>
                        <div class="height_18"></div>
                        <div class="rowblock clearfix">
                                <div class="pt">入学年份</div>
                                <div class="optDiv Js_state left mr10" style="width:148px;">
                                        <div class="tools text pr" style="width:auto">
                                                <div class="inputWarp opt">
                                                        <span class="Js_oldYear"></span>
                                                        <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                        </div>
                                        <ul>
                                                <li><a href="javascript:void(0)">2010年</a></li>
                                                <li><a href="javascript:void(0)">2011年</a></li>
                                        </ul>
                                        <select style="display:none;">
                                                <option>2010年</option>
                                                <option>2011年</option>
                                        </select>
                                </div>
                                <div class="optDiv Js_state left" style="width:148px;">
                                        <div class="tools text pr" style="width:auto">
                                                <div class="inputWarp opt">
                                                        <span class="Js_oldMonth"></span>
                                                        <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                        </div>
                                        <ul>
                                                <li><a href="javascript:void(0)">1月</a></li>
                                                <li><a href="javascript:void(0)">2月</a></li>
                                        </ul>
                                        <select style="display:none;">
                                                <option>1月</option>
                                                <option>2月</option>
                                        </select>
                                </div>
                                <s class="time-fh"></s>
                                <div class="optDiv Js_state left mr10" style="width:148px;">
                                        <div class="tools text pr" style="width:auto">
                                                <div class="inputWarp opt">
                                                        <span class="Js_newYear"></span>
                                                        <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                        </div>
                                        <ul>
                                                <li><a href="javascript:void(0)">2010年</a></li>
                                                <li><a href="javascript:void(0)">2011年</a></li>
                                        </ul>
                                        <select style="display:none;">
                                                <option>2010年</option>
                                                <option>2011年</option>
                                        </select>
                                </div>
                                <div class="optDiv Js_state left" style="width:148px;">
                                        <div class="tools text pr" style="width:auto">
                                                <div class="inputWarp opt">
                                                        <span class="Js_newMonth"></span>
                                                        <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                        </div>
                                        <ul>
                                                <li><a href="javascript:void(0)">1月</a></li>
                                                <li><a href="javascript:void(0)">2月</a></li>
                                        </ul>
                                        <select style="display:none;">
                                                <option>1月</option>
                                                <option>2月</option>
                                        </select>
                                </div>
                        </div>
                        <div class="height_50"></div>
                        <div class="p-warp">
                                <a href="javascript:;" class="btn h8 mr5 font14 Js_cancle_revise">取消修改</a>
                                <a href="javascript:;" class="btn b73 font14 Js_ok_revise">确认修改</a>
                        </div>
                </div>
        </div>
        <div class="height_40"></div>
        <div class="pro-wrap">
                <p class="post">Cornell University</p>
                <p class="company">Global Food Satety Management, Food Satety,Food Security</p>
                <p class="time-area">1998年5月<span class="spacing">-</span>2000年1月</p>
        </div>
        <div class="height_40"></div>
</div>
<div class="clearfix">
        <div class="sub-tit skill left">擅长技能</div>
        <div class="right mt35">
                <a class="sub-sbtn Js_jnset_btn">设置擅长技能</a>
        </div>
</div>
<div class="pro-wrap clearfix Js_jn_list">
        <div class="skill-tag mr16 left"><span>88</span><em>企业管理</em></div>
        <div class="skill-tag mr16 left"><span>16</span><em>市场营销</em></div>
        <div class="skill-tag mr16 left"><span>7</span><em>互联网宣传</em></div>
</div>
<div class="pro-wrap clearfix Js_jn_set" style="display:none;">
        <div class="tools sel left">
                <div class="inputWarp downMenuExperts">
                        <input type="text" placeholder="请输入你擅长的技能" class="font14">
                </div>
        </div>
        <a href="javascript:void(0)" class="blueBtn fl ml10 Js_jn_add" style="min-width:75px;">添加</a>
        <div class="height_20"></div>
        <div class="tools clearfix" style="width: 572px;">
                <ul class="jn-list clearfix"></ul>
        </div>
        <div class="height_50"></div>
        <div class="p-warp">
                <a href="javascript:;" class="grayBtn mr5 font14 Js_jn_cancle">取消修改</a>
                <a href="javascript:;" class="blueBtn font14 Js_jn_ok">确认修改</a>
        </div>
</div>
<div class="height_40"></div>
</div>
</div>
<!--<div class="detailPageMid" >-->
<!--        <div class="proContextDiv">-->
<!--                <div class="titDiv">-->
<!--                        <span></span>-->
<!--                        <h2 class="Js_scroll">我的相册</h2>-->
<!--                </div>-->
<!--                <div class='height_30'></div>-->
<!--                <ul  class="album-list">-->
<!--                        <li class="album-wrap">-->
<!--                                <div class="album-box">-->
<!--                                        <i class="icon video"></i>-->
<!--                                        <a href="javascript:;" class="manual1">-->
<!--                                                <div class="thumb">-->
<!--                                                        <img src="./assets/temp/a1.jpg" alt="">-->
<!--                                                        <div class="ctr-bg">-->
<!--                                                                <span class="h">25</span>-->
<!--                                                                <span class="c">230</span>-->
<!--                                                        </div>-->
<!--                                                </div>-->
<!--                                                <div class="bottom">-->
<!--                                                        <p class="title">我的自拍视频</p>-->
<!--                                                        <p class="des">8段视频</p>-->
<!--                                                </div>-->
<!--                                        </a>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                        <li class="album-wrap">-->
<!--                                <div class="album-box">-->
<!--                                        <i class="icon fav"></i>-->
<!--                                        <a href="javascript:;" class="manual1">-->
<!--                                                <div class="thumb">-->
<!--                                                        <img src="./assets/temp/a1.jpg" alt="">-->
<!--                                                        <div class="ctr-bg">-->
<!--                                                                <span class="h">4856</span>-->
<!--                                                                <span class="c">74</span>-->
<!--                                                        </div>-->
<!--                                                </div>-->
<!--                                                <div class="bottom">-->
<!--                                                        <p class="title">头像照片</p>-->
<!--                                                        <p class="des">12张照片</p>-->
<!--                                                </div>-->
<!--                                        </a>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                        <li class="album-wrap">-->
<!--                                <div class="album-box">-->
<!--                                        <i class="icon pic"></i>-->
<!--                                        <a href="javascript:;" class="manual1">-->
<!--                                                <div class="thumb">-->
<!--                                                        <img src="./assets/temp/a3.jpg" alt="">-->
<!--                                                        <div class="ctr-bg">-->
<!--                                                                <span class="h">5</span>-->
<!--                                                                <span class="c">8</span>-->
<!--                                                        </div>-->
<!--                                                </div>-->
<!--                                                <div class="bottom">-->
<!--                                                        <p class="title">创意世界大赛照片</p>-->
<!--                                                        <p class="des">12张照片</p>-->
<!--                                                </div>-->
<!--                                        </a>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                        <li class="album-wrap">-->
<!--                                <div class="album-box">-->
<!--                                        <a href="javascript:;" class="manual1">-->
<!--                                                <div class="thumb">-->
<!--                                                        <img src="./assets/temp/a4.jpg" alt="">-->
<!--                                                        <div class="ctr-bg">-->
<!--                                                                <span class="h">212</span>-->
<!--                                                                <span class="c">230</span>-->
<!--                                                        </div>-->
<!--                                                </div>-->
<!--                                                <div class="bottom">-->
<!--                                                        <p class="title">旅游2013</p>-->
<!--                                                        <p class="des">12张照片</p>-->
<!--                                                </div>-->
<!--                                        </a>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                </ul>-->
<!--        </div>-->
<!--        <div class="height_40"></div>-->
<!--</div>-->

<!--创意世界大赛-->
<div class="detailPageMid" style="padding-bottom:0">
<div class="proContextDiv">
<div class="titDiv">
    <span></span>
    <h2 class="Js_scroll">我的创意世界大赛</h2>
</div>
<div class="height_31"></div>
<div class="clearfix">
    <ul class="clearfix l-areaNewsTit fl">
        <li><a href="#">热门比赛</a></li>
        <li><a href="#">创意投资项目比赛</a></li>
        <li><a href="#">创意任务人才比赛</a></li>
        <li><a href="#">创意公益明星比赛</a></li>
        <!--                <li class="last"><a href="#">创意世界交易所</a></li>-->
    </ul>
    <a href="#" class="lookAll">查看全部</a>
</div>
<div class="height_10"></div>
<div class="clearfix">
    <div id="match-product" class="areaMatch" style="width:940px;">
        <div class="pagearea"></div>
        <div class="itemListDiv Js_pageChange" style="overflow: hidden; margin-left:-8px;">
            <ul class="clearfix">
                <!--列表数目务必保证是3的倍数-->
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>幻世纪大作再现-蚀刻暴龙拼装模型1 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型2 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型3 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型4 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型5 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--<div class="clearfix">
  <div id="match-product" class="areaMatch" style="width:940px;">
    <div class="pagearea"></div>
    <div class="itemListDiv Js_pageChange1" style="overflow: hidden; margin-left:-14px;">
      <ul class="clearfix">
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>幻世纪大作再现-蚀刻暴龙拼装模型1 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型2 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型3 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型4 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型5 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>-->
</div>
</div>

<!--创意世界交易所-->
<div class="detailPageMid" style="padding-bottom:0">
<div class="proContextDiv">
<div class="titDiv">
    <span></span>
    <h2 class="Js_scroll">我的创意世界交易所</h2>
</div>
<div class="height_31"></div>
<div class="clearfix">
    <!--    <ul class="clearfix l-areaNewsTit fl">-->
    <!--        <li><a href="#">热门比赛</a></li>-->
    <!--        <li><a href="#">创意投资项目比赛</a></li>-->
    <!--        <li><a href="#">创意任务人才比赛</a></li>-->
    <!--        <li><a href="#">创意公益明星比赛</a></li>-->
    <!--        <li class="last"><a href="#">创意世界交易所</a></li>-->
    <!--    </ul>-->
    <!--    <a href="#" class="lookAll">查看全部</a>-->
</div>
<div class="height_10"></div>
<div class="clearfix">
    <div id="match-product" class="areaMatch" style="width:940px;">
        <div class="pagearea"></div>
        <div class="itemListDiv Js_pageChange" style="overflow: hidden; margin-left:-8px;">
            <ul class="clearfix">
                <!--列表数目务必保证是3的倍数-->
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>幻世纪大作再现-蚀刻暴龙拼装模型1 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型2 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型3 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型4 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型5 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--<div class="clearfix">
  <div id="match-product" class="areaMatch" style="width:940px;">
    <div class="pagearea"></div>
    <div class="itemListDiv Js_pageChange1" style="overflow: hidden; margin-left:-14px;">
      <ul class="clearfix">
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>幻世纪大作再现-蚀刻暴龙拼装模型1 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型2 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型3 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型4 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型5 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>-->
<a href="javascript:;" class="yd-share-btn Js_share_btn" style="position: relative; top: 55px;"></a>
</div>
</div>

<!--帖子-->
<?php $itemTT = "我的动态" ; ?>
<?php include("./modules/item.php"); ?>
</div>
</div>
</div>
<script type="text/javascript">
        var container = $('.posts-wraplist');
        container.masonry({
                columnWidth: 448,
                gutter: 25,
                itemSelector: '.posts-item'
        });
        container.imagesLoaded( function() {
                container.masonry();
        });
        $('.manual1').click(function(){
                $.fancybox([
                        './assets/temp/bigpic1.jpg',
                        './assets/temp/bigpic2.jpg',
                        './assets/temp/bigpic3.jpg',
                        './assets/temp/bigpic4.jpg'
                ],{
                        'padding':0,
                        'transitionIn':'none',
                        'transitionOut':'none',
                        'type':'image',
                        'changeFade':0
                })
        });

        // var msnry

        // imagesLoaded( container, function() {
        //   msnry = new Masonry( container );
        // });

        // var msnry = new Masonry( container, {
        // 	// options
        // 	columnWidth: 468,
        // 	itemSelector: '.posts-item'
        // });
        //视频播放
        var a = $("#video1");
        var b = $(".js_player");
        b.css({"top":Math.floor((a.height()-b.height())/2)+"px","left":Math.floor((a.width()-b.width())/2)+"px"})
        var myVideo= a.get(0);
        b.click(function(){
                myVideo.play();
                $(this).fadeOut();
        });
        myVideo.addEventListener("ended",function(){b.fadeIn()})
</script>