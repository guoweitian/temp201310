<div class="content">
<div class="detailPageMid">
<div class="proContextDiv">
        <div class="height_38"></div>
        <div class="titDiv">
                <span></span>
                <h2>创创币</h2>
        </div>
        <div class="height_40"></div>

        <div class="clearfix">
                <div class="left pr">
                        <h3 class="n-tit">创创币余额</h3>
                        <div class="clearfix n-mon">
                                <span>1,234,567,898</span><s class="left">枚</s>
                                <a href="#" class="l-m_btn left Js_recharge">购买</a>
                        </div>
                        <s class="p-box b1 Js_recharge_b" style="display:none; left:330px; bottom:-18px;"></s>
                </div>
                <div class="left ml78">
                        <h3 class="n-tit">未领取的创意指数奖励</h3>
                        <div class="clearfix n-mon">
                                <span>1,234</span><s class="left">枚</s><s class="left ml40">创创币</s>
                                <a href="#" class="l-m_btn left Js_receive_ccb">立即领取</a>
                        </div>
                </div>
        </div>
        <div class="height_18"></div>
</div>

<div style="background:#e6e6e6; display:none;" class="Js_recharge_c">
        <div class="proContextDiv">
                <div class="height_18"></div>
                <div class="titDiv">
                        <span></span>
                        <h2>购买创创币</h2>
                </div>
                <div class="height_33"></div>
                <div class="ft-style"  style="font-size:16px;">
                        <div class="n-agree w256">
						<span class="Js_radio Js_labelck_ok" data-name="rad">
              <label class="radio">
                      <input type="radio" name="rad">
              </label>
                <span class="p-level b73">&nbsp;<b class="c-money">100</b>&nbsp;&nbsp;枚<s class="c-input">1元</s></span>

            </span>
                        </div>
                        <div class="height_27"></div>
                        <div class="n-agree w256">
						<span class="Js_radio Js_labelck_ok" data-name="rad">
              <label class="radio">
                      <input type="radio" name="rad" checked>
              </label>
                <span class="p-level b73">&nbsp;<b class="c-money">1000</b>&nbsp;&nbsp;枚<s class="c-input">10元</s></span>

            </span>
                        </div>
                        <div class="height_27"></div>
                        <div class="n-agree w256">
						<span class="Js_radio Js_labelck_ok " data-name="rad">
              <label class="radio">
                      <input type="radio" name="rad">
              </label>
                <span class="p-level b73">&nbsp;<b class="c-money">5000</b>&nbsp;&nbsp;枚<s class="c-input">50元</s></span>

            </span>
                        </div>
                        <div class="height_27"></div>
                        <div class="n-agree w256">
						<span class="Js_radio Js_labelck_ok" data-name="rad">
              <label class="radio">
                      <input type="radio" name="rad" class="Js_input_text">
              </label>
                <span class="p-level b73">&nbsp;<b class="c-money">10000</b>&nbsp;&nbsp;枚<s class="c-input">100元</s></span>
            </span>

                        </div>
                        <div class="height_27"></div>
                        <div class="n-agree clearfix js_change_money" style="line-height: 35px;">
						<span class="Js_radio Js_labelck_ok left" data-name="rad">
              <label class="radio">
                      <input type="radio" name="rad">
              </label>
                <span class="p-level b73"><input type="text" class="w108 ml10 Js_input_text" value="500">&nbsp;&nbsp;枚<s class="c-input" style="padding-left: 30px;">5元</s></span>

            </span>
                                <div class="errorMsg left" style="display: none;">错误提示内容</div>
                        </div>
                </div>
                <div class="height_45"></div>
                <div class="p-warp">
                        <a href="javascript::" class="btn h8 mr5 Js_cancle_buy">取消购买</a>
                        <a href="javascript:;" class="btn b73">确认购买</a>
                </div>
                <div class="height_50"></div>
                <div class="sp-line" style="top:0; margin:0;">
                        <div class="line-left"></div>
                        <div class="line-right"></div>
                </div>

        </div>
</div>
<div class="proContextDiv">
        <div class="height_30"></div>
        <div>
                <label class="l-start">起止时间：</label>
                <div class="tools text left" style="width:148px;">
                        <input class="datebox bbit-dp-input" type="text" id="dateone" readonly="readonly" style="width:144px !important;">
                </div>
                <s class="time-fh"></s>
                <div class="tools text left" style="width:148px;">
                        <input class="datebox bbit-dp-input" type="text" id="datetwo" readonly="readonly" style="width:144px !important;">
                </div>
                <ul class="clearfix l-areaNewsTit userfile">
                        <li><a href="javascript:;" class="Js_time_filter" data-type="2013/10/27">今天</a></li>
                        <li><a href="javascript:;" class="Js_time_filter current" data-type="2013/9/27">最近1个月</a></li>
                        <li><a href="javascript:;" class="Js_time_filter" data-type="2013/7/27">3个月</a></li>
                        <li><a href="javascript:;" class="Js_time_filter" data-type="2012/10/27">1年</a></li>
                        <li class="last"><a href="javascript:;" class="Js_time_filter" data-type="2012/10/27">1年前</a></li>
                </ul>
        </div>
        <div class="height_38"></div>
        <div>
                <div class="clearfix">
                        <ul class="clearfix l-areaNewsTit userfile left" style="line-height:normal;">
                                <li><a href="#" class="current">全部</a></li>
                                <li><a href="#">交易出入</a></li>
                                <li class="last"><a href="#">交易支出</a></li>
                        </ul>
                        <div class="pageItemDiv right mb25">
                                <a href="javascript:;" class="fyBtn prev Js_page_list_prev Js_net_rmb_account"></a>
						<span class="pageItem cfs19 Js_page_list_itemNum">
							<a href="javascript:;" class="current Js_net_rmb_account">1</a>
							<a href="javascript:;" class="Js_net_rmb_account">2</a>
							<a href="javascript:;" class="Js_net_rmb_account">3</a>
							<a href="javascript:;" class="Js_net_rmb_account">4</a>
							<a href="javascript:;" class="Js_net_rmb_account">5</a>
						</span>
                                <a href="javascript:;" class="fyBtn next Js_page_list_next Js_net_rmb_account"></a>
                        </div>
                </div>

                <div class="height_27"></div>
                <div>
                        <!--<div class="clearfix t-title">
                            <span class="time">时间</span>
                            <span class="dis">摘要</span>
                            <span class="sum">金额</span>
                            <span class="account-money">账户余额</span>
                            <span class="cz">操作</span>
                        </div>-->
                        <div>
                                <table width="940" border="0" cellpadding="0" cellspacing="0" class="t-detail Js_rmb_account_list">
                                        <tr style="background:#deded9">
                                                <td class="td-time">时间</td>
                                                <td class="td-dis">摘要</td>
                                                <td class="td-sum red" style="font-size:14px; color:#5F5F5F; font-weight:normal">金额</td>
                                                <td class="td-a-money" style="font-size:14px; color:#5F5F5F; font-weight:normal">账户余额</td>
                                                <td class="td-cz"><a href="#">操作</a></td>
                                        </tr>
                                        <tr>
                                                <td class="td-time">2013/01/25 22:20:22</td>
                                                <td class="td-dis">淘宝购物-聚壹之家牛津百纳箱聚壹之家牛津60L+30L二件套玩具收纳物箱</td>
                                                <td class="td-sum red">-98.00</td>
                                                <td class="td-a-money">1,235.65</td>
                                                <td class="td-cz"><a href="pay-order-detail.php">详情</a></td>
                                        </tr>
                                        <tr>
                                                <td class="td-time">2013/01/25 21:20:22</td>
                                                <td class="td-dis">ThinkPad E430C14英寸笔记本电脑</td>
                                                <td class="td-sum green">+98.00</td>
                                                <td class="td-a-money">1,235.65</td>
                                                <td class="td-cz"><a href="pay-order-detail.php">详情</a></td>
                                        </tr>
                                        <tr>
                                                <td class="td-time">2013/01/25 20:20:22</td>
                                                <td class="td-dis">神奇的台灯</td>
                                                <td class="td-sum green">+98.00</td>
                                                <td class="td-a-money">1,235.65</td>
                                                <td class="td-cz"><a href="pay-order-detail.php">详情</a></td>
                                        </tr>
                                        <tr>
                                                <td class="td-time">2013/01/25 19:20:22</td>
                                                <td class="td-dis">ThinkPad E430C14英寸笔记本电脑</td>
                                                <td class="td-sum red">-98.00</td>
                                                <td class="td-a-money">1,235.65</td>
                                                <td class="td-cz"><a href="pay-order-detail.php">详情</a></td>
                                        </tr>
                                        <tr>
                                                <td class="td-time">2013/01/25 18:20:22</td>
                                                <td class="td-dis">百诺（BENRO）C2681TB1 可拆卸独脚架</td>
                                                <td class="td-sum green">+98.00</td>
                                                <td class="td-a-money">1,235.65</td>
                                                <td class="td-cz"><a href="pay-order-detail.php">详情</a></td>
                                        </tr>
                                        <tr>
                                                <td class="td-time">2013/01/25 17:20:22</td>
                                                <td class="td-dis">百诺（BENRO）C2681TB1 可拆卸独脚架</td>
                                                <td class="td-sum green">+98.00</td>
                                                <td class="td-a-money">1,235.65</td>
                                                <td class="td-cz"><a href="pay-order-detail.php">详情</a></td>
                                        </tr>
                                </table>
                        </div>
                        <div class="height_40"></div>
                        <div class="pageItemDiv right mb25">
                                <a href="javascript:;" class="fyBtn prev Js_page_list_prev Js_net_rmb_account"></a>
						<span class="pageItem cfs19 Js_page_list_itemNum">
							<a href="javascript:;" class="current Js_net_rmb_account">1</a>
							<a href="javascript:;" class="Js_net_rmb_account">2</a>
							<a href="javascript:;" class="Js_net_rmb_account">3</a>
							<a href="javascript:;" class="Js_net_rmb_account">4</a>
							<a href="javascript:;" class="Js_net_rmb_account">5</a>
						</span>
                                <a href="javascript:;" class="fyBtn next Js_page_list_next Js_net_rmb_account"></a>
                        </div>
                </div>
        </div>
</div>
</div>
</div>
<script type="text/javascript">
        $(function(){
                var d = new Date();
				var dy = d.getFullYear();
				var dm = d.getMonth()+1;
				var dd = d.getDate();
				$('#dateone').val(dy + '/' + (dm-1) + '/' + dd);
				$('#datetwo').val(dy + '/' + dm + '/' + dd);
				dataControl('dateone','datetwo');
                var a = $(".js_change_money").find("input").eq(1);
                var b = $(".js_change_money").find("s");
                var c = $(".js_change_money").find(".errorMsg");
                var ex =/^\d+$/;
                a.bind("input propertychange",function(){
                        setTimeout(function(){ if(ex.test(a.val()/100)){
                                    b.html(a.val()/100+"元");
                                    c.css("display","none");
                            }
                            else{c.html("请输入100的倍数");
                                    c.css("display","block");
                            }},
                            800)

                });
        })
</script>
