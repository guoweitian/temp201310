<div class="content">
<div class="detail-wrap exchange">
<div class="detail-imgbg">
        <div class="toppattern"></div>
        <div class="detailTopDiv pew">
                <h3>这里是比赛名称，如果有很多字，这里也只能显示一行标题。这里是比赛名称，如果有很多字，这里也只能显示一行标题。</h3>
                <p class="disDiv">
                        <span class="loc">成都市青羊区小南街</span>
                        <span class="heart">已有52人奉献爱心</span>
                        <span class="m_num">T2013052400021</span>
                </p>
                <div>
                        <div class="u_listItem left">
                                <a href="#" class="leftArea shadow">
                                        <img src="./assets/temp/5.png" alt="">
                                </a>
                                <div class="rightArea w600">
                                        <p class="rightName"><a href="#" class="Js_visitCard cfff">刘兆宇</a></p>
                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                </div>
                        </div>
                        <div class="tz_moneyDiv right">
                                <p>已筹集善款:<b>10,000元</b></p>
                                <a href="./hire-donation.php" class="bgimgBtn js_detailPublish right">我要参与</a>
                        </div>
                </div>
        </div>
</div>
<div class="form-wrap js_detailForm">
        <div id="tabs-tab" class="publish-form publish w765">
                <div class="row">
                        <div class="large-12 columns w765"  style="overflow: hidden;">
                                <div class="publish-progress">
                                        <div class="height_40"></div>
                                        <div class="issueStrip pr">
                                                <div class="gray-bg"></div>
                                                <ul id="tab-t" class="clearfix strip strip2">
                                                        <!--当前current/没填notFill/最后一步没填notFill last-->
                                                        <li class="one current" style="background-color: #F3F8FC">
                                                                <span class="fousLine w-first"></span>
                                                                <a class="selected" href="#step-one">
                                                                        <b>1</b>
                                                                        <span class="st-one">填写金额</span>
                                                                </a>
                                                        </li>
                                                        <li class="notFill two ml" style="background-color: #F3F8FC">
                                                                <s></s>
                                                                <span class="fousLine wh"></span>
                                                                <span class="fousLine w-last"></span>
                                                                <a href="#step-two"><b>2</b><span class="st-two" style="margin-left: -11px">确认协议</span></a>
                                                        </li>
                                                </ul>
                                        </div>
                                </div>
                        </div>
                </div>
                <div id="tabs-items" class="Js_tabs_items clearfix">
                        <div class="row tabs-c">
                                <div class="large-12 columns">

                                        <div class="rowblock">
                                                <div class="pt">请填写您的最高参与金额</div>
                                                <!--<p class="pdes">最高出价是您能接受的最高价格，系统将根据竞价规则，在您的出价范围内以最低价格成交。</p>-->
                                                <div class="height_15"></div>
                                                <div class="tools text left">

                                                        <div class="inputWarp enterNum">
                                                                <input type="text" id="Js_jk_number">
                                                        </div>
                                                        <div class="numFormat">.00</div>
                                                </div>
                                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                                        </div>
                                        <div class="height_50"></div>
                                        <a href="#step-two" class="blueBtn Js_s_one">下一步</a>
                                </div>
                                <div class="height_60"></div>
                        </div>


                        <div class="row tabs-c">
                                <div class="large-12 columns">
                                        <div class="rowblock l-edit">
                                                <div class="pt">确认创意世界公益活动参与协议</div>
                                                <p class="pdes">请您阅读并确认创意世界公益活动参与协议</p>
                                                <div class="height_10"></div>
                                                <div class="tools textArea left">
                                                        <textarea style="font-size: 15px; line-height: 1.5" id="Js_release_agreement" readonly>
我要参与公益活动协议
1	简介
四川创意世界科技有限公司同意按照本协议的规定及其不时发布的操作规则提供创意世界公益活动服务（以下简称“公益活动”），为使用该服务，用户应当同意本协议的全部条款并按照页面上的提示完成参与公益活动程序。用户一旦发布或参与公益活动即表示用户完全接受本协议项下的全部条款。
用户应当对以其创意世界账户进行的所有公益活动和事件负法律责任，不得违反国家现行法律法规。
2	公益活动发起人定义
通过创意世界公益活动发起公益项目，包括但不限于个人求助、捐助等，以达到为求助人募集善款、物品、或者招募志愿者的目的，是整个创意世界公益活动求助流程中的核心用户。创意世界测试阶段，发起人仅限创意公益明星比赛参赛明星。
3	公益活动发起人的权利
发起人有权利申请使用创意世界公益活动服务，包括但不限于个人求助、拍卖等服务；并将在了解和保证项目真实性的基础上，作为项目重要参与人员全程跟进公益活动进展，使整个活动公开、透明的进行。
4	公益活动发起人的义务
	发起人使用创意世界公益活动服务时，必须向创意世界提供准确、真实的个人资料、项目基本信息及照片图片，如资料及信息有任何变动，必须及时更新；如因资料及信息有任何不准确造成项目失败或引起质疑，由用户自行承担全部风险和责任；与创意世界无涉。
	发起人需保证求助项目的公益性质,符合“公益活动”这个平台适用原则,不得夸张或者渲染事件,更不基于商业目的进行求助。
	发起人正式发布公益项目后，无论是否达到求助目标，都需及时在线上通过公益活动反馈救助情况及所获救助资源消耗情况；反馈信息需准确、真实。
5	公益活动发起人出现以下任一情况即失去资格：
	利用发起人身份谋求不正当的利益；
	禁止发起人以创意世界公益活动身份之便利，收受任何形式的财物，一经发现核实即失去资格。情节严重者将追究其法律责任。
	隐瞒或虚报个人信息；未提供真实个人信息。
发起人失去资格同时将面临包括但不限于全站公示、禁止再次使用创意世界公益活动、关闭创意世界账号等站方处理。
6	公益活动参与人定义
通过微公益平台参与公益活动的创意世界用户，实现方式为捐款、捐赠物资、报名志愿者活动、关注、分享等；参与人需保证所捐赠物资为个人合法所得，并对其捐赠钱款、物品有处分权。
7	公益活动参与人的权利：
	参与人有权利申请使用创意世界公益活动服务，包括但不限于个人求助、拍卖等产品。
	参与人在公益项目参与成功后有权向公益活动发起人索要捐款金额相应发票，需参与人在捐赠同时提供发票单位、个人、联系方式及地址。
	创意世界公益活动为所有参与人公开所有信息，参与人可线上查询所有参与过的公益活动、公益行为包括不限于支持、收藏、关注、捐款、捐物、报名志愿者、拍卖、个人求助。
	后期将推出的其它激励措施。
8	公益活动参与人的义务
	参与人使用创意世界公益活动服务时，建立在信任该项目中的公益支持（方）的前提下，信任求助人、发起人自主发布的个人的施助信息，并自愿支持求助人。
	参与人捐赠成功即为确认捐赠善款无误，捐款成功后由相关公益组织进行管理和使用，创意世界公益活动募捐项目款项一旦由用户捐赠成功即不可退回；如出现诈捐情况，创意世界公益活动敦促公益支持（方）确认项目情况，并协调公益组织退款，除此情况外捐赠款项不可退回。
	参与人同意，如遇自然因素及其他不可抗力导致公益活动结束，参与人所捐物资转做同类公益项目使用，创意世界公益活动需在其公益活动服务的相关页面进行公示，不再另行通知参与人。
	参与人需知悉创意世界公益活动为创意世界提供的支持公益事业的专业产品，在此产品上进行的任何操作默认为参与人本人对个人创意世界账号进行操作，包含捐款账户。
	参与人如需开具相应捐赠发票，可向受捐公益组织提交申请。
	企业用户作为参与人使用创意世界公益活动产品时，需按承诺捐赠所约定款项及实物。
9	其他
用户因微博行为引发的法律纠纷，与新浪公司无关。
微公益可依照互联网发展的不同阶段，随着公益产品管理经验的不断丰富，出于维护微博秩序的目的，不断完善本协议
                                                        </textarea>
                                                </div>
                                                <div class="errorMsg left" style="display:none;">请确认同意协议</div>
                                        </div>
                                        <div class="height_10"></div>
                                        <div class="l-edit">
                                                <p class="n-agree left">
                <span class="Js_checkBox" data-name="chk">
                <label class="Js_label_ok ok">
                        <input type="checkbox" name="chk" checked="checked" id="Js_readed_agreement">
                        我已经阅读并同意创意世界公益活动参与协议</label>
                </span>
                                                </p>
                                                <div class="errorMsg left" style="margin-top: -10px; display:none;">请确认同意协议</div>
                                        </div>

                                        <div class="height_40"></div>
                                        <div class="rowblock l-edit">
                                                <div class="pt left">
                                                        <div class="sign-bg">
                                                                <input class="signature" id="Js_signature"></div>
                                                </div>
                                                <div class="errorMsg left mt8" style="display:none;">错误提示内容</div>
                                        </div>
                                        <div class="height_50"></div>
                                        <a href="#step-one" class="grayBtn Js_s_two_pre mr16">上一步</a>
                                        <a href="#step-two" class="blueBtn big-blueBtn" id="Js_donations_agreement">确认使用协议</a>
                                </div>
                                <div class="height_60"></div>
                        </div>
                </div>
        </div>
</div>

<div class="detailPageMidDiv">

        <!--公益详情 开始-->
        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="titDiv">
                                <span></span>
                                <h2 class="Js_scroll">公益详情</h2>
                        </div>
                        <div class="context">〖商机〗2006—2008年，因奥运会而产生的各类商机不断出现，每一个商机背后都意味着巨大的财富，这期间，如果把握好其中一项商机，一生的命运可能都会改变。对于个人而言，怎么样把握这个难得的商机？〖发现〗其实奥运会上最有值钱的东西就是5个吉祥物了。现在很多的人已经开始打它的主意，相关的吉祥物产品，诸如卡通像、剪纸等等     也将不断出现。对于个人在这方面而言，获利可能只是经销产品而已，而经营的人多，竟争也会激烈！但是，却有一门被忽略的冷门绝活手艺，将它应到吉祥物的制作上，所制作的吉祥物产品不仅形象出众、与众不同，而且很有内涵，极其新颖独特！绝不是我们可见到的普通吉祥物产品。如果面世，一定会深受迎！</div>
                </div>
        </div>
        <!--公益详情 结束-->

        <!--爱心用户 开始-->
        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="titDiv">
                                <span></span>
                                <h2 class="Js_scroll">爱心用户</h2>
                        </div>
                        <ul class="clearfix u_list Js_dona_u_list">
                                <li>
                                        <div class="auctionHisList clearfix">
                                                <div class="u_listItem left mr160 w460">
                                                        <a href="#" class="leftArea shadow">
                                                                <img src="./assets/temp/5.png">
                                                        </a>
                                                        <div class="rightArea">
                                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                                                                <p class="rightList"><a href="#">中国四川成都</a></p>
                                                        </div>
                                                </div>
                                                <div class="c-a left">
                                                        <span><em>50,000</em><small>元</small></span>
                                                        <p>已筹善款</p>
                                                </div>
                                                <div class="a-t right">
                                                        <span>22:52:18 2013/05/17</span>
                                                        <p>参与时间</p>
                                                </div>
                                        </div>
                                </li>
                                <li>
                                        <div class="auctionHisList clearfix">
                                                <div class="u_listItem left mr160 w460">
                                                        <a href="#" class="leftArea shadow">
                                                                <img src="./assets/temp/5.png">
                                                        </a>
                                                        <div class="rightArea">
                                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                                                                <p class="rightList"><a href="#">中国四川成都</a></p>
                                                        </div>
                                                </div>
                                                <div class="c-a left">
                                                        <span><em>42,000</em><small>元</small></span>
                                                        <p>已筹善款</p>
                                                </div>
                                                <div class="a-t right">
                                                        <span>13:21:33 2013/05/13</span>
                                                        <p>参与时间</p>
                                                </div>
                                        </div>
                                </li>
                                <li>
                                        <div class="auctionHisList clearfix">
                                                <div class="u_listItem left mr160 w460">
                                                        <a href="#" class="leftArea shadow">
                                                                <img src="./assets/temp/5.png">
                                                        </a>
                                                        <div class="rightArea">
                                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                                                                <p class="rightList"><a href="#">中国四川成都</a></p>
                                                        </div>
                                                </div>
                                                <div class="c-a left">
                                                        <span><em>33,000</em><small>元</small></span>
                                                        <p>已筹善款</p>
                                                </div>
                                                <div class="a-t right">
                                                        <span>09:11:20 2013/05/11</span>
                                                        <p>参与时间</p>
                                                </div>
                                        </div>
                                </li>
                        </ul>
                </div>
        </div>
        <!--爱心用户 结束-->

        <!--社区论坛引用文件-->
        <?php $forumTT = "讨论区"; //社区论坛名称变量 ?>
        <?php include("./modules/discuss.php"); ?>

        <!--竞猜引用文件-->
        <?php $guessTT = "竞猜"; ?>
        <?php include("./modules/lottery.php"); ?>

</div>
</div>
</div>
<script type="text/javascript">
        $(function(){
                tabs('tabs-tab','tab-t', 1, 'click');
                tabs('tabs-tab','tab-w', 1, 'click');
        });
</script>