<div class="content">
<div class="detail-wrap exchange">
<div class="detail-imgbg">
        <div class="toppattern"></div>
        <div class="detailTopDiv pew">
                <div style="height:348px;"></div>
                <div>
                        <div class="u_listItem left">
                                <a href="#" class="leftArea  shadow">
                                        <img src="./assets/temp/5.png" alt="">
                                </a>
                                <div class="rightArea w600">
                                        <p class="rightName "><a class="Js_visitCard cfff" href="#">刘兆宇</a></p>
                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                </div>
                        </div>
                        <div class="tz_moneyDiv right" style=" padding-top: 40px;">
                                <a href="javascript:" class="attenBtn ml10 likeBtn right">123,585</a>
                                <a href="javascript:" class="attenBtn right" id="Js_add_attention">添加关注</a>
                        </div>
                </div>
        </div>
</div>
<div class="detailPageMidDiv">
<!--关于-->
<div class="detailPageMid" >
        <div class="proContextDiv" style="width: 880px;">
                <div class="titDiv">
                        <span></span>
                        <h2 class="Js_scroll">关于他</h2>
                </div>
                <div class="sub-tit info">个人介绍</div>
                <div class="pro-wrap">
                        <p class="con">中国著名企业家，生于浙江杭州，祖籍绍兴，阿里巴巴集团主要创始人之一。现任阿里巴巴集团董事局主席，他是《福布斯》杂志创办50多年来成为封面人物的首位大陆企业家，曾获选为未来全球领袖。除此之外，马云还担任中国雅虎董事局主席、杭州师范大学阿里巴巴商学院院长、华谊兄弟传媒集团董事、艺术品中国商业顾问等职务。2012年11月，阿里巴巴在网上的交易额突破一万亿大关，马云由此被扣以“万亿侯”的称号。2013年5月10日，马云正式卸任阿里巴巴CEO。2013年5月28日，马云联合阿里巴巴集团、银泰集团、复星集团、富春集团、顺丰集团、中通、圆通、申通、韵达等多家民营快递企业，组建物流网络平台“菜鸟网络科技有限公司”。马云出任董事长。
                </div>
                <div class="height_40"></div>
                <div class="sub-tit experience">个人经历</div>
                <div class="pro-wrap">
                        <p class="post">客户支持部经理</p>
                        <p class="company">联想集团</p>
                        <p class="time-area">2010年1月<span class="spacing">-</span>现在（3年5个月）<span class="spacing">|</span> 中国成都</p>
                        <p class="des">成立了联想集团客户支持部，处理客户售后问题。</p>
                </div>
                <div class="height_50"></div>
                <div class="pro-wrap">
                        <p class="post">Head - Business Relations</p>
                        <p class="company">Equionox</p>
                        <p class="time-area">2008年1月<span class="spacing">-</span>现在（3年11个月）<span class="spacing">|</span>中国成都</p>
                        <p class="des">In Equinox Labs I Head the Business Relations Team. Interacting with Key Clients, Government and Media on matters of FSSAI Compliance, Food Quality Monitoring, Air Quality Monitoring, etc.	</p>
                </div>
                <div class="height_40"></div>
                <div class="sub-tit edu">教育经历</div>
                <div class="pro-wrap">
                        <p class="post">Indian institute of Management,Ahmedabad</p>
                        <p class="company">Enhancing Sales Force Performance, Sales,Marketing,Management</p>
                        <p class="time-area">2010年1月<span class="spacing">-</span>2012年1月</p>
                </div>
                <div class="height_40"></div>
                <div class="pro-wrap">
                        <p class="post">Cornell University</p>
                        <p class="company">Global Food Satety Management, Food Satety,Food Security</p>
                        <p class="time-area">1998年5月<span class="spacing">-</span>2000年1月</p>
                </div>
                <div class="height_40"></div>
                <div class="sub-tit skill">擅长技能</div>
                <div class="pro-wrap">
                        <div class="skill-tag mr16 left"><span>88</span><em>企业管理</em></div>
                        <div class="skill-tag mr16 left"><span>16</span><em>市场营销</em></div>
                        <div class="skill-tag mr16 left"><span>7</span><em>互联网宣传</em></div>
                </div>
                <div class="height_40"></div>
        </div>
</div>

<!--<!--照片-->-->
<!--<div class="detailPageMid" >-->
<!--        <div class="proContextDiv">-->
<!--                <div class="titDiv">-->
<!--                        <span></span>-->
<!--                        <h2 class="Js_scroll">他的相册</h2>-->
<!--                </div>-->
<!--                <div class='height_30'></div>-->
<!--                <ul  class="album-list">-->
<!--                        <li class="album-wrap">-->
<!--                                <div class="album-box">-->
<!--                                        <i class="icon video"></i>-->
<!--                                        <a href="#">-->
<!--                                                <div class="thumb">-->
<!--                                                        <img src="./assets/temp/a1.jpg" alt="">-->
<!--                                                        <div class="ctr-bg">-->
<!--                                                                <span class="h">25</span>-->
<!--                                                                <span class="c">230</span>-->
<!--                                                        </div>-->
<!--                                                </div>-->
<!--                                                <div class="bottom">-->
<!--                                                        <p class="title">我的自拍视频</p>-->
<!--                                                        <p class="des">8段视频</p>-->
<!--                                                </div>-->
<!--                                        </a>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                        <li class="album-wrap">-->
<!--                                <div class="album-box">-->
<!--                                        <i class="icon fav"></i>-->
<!--                                        <a href="#">-->
<!--                                                <div class="thumb">-->
<!--                                                        <img src="./assets/temp/a1.jpg" alt="">-->
<!--                                                        <div class="ctr-bg">-->
<!--                                                                <span class="h">4856</span>-->
<!--                                                                <span class="c">74</span>-->
<!--                                                        </div>-->
<!--                                                </div>-->
<!--                                                <div class="bottom">-->
<!--                                                        <p class="title">头像照片</p>-->
<!--                                                        <p class="des">12张照片</p>-->
<!--                                                </div>-->
<!--                                        </a>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                        <li class="album-wrap">-->
<!--                                <div class="album-box">-->
<!--                                        <i class="icon pic"></i>-->
<!--                                        <a href="#">-->
<!--                                                <div class="thumb">-->
<!--                                                        <img src="./assets/temp/a3.jpg" alt="">-->
<!--                                                        <div class="ctr-bg">-->
<!--                                                                <span class="h">5</span>-->
<!--                                                                <span class="c">8</span>-->
<!--                                                        </div>-->
<!--                                                </div>-->
<!--                                                <div class="bottom">-->
<!--                                                        <p class="title">创意世界大赛照片</p>-->
<!--                                                        <p class="des">12张照片</p>-->
<!--                                                </div>-->
<!--                                        </a>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                        <li class="album-wrap">-->
<!--                                <div class="album-box">-->
<!--                                        <a href="#">-->
<!--                                                <div class="thumb">-->
<!--                                                        <img src="./assets/temp/a4.jpg" alt="">-->
<!--                                                        <div class="ctr-bg">-->
<!--                                                                <span class="h">212</span>-->
<!--                                                                <span class="c">230</span>-->
<!--                                                        </div>-->
<!--                                                </div>-->
<!--                                                <div class="bottom">-->
<!--                                                        <p class="title">旅游2013</p>-->
<!--                                                        <p class="des">12张照片</p>-->
<!--                                                </div>-->
<!--                                        </a>-->
<!--                                </div>-->
<!--                        </li>-->
<!--                </ul>-->
<!--        </div>-->
<!--        <div class="height_40"></div>-->
<!--</div>-->

<!--创意世界大赛-->
<div class="detailPageMid" style="padding-bottom:0">
<div class="proContextDiv">
<div class="titDiv">
    <span></span>
    <h2 class="Js_scroll">我的创意世界大赛</h2>
</div>
<div class="height_31"></div>
<div class="clearfix">
    <ul class="clearfix l-areaNewsTit fl">
        <li><a href="#">热门比赛</a></li>
        <li><a href="#">创意投资项目比赛</a></li>
        <li><a href="#">创意任务人才比赛</a></li>
        <li><a href="#">创意公益明星比赛</a></li>
        <!--                <li class="last"><a href="#">创意世界交易所</a></li>-->
    </ul>
    <a href="#" class="lookAll">查看全部</a>
</div>
<div class="height_10"></div>
<div class="clearfix">
    <div id="match-product" class="areaMatch" style="width:940px;">
        <div class="pagearea"></div>
        <div class="itemListDiv Js_pageChange" style="overflow: hidden; margin-left:-8px;">
            <ul class="clearfix">
                <!--列表数目务必保证是3的倍数-->
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>幻世纪大作再现-蚀刻暴龙拼装模型1 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型2 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型3 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型4 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型5 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--<div class="clearfix">
  <div id="match-product" class="areaMatch" style="width:940px;">
    <div class="pagearea"></div>
    <div class="itemListDiv Js_pageChange1" style="overflow: hidden; margin-left:-14px;">
      <ul class="clearfix">
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>幻世纪大作再现-蚀刻暴龙拼装模型1 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型2 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型3 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型4 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型5 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>-->
</div>
</div>

<!--创意世界交易所-->
<div class="detailPageMid" style="padding-bottom:0">
<div class="proContextDiv">
<div class="titDiv">
    <span></span>
    <h2 class="Js_scroll">我的创意世界交易所</h2>
</div>
<div class="height_31"></div>
<div class="clearfix">
    <!--    <ul class="clearfix l-areaNewsTit fl">-->
    <!--        <li><a href="#">热门比赛</a></li>-->
    <!--        <li><a href="#">创意投资项目比赛</a></li>-->
    <!--        <li><a href="#">创意任务人才比赛</a></li>-->
    <!--        <li><a href="#">创意公益明星比赛</a></li>-->
    <!--        <li class="last"><a href="#">创意世界交易所</a></li>-->
    <!--    </ul>-->
    <!--    <a href="#" class="lookAll">查看全部</a>-->
</div>
<div class="height_10"></div>
<div class="clearfix">
    <div id="match-product" class="areaMatch" style="width:940px;">
        <div class="pagearea"></div>
        <div class="itemListDiv Js_pageChange" style="overflow: hidden; margin-left:-8px;">
            <ul class="clearfix">
                <!--列表数目务必保证是3的倍数-->
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>幻世纪大作再现-蚀刻暴龙拼装模型1 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型2 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型3 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型4 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
                <li class="p-match">
                    <div class="match-box">
                        <div class="listItemImg">
                            <div class="rightBox mark"></div>
                            <s class="mark"></s>
                            <div class="bottomBox"></div>
                            <img src="./assets/temp/p1.png" alt="">
                        </div>
                        <div class="contextDiv">
                            <h3>侏罗纪的召唤-蚀刻暴龙拼装模型5 -杰思模型</h3>
                            <div class="progress"></div>
                            <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
                            <p class="promulgatorMoney">10,000元</p>
                            <span class="status">所需投资金额</span>
                        </div>
                        <div class="bottomDiv clearfix">
                            <span class="left">长期</span>
                            <span class="right rr">有3位用户参与投资</span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--<div class="clearfix">
  <div id="match-product" class="areaMatch" style="width:940px;">
    <div class="pagearea"></div>
    <div class="itemListDiv Js_pageChange1" style="overflow: hidden; margin-left:-14px;">
      <ul class="clearfix">
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>幻世纪大作再现-蚀刻暴龙拼装模型1 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型2 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型3 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型4 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
        <li class="p-match">
          <div class="match-box">
            <div class="listItemImg">
              <div class="rightBox mark"></div>
              <s class="mark"></s>
              <div class="bottomBox"></div>
              <img src="./assets/temp/p1.png" alt="">
            </div>
            <div class="contextDiv">
              <h3>侏罗纪的召唤-蚀刻暴龙拼装模型5 -杰思模型</h3>
              <div class="progress"></div>
              <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>
              <p class="promulgatorMoney">$10,000</p>
              <span class="status">所需投资金额</span>
            </div>
            <div class="bottomDiv clearfix">
              <span class="left">永久</span>
              <span class="right tk">3人已洽谈</span>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>-->
<a href="javascript:;" class="yd-share-btn Js_share_btn" style="position: relative; top: 55px;"></a>
</div>
</div>

<!--帖子-->
<?php $itemTT = "他的需求" ; ?>
<?php include("./modules/item.php"); ?>
</div>
</div>
</div>
<script type="text/javascript">
        var container = $('.posts-wraplist');
        container.masonry({
                columnWidth: 448,
                gutter: 25,
                itemSelector: '.posts-item'
        });
        container.imagesLoaded( function() {
                container.masonry();
        });
        $('.Js_fancyBox,.Js_fancyBox1,.Js_fancyBox2,.Js_fancyBox3').fancybox({
                prevEffect : 'none',
                nextEffect : 'none',

                closeBtn  : true,
                arrows    : true,
                nextClick : true,
                tpl : {
                        closeBtn : '<a title="Close" style="background-image: url(assets/images/fancybox/fancybox_sprite.png); width:36px; height:36px; top:-18px; right:-18px; position:absolute"></a>'
                },
                helpers : {
                        thumbs : {
                                width  : 50,
                                height : 50
                        }
                }
        });
        $('.manual1').click(function(){
                $.fancybox([
                        './assets/temp/bigpic1.jpg',
                        './assets/temp/bigpic2.jpg',
                        './assets/temp/bigpic3.jpg',
                        './assets/temp/bigpic4.jpg'
                ],{
                        'padding':0,
                        'transitionIn':'none',
                        'transitionOut':'none',
                        'type':'image',
                        'changeFade':0
                })
        });
        //视频播放
        var a = $("#video1");
        var b = $(".js_player");
        b.css({"top":Math.floor((a.height()-b.height())/2)+"px","left":Math.floor((a.width()-b.width())/2)+"px"})
        var myVideo= a.get(0);
        b.click(function(){
                myVideo.play();
                $(this).fadeOut();
        });
        myVideo.addEventListener("ended",function(){b.fadeIn()})

</script>