<div class="content">
        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="height_18"></div>
                        <div class="titDiv">
                                <span></span>
                                <h2>订单支付</h2>
                        </div>

                        <div class="height_40"></div>
                        <div>
                                <span class="order-lab">确认订单信息</span>
                                <div class="height_20"></div>
                                <div class="clearfix order-detail">
                                        <span class="left">名称</span>
                                        <span class="right">金额</span>
                                </div>
                                <ul class="order-num">
                                        <li class="clearfix">
                                                <span class="name">淘宝购物-聚壹之家牛津百纳箱60L+30L二件套玩具收纳物箱</span>
                                                <span class="money">1,234.00</span>
                                        </li>
                                </ul>
                        </div>
                        <div class="height_42"></div>
                        <div>
                                <span class="order-lab">支付信息</span>
                                <div class="height_25"></div>

                                <div class="pay-mon">支付金额：<span>1,234.00</span><s>元</s></div>
                                <div class="height_30"></div>
                                <div>
                                        <div class="pay-mon">账户余额：<span>4,998.00</span><s>元</s></div>
                                        <p class="n-agree font14 mt20" style="margin-left:111px;">
            <span class="Js_checkBox" data-name="chk">
              <label class="Js_label_ok ">
                      <input type="checkbox" name="chk" class="mr10 pr" style="top:2px;">
                      使用账户余额支付<b style="font-size:16px; padding:0 11px;">$1,234.00</b>元
              </label>
            </span>
                                        </p>

                                </div>
                        </div>
                        <div class="height_85"></div>
                        <div style="width:890px;">
                                <span class="order-lab" style="line-height:18px; display:block;">网银直接支付</span>
                                <div class="height_27"></div>
                                <div class="isAgree">
                                        <div class="mb25">
                                                <label class="Js_bank_ok"> <input type="radio" name="bank"><img  src="./assets/images/bank/zfb.png" alt="" class="b-cs ml10" /></label>
                                        </div>
                                        <div class="mb25">
                                                <label class="Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/nh.png" alt="" class="b-hs ml10" /></label>
                                                <label class="ml10 Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/gsyh.png" alt="" class="b-hs ml10" /></label>
                                                <label class="ml10 Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/jsyh.png" alt="" class="b-hs ml10" /></label>
                                                <label class="ml10 Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/yzyh.png" alt="" class="b-hs ml10" /></label>
                                        </div>
                                        <div class="mb25">
                                                <label class="Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/zgyh.png" alt="" class="b-hs ml10" /></label>
                                                <label class="ml10 Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/zsyh.png" alt="" class="b-hs ml10" /></label>
                                                <label class="ml10 Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/jtyh.png" alt="" class="b-hs ml10" /></label>
                                                <label class="ml10 Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/pfyh.png" alt="" class="b-hs ml10" /></label>
                                        </div>
                                        <div class="mb25">
                                                <label class="Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/gdyh.png" alt="" class="b-hs ml10" /></label>
                                                <label class="ml10 Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/zxyh.png" alt="" class="b-hs ml10" /></label>
                                                <label class="ml10 Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/payh.png" alt="" class="b-hs ml10" /></label>
                                                <label class="ml10 Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/msyh.png" alt="" class="b-hs ml10" /></label>
                                        </div>
                                        <div class="mb25">
                                                <label class="Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/szyh.png" alt="" class="b-hs ml10" /></label>
                                                <label class="ml10 Js_bank_ok"><input type="radio" name="bank"><img src="./assets/images/bank/gfyh.png" alt="" class="b-hs ml10" /></label>
                                        </div>
                                </div>
                                <div class="height_22"></div>
                                <a href="#" class="blueBtn ml32">确认支付</a>
                        </div>
                        <div class="height_40"></div>
                </div>
        </div>
</div>