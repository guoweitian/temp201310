<div class="content">
    <form id="form_register">
        <div class="login-box" style="width: 640px;">
            <div class="l-login-form">
                <div class="height_18"></div>
                <div class="box-title clearfix">
                    <h4 class="left ml32">注册账号</h4>
                    <p class="right mr30" style="font-size: 12px; padding-top: 6px;">已经拥有创意世界账号？请<a class="l-tag" href="login.php"> 登录</a></p>
                </div>
                <div class="form-item" id="email">
                    <p class="ptext left">邮箱:</p>
                    <div class="tools text left" style="width: 320px;">
                        <div class="inputWarp">
                            <input class="font14" type="text" placeholder="请输入您的邮箱号" name="email">
                        </div>
                    </div>
                    <div class="errorMsg fl" style="margin-top: 8px;"></div>
                </div>
                <div class="height_15"></div>
                <div class="form-item" id="user_name">
                    <p class="ptext left">用户名:</p>
                    <div class="tools text left" style="width: 320px;">
                        <div class="inputWarp">
                            <input type="text" class="font14" placeholder="请输入您的用户名" name="user_name">
                        </div>
                    </div>
                    <div class="errorMsg fl" style="margin-top: 8px;"></div>
                </div>
                <div class="height_15"></div>
                <div class="form-item" id="password">
                    <p class="ptext left">密码:</p>
                    <div class="tools text left" style="width: 320px;">
                        <div class="inputWarp">
                            <input class="Js_password ipt_password_r font14" name="password" type="password" placeholder="请输入您的新密码">
                        </div>
                    </div>
                    <div class="errorMsg fl" style="margin-top: 8px;"></div>
                    <div class="strength" style="clear: left; margin-left: 145px;">
                        <span>密码强度：<span class="f1">弱</span></span>
                        <div class="bk"></div>
                        <div class="over">
                            <span class="level level_1"></span>
                            <span class="level level_2"></span>
                            <span class="level level_3"></span>
                        </div>
                        <span class="f2">强</span>
                        <input type="hidden" class="Js_strengthv_alue" name="password_strength">
                    </div>

                </div>
                <div class="height_5"></div>
                <div class="form-item" id="password_again">
                    <p class="ptext left"></p>
                    <div class="tools text left" style="width: 320px;">
                        <div class="inputWarp">
                            <input type="password" class="font14" placeholder="请再次输入您的密码" name="password_again">
                        </div>
                    </div>
                    <div class="errorMsg fl" style="margin-top: 8px;"></div>
                </div>
                <div class="height_10"></div>
                <div class="form-item" id="verify_code">
                    <p class="ptext left">验证码:</p>
                    <div class="tools text left" style="width:180px;">
                        <div class="inputWarp empty" style="padding-left: 0">
                            <input type="text" class="font14" placeholder="请输入右侧的验证码" name="verify_code">
                        </div>
                    </div>
                    <img class="verification left ml10" src="assets/temp/number.png">
                    <div class="errorMsg fl" style="margin-top: 8px;"></div>
                </div>
                <div class="height_10"></div>
                <div class="l-edit" id="is_agree_protocol">
                    <p class="n-agree left" style="padding-left: 145px; font-size: 12px;">
                <span class="Js_checkBox" data-name="chk"><label class="ok">
                        <input type="checkbox" name="is_agree_protocol" checked="checked" class="Js_label_ok">
                        我接受 创意世界</label>
                  <a class="l-tag Js_l_xy" href="javascript:;">注册协议</a>及
                  <a class="l-tag Js_l_sm" href="javascript:;">版权声明</a>
                </span>
                    </p>
                    <div class="errorMsg left" style="margin-top: -2px"></div>
                </div>
            </div>
            <div class="height_15"></div>
            <div class="login-btnbar" style="padding-left: 145px;">
                <a href="javascript:;" class="blueBtn login-btn Js_do_register">注册</a>
            </div>
        </div>
    </form>
</div>