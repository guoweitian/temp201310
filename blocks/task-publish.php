<div class="content">
        <div class="publish-wrap">
            <form method="post" id="form-send-task" action="">
                <div id="tabs-tab" class="publish-form publish w765">
                        <div class="row" style="overflow:hidden;">
                                <div class="large-12 columns">
                                        <div class="publish-progress">
                                                <div class="height_40"></div>
                                                <div class="issueStrip pr">
                                                        <div class="gray-bg"></div>
                                                        <ul id="tab-t" class="clearfix strip strip3">
                                                                <!--当前current/没填notFill/最后一步没填notFill last-->
                                                                <li class="one current">
                                                                        <span class="fousLine w-first"></span>
                                                                        <a class="selected" href="#step-one">
                                                                                <b>1</b>
                                                                                <span class="st-one">基本信息</span>
                                                                        </a>
                                                                </li>
                                                                <li class="notFill two ml">
                                                                        <s></s>
                                                                        <span class="fousLine wh"></span>
                                                                        <a href="#step-two"><b>2</b><span class="st-two" style="margin-left: -27px">描述比赛内容</span></a>
                                                                </li>
                                                                <li class="notFill three ml">
                                                                        <s></s>
                                                                        <span class="fousLine wh"></span>
                                                                        <span class="fousLine w-last"></span>
                                                                        <a href="#step-three"><b>3</b><span class="st-three" style="margin-left: -27px">确认发布协议</span></a>
                                                                </li>
                                                        </ul>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div id="tabs-items" class="Js_tabs_items clearfix">
                                <div class="row tabs-c">
                                        <div class="large-12 columns">
                                                <div class="rowblock">
                                                        <div class="pt">填写任务名称</div>
                                                        <div class="tools text left">
                                                                <div class="inputWarp empty">
                                                                        <input type="text" id="Js_task_name_input">
                                                                </div>
                                                        </div>
                                                        <div class="errorMsg left" style="display:none">错误提示内容</div>
                                                </div>
                                                <div class="height_25"></div>
                                                <div class="rowblock">
                                                        <div class="pt">任务所在地</div>
                                                        <div class="tools text left Js_cityList">
                                                                <s class="locBg"></s>
                                                                <div class="inputWarp downMenuCity">
                                                                        <input type="text" class="Js_loc_input" id="Js_loc_input">
                                                                        <div class="selDiv cityList gloc">
                                                                                <ul>
                                                                                        <li class="s1">可以根据城市名，街道名，国家名等搜索</li>
                                                                                        <li class="s2 Js_loc_ad"><a href="javascript:;">自动定位当前位置</a></li>
                                                                                        <li class="s3 Js_world_ad"><a href="javascript:;">全世界</a></li>
                                                                                </ul>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="errorMsg left" style="display:none">错误提示内容</div>
                                                </div>

                                            <div class="height_25"></div>
                                            <div class="rowblock">
                                                <div class="pt">寻找项目投资时间</div>
                                                <div class="l-edit">
                                                    <p class="n-agree mt10 left">
                  <span class="Js_radio mr25 Js_forever" data-name="rad">
                     <label class="Js_label_ok ok">
                         <input type="radio" name="rad" checked>长期</label> <!--选择后文中颜色-->

                  </span>
                  <span class="Js_radio Js_limit_time" data-name="rad">
                    <label class="Js_label_ok"> <input type="radio" name="rad"> 限制时间</label>

                  </span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="height_25"></div>
                                            <div class="rowblock Js_limit_content" style="display:none;">
                                                <div class="pt">设置项目投资的截止日期</div>
                                                <div class="tools text error left">
                                                    <input class="datebox Js_input_date" type="text" id="date1" readonly="readonly" >
                                                </div>
                                                <div class="errorMsg left" style="display:none;">请选择日期</div>
                                            </div>
                                                <div class="height_25"></div>
                                                <div class="rowblock">
                                                        <div class="pt">获胜人才的奖励金额</div>
                                                        <div class="tools text left">
                                                                <div class="inputWarp enterNum">
                                                                        <input type="text" class="Js_input_text" id="Js_find_money">
                                                                </div>
                                                                <div class="numFormat">.00</div>
                                                        </div>
                                                        <div class="errorMsg left" style="display:none">错误提示内容</div>
                                                </div>
                                                <div class="height_25"></div>
                                                <div class="rowblock Js_cover">
                                                        <div class="pt">上传封面图片</div>
                                                        <div class="uploadWrap-single dargArea left">
                                                                <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
                                                                <input class="file_uploadbox-single" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">
                                                        </div>
                                                        <div class="errorMsg left" style="display:none">请选上传封面图片</div>
                                                        <div class="height_10"></div>
                                                        <div id="upload-single" class="clearfix Js_cover_div Js_fm_cover_div upload-single"></div>
                                                </div>
                                                <div></div>
                                                <div class="height_50"></div>
                                                <a href="#step-two" class="blueBtn Js_s_one">下一步</a>
                                        </div>
                                        <div class="height_60"></div>
                                </div>

                                <div class="row tabs-c">
                                        <div class="large-12 columns">
                                                <div class="rowblock l-edit">
                                                        <div class="pt">描述任务内容</div>
                                                        <p class="pdes">请尽可能详细的描述你的任务内容和需求，以便任务人才帮助你完成</p>
                                                        <div class="height_10"></div>
                                                        <div id="tz_toolbar" class="toolbar-wrap"></div>

                                                        <div class="toolbar-content left">
                                                                <textarea id="tz_content" name="content"></textarea>
                                                        </div>
                                                        <div class="errorMsg left" style="position: absolute; left: 650px; white-space: nowrap; display:none;">请填写描述内容</div>
                                                </div>

                                                <div class="height_50"></div>
                                                <a href="#step-one" class="grayBtn mr16 Js_s_two_pre">上一步</a>
                                                <a href="#step-three" class="blueBtn Js_s_two_next">下一步</a>

                                        </div>
                                        <div class="height_60"></div>
                                </div>

                                <div class="row tabs-c">
                                        <div class="large-12 columns">
                                                <div class="rowblock l-edit">
                                                        <div class="pt">确认发布协议</div>
                                                        <p class="pdes">请您阅读并确认创意任务人才比赛的发布协议</p>
                                                        <div class="height_10"></div>
                                                        <div class="tools textArea">
                                                                <textarea style="font-size: 15px; line-height: 1.5" id="Js_release_agreement" readonly>
确认发布创意任务人才比赛协议
1 简介
您只有在完全同意下列服务条款，并完成注册后，才能成为而且立即成为四川创意世界科技有限公司创意任务人才比赛的用户并使用创意任务人才比赛所提供的服务。一旦您浏览或开始使用创意任务人才比赛的服务，即视为您已经完全了解并同意下列条款，包括创意任务人才比赛对条款做的修改。
创意任务人才比赛是创意世界提供的一个让用户(即“任务发布者”)通过提供奖金向任务参赛者征集任务解决方案的服务。
您作为任务发布者：任务参赛者将为您提供任务解决方案，您选择满意的解决方案后支付任务奖金给获胜者并获取全部解决方案，在签订《作品转让协议》前您都可以随时收回任务奖金。
您作为任务参赛者：可以通过提供解决方案给任务发布者，任务发布者将从所有任务参赛者中选出可以赢取奖金的任务获胜者，如果您成为了任务获胜者，您将在任务发布者点击“确认成果”按钮后获得任务发布者提供的任务奖金。注意：在签订《作品转让协议》前，任务发布者可以选择收回任务奖金。
创意世界：并不是任务发布者和任务参赛者中的任何一方。所有任务作品的交易仅存在于用户和用户之间。
2 禁止行为
发布或参加创意任务人才比赛，您须同意并遵守以下禁止行为协议，包括如下条款：
	征集或提交软件破解、程序破解类作品；
	征集或提交游戏外挂、程序外挂类作品；
	征集或提交盗取网银账号、游戏账号类作品；
	征集或提交侵犯第三方知识产权的作品；
	征集或提交侵犯第三方权利的作品；
	征集或提交木马、黑客程序等有损网络安全的作品；
	征集或提交涉黄、赌博等作品；
	征集或提交其他违反法律、法规、行政规章等相关规定的作品。
	征集或提交论文代写类作品；
	征集或提交刷钻、刷信用等作品；
	征集或提交虚假信息的作品；
	征集或提交通过链接等方式逃避创意世界及其他用户审核的作品；
	征集或提交可能给他人或者其他机构带来损害的作品；
征集或提交其他违背社会伦理或社会主流价值观的作品。
3 知识产权的转让
	在任务发布者和任务参赛者签订《作品转让协议》后，该作品的知识产权即转让给任务发布者所有。
	任务发布者若需要线下签署知识产权转让协议，被选为任务获胜者的参赛者有义务配合任务发布者完成协议的签署，若被选为任务获胜的参赛者拒绝签署，买家有权随时退还任务奖金。
4 创意世界的责任范围
创意世界有权随时以任何理由拒绝、取消、中断、删除或暂停该任务。创意世界不因该行为承担任何赔偿。创意世界的政策并非评论此类行为的理由。
创意世界不承担任何作品或使用服务产生的损失或亏损。创意世界无义务介入任何用户之间的纠纷，或用户与其他第三方就服务使用方面产生的纠纷。包括但不限于货物及服务的交付，其他条款、条件、保证或与网站活动相关联的有关陈述。创意世界不负责监督任务的实现与严格执行。您可授权创意世界、其工作人员、职员、代理人及对损失索赔权的继任者所有已知或未知、公开或秘密的解决争议的方法和服务。
                                                                </textarea>
                                                        </div>
                                                        <div class="errorMsg left" style="display:none;">请确认同意协议</div>
                                                </div>
                                                <div class="height_10"></div>
                                                <div class="l-edit">
                                                        <p class="n-agree left">
                          <span class="Js_checkBox" data-name="chk">
                            <label class="Js_label_ok ok">
                                    <input type="checkbox" name="chk" checked="checked" id="Js_readed_agreement">
                                    我已经阅读并同意创意任务人才比赛的发布协议</label> <!--选择后文中颜色-->
                          </span>
                                                        </p>
                                                        <div class="errorMsg left" style="margin-top: -10px; display:none;">请确认同意协议</div>
                                                </div>
                                                <div class="height_40"></div>
                                                <div class="rowblock l-edit">
                                                        <div class="pt left">
                                                                <div class="sign-bg">
                                                                        <input class="signature" id="Js_signature">
                                                                </div>
                                                        </div>
                                                        <div class="errorMsg left mt8" style="display:none;">错误提示内容</div>
                                                </div>
                                                <div class="height_50"></div>
                                                <a href="#step-two" class="grayBtn mr16 Js_s_three_pre">上一步</a>
                                                <a href="#step-one" class="blueBtn Js_send_project">发布项目</a><!--跳转到第一步，其他class为 Js_goto_one/Js_goto_two/Js_goto_three/Js_goto_four，同时设置一下a标签的锚点到对应位置 -->
                                        </div>
                                        <div class="height_60"></div>
                                </div>

                        </div>
                </div>
            </form>
        </div>
</div>
<script type="text/javascript">
        $(function(){
                tabs('tabs-tab','tab-t', 1, 'click');
                tabs('tabs-tab','tab-w', 1, 'click');
                dataControl('date1');
                dataControl('date2');
        })
</script>