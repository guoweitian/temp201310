<div class="content">
        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="height_38"></div>
                        <div class="titDiv">
                                <span></span>
                                <h2>现金</h2>
                        </div>
                        <div class="height_40"></div>
                        <div>
                                <h3 class="n-tit">帐户余额</h3>
                                <div class="clearfix n-mon">
                                        <span>1,123,456,789</span><s class="left">元</s>
                                        <a href="#" class="l-m_btn left Js_recharge">账户充值</a>
                                        <a href="#" class="s_btn left ml10" id="Js_account_getmoney">账户提现</a>
                                </div>
                        </div>
                        <div class="height_18"></div>
                </div>

                <!--充值弹出层开始-->
                <div style="background:#e6e6e6; display:none;" class="Js_recharge_c">
                        <div class="proContextDiv">
                                <div class="height_18"></div>
                                <div class="titDiv">
                                        <span></span>
                                        <h2>账户充值</h2>
                                </div>
                                <div class="height_33"></div>
                                <div class="ft-style" style="font-size: 16px;">
                                        <div class="n-agree w256">
              <span class="Js_radio Js_labelck_ok" data-name="rad">
                <label class="radio">
                        <input type="radio" name="rad" checked>
                </label>
                  <span class="p-level b73">&nbsp;<b class="c-money">100</b>&nbsp;&nbsp;元</span>

               </span>
                                        </div>
                                        <div class="height_27"></div>
                                        <div class="n-agree w256">
              <span class="Js_radio Js_labelck_ok" data-name="rad">
                <label class="radio">
                        <input type="radio" name="rad">
                </label>
                  <span class="p-level c7ef">&nbsp;<b class="c-money">200</b>&nbsp;&nbsp;元</span>

              </span>
                                        </div>
                                        <div class="height_27"></div>
                                        <div class="n-agree w256">
              <span class="Js_radio Js_labelck_ok" data-name="rad">
                <label class="radio">
                        <input type="radio" name="rad">
                </label>
                  <span class="p-level c0">&nbsp;<b class="c-money">500</b>&nbsp;&nbsp;元</span>
              </span>
                                        </div>
                                        <div class="height_27"></div>
                                        <div class="n-agree w256">
              <span class="Js_radio Js_labelck_ok" data-name="rad">
                <label class="radio">
                        <input type="radio" name="rad">
                </label>
                        <span class="p-level c0"><input type="text" class="w108 ml10 Js_input_text">&nbsp;&nbsp;元</span>
              </span>
                                        </div>
                                        <div class="height_27"></div>
                                </div>
                                <div class="height_45"></div>
                                <div class="p-warp">
                                        <a href="#" class="btn h8 mr5 Js_cancle_buy">取消充值</a>
                                        <a href="#" class="btn b73">确认充值</a>
                                </div>
                                <div class="height_50"></div>
                                <div class="sp-line" style="top:0; margin:0;">
                                        <div class="line-left"></div>
                                        <div class="line-right"></div>
                                </div>
                        </div>
                </div>
                <!--充值弹出层结束-->
                <div class="proContextDiv">
                        <div class="height_30"></div>
                        <div>
                                <label class="l-start">起止时间：</label>
                                <div class="tools text left" style="width:148px;">
                                        <input class="datebox bbit-dp-input" type="text" id="dateone" readonly="readonly" style="width:144px !important;" value="2013/10/15">
                                </div>
                                <s class="time-fh"></s>
                                <div class="tools text left" style="width:148px;">
                                        <input class="datebox bbit-dp-input" type="text" id="datetwo" readonly="readonly" style="width:144px !important;" value="2013/11/14">
                                </div>
                                <ul class="clearfix l-areaNewsTit userfile">
                                        <li><a href="javascript:;" class="Js_time_filter" data-type="2013/10/27">今天</a></li>
                                        <li><a href="javascript:;" class="current Js_time_filter" data-type="2013/9/27">最近1个月</a></li>
                                        <li><a href="javascript:;" class="Js_time_filter" data-type="2013/7/27">3个月</a></li>
                                        <li><a href="javascript:;" class="Js_time_filter" data-type="2012/10/27">1年</a></li>
                                        <li class="last"><a href="javascript:;" class="Js_time_filter" data-type="2012/10/27">1年前</a></li>
                                </ul>
                        </div>
                        <div class="height_38"></div>
                        <div>
                                <div class="clearfix">
                                        <ul class="clearfix l-areaNewsTit userfile left" style="line-height:normal;">
                                                <li><a href="javascript:;" class="current Js_rmb_record_filter">全部</a></li>
                                                <li><a href="javascript:;" class="Js_rmb_record_filter">交易收入</a></li>
                                                <li class="last"><a href="javascript:;" class="Js_rmb_record_filter">交易支出</a></li>
                                        </ul>
                                        <div class="pageItemDiv right">
                                                <a href="javascript:;" class="fyBtn prev Js_page_list_prev Js_net_rmb_account"></a>
						<span class="pageItem cfs19 Js_page_list_itemNum">
							<a href="javascript:;" class="current Js_net_rmb_account">1</a>
							<a href="javascript:;" class="Js_net_rmb_account">2</a>
							<a href="javascript:;" class="Js_net_rmb_account">3</a>
							<a href="javascript:;" class="Js_net_rmb_account">4</a>
							<a href="javascript:;" class="Js_net_rmb_account">5</a>
						</span>
                                                <a href="javascript:;" class="fyBtn next Js_page_list_next Js_net_rmb_account"></a>
                                        </div>
                                </div>

                                <div class="height_27"></div>
                                <div>
                                        <div>
                                                <table width="940" border="0" cellpadding="0" cellspacing="0" class="t-detail Js_rmb_account_list">
                                                        <tr style="background:#deded9">
                                                                <td class="td-time">时间</td>
                                                                <td class="td-dis">摘要</td>
                                                                <td class="td-sum red" style="font-size:14px; color:#5F5F5F; font-weight:normal">金额</td>
                                                                <td class="td-a-money" style="font-size:14px; color:#5F5F5F; font-weight:normal">账户余额</td>
                                                                <td class="td-cz">操作</td>
                                                        </tr>
                                                        <tr>
                                                                <td class="td-time">2013/01/25 22:20:22</td>
                                                                <td class="td-dis">淘宝购物-聚壹之家牛津百纳箱聚壹之家牛津60L+30L二件套玩具收纳物箱</td>
                                                                <td class="td-sum red">-98.00</td>
                                                                <td class="td-a-money">1,235.65</td>
                                                                <td class="td-cz"><a href="pay-order-detail.php">详情</a></td>
                                                        </tr>
                                                        <tr>
                                                                <td class="td-time">2013/01/25 21:20:22</td>
                                                                <td class="td-dis">ThinkPad E430C14英寸笔记本电脑</td>
                                                                <td class="td-sum green">+98.00</td>
                                                                <td class="td-a-money">1,235.65</td>
                                                                <td class="td-cz"><a href="pay-order-detail.php">详情</a></td>
                                                        </tr>
                                                        <tr>
                                                                <td class="td-time">2013/01/25 20:20:22</td>
                                                                <td class="td-dis">神奇的台灯</td>
                                                                <td class="td-sum green">+98.00</td>
                                                                <td class="td-a-money">1,235.65</td>
                                                                <td class="td-cz"><a href="pay-order-detail.php">详情</a></td>
                                                        </tr>
                                                        <tr>
                                                                <td class="td-time">2013/01/25 19:20:22</td>
                                                                <td class="td-dis">ThinkPad E430C14英寸笔记本电脑</td>
                                                                <td class="td-sum red">-98.00</td>
                                                                <td class="td-a-money">1,235.65</td>
                                                                <td class="td-cz"><a href="pay-order-detail.php">详情</a></td>
                                                        </tr>
                                                        <tr>
                                                                <td class="td-time">2013/01/25 18:20:22</td>
                                                                <td class="td-dis">百诺（BENRO）C2681TB1 可拆卸独脚架</td>
                                                                <td class="td-sum green">+98.00</td>
                                                                <td class="td-a-money">1,235.65</td>
                                                                <td class="td-cz"><a href="pay-order-detail.php">详情</a></td>
                                                        </tr>
                                                        <tr>
                                                                <td class="td-time">2013/01/25 17:20:22</td>
                                                                <td class="td-dis">百诺（BENRO）C2681TB1 可拆卸独脚架</td>
                                                                <td class="td-sum green">+98.00</td>
                                                                <td class="td-a-money">1,235.65</td>
                                                                <td class="td-cz"><a href="pay-order-detail.php">详情</a></td>
                                                        </tr>
                                                </table>
                                        </div>
                                        <div class="height_40"></div>
                                        <div class="pageItemDiv right mb25">
                                                <a href="javascript:;" class="fyBtn prev Js_page_list_prev Js_net_rmb_account"></a>
						<span class="pageItem cfs19 Js_page_list_itemNum">
							<a href="javascript:;" class="current Js_net_rmb_account">1</a>
							<a href="javascript:;" class="Js_net_rmb_account">2</a>
							<a href="javascript:;" class="Js_net_rmb_account">3</a>
							<a href="javascript:;" class="Js_net_rmb_account">4</a>
							<a href="javascript:;" class="Js_net_rmb_account">5</a>
						</span>
                                                <a href="javascript:;" class="fyBtn next Js_page_list_next Js_net_rmb_account"></a>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
<script type="text/javascript">
var d = new Date();
var dy = d.getFullYear();
var dm = d.getMonth()+1;
var dd = d.getDate();
$('#dateone').val(dy + '/' + (dm-1) + '/' + dd);
$('#datetwo').val(dy + '/' + dm + '/' + dd);
dataControl('dateone','datetwo');
</script>