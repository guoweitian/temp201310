<div class="content">
<div class="detail-wrap project">
<!--PreContent 开始-->
<div class="detail-imgbg">
        <div class="toppattern"></div>
        <div class="detailTopDiv pew">
                <h3>这里是比赛名称，如果有很多字，这里也只能显示一行标题。这里是比赛名称，如果有很多字，这里也只能显示一行标题。</h3>

                <p class="disDiv">
                        <span class="loc">成都市青羊区小南街</span>
                        <span class="time">剩余3天</span>
                        <span class="m_num">P2013052400021</span>
                </p>

                <div>
                        <div class="u_listItem left">
                                <a href="../user/home.php" class="leftArea shadow">
                                        <img src="./assets/temp/5.png" alt="">
                                </a>

                                <div class="rightArea w600">
                                        <p class="rightName"><a class="Js_visitCard cfff" href="#">刘兆宇</a></p>

                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a
                                                    href="#">计算机科技与技术</a></p>
                                </div>
                        </div>
                        <div class="tz_moneyDiv right" style="padding-top: 29px">
                                <p>所需投资金额:<b>10,000元</b></p>
<!--                                <a href="#" class="bgimgBtn js_detailPublish right">我要投资</a>-->
                        </div>
                </div>
        </div>
</div>

<div class="form-wrap js_detailForm">

<form method="post" id="form-project-invest" action="">
<div id="tabs-tab" class="publish-form publish w765">
<div class="row">
        <div class="large-12 columns w765"  style="overflow:hidden;">
                <div class="publish-progress">
                        <div class="height_40"></div>
                        <div class="issueStrip pr">
                                <div class="gray-bg"></div>
                                <ul id="tab-t" class="clearfix strip strip3">
                                        <!--当前current/没填notFill/最后一步没填notFill last-->
                                        <li class="one current" style="background-color: #F3F8FC">
                                                <span class="fousLine w-first"></span>
                                                <a class="selected" href="#step-one">
                                                        <b>1</b>
                                                        <span class="st-one" style="margin-left: -27px">设置投资方式</span>
                                                </a>
                                        </li>
                                        <li class="notFill two ml" style="background-color: #F3F8FC">
                                                <s></s>
                                                <span class="fousLine wh"></span>
                                                <a href="#step-two"><b>2</b><span class="st-two" style="margin-left: -28px;">设置投资信息</span></a>
                                        </li>
                                        <li class="notFill three ml" style="background-color: #F3F8FC">
                                                <s></s>
                                                <span class="fousLine wh"></span>
                                                <span class="fousLine w-last"></span>
                                                <a href="#step-three"><b>3</b><span class="st-three" style="margin-left: -27px">确认发布协议</span></a>
                                        </li>
                                </ul>
                        </div>
                </div>
        </div>
</div>
<form name="i_invest">
<div id="tabs-items" class="Js_tabs_items clearfix">
<div class="row tabs-c">
        <div class="large-12 columns">
                <!--你要投资的金额-->
                <div class="rowblock">
                        <div class="pt">你要投资的金额</div>
                        <div class="tools text left">
                                <div class="inputWarp enterNum">
                                        <input type="text" class="Js_enterNum" id="Js_invest_money_input" name="invest_money_input">
                                </div>
                                <div class="numFormat">.00</div>
                        </div>
                        <div class="errorMsg left" style="display:none;">错误内容信息</div>
                </div>
                <div class="height_25"></div>
                <!--选择投资方式-->
                <div class="rowblock">
                        <div class="pt">选择投资方式</div>
                        <div class="height_5"></div>
                        <div class="l-edit">
                                <p class="n-agree mt10 left">
          <span class="Js_radio mr25 Js_pay_alls" data-name="rad">
            <label class="Js_label_ok ok"> <input type="radio" name="rad" checked> 一次性投资</label>  <!--选择后文中颜色-->
          </span>
          <span class="Js_radio Js_pay_fq" data-name="rad">
           <label class="Js_label_ok"><input type="radio" name="rad">先付定金再付尾款</label>
          </span>
                                </p>
                        </div>
                </div>

                <div class="height_30"></div>
                <!--投资比例弹出层-->
                <div class="l-edit pay-wrap Js_pay_fq_div" style="display: none;">
                        <div class="height_18"></div>
                        <div class="rowblock">
                                <div class="pt">先付定金再付尾款</div>
                                <div class="sliderBarDiv">
                                        <div id="slider-range-min0" style="width:248px; float:left; top:12px;" class="Js_slider"></div>
          <span class="percentage Js_h sliderBarNum" id="deposit_percent"
                style=" line-height:36px; padding:8px 0 0 42px; color:#888888; font-size:18px;"><b>0%</b></span>
                                        <input type="hidden" value="100" name="slider_precent_one"/>

                                        <div class="funds sliderBar">
                                                <label
                                                    style="font-size:14px; color:#5f5f5f; text-shadow:0 1px 1px #FFF; display:inline-block;">定金支付金额&nbsp;&nbsp;&nbsp;</label><em>¥ <span
                                                            id="deposit_money" class="Js_deposit_money"></span></em>
                                        </div>
                                </div>

                        </div>
                        <div class="height_10"></div>
                        <div class="rowblock">
                                <div class="pt">尾款支付比例</div>
                                <div class="sliderBarDiv">
                                        <div id="slider-range-min1" style="width:248px; float:left; top:12px;" class="Js_slider"></div>
          <span class="percentage Js_h sliderBarNum" id="deposit_percent"
                style=" line-height:36px; padding:8px 0 0 42px; color:#888888; font-size:18px;"><b>0%</b></span>
                                        <input type="hidden" value="0" name="slider_precent_two"/>

                                        <div class="funds sliderBar">
                                                <label style="font-size:14px; color:#5f5f5f; text-shadow:0 1px 1px #FFF;  display:inline-block;">尾款支付金额&nbsp;&nbsp;&nbsp;</label><em>¥ <span
                                                            id="deposit_money" class="Js_deposit_money"></span></em>
                                        </div>
                                </div>

                        </div>
                        <div class="height_10"></div>
                        <div class="rowblock">
                                <div class="pt">尾款支付时间</div>
                                <div class="tools text">
                                        <input class="datebox" type="text" id="date1" readonly="readonly" d-type="1" name="final_money">
                                </div>
                                <div class="errorMsg" style="display: none;">错误提示内容</div>
                        </div>
                        <div class="height_30"></div>
                </div>
                <!--投资比例弹出层结束-->
                <div class="height_15"></div>

                <div class="rowblock l-edit">
                        <div class="pt">其他投资事项说明</div>
                        <div class="height_10"></div>
                        <div id="js_toolbar" class="toolbar-wrap"></div>
                        <div class="toolbar-content left">
                                <textarea id="js_content" name="content" ></textarea>
                        </div>
                        <div class="errorMsg left" style="display: none;">错误提示内容</div>
                </div>
                <div class="height_50"></div>
                <a href="#step-two" class="blueBtn Js_s_one">下一步</a>
        </div>
        <div class="height_60"></div>
</div>

<div class="row tabs-c">
        <div class="large-12 columns">
                <div class="rowblock">
                        <span class="pt">投资人</span>
                        <span style="font-size:20px; font-family:'黑体'; font-weight:bold; margin-left:40px; color: #cf6e6e">马云</span>
                </div>
                <div class="height_15"></div>
                <!--城市联动选择开始-->
                <div class="rowblock l-edit">
                        <div class="n-area">
                                <div class="optDiv left Js_state mr10" style="width:106px; z-index:2">
                                        <div class="tools text pr" style="width:106px;">
                                                <div class="inputWarp opt">
                                                        <span>中国</span>
                                                        <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                        </div>
                                        <ul>
                                                <li><a href="javascript:void(0)">中国</a></li>
                                        </ul>
                                </div>
                                <div class="left Js_city_select_code" code="100101">
                                        <div class="optDiv Js_state left mr10" style="width:106px;">
                                                <div class="tools text pr" style="width:106px;">
                                                        <div class="inputWarp opt">
                                                                <span></span>
                                                                <a href="javascript:void(0)" class="showMenu"></a>
                                                        </div>
                                                </div>
                                                <ul id="Js_t1"></ul>
                                                <select id="t1" style="display:none;">
                                                        <option>数据加载中...</option>
                                                </select>
                                        </div>
                                        <div class="optDiv Js_state left mr10" style="width:106px;" id="Js_t2">
                                                <div class="tools text pr" style="width:106px;">
                                                        <div class="inputWarp opt">
                                                                <span></span>
                                                                <a href="javascript:void(0)" class="showMenu"></a>
                                                        </div>
                                                </div>
                                                <ul></ul>
                                                <select id="t2" style="display:none;">
                                                        <option>数据加载中...</option>
                                                </select>
                                        </div>
                                        <div class="optDiv Js_state left" style="width:106px;" id="Js_t3">
                                                <div class="tools text pr" style="width:106px;">
                                                        <div class="inputWarp opt">
                                                                <span></span>
                                                                <a href="javascript:void(0)" class="showMenu"></a>
                                                        </div>
                                                </div>
                                                <ul></ul>
                                                <select id="t3" style="display:none;">
                                                        <option>数据加载中...</option>
                                                </select>
                                        </div>
                                </div>
                        </div>
						<input type="hidden" name="cityCode" value="100101" id="cityCode" />
                        <!--城市联动选择结束-->
                        <div class="height_15"></div>
                        <div class="rowblock">
                                <div class="tools text left" style=" width: 410px;">
                                        <div class="inputWarp empty">
                                                <input type="text" placeholder="所在地址" id="Js_agreement_addr" name="address">
                                        </div>
                                </div>
                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        </div>
                        <div class="height_15"></div>
                        <div class="rowblock">
                                <div class="tools text left" style=" width: 230px;">
                                        <div class="inputWarp empty">
                                                <input type="text" placeholder="邮编" id="Js_agreement_zip" name="zip_code">
                                        </div>
                                </div>
                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        </div>
                        <div class="height_15"></div>
                        <div class="rowblock">
                                <div class="tools text left" style=" width: 230px;">
                                        <div class="inputWarp empty">
                                                <input type="text" placeholder="联系方式" id="Js_agreement_contact" name="contact_type">
                                        </div>
                                </div>
                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        </div>
                        <div class="height_50"></div>
                        <a href="#step-one" class="grayBtn Js_s_two_pre mr16">上一步</a>
                        <a href="#step-three" class="blueBtn Js_s_two_next">下一步</a>
                </div>
                <div class="height_60"></div>
        </div>

</div>

<div class="row tabs-c">
        <div class="large-12 columns">
                <div class="rowblock l-edit">
                        <div class="pt">确认投资协议</div>
                        <p class="pdes">请您阅读并确认创意投资项目比赛的投资协议</p>

                        <div class="height_10"></div>
                        <div class="tools textArea left">
                                <textarea style="font-size: 15px; line-height: 1.5" id="Js_release_agreement" readonly>
我要投资协议
1 简介
您只有在完全同意下列服务条款，并完成注册后，才能成为而且立即成为四川创意世界科技有限公司创意投资项目比赛的用户并使用创意投资项目比赛所提供的服务。一旦您浏览或开始使用创意投资项目比赛的服务，即视为您已经完全了解并同意下列条款，包括创意投资项目比赛对条款做的修改。
创意投资项目比赛是创意世界提供的一个允许用户(即“项目发布者”)通过展示项目或创意，并承诺分享项目成果，以便寻找项目投资者共同实现项目或创意的服务。
您作为项目发布者：项目投资者可以与您订立合同，为您的项目或创意提供资金的投资，而您则在接受投资后努力实现项目或创意，并兑现您给项目投资者做出的承诺。
您作为项目投资者：可以接受项目发布者对您做出的的项目成果分享的承诺，以资金的方式投资项目发布者的项目或创意。
创意世界并不是项目发布者和项目投资者中的任何一方。所有交易仅存在于用户和用户之间。
2 行为规则
通过创意世界投资项目，您须同意并遵守以下协议，包括如下条款：
	项目投资者同意接受在其承诺投资某项目时提供付款授权信息 。
	项目投资者同意创意世界及其合作伙伴授权或保留收费的权利。
	在项目进行中，只要项目投资者还没有点击“确认收到项目成果”按钮，项目投资者都可以任意取消投资。
	投资回报的预期发放日期并非约定实现的期限，它仅为项目发布者希望实现的日期。
	为建立良好的信用和名声，项目发布者会尽力依照预期完成日期实现项目。
	对于所有项目，创意世界将提供项目投资者的姓名、联系方式和邮寄地址等信息给于项目发布者。
	项目发布者可以在项目成功后直接向项目投资者要求额外信息。为了顺利获得投资回报，项目投资者须同意在合理期限内提供给项目发布者相关信息。
3 创意世界的责任范围
创意世界保留随时以任何理由取消项目的权利。
创意世界有权随时以任何理由拒绝、取消、中断、删除或暂停该项目。创意世界不因该行为承担任何赔偿。创意世界的政策并非评论此类行为的理由。
在项目成功和获得款项之间可能存在延迟。
创意世界不承担任何相关回报或使用服务产生的损失或亏损。创意世界无义务介入任何用户之间的纠纷，或用户与其他第三方就服务使用方面产生的纠纷。包括但不限于货物及服务的交付，其他条款、条件、保证或与网站活动相关联的有关陈述。创意世界不负责监督项目的实现与严格执行。您可授权创意世界、其工作人员、职员、代理人及对损失索赔权的继任者所有已知或未知、公开或秘密的解决争议的方法和服务。
                                </textarea>
                        </div>
                        <div class="errorMsg left" style="display:none;">请确认同意协议</div>
                </div>
                <div class="height_10"></div>
                <div class="l-edit">
                        <p class="n-agree left">
        <span class="Js_checkBox" data-name="chk">
       	 <label class="Js_label_ok ok"><input type="checkbox" name="chk" checked="checked" id="Js_readed_agreement">我已经阅读并确认创意投资项目比赛的投资协议</label>

        </span>
                        </p>
                        <div class="errorMsg left" style="margin-top: -10px; display:none;">请确认同意协议</div>
                </div>
                <div class="height_40"></div>
                <div class="rowblock l-edit">
                        <div class="pt left">
                                <div class="sign-bg">
                                        <input class="signature" id="Js_signature" name="signature"></div>
                        </div>
                        <div class="errorMsg left mt8" style="display:none;">错误提示内容</div>
                </div>
                <div class="height_50"></div>
                <a href="#step-one" class="grayBtn Js_s_three_pre mr16">上一步</a>
                <a href="javascript:;" class="blueBtn big-blueBtn" id="Js_pro_send_agreement">确认比赛协议</a>
        </div>
        <div class="height_60"></div>
</div>


</div>
</form>
</div>
</div>
<!--PreContent 结束-->

<div class="detailPageMidDiv">

<!--投资意向 开始-->
<div class="detailPageMid">
    <div class="proContextDiv">
        <div class="titDiv">
            <span></span>

            <h2>投资意向</h2>
        </div>
        <ul class="intentionUl clearfix Js_intention_ul">
            <li class="left intentionList mt30">
                <form method="post" id="form-project-accept-invest" action="">
                    <div class="intention">
                        <input type="hidden" name="project_id" value="">
                        <input type="hidden" name="investment_id" value="">
                        <div class="header clearfix">
                            <a href="#" class="left">
                                <img src="./assets/temp/9.png" alt=""/>
                            </a>

                            <div class="left u-name">
                                <a href="#">如果我不是郦道元怎么办</a>
                                <span>2013/08/14&nbsp;&nbsp;上午09:05</span>
                            </div>
                        </div>
                        <div class="t-con">
                            <div class="t-conDiv">
                                <p class="clearfix">
                                    <label class="left sh">投资金额:</label>
                                    <span class="right font16">$10,000</span>
                                </p>

                                <p class="clearfix">
                                    <label class="left sh">投资方式:</label>
                                    <span class="right qh">一次性投资</span>
                                </p>
                                <dl>
                                    <dt>其它投资事项说明</dt>
                                    <dd class="ycx">
                                        我们的梦想是做一部深深打动人心的动画电影，带给少年爱与信仰的力量带给少年爱与信。”《大鱼•海棠》原作者清华学生Tidus和好友Breath一起创立了彼岸天，2008年启动了动的梦想是做一部深深打动的梦想是做一部深深打动的梦想是做一部深深打动画电影处女作《大鱼•海棠》
                                    </dd>
                                </dl>
                            </div>
                        </div>
                        <div class="foot">
                            <a href="javascript:;" class="lv ml20 Js_withdraw_pro">撤回投资</a>
                            <a class="talk mrr20"><p>和发布者交流</p></a>
                        </div>
                    </div>

            </li>
            <li class="left intentionList mt30">
                <form method="post" id="form-project-accept-invest" action="">
                    <div class="intention">
                        <div class="header clearfix">
                            <a href="#" class="left">
                                <img src="./assets/temp/9.png" alt=""/>
                            </a>

                            <div class="left u-name">
                                <a href="#">如果我不是郦道元怎么办</a>
                                <span>2013/08/14&nbsp;&nbsp;上午09:05</span>
                            </div>
                        </div>
                        <div class="t-con">
                            <div class="t-conDiv">
                                <p class="clearfix">
                                    <label class="left sh">投资金额:</label>
                                    <span class="right font16">$10,000</span>
                                </p>

                                <p class="clearfix">
                                    <label class="left sh">投资方式:</label>
                                    <span class="right fs">先付定金后付尾款</span>
                                </p>

                                <p class="clearfix">
                                    <label class="left sh">定金/尾款支付比例：</label>
                                    <span class="right font14">60%/40%</span>
                                </p>

                                <p class="clearfix">
                                    <label class="left sh">尾款支付时间：</label>
                                    <span class="right font14">2013/08/14</span>
                                </p>
                                <dl>
                                    <dt>其它投资事项说明</dt>
                                    <dd class="fqt">我们的梦想是做一部深深打动人心的动画电影，带给少年爱与信仰的力量...<a href="#" class="lys"> 查看更多</a></dd>
                                </dl>
                            </div>
                        </div>
                        <div class="foot">
                            <a href="javascript:;" class="lv ml20 Js_withdraw_pro">撤回投资</a>
                            <a class="talk mrr20"><p>和发布者交流</p></a>
                        </div>
                    </div>
                </form>
            </li>
            <li class="left intentionList mt30">
                <form method="post" id="form-project-accept-invest" action="">
                    <div class="intention">
                        <div class="header clearfix">
                            <a href="#" class="left">
                                <img src="./assets/temp/9.png" alt=""/>
                            </a>

                            <div class="left u-name">
                                <a href="#">如果我不是郦道元怎么办</a>
                                <span>2013/08/14&nbsp;&nbsp;上午09:05</span>
                            </div>
                        </div>
                        <div class="t-con">
                            <div class="t-conDiv">
                                <p class="clearfix">
                                    <label class="left sh">投资金额:</label>
                                    <span class="right font16">$10,000</span>
                                </p>

                                <p class="clearfix">
                                    <label class="left sh">投资方式:</label>
                                    <span class="right fs">先付定金后付尾款</span>
                                </p>

                                <p class="clearfix">
                                    <label class="left sh">定金/尾款支付比例：</label>
                                    <span class="right font14">80%/20%</span>
                                </p>

                                <p class="clearfix">
                                    <label class="left sh">尾款支付时间：</label>
                                    <span class="right font14">2013/08/14</span>
                                </p>
                                <dl>
                                    <dt>其它投资事项说明</dt>
                                    <dd class="fqt">我们的梦想是做一部深深打动人心的动画电影，带给我们的梦想是做一部深深打动人心的动画电影，带给我们的梦想是做一部深深打动人心的动画电影，带给少年爱与信仰的力量...<a
                                            href="#" class="lys"> 查看更多</a></dd>
                                </dl>
                            </div>
                        </div>
                        <div class="foot">
                            <a href="javascript:;" class="fen ml20 Js_withdraw_ok">确认</a>
                            <a href="javascript:;" class="hui Js_pro_cancles_withdraw">取消</a>
                            <a class="talk mrr20"><p>和发布者交流</p></a>
                        </div>
                    </div>
                </form>
            </li>
        </ul>
    </div>
</div>
<!--投资意向 结束-->

<!--项目详情 开始-->
<div class="detailPageMid">
        <div class="proContextDiv">
                <div class="titDiv">
                        <span></span>

                        <h2 class="Js_scroll">项目详情</h2>
                </div>
                <div class="context">
                        〖商机〗2006—2008年，因奥运会而产生的各类商机不断出现，每一个商机背后都意味着巨大的财富，这期间，如果把握好其中一项商机，一生的命运可能都会改变。对于个人而言，怎么样把握这个难得的商机？〖发现〗其实奥运会上最有值钱的东西就是5个吉祥物了。现在很多的人已经开始打它的主意，相关的吉祥物产品，诸如卡通像、剪纸等等
                        也将不断出现。对于个人在这方面而言，获利可能只是经销产品而已，而经营的人多，竟争也会激烈！但是，却有一门被忽略的冷门绝活手艺，将它应到吉祥物的制作上，所制作的吉祥物产品不仅形象出众、与众不同，而且很有内涵，极其新颖独特！绝不是我们可见到的普通吉祥物产品。如果面世，一定会深受迎！
                </div>
        </div>
</div>
<!--项目详情 结束-->

<!--创意项目回报 开始-->
<div class="detailPageMid">
        <div class="proContextDiv" id="pro_detail" name="pro_detail">
                <div class="titDiv">
                        <span></span>

                        <h2>创意项目回报</h2>
                </div>
                <div class="context">
                        〖获利〗用这个冷门绝活手艺制作吉祥物，非常适合个人操作。全部投资有几百元即可。两个月就可熟练掌握。既可自己制作销售，同时还可在当地进行技术培训，可小量制作，也可批量生产。从现在一直到奥运会到来之前的两年多时间，如果能抓好这个商机，掌握这门制作手工艺，抢先经营，一定会大获其利，赚到大钱的。我们详细揭秘这个手艺项目，转让全套技术及方法，包括赠送7张操作光盘。通过这些光盘可完全学会。全部内容收入了本套创意方案资料中。
                </div>
        </div>
</div>
<!--创意项目回报 结束-->

<!--项目更新 开始-->
<div class="detailPageMid">
        <div class="titDiv clearfix" style="margin-bottom:15px;">
                <span></span>

                <h2 class="left Js_scroll">项目更新</h2>

                <div class="right mt20">
<!--                        <a class="sub-sbtn Js_pro_upadate">发布项目更新</a>-->
                </div>
                <s class="p-box b2 Js_pro_b" style="bottom:-15px;"></s>
        </div>

		<form name="update_pro">
       	 <div style="background:#e6e6e6; display:none;" class="js_pro_c">
                <div class="expertDpDiv">
                        <div style="width:940px; margin:0 auto;">
                                <div class="height_27"></div>
                                <div class="rowblock clearfix">
                                        <div class="pt">标题</div>
                                        <div class="tools text left">
                                                <div class="inputWarp empty">
                                                        <input type="text" id="Js_update_tit">
                                                </div>
                                        </div>
										<div class="errorMsg left" style="display:none;">错误提示内容</div>
                                </div>

                                <div class="height_18"></div>
                                <div class="rowblock">
                                        <div class="pt">更新内容</div>
                                        <div id="js_context_toolbar" class="toolbar-wrap" style="width: 536px;"></div>
                                        <div class="toolbar-content left">
                                                <textarea id="js_context_update" name="content" style="height:90px"></textarea>
                                        </div>
										<div class="errorMsg left" style="display:none;">错误提示内容</div>
                                </div>
                                <div class="height_15"></div>
                                <div class="rowblock">
                                        <div class="pt">上传项目更新图片</div>
                                        <div class="uploadWrap-more dargArea left">
                                                <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
												<input class="file_uploadbox-more" type="file" multiple="multiple" accept="image/gif,image/jpeg,image/x-png,image/tiff">
												<input type="hidden" name="img_way" value="" />
                                        </div>
                                        <div class="errorMsg left" style="display:none;">错误提示内容</div>
                                        <div class="height_10"></div>
                                        <div id="upload-more" class="clearfix Js_update_img upload-more"></div>

                                </div>

                                <div class="height_40"></div>
                                <div class="p-warp">
                                        <a href="javascript:;" class="btn h8 mr5 Js_pro_upadate">取消更新</a>
                                        <a href="javascript:;" class="btn b73 Js_submit_update">提交更新</a>
                                </div>
                                <div class="height_50"></div>
                                <div class="sp-line" style="top:0; left:52px;">
                                        <div class="line-left"></div>
                                        <div class="line-right"></div>
                                </div>
                        </div>
                </div>
        </div>
		</form>
        <div class="proContextDiv">
                <div class="pro_update">
                        <dl>
                                <dt><a href="#">重回频率大战 AMD推出FX-9590 首次触及5GHz</a></dt>
                                <dd class="date">2012年10月3日</dd>
                                <dd class="txt">
                                        21世纪初处理器的频率大战在多年前止步于4GHz，而到了2013年，随着技术的进步这场战役似乎又要开始了。AMD今天公布了第一款5GHz处理器FX-9590，它八个核心都可以稳定运行在4.7GHz，而通过"Max
                                        Turbo"的短暂自动超频，它的频率最高可达到5GHz。<br/>
                                        FX-9590: Eight "Piledriver" cores, 5 GHz Max Turbo<br/>
                                        FX-9370: Eight "Piledriver" cores, 4.7 GHz Max Turbo<br/>
                                        这款产品将随着另一型号FX-9370在今年夏天发布，值得注意的是它的TDP高达220W，相对不极端一点的FX-8350运行在4.2GHz下时的TDP为125W。<br/>
                                        FX-9590和FX-9370芯片将在今年夏季晚些时候开始上市。但是，这些芯片首先仅向生产配置这些芯片的设备PC
                                        OEM厂商提供。其中一家厂商是Maingear。这家厂商已经宣布计划在其Shift台式电脑产品线中使用AMD的这些新的芯片。
                                        虽然主频速度曾经是确定一个处理器的性能的最佳途径，但是，多核处理器的新时代已经改变了这种情况。有趣的是，AMD现在用新的5GHz处理器再一次挑起"主频速度大战"。
                                </dd>
                        </dl>
                        <dl class="last">
                                <dt><a href="#">重回频率大战 AMD推出FX-9590 首次触及5GHz</a></dt>
                                <dd class="date">2012年10月3日</dd>
                                <dd class="txt">相对于以往笨重的数码相机而言，现在的卡片机轻薄便携，但如果真要冠以"卡片"的名义，它就非得安装Plastic
                                        Logic刚研发出的传感器不可。这是一款40×40毫米传感器，包含一个透射背板和ISORG有机光电探测器材料，它可以形成一个有弹性可弯曲，轻巧无比的相机模块，可以用来取代传统的图像传感器。但目前为止分辨率只能实现到94x95像素，像素大小175微米见方，间距200微米。<br/>
                                        但公司有信心制作出大面积图像传感器应用。这种柔性传感器可以被用在医疗、工业、安全控制等行业，同时还可以实现指纹扫描以及用在健康传感器上当然，它对数码产品消费者而言，最大的好处就是可以极大减少卡片机的厚度以及智能手机、平板电脑的厚度，同时也可以当做可塑性输入表面所用
                                </dd>
                        </dl>
                </div>
        </div>
</div>
<!--项目更新 结束-->

<!-- 洽谈的投资者 开始-->
<div class="detailPageMid">
        <div class="proContextDiv">
                <div class="titDiv">
                        <span></span>

                        <h2 class="Js_scroll">洽谈的投资者</h2>
                </div>
                <ul class="clearfix u_list">
                        <li class="left">
                                <div class="u_listItem">
                                        <a href="#" class="leftArea shadow">
                                                <img src="./assets/temp/5.png" alt="">
                                        </a>

                                        <div class="rightArea">
                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>

                                                <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a
                                                            href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                        </div>
                                </div>
                        </li>
                        <li class="left">
                                <div class="u_listItem">
                                        <a href="#" class="leftArea shadow">
                                                <img src="./assets/temp/5.png" alt="">
                                        </a>

                                        <div class="rightArea">
                                                <p class="rightName"><a href="#" class="Js_visitCard">刘兆宇</a></p>

                                                <p class="rightList"><a href="#">成都电子科技大学</a></p>
                                        </div>
                                </div>
                        </li>
                        <li class="left">
                                <div class="u_listItem">
                                        <a href="#" class="leftArea shadow">
                                                <img src="./assets/temp/5.png" alt="">
                                        </a>

                                        <div class="rightArea">
                                                <p class="rightName"><a href="#" class="Js_visitCard">刘兆宇</a></p>

                                                <p class="rightList"><a href="#">创意世界</a></p>
                                        </div>
                                </div>
                        </li>
                        <li class="left">
                                <div class="u_listItem">
                                        <a href="#" class="leftArea shadow">
                                                <img src="./assets/temp/5.png" alt="">
                                        </a>

                                        <div class="rightArea">
                                                <p class="rightName"><a href="#" class="Js_visitCard">刘兆宇</a></p>

                                                <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a
                                                            href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a><a href="#">电子科技大学</a><i>，</i><a
                                                            href="#">计算机科技与技术</a><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a><a
                                                            href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                        </div>
                                </div>
                        </li>
                        <li class="left">
                                <div class="u_listItem">
                                        <a href="#" class="leftArea shadow">
                                                <img src="./assets/temp/5.png" alt="">
                                        </a>

                                        <div class="rightArea">
                                                <p class="rightName"><a href="#" class="Js_visitCard">刘兆宇</a></p>

                                                <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a
                                                            href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                        </div>
                                </div>
                        </li>
                        <li class="left">
                                <div class="u_listItem">
                                        <a href="#" class="leftArea shadow">
                                                <img src="./assets/temp/5.png" alt="">
                                        </a>

                                        <div class="rightArea">
                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>

                                                <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a
                                                            href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                        </div>
                                </div>
                        </li>
                </ul>
        </div>
</div>
<!--洽谈的投资者 结束-->

<!--专家点评 开始-->
<?php include("./modules/expert-comment-others.php"); ?>
<!--专家点评 结束-->

<!--讨论区 开始-->
<?php $forumTT = "讨论区"; //社区论坛名称变量?>
<?php include("./modules/discuss.php") ; ?>
<!--讨论区 结束-->

<!--竞猜 开始-->
<?php $guessTT = "竞猜";  ?>
<?php include("./modules/lottery.php"); ?>
<!--竞猜 结束-->

</div>
</div>
</div>
<script type="text/javascript">
        tabs('tabs-tab','tab-t', 1, 'click');
        tabs('tabs-tab','tab-w', 1, 'click');
        getPercentMoney = {percent:[100,0],money:100000};
        function callback(ev){};
        if(dataControl)
                dataControl('date1');

</script>