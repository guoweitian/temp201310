<div class="content">
        <div class="detailPageMid">
                <div class="proContextDiv">
                    <form>
                        <div class="height_18"></div>
                        <div class="titDiv">
                                <span></span>
                                <h2>提现</h2>
                        </div>
                        <div class="height_42"></div>
                        <div>
                                <div class="pay-withdraw"><s>人民币金额：</s><span style="color: #ea7613">10000</span><s>元</s></div>
                                <div class="height_30"></div>
                                <div class="pay-withdraw"><s>可提现金额：</s><span style="color: #ea7613">10000</span><s>元</s></div>
                        </div>
                        <div class="height_35"></div>
                        <div class="t_warp clearfix">
                            <label class="lab" style="width:90px; font-weight: bold">提现金额：</label>
                            <div class="tools text left" style="background:#efefeb; margin-left: 41px">
                                <div class="inputWarp empty">
                                    <input onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" type="text" class="Js_input_text" style="background:#efefeb">
                                </div>
                            </div>
                            <label class="lab" style="width:10px; margin-left: 8px">元</label>
                            <div class="errorMsg left" style="display: none">错误提示内容</div>
                        </div>
                        <div class="height_50"></div>
                        <div>
                            <div class="pay-withdraw left"><s>选择提现账户：</s></div>
                            <div class="l-edit">
                                <p class="n-agree left" style="margin-left: 26px">

                                        <span class="Js_radio" data-name="rad" style="display: block">
                                            <label class="Js_label_ok ok"><input type="radio" name="rad" checked>光大银行<b style="margin-left: 56px; font-family: simsum, sans-serif; font-weight: normal">尾号：</b><b style="font-family: simsum, sans-serif; font-weight: normal">1799</b></label> <!--选择后文中颜色-->
                                        </span>

                                        <span class="Js_radio" data-name="rad" style="margin-top: 28px; display: block;">
                                            <label class="Js_label_ok"> <input type="radio" name="rad">招商银行<b style="margin-left: 56px; font-family: simsum, sans-serif; font-weight: normal">尾号：</b><b style="font-family: simsum, sans-serif; font-weight: normal">1790</b></label>
                                        </span>

                                </p>
                            </div>
                            <div class="height_27"></div>
                        </div>

                        <div class="height_60"></div>
                        <a href="#" class="blueBtn" style="margin-left: 131px" id="Js_account_getmoney_success">提现</a>
                        <a href="#" class="grayBtn" style="margin-left: 7px">返回</a>
                        <div class="height_40"></div>
                    </form>
                </div>
        </div>
</div>