<div class="content">
    <div class="login-box" style="width: 660px">
        <div class="l-login-form">
            <div class="height_18"></div>
            <div class="box-title">
                <h4 class="ml32">找回密码</h4>
            </div>
            <form id="forget_password">
            <div class="form-item left" style="margin-left: 115px;">
                <p class="font14">请输入你注册的邮箱账号/用户名/创创号</p>
                <div class="height_8"></div>
                <div class="tools text left" style="width:335px;">
                    <div class="inputWarp">
                        <input type="text" placeholder="" id="Js_find_pwd_input">
                    </div>
                </div>
				<div class="errorMsg left" style="display:none;">错误提示内容</div>
            </div>
            </form>
            <div class="errorMsg left" style="margin-top: 30px;"></div>
            <div class="height_25"></div>
            <div class="login-btnbar" style="margin-left: 92px;">
                <a href="send-email-to-find-password.php" class="blueBtn login-btn" id="Js_find_pwd">找回密码</a>
            </div>
        </div>
    </div>