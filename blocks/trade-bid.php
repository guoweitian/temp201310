<div class="content">
<div class="detail-wrap exchange">
<div class="detail-imgbg">
        <div class="toppattern"></div>
        <div class="detailTopDiv pew">
                <h3>这里是比赛名称，如果有很多字，这里也只能显示一行标题。这里是比赛名称，如果有很多字，这里也只能显示一行标题。</h3>
                <p class="disDiv">
                        <span class="loc">成都市青羊区小南街</span>
                        <span class="time">剩余3天</span>
                        <span class="m_num">T2013052400021</span>
                </p>
                <div>
                        <div class="u_listItem left">
                                <a href="../user/home.php" class="leftArea shadow">
                                        <img src="./assets/temp/5.png" alt="">
                                </a>
                                <div class="rightArea w600">
                                        <p class="rightName"><a href="#" class="Js_visitCard cfff" style="color: #372f2b;" >刘兆宇</a></p>
                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                </div>
                        </div>
                        <div class="tz_moneyDiv right">
                                <p>当前竞价:<b>10,000元</b></p>
                                <a href="./trade-bid.php" class="bgimgBtn js_detailPublish right">我要竞价</a>
                        </div>
                </div>
        </div>
</div>
<div class="form-wrap js_detailForm">
        <div id="tabs-tab" class="publish-form publish w765">
                <div class="row">
                        <div class="large-12 columns w765"  style="overflow:hidden;">
                                <div class="publish-progress">
                                        <div class="height_40"></div>
                                        <div class="issueStrip pr">
                                                <div class="gray-bg"></div>
                                                <ul id="tab-t" class="clearfix strip strip3">
                                                        <!--当前current/没填notFill/最后一步没填notFill last-->
                                                        <li class="one current" style="background-color: #F3F8FC">
                                                                <span class="fousLine w-first"></span>
                                                                <a class="selected" href="#step-one">
                                                                        <b>1</b>
                                                                        <span class="st-one">填写出价</span>
                                                                </a>
                                                        </li>
                                                        <li class="two ml notFill" style="background-color: #F3F8FC">
                                                                <s></s>
                                                                <span class="fousLine wh"></span>
                                                                <a href="#step-two" class=""><b>2</b><span class="st-two" style="margin-left: -27px">设置交易信息</span></a>
                                                        </li>
                                                        <li class="three ml notFill" style="background-color: #F3F8FC">
                                                                <s></s>
                                                                <span class="fousLine wh"></span>
                                                                <span class="fousLine w-last"></span>
                                                                <a href="#step-three"><b>3</b><span class="st-two" style="margin-left: -28px">确认竞价协议</span></a>
                                                        </li>

                                                </ul>
                                        </div>
                                </div>
                        </div>
                </div>
                <div id="tabs-items" class="Js_tabs_items clearfix">
                        <div class="row tabs-c">
                                <div class="large-12 columns">

                                        <div class="rowblock">
                                                <div class="pt">请填写您的最高出价</div>
                                                <p class="pdes">最高出价是您能接受的最高价格，系统将根据竞价规则，在您的出价范围内以最低价格成交</p>
                                                <div class="height_15"></div>
                                                <div class="tools text left">
                                                        <div class="inputWarp enterNum">
                                                                <input type="text" class="Js_input_text" id="Js_buy_number">
                                                        </div>
                                                        <div class="numFormat">.00</div>
                                                </div>
                                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                                        </div>
                                        <div class="height_50"></div>
                                        <a href="#step-two" class="blueBtn Js_s_one">下一步</a>
                                </div>
                                <div class="height_60"></div>
                        </div>
                    <div class="row tabs-c" style="display: block;">
                        <div class="large-12 columns">
                            <div class="rowblock">
                                <span class="pt">交易人</span>
			  <span style="font-size:20px; font-family:'黑体'; font-weight:bold; margin-left:40px; color: #cf6e6e">
			  			  郦道元
			  			  </span>
                            </div>
                            <div class="height_15"></div>
                            <!--城市联动选择开始-->
                            <div class="rowblock l-edit">
                                <div class="n-area">
                                    <div class="optDiv left Js_state mr10" style="width:106px; z-index:2">
                                        <input type="hidden" name="national_name" value="中国">
                                        <div class="tools text pr" style="width:106px;">
                                            <div class="inputWarp opt">
                                                <span>中国</span>
                                                <a href="javascript:void(0)" class="showMenu"></a>
                                            </div>
                                        </div>
                                        <ul style="display: none;">
                                            <li><a href="javascript:void(0)">中国</a></li>
                                        </ul>
                                    </div>
                                    <div class="left">
                                        <div class="optDiv Js_state left mr10" style="width:106px;">
                                            <div class="tools text pr" style="width:106px;">
                                                <div class="inputWarp opt">
                                                    <span>四川</span>
                                                    <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                            </div>
                                            <ul id="Js_t1" style="display: none;"><li><a href="javascript:void(0)">北京</a></li><li><a href="javascript:void(0)">天津</a></li><li><a href="javascript:void(0)">河北</a></li><li><a href="javascript:void(0)">山西</a></li><li><a href="javascript:void(0)">内蒙古</a></li><li><a href="javascript:void(0)">辽宁</a></li><li><a href="javascript:void(0)">吉林</a></li><li><a href="javascript:void(0)">黑龙江</a></li><li><a href="javascript:void(0)">上海</a></li><li><a href="javascript:void(0)">江苏</a></li><li><a href="javascript:void(0)">浙江</a></li><li><a href="javascript:void(0)">安徽</a></li><li><a href="javascript:void(0)">福建</a></li><li><a href="javascript:void(0)">江西</a></li><li><a href="javascript:void(0)">山东</a></li><li><a href="javascript:void(0)">河南</a></li><li><a href="javascript:void(0)">湖北</a></li><li><a href="javascript:void(0)">湖南</a></li><li><a href="javascript:void(0)">广东</a></li><li><a href="javascript:void(0)">广西</a></li><li><a href="javascript:void(0)">海南</a></li><li><a href="javascript:void(0)">重庆</a></li><li><a href="javascript:void(0)">四川</a></li><li><a href="javascript:void(0)">贵州</a></li><li><a href="javascript:void(0)">云南</a></li><li><a href="javascript:void(0)">西藏</a></li><li><a href="javascript:void(0)">陕西</a></li><li><a href="javascript:void(0)">甘肃</a></li><li><a href="javascript:void(0)">青海</a></li><li><a href="javascript:void(0)">宁夏</a></li><li><a href="javascript:void(0)">新疆</a></li><li><a href="javascript:void(0)">香港</a></li><li><a href="javascript:void(0)">澳门</a></li><li><a href="javascript:void(0)">台湾</a></li></ul>
                                            <select id="t1" style="display:none;" name="province_name"><option>北京</option><option>天津</option><option>河北</option><option>山西</option><option>内蒙古</option><option>辽宁</option><option>吉林</option><option>黑龙江</option><option>上海</option><option>江苏</option><option>浙江</option><option>安徽</option><option>福建</option><option>江西</option><option>山东</option><option>河南</option><option>湖北</option><option>湖南</option><option>广东</option><option>广西</option><option>海南</option><option>重庆</option><option selected="selected">四川</option><option>贵州</option><option>云南</option><option>西藏</option><option>陕西</option><option>甘肃</option><option>青海</option><option>宁夏</option><option>新疆</option><option>香港</option><option>澳门</option><option>台湾</option></select>
                                        </div>
                                        <div class="optDiv Js_state left mr10" style="width:106px;" id="Js_t2">
                                            <div class="tools text pr" style="width:106px;">
                                                <div class="inputWarp opt">
                                                    <span>成都</span>
                                                    <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                            </div>
                                            <ul style="display: none;"><li><a href="javascript:void(0)">成都</a></li><li><a href="javascript:void(0)">自贡</a></li><li><a href="javascript:void(0)">攀枝花</a></li><li><a href="javascript:void(0)">泸州</a></li><li><a href="javascript:void(0)">德阳</a></li><li><a href="javascript:void(0)">绵阳</a></li><li><a href="javascript:void(0)">广元</a></li><li><a href="javascript:void(0)">遂宁</a></li><li><a href="javascript:void(0)">内江</a></li><li><a href="javascript:void(0)">乐山</a></li><li><a href="javascript:void(0)">南充</a></li><li><a href="javascript:void(0)">眉山</a></li><li><a href="javascript:void(0)">宜宾</a></li><li><a href="javascript:void(0)">广安</a></li><li><a href="javascript:void(0)">达川</a></li><li><a href="javascript:void(0)">雅安</a></li><li><a href="javascript:void(0)">巴中</a></li><li><a href="javascript:void(0)">资阳</a></li><li><a href="javascript:void(0)">阿坝</a></li><li><a href="javascript:void(0)">甘孜</a></li><li><a href="javascript:void(0)">凉山</a></li></ul>
                                            <select id="t2" style="display:none;" name="city_name"><option selected="selected">成都</option><option>自贡</option><option>攀枝花</option><option>泸州</option><option>德阳</option><option>绵阳</option><option>广元</option><option>遂宁</option><option>内江</option><option>乐山</option><option>南充</option><option>眉山</option><option>宜宾</option><option>广安</option><option>达川</option><option>雅安</option><option>巴中</option><option>资阳</option><option>阿坝</option><option>甘孜</option><option>凉山</option></select>
                                        </div>
                                        <div class="optDiv Js_state left" style="width:106px;" id="Js_t3">
                                            <div class="tools text pr" style="width:106px;">
                                                <div class="inputWarp opt">
                                                    <span>锦江区</span>
                                                    <a href="javascript:void(0)" class="showMenu"></a>
                                                </div>
                                            </div>
                                            <ul style="display: none;"><li><a href="javascript:void(0)">锦江区</a></li><li><a href="javascript:void(0)">青羊区</a></li><li><a href="javascript:void(0)">金牛区</a></li><li><a href="javascript:void(0)">武侯区</a></li><li><a href="javascript:void(0)">成华区</a></li><li><a href="javascript:void(0)">龙泉驿区</a></li><li><a href="javascript:void(0)">青白江区</a></li><li><a href="javascript:void(0)">新都区</a></li><li><a href="javascript:void(0)">温江区</a></li><li><a href="javascript:void(0)">金堂县</a></li><li><a href="javascript:void(0)">双流县</a></li><li><a href="javascript:void(0)">郫县</a></li><li><a href="javascript:void(0)">大邑县</a></li><li><a href="javascript:void(0)">蒲江县</a></li><li><a href="javascript:void(0)">新津县</a></li><li><a href="javascript:void(0)">都江堰市</a></li><li><a href="javascript:void(0)">彭州市</a></li><li><a href="javascript:void(0)">邛崃市</a></li><li><a href="javascript:void(0)">崇州市</a></li></ul>
                                            <select id="t3" style="display:none;" name="district_name"><option selected="selected">锦江区</option><option>青羊区</option><option>金牛区</option><option>武侯区</option><option>成华区</option><option>龙泉驿区</option><option>青白江区</option><option>新都区</option><option>温江区</option><option>金堂县</option><option>双流县</option><option>郫县</option><option>大邑县</option><option>蒲江县</option><option>新津县</option><option>都江堰市</option><option>彭州市</option><option>邛崃市</option><option>崇州市</option></select>
                                        </div>
                                    </div>
                                </div>
                                <!--城市联动选择结束-->
                                <div class="height_15"></div>
                                <div class="rowblock" id="address">
                                    <div class="tools text left" style=" width: 410px;">
                                        <div class="inputWarp empty">
                                            <input type="text" placeholder="所在地址" id="Js_agreement_addr" name="address">
                                        </div>
                                    </div>
                                    <div class="errorMsg left" style="display:none;">错误提示内容</div>
                                </div>
                                <div class="height_15"></div>
                                <div class="rowblock" id="zip_code">
                                    <div class="tools text left" style=" width: 230px;">
                                        <div class="inputWarp empty">
                                            <input type="text" placeholder="邮编" id="Js_agreement_zip" name="zip_code" maxlength="6" class="Js_input_text">
                                        </div>
                                    </div>
                                    <div class="errorMsg left" style="display:none;">错误提示内容</div>
                                </div>
                                <div class="height_15"></div>
                                <div class="rowblock" id="phone_number">
                                    <div class="tools text left" style=" width: 230px;">
                                        <div class="inputWarp empty">
                                            <input type="text" placeholder="联系方式" id="Js_agreement_contact" name="phone_number" maxlength="11" class="Js_input_text">
                                        </div>
                                    </div>
                                    <div class="errorMsg left" style="display:none;">错误提示内容</div>
                                </div>
                                <div class="height_50"></div>
                                <a href="#step-one" class="grayBtn Js_s_two_pre mr16">上一步</a>
                                <a href="#step-three" class="blueBtn Js_s_two_next">下一步</a>
                            </div>
                            <div class="height_60"></div>
                        </div>

                    </div>

                        <div class="row tabs-c">
                                <div class="large-12 columns">
                                        <div class="rowblock l-edit">
                                                <div class="pt">确认创意世界交易所使用协议</div>
                                                <p class="pdes">请您阅读并确认创意世界交易所使用协议</p>
                                                <div class="height_15"></div>
                                                <div class="tools textArea left">
                                                        <textarea style="font-size: 15px; line-height: 1.5" id="Js_release_agreement" readonly>
我要竞价协议
1 简介
创意世界交易所是创意世界网站中用户之间（创意交易发布者和创意交易购买者）交易创意的版块。
您作为创意交易发布者：您可以在创意世界交易所展示您的创意，您也可以通过设置竞价模式或者一口价购买模式来出售您的创意。
您作为创意交易购买者：您可以通过创意世界交易所寻找您需要的任何创意，并且您可以通过参加某个创意交易发布者提供的交易方式（竞价或一口价）来获得其出售的创意。
创意世界：并不是创意交易发布者和创意交易购买者中的任何一方。所有创意交易仅存在于用户和用户之间。
2 行为规则
创意交易出售者：
	您必须在法律允许的范围内出售您的创意。
	您必须在创意世界交易所对您所出售的创意或物品，以及它们的所有出售条件作出文字说明。
	您不得发布任何含有欺诈行为的信息。如创意世界（通过定罪、和解、保险或其它调查或创意世界自行酌情决定的其它方式）怀疑您进行欺诈活动，创意世界可全权酌情决定中止或终止您的帐户。
	您不得出售任何侵犯知识产权，第三方的著作权、商标权或其他知识产权的创意交易。知识产权权利人可通过联系创意世界客服申请删除侵犯其知识产权的物品。
创意交易购买者：
为了维护创意世界交易所的交易秩序，防止创意交易购买者恶拍，尽量避免并妥善处理买卖双方之间的交易纠纷，您参与竞拍时冻结的保证金将为本次竞拍提供保障。如您未在竞拍成功之日起三日内按照成交价格支付拍品货款，您同意创意世界交易有权将您在竞拍流程中最终冻结的竞拍保证金赔偿给卖家。
3 知识产权的转让：
在创意交易出售者和创意交易购买者完成交易资金交接后，该创意的知识产权即转让给交易购买者所有。
4 创意世界的责任范围
创意世界有权随时以任何理由拒绝、取消、中断、删除或暂停该任务。创意世界不因该行为承担任何赔偿。创意世界的政策并非评论此类行为的理由。
创意世界不承担任何创意或使用服务产生的损失或亏损。创意世界无义务介入任何用户之间的纠纷，或用户与其他第三方就服务使用方面产生的纠纷。包括但不限于货物及服务的交付，其他条款、条件、保证或与网站活动相关联的有关陈述。创意世界不负责监督任务的实现与严格执行。您可授权创意世界、其工作人员、职员、代理人及对损失索赔权的继任者所有已知或未知、公开或秘密的解决争议的方法和服务。
                                                        </textarea>
                                                </div>
                                                <div class="errorMsg left" style="display:none;">请确认同意协议</div>
                                        </div>
                                        <div class="height_10"></div>
                                        <div class="l-edit">
                                                <p class="n-agree left">
                <span class="Js_checkBox" data-name="chk">
                <label class="Js_label_ok ok">
                        <input type="checkbox" name="chk" checked="checked" id="Js_readed_agreement">
                        我已经阅读并同意创意世界交易竞价协议</label>
                </span>
                                                </p>
                                                <div class="errorMsg left" style="margin-top: -10px; display:none;">请确认同意协议</div>
                                        </div>

                                        <div class="height_40"></div>
                                        <div class="rowblock l-edit">
                                                <div class="pt left">
                                                        <div class="sign-bg">
                                                                <input class="signature" id="Js_signature"></div>
                                                </div>
                                                <div class="errorMsg left mt8" style="display:none;">错误提示内容</div>
                                        </div>
                                        <div class="height_50"></div>
                                        <a href="#step-two" class="grayBtn Js_s_two_pre mr16">上一步</a>
                                        <a href="javascript:;" class="blueBtn big-blueBtn" id="Js_bid_agreement">确认使用协议</a>
                                </div>
                                <div class="height_60"></div>
                        </div>
                </div>
        </div>
</div>

<div class="detailPageMidDiv">
        <!--交易信息 开始-->
        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="titDiv">
                                <span></span>
                                <h2 class="Js_scroll">交易信息</h2>
                        </div>
                        <div class="context">〖商机〗2006—2008年，因奥运会而产生的各类商机不断出现，每一个商机背后都意味着巨大的财富，这期间，如果把握好其中一项商机，一生的命运可能都会改变。对于个人而言，怎么样把握这个难得的商机？〖发现〗其实奥运会上最有值钱的东西就是5个吉祥物了。现在很多的人已经开始打它的主意，相关的吉祥物产品，诸如卡通像、剪纸等等     也将不断出现。对于个人在这方面而言，获利可能只是经销产品而已，而经营的人多，竟争也会激烈！但是，却有一门被忽略的冷门绝活手艺，将它应到吉祥物的制作上，所制作的吉祥物产品不仅形象出众、与众不同，而且很有内涵，极其新颖独特！绝不是我们可见到的普通吉祥物产品。如果面世，一定会深受迎！</div>
                </div>
        </div>
        <!--交易信息 结束-->

        <!--已竞价的用户 开始-->
        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="titDiv">
                                <span></span>
                                <h2 class="Js_scroll">已竞价的用户</h2>
                        </div>
                        <ul class="clearfix u_list Js_bidd_u_list">
                                <li>
                                        <div class="auctionHisList clearfix">
                                                <div class="u_listItem left mr160">
                                                        <a href="#" class="leftArea shadow">
                                                                <img src="./assets/temp/5.png">
                                                        </a>
                                                        <div class="rightArea">
                                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                                                                <p class="rightList"><a href="#">中国四川成都</a></p>
                                                        </div>
                                                </div>
                                                <div class="c-a left">
                                                        <span><em>50,000</em><i>元</i></span>
                                                        <p>当前竞价</p>
                                                </div>
                                                <div class="a-t right">
                                                        <span>22:52:18 2013/05/17</span>
                                                        <p>出价时间</p>
                                                </div>
                                        </div>
                                </li>
                                <li>
                                        <div class="auctionHisList clearfix">
                                                <div class="u_listItem left mr160">
                                                        <a href="#" class="leftArea shadow">
                                                                <img src="./assets/temp/5.png">
                                                        </a>
                                                        <div class="rightArea">
                                                                <p class="rightName"><a href="#" class="Js_visitCard" style="color: #372f2b;>如果我不是郦道元怎么办</a></p>
                                                                <p class="rightList"><a href="#">中国四川成都</a></p>
                                                        </div>
                                                </div>
                                                <div class="c-a left">
                                                        <span><em>50,000</em><i>元</i></span>
                                                        <p>历史竞价</p>
                                                </div>
                                                <div class="a-t right">
                                                        <span>13:21:33 2013/05/13</span>
                                                        <p>出价时间</p>
                                                </div>
                                        </div>
                                </li>
                                <li>
                                        <div class="auctionHisList clearfix">
                                                <div class="u_listItem left mr160">
                                                        <a href="#" class="leftArea shadow">
                                                                <img src="./assets/temp/5.png">
                                                        </a>
                                                        <div class="rightArea">
                                                                <p class="rightName"><a href="#" class="Js_visitCard" style="color: #372f2b;>如果我不是郦道元怎么办</a></p>
                                                                <p class="rightList"><a href="#">中国四川成都</a></p>
                                                        </div>
                                                </div>
                                                <div class="c-a left">
                                                        <span><em>50,000</em><i>元</i></span>
                                                        <p>历史竞价</p>
                                                </div>
                                                <div class="a-t right">
                                                        <span>09:11:20 2013/05/11</span>
                                                        <p>出价时间</p>
                                                </div>
                                        </div>
                                </li>
                        </ul>
                </div>
        </div>
        <!--已竞价的用户 结束-->

        <!--专家引用文件-->
        <?php include("./modules/expert-comment-trade-rate.php"); ?>

        <!--社区论坛引用文件-->
        <?php $forumTT = "讨论区"; //社区论坛标题 ?>
        <?php include("./modules/discuss.php"); ?>

        <!--竞猜引用文件-->
        <?php $guessTT = "交易竞猜"; ?>
        <?php include("./modules/lottery.php"); ?>


</div>
</div>
</div>
<script type="text/javascript">
        tabs('tabs-tab','tab-t', 1, 'click');
        tabs('tabs-tab','tab-w', 1, 'click');
        getPercentMoney = {percent:[100,0],money:100000};
        function callback(ev){}
        if(dataControl)
                dataControl('date1');

</script>