<div class="content">
<div class="publish-wrap">
<form method="post" id="form-send-hire" action="">
<div id="tabs-tab" class="publish-form publish w765">
<div class="row" style="overflow:hidden;">
        <div class="large-12 columns">
                <div class="publish-progress">
                        <div class="height_40"></div>
                        <div class="issueStrip pr">
                                <div class="gray-bg"></div>
                                <ul id="tab-t" class="clearfix strip strip4">
                                        <!--当前current/没填notFill/最后一步没填notFill last-->
                                        <li class="one">
                                                <span class="fousLine w-first"></span>
                                                <a class="selected" href="#step-one">
                                                        <b>1</b>
                                                        <span class="st-one">基本信息</span>
                                                </a>
                                        </li>
                                        <li class="notFill two ml">
                                                <s></s>
                                                <span class="fousLine wh"></span>
                                                <a href="#step-two"><b>2</b><span class="st-two">描述内容</span></a>
                                        </li>
                                        <li class="notFill three ml">
                                                <s></s>
                                                <span class="fousLine wh"></span>
                                                <a href="#step-three"><b>3</b><span class="st-three">邀请评委</span></a>
                                        </li>
                                        <li class="notFill four ml">
                                                <s></s>
                                                <span class="fousLine wh"></span>
                                                <span class="fousLine w-last"></span>
                                                <a href="#step-four"><b>4</b><span class="st-fore" style="margin-left: -27px">确认发布协议</span></a>
                                        </li>
                                </ul>
                        </div>
                </div>
        </div>
</div>
<div id="tabs-items" class="Js_tabs_items clearfix">
        <div class="row tabs-c">
                <div class="large-12 columns">
                        <div class="rowblock">
                                <div class="pt">填写比赛名称</div>
                                <div class="tools text left">
                                        <div class="inputWarp empty">
                                                <input type="text" id="Js_hiring_name_input">
                                        </div>
                                </div>
                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        </div>
                        <div class="height_25"></div>
                        <div class="rowblock">
                                <div class="pt">比赛所在地</div>
                                <div class="tools text left Js_cityList">
                                        <s class="locBg"></s>
                                        <div class="inputWarp downMenuCity">
                                                <input type="text" class="Js_loc_input" id="Js_loc_input">
                                                <div class="selDiv cityList gloc">
                                                        <ul>
                                                                <li class="s1">可以根据城市名，街道名，国家名等搜索</li>
                                                                <li class="s2 Js_loc_ad"><a href="javascript:;">自动定位当前位置</a></li>
                                                                <li class="s3 Js_world_ad"><a href="javascript:;">全世界</a></li>
                                                        </ul>
                                                </div>
                                        </div>
                                </div>
                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                        </div>

                        <div class="height_25"></div>
                        <div class="rowblock">
                                <div class="pt">寻找公益明星的时间</div>
                                <div class="l-edit">
                                        <p class="n-agree mt10 left">
                                                <span class="Js_radio mr25 Js_forever" data-name="rad">
                                                        <label class="Js_label_ok ok"><input type="radio" name="rad" checked>长期</label> <!--选择后文中颜色-->
                                                </span>
                                                <span class="Js_radio Js_limit_time" data-name="rad">
                                                        <label class="Js_label_ok"> <input type="radio" name="rad"> 限制时间</label>
                                                </span>
                                        </p>
                                </div>
                        </div>
                        <div class="height_25"></div>

                        <div class="rowblock Js_limit_content" style="display:none;">
                            <div class="pt">设置获胜明星的截止日期</div>
                            <div class="tools text error left">
                                <input class="datebox Js_input_date" type="text" id="date1" readonly="readonly" >
                            </div>
                            <div class="errorMsg left" style="display:none;">请选择日期</div>
                        </div>

                        <div class="height_25"></div>
                        <div class="rowblock">
                                <div class="pt">上传比赛封面图片</div>
                                <div class="uploadWrap-single dargArea left">
                                        <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
                                        <input class="file_uploadbox-single" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">
                                </div>
                                <div class="errorMsg left" style="display:none">请选上传封面图片</div>
                                <div class="height_10"></div>
                                <div id="upload-single" class="clearfix upload-single Js_upload_more"></div>
                        </div>
                        <div class="height_50"></div>
                        <a href="#step-two" class="blueBtn Js_s_one">下一步</a>
                </div>
                <div class="height_60"></div>
        </div>

        <div class="row tabs-c">
                <div class="large-12 columns">
                        <!--设置比赛奖项 开始-->
                        <div class="rowblock">
                                <div class="pt">比赛奖项</div>
                            <div>
                                <div class="awards-collect"><p>总奖项：</p><span style="color: #cf6e6e" id="Js_title_num">0</span><p>个</p></div>
                                <div class="awards-collect" style="margin-top: 18px">
                                    <p>总奖金：</p>
                                        <span style="color: #cf6e6e; margin-left: 26px;" id="Js_awards_money" target_money_num="0">
                                            0
                                        </span>
                                    <p>元</p>
                                </div>
                            </div>
                            <div class="height_30"></div>
                            <div style="width:497px; height:99px; border:2px dashed #cccccc" class="Js_add_awards_btn">
                                <div class="clearfix" style="margin-top:29px; margin-left:21px;">
                                    <a href="javascript:;" class="new-btn Js_add_newawards" style="float:left; margin-right:10px;">添加奖项</a>
                                    <span style="float:left; font-size:14px; color:#888888; line-height:20px;">您还没有添加比赛奖项，为了让您的比赛更有吸<br />引力，请添加奖项。</span>
                                </div>
                                <div class="errorMsg" style="display:none; position: relative; left: 490px; top: -40px">错误提示内容</div>
                            </div>
                            <div class="height_10"></div>
                            <div>
                                <div class="t_warp clearfix Js_add_awardslist" style="display:none;">
                                    <div class="height_20"></div>
                                    <a href="javascript:;" class="c-bank-btn c-awards-btn left">添加新奖项</a>
                                </div>
                            </div>
                            <div class="t_warp clearfix Js_addawards_form" style="display:none;">
                                <div class="left">

                                    <div class="height_22"></div>

                                    <div class="height_22"></div>
                                    <div class="t_warp clearfix">
                                        <label class="lab" style="width: 102px;">头衔：</label>
                                        <div class="tools left" style="width:326px;">
                                            <div class="inputWarp empty">
                                                <input type="text" style="color: #888; font-family: simsun,sans-serif;" maxlength="16" class="Js_add_ngawards" placeholder="例如“冠军”"">
                                            </div>
                                        </div>
                                        <div class="errorMsg left" style="display:none;">请填写奖项头衔</div>
                                    </div>
                                    <div class="height_22"></div>
                                    <div class="t_warp clearfix">
                                        <label class="lab" style="width: 102px;">奖金：</label>
                                        <div class="tools text left" style="width:326px;">



                                                    <div class="inputWarp enterNum">
                                                        <input type="text" class="Js_add_ngawardsnum Js_enterOnlyNum" style="font-family: simsun,sans-serif; font-size: 14px;color: #888;">
                                                    </div>
                                                    <div class="numFormat">.00</div>


                                        </div>
                                        <div class="errorMsg left" style="display:none;">请填写奖项奖金</div>
                                    </div>

                                    <div class="height_20"></div>
                                    <div class="t_warp clearfix">
                                        <label class="lab" style="width: 102px;"></label>
                                        <a href="javascript:void(0)" class="grayBtn Js_add_awards_cancle" style="margin-right:4px; font-family: simsun,sans-serif">取消添加</a>
                                        <a href="javascript:void(0)" class="blueBtn Js_add_awards_ok" style="font-family:simsun">确认添加</a>
                                    </div>
                                    <div class="height_20"></div>
                                </div>
                            </div>

                        </div>
                        <!--设置比赛奖项 结束-->
                        <div class="height_25"></div>
                        <div class="rowblock l-edit">
                                <div class="pt">比赛其他奖励内容</div>
                                <p class="pdes">请填写您为获胜明星准备的奖励内容</p>
                                <div class="height_15"></div>
                                <div id="tz_toolbar" class="toolbar-wrap"></div>
                                <div class="toolbar-content">
                                        <textarea id="tz_content" name="content"></textarea>
                                </div>
                                <div class="errorMsg" style="display: none">错误提示内容</div>
                        </div>
                        <div class="height_25"></div>
                        <div class="rowblock l-edit">
                                <div class="pt">详细描述比赛内容</div>
                                <p class="pdes">请填写选拔明星的具体要求和比赛详细介绍。</p>
                                <div class="height_10"></div>
                                <div id="xb_toolbar" class="toolbar-wrap"></div>
                                <div class="toolbar-content">
                                        <textarea id="xb_content" name="content"></textarea>
                                </div>
                                <div class="errorMsg" style="display: none">错误提示内容</div>
                        </div>

                        <div class="height_50"></div>
                        <a href="#step-one" class="grayBtn mr16 Js_s_two_pre">上一步</a>
                        <a href="#step-three" class="blueBtn Js_s_two_next">下一步</a>
                </div>
                <div class="height_60"></div>
        </div>

        <div class="row tabs-c">
                <div class="large-12 columns">
                        <div class="rowblock">
                                <div class="tools sel left Js_expertList">
                                        <div class="inputWarp downMenuExperts">
                                                <input type="text" />
                                                <div class="selDiv expertList">
                                                        <div class="describe">让创创召帮您查找关键词为"<span class="Js_search_txt"></span>"的内容</div>
                                                        <ul class="mt-2">
                                                        </ul>
                                                </div>
                                        </div>
                                </div>
                                <a href="javascript:void(0)" class="blueBtn fl ml10 Js_recommend_expert">推荐站外专家</a>
                        </div>
                        <div class="height_15"></div>
                        <div class="inviteExpert">
                                <h3>已邀请专家</h3>
                                <ul class="Js_invitedexplist">
                                        <li class="clearfix">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">刘兆宇</p>
                                                        <p class="rightDis"><a href="#">成都电子科技大学</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">如果我不是郦道元怎么办</p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">创意世界</a><i>，</i><a href="#">创意世界</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix" style="margin-top:31px;">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">刘兆宇</p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">创意世界</a><i>，</i><a href="#">创意世界</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                        <li class="clearfix" style="margin-top:31px;">
                                                <a href="#" class="leftArea"><img src="./assets/temp/5.png" alt=""></a>
                                                <div>
                                                        <p class="rightName">站外专家姓名</p>
                                                        <p class="rightDis"><a href="#">站外专家的简介内容</a></p>
                                                </div>
                                                <span class="expert-del Js_exp_del"></span>
                                        </li>
                                </ul>

                        </div>

                        <div class="height_50"></div>
                        <a href="#step-two" class="grayBtn mr16 Js_s_three_pre">上一步</a>
                        <a href="#step-four" class="blueBtn Js_s_three_next">下一步</a>
                </div>
                <div class="height_60"></div>
        </div>

        <div class="row tabs-c">
                <div class="large-12 columns">
                        <div class="rowblock l-edit">
                                <div class="pt">确认发布协议</div>
                                <p class="pdes">请您阅读并确认创意公益明星比赛的发布协议</p>
                                <div class="height_10"></div>
                                <div class="tools textArea">
                                        <textarea style="font-size: 15px; line-height: 1.5" id="Js_release_agreement" readonly>
确认发布创意公益明星比赛协议
您只有在完全同意下列服务条款，并完成注册后，才能成为而且立即成为四川创意世界科技有限公司创意公益明星比赛的用户并使用创意博爱明星比赛所提供的服务。一旦您浏览或开始使用创意博爱明星比赛的服务，即视为您已经完全了解并同意下列条款，包括创意博爱明星比赛对条款做的修改。
创意公益明星比赛是创意世界提供的一个让用户(即“明星比赛发布者”)通过提供奖金或其他实物/非实物奖励召集用户（即”明星比赛参赛者“）参赛并选拔最终获奖者的服务。
创意世界：并不是明星比赛发布者和明星比赛参赛者中的任何一方。所有明星的选拔和奖励仅存在于用户和用户之间。
1 行为规范
	您应遵循《创意世界服务协议》使用创意公益明星比赛的相关服务。
	您通过发布创意公益明星比赛时，应明确比赛详细内容。活动内容不明确的，由创意世界予以判定，您应按照创意世界的判定执行。
	您应严格按照其设定或创意世界判定的规则发布创意公益明星比赛、履行承诺，切实保护报名参赛的创意世界会员或互联网用户权益。
	您在使用创意世界各项服务过程中侵犯创意世界权益的，您应赔偿因此造成的损失。如果相关方对是否侵权及是否损失多少有异议的，您同意由创意世界做独立判断，并以该判断结果作为赔偿的唯一依据。
2 服务变更，中断或终止
如因系统升级或维护而需要暂停或终止服务，创意世界会提前在网站首页张贴公告。
如遇以下情形，创意世界有权立刻单方面终止向创意世界提供服务，且无需通知创意世界用户。
	在创意公益明星比赛中辱骂其他用户
	对其他用户进行人身攻击
	发布黄色，反动内容
	发布广告信息（广告信息是指利用创意公益明星比赛作为渠道，发布违规信息（如黄色，反动内容），大量发送诱导用户点击网页链接，以及以商业合作为由，大量发送的诱导用户提供个人资料的信息。）
	盗用他人资料，冒充他人
	创意世界用户违反本协议
	创意世界用户的行为可能不利于创意世界、创意世界科技有限公司或其会员、其它用户或第三方的合理利益。
3 使用规则
	用户在申请使用创意公益明星比赛服务时，必须向创意世界提供准确的个人资料，包括肖像、个人数据、联系方式等，如个人资料有任何变动，必须及时更新。
	创意世界用户不应将其帐号、密码转让或出借予他人使用。如创意世界用户发现其帐号遭他人非法使用，应立即通知创意世界。因黑客行为或创意世界用户的保管疏忽导致帐号、密码遭他人非法使用，四川创意世界科技有限公司不承担任何责任。
	创意世界用户需遵守法律、法规、政策、道德、创意世界各项规则，而且不得违背、侵犯、影响四川创意世界科技有限公司的利益、名誉。
                                        </textarea>
                                </div>
                                <div class="errorMsg left" style="display:none;">请确认同意协议</div>
                        </div>
                        <div class="height_10"></div>
                        <div class="l-edit">
                                <p class="n-agree left">
                <span class="Js_checkBox" data-name="chk">
                 <label class="Js_label_ok ok">
                         <input type="checkbox" name="chk" checked="checked" id="Js_readed_agreement">
                         我已经阅读并同意创意公益明星比赛发布协议</label>
                </span>
                                </p>
                                <div class="errorMsg left" style="margin-top: -10px; display:none;">请确认同意协议</div>
                        </div>
                        <div class="height_40"></div>
                        <div class="rowblock l-edit">
                                <div class="pt left">
                                        <div class="sign-bg">
                                                <input class="signature" id="Js_signature"></div>
                                </div>
                                <div class="errorMsg left mt8" style="display:none;">错误提示内容</div>
                        </div>
                        <div class="height_50"></div>
                        <a href="#step-four" class="grayBtn mr16 Js_s_four_pre">上一步</a>
                        <a href="#" class="blueBtn Js_send_project">发布比赛</a>
                </div>
                <div class="height_60"></div>
        </div>
</div>
</div>
</form>
</div>
</div>
<script type="text/javascript">
        $(function(){
                //初始化tabs
                tabs('tabs-tab','tab-t', 1, 'click');
                tabs('tabs-tab','tab-w', 1, 'click');


                dataControl('date1');
        })
</script>