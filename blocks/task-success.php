<div class="content">
<div class="detail-wrap task">
<div class="detail-imgbg">
        <div class="toppattern"></div>
        <div class="detailTopDiv pew">
                <h3>这里是比赛名称，如果有很多字，这里也只能显示一行标题。这里是比赛名称，如果有很多字，这里也只能显示一行标题。</h3>
                <p class="disDiv">
                        <span class="loc">成都市青羊区小南街</span>
                        <span class="time">剩余3天</span>
                        <span class="m_num">M2013052400021</span>
                </p>
                <div>
                        <div class="u_listItem left">
                                <a href="../user/home.php" class="leftArea shadow">
                                        <img src="./assets/temp/5.png" alt="">
                                </a>
                                <div class="rightArea w600">
                                        <p class="rightName">
                                                <a class="Js_visitCard cfff" href="#">刘兆宇</a>
                                        </p>
                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                </div>
                        </div>
                        <div class="tz_moneyDiv right">
                                <p>任务比赛奖金:<b>10,000元</b></p>
<!--                                <a href="javascript:" class="bgimgBtn js_detailPublish right task">我要参赛</a>-->
                        </div>
                </div>
        </div>
</div>
<!--我要参赛弹出层开始-->
<div class="form-wrap js_detailForm">
        <div id="tabs-tab" class="publish-form publish w765">
                <div class="row">
                        <div class="large-12 columns w765"  style="overflow:hidden;">
                                <div class="publish-progress">
                                        <div class="height_40"></div>
                                        <div class="issueStrip pr">
                                                <div class="gray-bg"></div>
                                                <ul id="tab-t" class="clearfix strip strip2">
                                                        <!--当前current/没填notFill/最后一步没填notFill last-->
                                                        <li class="one current" style="background-color: #F3F8FC">
                                                                <span class="fousLine w-first"></span>
                                                                <a class="selected" href="#step-one">
                                                                        <b>1</b>
                                                                        <span class="st-one">确认协议</span>
                                                                </a>
                                                        </li>
                                                        <li class="notFill two ml"  style="background-color: #F3F8FC">
                                                                <s></s>
                                                                <span class="fousLine wh"></span>
                                                                <span class="fousLine w-last"></span>
                                                                <a href="#step-two"><b>2</b><span class="st-two">提交作品</span></a>
                                                        </li>
                                                </ul>
                                        </div>
                                </div>
                        </div>
                </div>
                <div id="tabs-items" class="Js_tabs_items clearfix">
                        <div class="row tabs-c">
                                <div class="large-12 columns">

                                        <div class="rowblock l-edit">
                                                <div class="pt">确认创意任务人才比赛作品转让协议</div>
                                                <p class="pdes">请您阅读并确认创意任务人才比赛作品转让的协议</p>
                                                <div class="height_10"></div>
                                                <div class="tools textArea left">
                                                        <textarea style="font-size: 15px; line-height: 1.5" id="Js_release_agreement" readonly>
签订作品转让协议
创意任务人才比赛的发布者和参赛者，将基于本协议完成该参赛者提交的参赛作品的知识产权的转让，出售。
本协议适用于所有创意任务人才比赛的发布者和参赛者。
                                                        </textarea>
                                                </div>
                                                <div class="errorMsg left" style="display: none;">错误内容提示</div>
                                        </div>
                                        <div class="height_10"></div>
                                        <div class="l-edit">
                                                <p class="n-agree left">
                <span class="Js_checkBox" data-name="chk">
                <label class="Js_label_ok ok">
                        <input type="checkbox" name="chk" checked="checked" id="Js_readed_agreement">
                        我已经阅读并同意创意任务人才比赛作品转让协议</label> <!--选择后文中颜色-->
                </span>
                                                </p>
                                                <div class="errorMsg left" style="margin-top: -10px; display: none;">请确认同意协议</div>
                                        </div>

                                        <div class="height_40"></div>
                                        <div class="rowblock l-edit">
                                                <div class="pt left">
                                                        <div class="sign-bg">
                                                                <input class="signature" id="Js_signature">
                                                        </div>
                                                </div>
                                                <div class="errorMsg left mt8" style="display:none;">错误提示内容</div>
                                        </div>
                                        <div class="height_50"></div>
                                        <a href="#step-two" class="blueBtn Js_s_one">确认比赛协议</a>
                                </div>
                                <div class="height_60"></div>
                        </div>

                        <div class="row tabs-c">
                                <div class="large-12 columns">
                                        <div class="rowblock l-edit">
                                                <div class="pt">请填写任务作品的详细内容</div>
                                                <p class="pdes">您提交的任务作品将会展示在比赛中，以便创意任务人才比赛的发布者选择。</p>
                                                <div class="height_10"></div>
                                                <div id="js_toolbar" class="toolbar-wrap"></div>
                                                <div class="toolbar-content left">
                                                        <textarea id="js_content" name="content"></textarea>
                                                </div>
                                                <div class="errorMsg left" style="position: absolute; left: 650px; white-space: nowrap; display:none;">错误提示内容</div>
                                        </div>
                                        <div class="height_25"></div>
                                        <div class="rowblock">
                                                <div class="pt">上传任务作品图片</div>
                                                <div class="uploadWrap-single dargArea left">
                                                        <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
                                                        <input class="file_uploadbox-single" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">
                                                </div>
                                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                                                <div class="height_10"></div>
                                                <div id="upload-single" class="clearfix Js_fm_cover_div"></div>
                                        </div>
                                        <div class="height_50"></div>
                                        <a href="#step-two" class="grayBtn Js_s_two_pre mr16">上一步</a>
                                        <a href="#step-two" class="blueBtn big-blueBtn" id="Js_task_send_agreement">确认提交任务作品</a>
                                </div>
                                <div class="height_60"></div>
                        </div>
                </div>
        </div>
</div>
<!--我要参数弹出层结束-->


<div class="detailPageMidDiv">

        <!--获胜任务人才和作品 开始-->
        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="titDiv">
                                <span></span>
                                <h2>获胜任务人才和作品</h2>
                                <a class="talk right"><p>和发布者交流</p></a>
                        </div>
                        <!--头像、ID和作品信息-->
                        <div class="u_listItem w937 ph100">
                                <div class="height_40"></div>
                                <a href="#" class="leftArea shadow">
                                        <img src="./assets/temp/5.png" alt="">
                                </a>
                                <div class="rightArea pw100 ph100">
                                        <p class="rightName"><a href="#" class="Js_visitCard">郦道元 （发布者页面）</a></p>
                                        <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                        <p class="des">用户还可以在App Store的"更新"栏看到最近刚刚升级过的应用列表。用户可以在这里看到安装升级程序的时间以及升级的内容。 如果App Store还没有进行自动更新，用户也可以选择手动安装最新的更新文件。</p>
                                        <ul class="worklists clearfix">
                                                <li>
                                                        <a data-fancybox-group="gallery3" class="Js_fancyBox3" href="./assets/temp/tp1.jpg">
                                                                <img src="./assets/temp/tp1.jpg" alt="">
                                                        </a>
                                                </li>
                                                <li>
                                                        <a data-fancybox-group="gallery3" class="Js_fancyBox3" href="./assets/temp/tp3.jpg">
                                                                <img src="./assets/temp/tp2.jpg" alt="">
                                                        </a>
                                                </li>
                                                <li>
                                                        <a data-fancybox-group="gallery3" class="Js_fancyBox3" href="./assets/temp/tp4.jpg">
                                                                <img src="./assets/temp/tp3.jpg" alt="">
                                                        </a>
                                                </li>
                                                <li>
                                                        <a data-fancybox-group="gallery3" class="Js_fancyBox3" href="./assets/temp/tp4.jpg">
                                                                <img src="./assets/temp/tp4.jpg" alt="">
                                                        </a>
                                                </li>
                                        </ul>
                                </div>
                        </div>
                        <div class="height_45"></div>

                        <!--签订作品转让协议-->
                        <div>
                        <div class="clearfix ml94">
                                <div class="rowblock">
                                        <div class="pt">签订作品转让协议</div>
                                        <div class="tools textArea" style="width:685px;">
                                                <textarea style="font-size: 15px; line-height: 1.5">
签订作品转让协议
创意任务人才比赛的发布者和参赛者，将基于本协议完成该参赛者提交的参赛作品的知识产权的转让，出售。
本协议适用于所有创意任务人才比赛的发布者和参赛者。
 知识产权的归属
	任务发布者全额支付比赛奖金后，该作品的知识产权即转让给任务发布者所有。
	任务发布者若需要线下签署知识产权转让协议，被选为任务获胜者的参赛者有义务配合任务发布者完成协议的签署，若被选为任务获胜的参赛者拒绝签署，买家有权要求退还任务奖金。

                                                </textarea>
                                        </div>
                                </div>
                                <div class="height_5"></div>
                                <p class="n-agree left mt10">
                                        <label class="Js_label_ok ok">
                                                <input type="checkbox" checked id="Js_readed_agreementzp" />
                                                我已经阅读并同意签订作品转让协议
                                        </label>
                                </p>
                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                                <div class="height_30"></div>
                                <div class="rowblock l-edit">
                                        <div class="pt left">
                                                <div class="sign-bg">
                                                        <input class="signature" id="Js_signaturezp"></div>
                                        </div>
                                        <div class="errorMsg left mt8" style="display:none;">错误提示内容</div>
                                </div>
                        </div>
                        <div class="height_40"></div>
                        <div><a href="javascript:;" class="doBtn ml94" id="Js_task_sucbtn">确认协议</a></div>
                    </div>
                        <div class="height_35"></div>
                        <!--上传任务作品成果-->
                        <div class="rowblock clearfix ml94">
                                <div class="pt">上传任务作品成果</div>
                                <div class="height_8"></div>
                                <div class="uploadWrap-single left clearfix">
                                        <div class="upfileDiv">
                                                <ul>
                                                        <li>
                                                                <label class="lv">PNG</label>
                                                                <a>证明材料.jpg</a>
                                                                <a href="javascript:;" class="Js_upfile_del"></a>
                                                        </li>
                                                        <li>
                                                                <label class="lan">ZIP</label>
                                                                <a>证明材料.jpg</a>
                                                                <a href="javascript:;" class="Js_upfile_del"></a>
                                                        </li>
                                                        <li>
                                                                <label class="fen">DOC</label>
                                                                <a>证明材料.jpg</a>
                                                                <a href="javascript:;" class="Js_upfile_del"></a>
                                                        </li>
                                                </ul>

                                        </div>
                                </div>
                                <div id="upload-single" class="clearfix"></div>

                                <input class="file_uploadbox-single" type="file">

                                <div class="clearfix">
<!--                                        <a href="javascript:;" class="doBtn tuploadbox left mr10">上传文件</a>-->
                                        <a href="javascript:;" class="doBtn left" id="Js_task_cgbtn">确认成果</a>
                                </div>
                        </div>

                        <!--头像、ID和作品信息-->
                        <div class="u_listItem w937 ph100">
                            <div class="height_40"></div>
                            <a href="#" class="leftArea shadow">
                                <img src="./assets/temp/5.png" alt="">
                            </a>
                            <div class="rightArea pw100 ph100">
                                <p class="rightName"><a href="#" class="Js_visitCard">郦道元 （专家页面）</a></p>
                                <p class="rightList"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                <p class="des">用户还可以在App Store的"更新"栏看到最近刚刚升级过的应用列表。用户可以在这里看到安装升级程序的时间以及升级的内容。 如果App Store还没有进行自动更新，用户也可以选择手动安装最新的更新文件。</p>
                                <ul class="worklists clearfix">
                                    <li>
                                        <a data-fancybox-group="gallery3" class="Js_fancyBox3" href="./assets/temp/tp1.jpg">
                                            <img src="./assets/temp/tp1.jpg" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a data-fancybox-group="gallery3" class="Js_fancyBox3" href="./assets/temp/tp3.jpg">
                                            <img src="./assets/temp/tp2.jpg" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a data-fancybox-group="gallery3" class="Js_fancyBox3" href="./assets/temp/tp4.jpg">
                                            <img src="./assets/temp/tp3.jpg" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a data-fancybox-group="gallery3" class="Js_fancyBox3" href="./assets/temp/tp4.jpg">
                                            <img src="./assets/temp/tp4.jpg" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="height_45"></div>

                </div>
        </div>
        <!--获胜任务人才和作品 结束-->

        <!--任务详情 开始-->
        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="titDiv">
                                <span></span>
                                <h2 class="Js_scroll">任务详情</h2>
                        </div>
                        <div class="context">苹果用户每天手动安装应用更新的日子已经成为过去时了，由于iOS 7中的App Store增强了功能，发现新软件将变得比以前更容易。虽然App Store中无处不在的红色升级标记令用户们感到非常迷惑，但是当iOS 7在今年秋季正式发布的时候，用户们就会体会到它的好处了。如果启动自动更新的功能，iOS 7中的App Store就可以在后台自动检查和安装应用更新。
                                用户还可以在App Store的“更新”栏看到最近刚刚升级过的应用列表。用户可以在这里看到安装升级程序的时间以及升级的内容。 如果App Store还没有进行自动更新，用户也可以选择手动安装最新的更新文件。

                                “更新”栏中还将包括一个指向已购买内容的链接，用户们可以重新下载他们以前在另一台iOS设备上已经安装过的应用程序。

                                用户可以在“iOS设置”应用的“iTunes App Store”部分控制自动更新功能，在“自动下载”部分根据需要开启或关闭“更新”、“应用”和“音乐”等选项。 与在以前版本的iOS系统中一样，用户们还可以决定是否允许自动下载功能使用蜂窝网络数据。</div>
                </div>
        </div>
        <!--任务详情 结束-->

        <!--任务人才和作品 开始-->
        <?php include("./modules/task-participator.php"); ?>
        <!--任务人才和作品 结束-->

        <!--讨论区-->
        <?php $forumTT = "讨论区"; //社区论坛名称变量 ?>
        <?php include("./modules/discuss.php"); ?>

        <!--竞猜-->
        <?php $guessTT = "任务竞猜"; ?>
        <?php include("./modules/lottery.php"); ?>

</div>
</div>
</div>
<script type="text/javascript">
        $(function(){
                tabs('tabs-tab','tab-t', 1, 'click');
                tabs('tabs-tab','tab-w', 1, 'click');
                $('.Js_fancyBox,.Js_fancyBox1,.Js_fancyBox2,.Js_fancyBox3').fancybox({
                        prevEffect : 'none',
                        nextEffect : 'none',

                        closeBtn  : true,
                        arrows    : true,
                        nextClick : true,
                        tpl : {
                                closeBtn : '<a title="Close" style="background-image: url(assets/images/fancybox/fancybox_sprite.png); width:36px; height:36px; top:-18px; right:-18px; position:absolute"></a>'
                        },
                        helpers : {
                                thumbs : {
                                        width  : 50,
                                        height : 50
                                }
                        }
                })

        });
</script>