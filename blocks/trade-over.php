<div class="content">
        <div class="detail-wrap exchange">
                <div class="detail-imgbg">
                        <div class="toppattern"></div>
                        <div class="detailTopDiv pew">
                                <h3>这里是比赛名称，如果有很多字，这里也只能显示一行标题。这里是比赛名称，如果有很多字，这里也只能显示一行标题。</h3>
                                <p class="disDiv">
                                        <span class="loc">成都市青羊区小南街</span>
                                        <span class="time">已结束</span>
                                        <span class="m_num">T2013052400021</span>
                                </p>
                                <div>
                                        <div class="u_listItem left">
                                                <a href="profile-other.php" class="leftArea shadow">
                                                        <img src="./assets/temp/5.png" alt="">
                                                </a>
                                                <div class="rightArea w600">
                                                        <p class="rightName"><a href="#" class="Js_visitCard cfff">刘兆宇</a></p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                                </div>
                                        </div>
                                        <div class="tz_moneyDiv right">
                                                <p>当前竞价:<b>10,000元</b></p>
                                                <a href="#" class="bgimgBtn_over right over" onclick="return false">已结束</a>
                                        </div>
                                </div>
                        </div>
                </div>

                <div class="detailPageMidDiv">
                        <!--交易已结束 开始-->
                        <div class="detailPageMid">
                                <div class="proContextDiv">
                                        <div class="titDiv">
                                                <span></span>
                                                <h2>交易已经结束</h2>
                                        </div>

                                        <div class="tOverDiv">
                                                <p class="top">您发布的创意交易已经结束。</p>
                                                <p class="iList">在比赛交易期间，暂时没有潜在的交易者出价；</p>
                                                <p class="iList">您可以更新一部份交易介绍信息，以便吸引更多的潜在交易者参与出价；</p>
                                                <p class="iList">您可以邀请更多的创意世界智库专家点评您的交易内容，为潜在的交易者提供专业意见；</p>
                                                <p class="iList">为了方便您再次发布本创意交易，您可以使用下方的"再次发布"按钮。</p>
                                        </div>
                                        <div class="height_40"></div>
                                        <a href="./trade-publish.php" class="nsBtn">再次发布</a>
                                </div>
                        </div>
                        <!--交易已结束 结束-->

                        <!--交易信息 开始-->
                        <div class="detailPageMid">
                                <div class="proContextDiv">
                                        <div class="titDiv">
                                                <span></span>
                                                <h2 class="Js_scroll">交易信息</h2>
                                        </div>
                                        <div class="context">〖商机〗2006—2008年，因奥运会而产生的各类商机不断出现，每一个商机背后都意味着巨大的财富，这期间，如果把握好其中一项商机，一生的命运可能都会改变。对于个人而言，怎么样把握这个难得的商机？〖发现〗其实奥运会上最有值钱的东西就是5个吉祥物了。现在很多的人已经开始打它的主意，相关的吉祥物产品，诸如卡通像、剪纸等等     也将不断出现。对于个人在这方面而言，获利可能只是经销产品而已，而经营的人多，竟争也会激烈！但是，却有一门被忽略的冷门绝活手艺，将它应到吉祥物的制作上，所制作的吉祥物产品不仅形象出众、与众不同，而且很有内涵，极其新颖独特！绝不是我们可见到的普通吉祥物产品。如果面世，一定会深受迎！</div>
                                </div>
                        </div>
                        <!--交易信息 结束-->

                        <!--已竞价的用户 开始-->
                        <div class="detailPageMid">
                                <div class="proContextDiv">
                                        <div class="titDiv">
                                                <span></span>
                                                <h2 class="Js_scroll">已竞价的用户</h2>
                                        </div>
                                        <ul class="clearfix u_list">
                                                <li>
                                                        <div class="auctionHisList clearfix">
                                                                <div class="u_listItem left mr160">
                                                                        <a href="#" class="leftArea shadow">
                                                                                <img src="./assets/temp/5.png">
                                                                        </a>
                                                                        <div class="rightArea">
                                                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                                                                                <p class="rightList"><a href="#">中国四川成都</a></p>
                                                                        </div>
                                                                </div>
                                                                <div class="c-a left">
                                                                        <span><em>50,000</em><i>元</i></span>
                                                                        <p>当前竞价</p>
                                                                </div>
                                                                <div class="a-t right">
                                                                        <span>22:52:18 2013/05/17</span>
                                                                        <p>出价时间</p>
                                                                </div>
                                                        </div>
                                                </li>
                                                <li>
                                                        <div class="auctionHisList clearfix">
                                                                <div class="u_listItem left mr160">
                                                                        <a href="#" class="leftArea shadow">
                                                                                <img src="./assets/temp/5.png">
                                                                        </a>
                                                                        <div class="rightArea">
                                                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                                                                                <p class="rightList"><a href="#">中国四川成都</a></p>
                                                                        </div>
                                                                </div>
                                                                <div class="c-a left">
                                                                        <span><em>50,000</em><i>元</i></span>
                                                                        <p>历史竞价</p>
                                                                </div>
                                                                <div class="a-t right">
                                                                        <span>13:21:33 2013/05/13</span>
                                                                        <p>出价时间</p>
                                                                </div>
                                                        </div>
                                                </li>
                                                <li>
                                                        <div class="auctionHisList clearfix">
                                                                <div class="u_listItem left mr160">
                                                                        <a href="#" class="leftArea shadow">
                                                                                <img src="./assets/temp/5.png">
                                                                        </a>
                                                                        <div class="rightArea">
                                                                                <p class="rightName"><a href="#" class="Js_visitCard">如果我不是郦道元怎么办</a></p>
                                                                                <p class="rightList"><a href="#">中国四川成都</a></p>
                                                                        </div>
                                                                </div>
                                                                <div class="c-a left">
                                                                        <span><em>50,000</em><i>元</i></span>
                                                                        <p>历史竞价</p>
                                                                </div>
                                                                <div class="a-t right">
                                                                        <span>09:11:20 2013/05/11</span>
                                                                        <p>出价时间</p>
                                                                </div>
                                                        </div>
                                                </li>
                                        </ul>
                                </div>
                        </div>
                        <!--已竞价的用户 结束-->

                        <!--专家点评引用文件-->
                        <?php include("./modules/expert-comment.php"); ?>

                        <!--社区论坛引用文件-->
                        <?php $forumTT = "讨论区" ; //社区论坛变量 ?>
                        <?php include("./modules/discuss.php"); ?>

                        <!--竞猜引用文件-->
                        <?php $guessTT = "交易竞猜"; ?>
                        <?php include("./modules/lottery.php"); ?>
                </div>
        </div>
</div>
<script type="text/javascript">
        getPercentMoney = {percent:[100,0],money:100000};

</script>