<div class="content">
        <div class="detail-wrap task">
                <div class="detail-imgbg">
                        <div class="toppattern"></div>
                        <div class="detailTopDiv pew">
                                <h3>这里是比赛名称，如果有很多字，这里也只能显示一行标题。这里是比赛名称，如果有很多字，这里也只能显示一行标题。</h3>
                                <p class="disDiv">
                                        <span class="loc">成都市青羊区小南街</span>
                                        <span class="time">剩余3天</span>
                                        <span class="m_num">M2013052400021</span>
                                </p>
                                <div>
                                        <div class="u_listItem left">
                                                <a href="../user/home.php" class="leftArea shadow">
                                                        <img src="./assets/temp/5.png" alt="">
                                                </a>
                                                <div class="rightArea w600">
                                                        <p class="rightName"><a class="Js_visitCard cfff" href="#">刘兆宇</a></p>
                                                        <p class="rightDis"><a href="#">创意世界</a><i>，</i><a href="#">中国四川成都</a><i>，</i><a href="#">电子科技大学</a><i>，</i><a href="#">计算机科技与技术</a></p>
                                                </div>
                                        </div>
                                        <div class="tz_moneyDiv right">
                                                <p>任务比赛奖金:<b>10,000</b>元</p>
                                                <a href="javascript:" class="bgimgBtn js_detailPublish right task">我要参赛</a>
                                        </div>
                                </div>
                        </div>
                </div>
                <!--我要参赛弹出层开始-->
            <form method="post" id="form-task-send-agreement" action="">
                <div class="form-wrap js_detailForm">
                        <div id="tabs-tab" class="publish-form publish w765">
                                <div class="row">
                                        <div class="large-12 columns w765"  style="overflow:hidden;">
                                                <div class="publish-progress">
                                                        <div class="height_40"></div>
                                                        <div class="issueStrip pr">
                                                                <div class="gray-bg"></div>
                                                                <ul id="tab-t" class="clearfix strip strip2">
                                                                        <!--当前current/没填notFill/最后一步没填notFill last-->
                                                                        <li class="one current" style="background-color: #F3F8FC">
                                                                                <span class="fousLine w-first"></span>
                                                                                <a class="selected" href="#step-one">
                                                                                        <b>1</b>
                                                                                        <span class="st-one">确认协议</span>
                                                                                </a>
                                                                        </li>
                                                                        <li class="notFill two ml"  style="background-color: #F3F8FC">
                                                                                <s></s>
                                                                                <span class="fousLine wh"></span>
                                                                                <span class="fousLine w-last"></span>
                                                                                <a href="#step-two"><b>2</b><span class="st-two" style="margin-left: -12px">提交作品</span></a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                                <div id="tabs-items" class="Js_tabs_items clearfix">
                                        <div class="row tabs-c">
                                                <div class="large-12 columns">

                                                        <div class="rowblock l-edit">
                                                                <div class="pt">确认创意任务人才比赛协议</div>
                                                                <p class="pdes">请您阅读并确认创意任务人才比赛的协议</p>
                                                                <div class="height_10"></div>
                                                                <div class="tools textArea left">
                                                                        <textarea style="font-size: 15px; line-height: 1.5" id="Js_release_agreement" readonly>
我要参赛协议
1 简介
您只有在完全同意下列服务条款，并完成注册后，才能成为而且立即成为四川创意世界科技有限公司创意任务人才比赛的用户并使用创意任务人才比赛所提供的服务。一旦您浏览或开始使用创意任务人才比赛的服务，即视为您已经完全了解并同意下列条款，包括创意任务人才比赛对条款做的修改。
创意任务人才比赛是创意世界提供的一个让用户(即“任务发布者”)通过提供奖金向任务参赛者征集任务解决方案的服务。
您作为任务发布者：任务参赛者将为您提供任务解决方案，您选择满意的解决方案后支付任务奖金给获胜者并获取全部 解决方案，在签订《作品转让协议》前您都可以随时收回任务奖金。
您作为任务参赛者：可以通过提供解决方案给任务发布者，任务发布者将从所有任务参赛者中选出可以赢取奖金的任务获胜者，如果您成为了任务获胜者，您将在任务发布者点击“确认成果”按钮后获得任务发布者提供的任务奖金。注意：在签订《作品转让协议》前，任务发布者可以选择收回任务奖金。
创意世界：并不是任务发布者和任务参赛者中的任何一方。所有任务作品的交易仅存在于用户和用户之间。
2 禁止行为
发布或参加创意任务人才比赛，您须同意并遵守以下禁止行为协议，包括如下条款：
	征集或提交软件破解、程序破解类作品；
	征集或提交游戏外挂、程序外挂类作品；
	征集或提交盗取网银账号、游戏账号类作品；
	征集或提交侵犯第三方知识产权的作品；
	征集或提交侵犯第三方权利的作品；
	征集或提交木马、黑客程序等有损网络安全的作品；
	征集或提交涉黄、赌博等作品；
	征集或提交其他违反法律、法规、行政规章等相关规定的作品。
	征集或提交论文代写类作品；
	征集或提交刷钻、刷信用等作品；
	征集或提交虚假信息的作品；
	征集或提交通过链接等方式逃避创意世界及其他用户审核的作品；
	征集或提交可能给他人或者其他机构带来损害的作品；
征集或提交其他违背社会伦理或社会主流价值观的作品。
3 知识产权的转让
	在任务发布者和任务参赛者签订《作品转让协议》后，该作品的知识产权即转让给任务发布者所有。
	任务发布者若需要线下签署知识产权转让协议，被选为任务获胜者的参赛者有义务配合任务发布者完成协议的签署，若被选为任务获胜的参赛者拒绝签署，买家有权随时退还任务奖金。
4 创意世界的责任范围
创意世界有权随时以任何理由拒绝、取消、中断、删除或暂停该任务。创意世界不因该行为承担任何赔偿。创意世界的政策并非评论此类行为的理由。
创意世界不承担任何作品或使用服务产生的损失或亏损。创意世界无义务介入任何用户之间的纠纷，或用户与其他第三方就服务使用方面产生的纠纷。包括但不限于货物及服务的交付，其他条款、条件、保证或与网站活动相关联的有关陈述。创意世界不负责监督任务的实现与严格执行。您可授权创意世界、其工作人员、职员、代理人及对损失索赔权的继任者所有已知或未知、公开或秘密的解决争议的方法和服务。
                                                                        </textarea>
                                                                </div>
                                                                <div class="errorMsg left" style="display: none;">错误内容提示</div>
                                                        </div>
                                                        <div class="height_10"></div>
                                                        <div class="l-edit">
                                                                <p class="n-agree left">
                <span class="Js_checkBox" data-name="chk">
                <label class="Js_label_ok ok">
                        <input type="checkbox" name="chk" checked="checked" id="Js_readed_agreement">
                        我已经阅读并同意创意任务人才比赛的参赛协议</label> <!--选择后文中颜色-->
                </span>
                                                                </p>
                                                                <div class="errorMsg left" style="margin-top: -10px; display: none;">请确认同意协议</div>
                                                        </div>

                                                        <div class="height_40"></div>
                                                        <div class="rowblock l-edit">
                                                                <div class="pt left">
                                                                        <div class="sign-bg">
                                                                                <input class="signature" id="Js_signature">
                                                                        </div>
                                                                </div>
                                                                <div class="errorMsg left mt8" style="display:none;">错误提示内容</div>
                                                        </div>
                                                        <div class="height_50"></div>
                                                        <a href="#step-two" class="blueBtn Js_s_one">确认比赛协议</a>
                                                </div>
                                                <div class="height_60"></div>
                                        </div>

                                        <div class="row tabs-c">
                                                <div class="large-12 columns">
                                                        <div class="rowblock l-edit">
                                                                <div class="pt">请填写任务作品的详细内容</div>
                                                                <p class="pdes">您提交的任务作品将会展示在比赛中，以便创意任务人才比赛的发布者选择。</p>
                                                                <div class="height_10"></div>
                                                                <div id="js_toolbar" class="toolbar-wrap"></div>
                                                                <div class="toolbar-content left">
                                                                        <textarea id="js_content" name="content"></textarea>
                                                                </div>
                                                                <div class="errorMsg left" style="position: absolute; left: 650px; white-space: nowrap; display:none;">错误提示内容</div>
                                                        </div>
                                                        <div class="height_25"></div>
                                                        <div class="rowblock">
                                                                <div class="pt">上传任务作品图片</div>
                                                                <div class="uploadWrap-single dargArea left">
                                                                        <span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>
                                                                        <input class="file_uploadbox-single" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">
                                                                </div>
                                                                <div class="errorMsg left" style="display:none;">错误提示内容</div>
                                                                <div class="height_10"></div>
                                                                <div id="upload-single" class="clearfix Js_fm_cover_div upload-single"></div>
                                                        </div>
                                                        <div class="height_50"></div>
                                                        <a href="#step-two" class="grayBtn Js_s_two_pre mr16">上一步</a>
                                                        <a href="#step-two" class="blueBtn big-blueBtn" id="Js_task_send_agreement">确认提交任务作品</a>
                                                </div>
                                                <div class="height_60"></div>
                                        </div>
                                </div>
                        </div>
                </div>
            </form>
                <!--我要参赛弹出层结束-->


                <div class="detailPageMidDiv">
                        <div class="detailPageMid">
                                <div class="proContextDiv">
                                        <div class="titDiv">
                                                <span></span>
                                                <h2 class="Js_scroll">任务详情</h2>
                                        </div>
                                        <div class="context">苹果用户每天手动安装应用更新的日子已经成为过去时了，由于iOS 7中的App Store增强了功能，发现新软件将变得比以前更容易。虽然App Store中无处不在的红色升级标记令用户们感到非常迷惑，但是当iOS 7在今年秋季正式发布的时候，用户们就会体会到它的好处了。如果启动自动更新的功能，iOS 7中的App Store就可以在后台自动检查和安装应用更新。
                                                用户还可以在App Store的“更新”栏看到最近刚刚升级过的应用列表。用户可以在这里看到安装升级程序的时间以及升级的内容。 如果App Store还没有进行自动更新，用户也可以选择手动安装最新的更新文件。

                                                “更新”栏中还将包括一个指向已购买内容的链接，用户们可以重新下载他们以前在另一台iOS设备上已经安装过的应用程序。

                                                用户可以在“iOS设置”应用的“iTunes App Store”部分控制自动更新功能，在“自动下载”部分根据需要开启或关闭“更新”、“应用”和“音乐”等选项。 与在以前版本的iOS系统中一样，用户们还可以决定是否允许自动下载功能使用蜂窝网络数据。
                                        </div>
                                </div>
                        </div>

                        <!--任务人才和作品-->
                        <?php include("./modules/task-participator.php");?>

                        <!--todo 社区论坛引用文件-->
                        <?php $forumTT = "讨论区"; //社区论坛名称变量?>
                        <?php include("./modules/discuss.php"); ?>
                        <!--todo 竞猜引用文件-->
                        <?php $guessTT = "竞猜"; ?>
                        <?php include("./modules/lottery.php"); ?>
                </div>
        </div>
</div>
<script type="text/javascript">
        $(function(){
                tabs('tabs-tab','tab-t', 1, 'click');
                tabs('tabs-tab','tab-w', 1, 'click');
                $('.Js_fancyBox,.Js_fancyBox1,.Js_fancyBox2,.Js_fancyBox3').fancybox({
                        prevEffect : 'none',
                        nextEffect : 'none',

                        closeBtn  : true,
                        arrows    : true,
                        nextClick : true,
                        tpl : {
                                closeBtn : '<a title="Close" style="background-image: url(assets/images/fancybox/fancybox_sprite.png); width:36px; height:36px; top:-18px; right:-18px; position:absolute"></a>'
                        },
                        helpers : {
                                thumbs : {
                                        width  : 50,
                                        height : 50
                                }
                        }
                })

        });
</script>