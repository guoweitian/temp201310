<div class="content">
        <div class="detailPageMid">
                <div class="proContextDiv">
                        <div class="height_38"></div>
                        <div class="titDiv">
                                <span></span>
                                <h2>订单支付遇到的问题？</h2>
                        </div>
                        <div class="height_33"></div>
                        <div class="pay_error">
                                <p><b class="tit">常见问题汇总</b></p>
                                <div class="height_20"></div>
                                <p><b>网上银行已经扣款，但账户余额没有增加？</b></p>
                                <p>答：这是因为银行的数据没有即使传输给我们，我们会在第二个工作日与银行对账后给予确认，请再耐心等待一下</p>
                                <div class="height_48"></div>
                                <p><b>网上银行页面打不开，怎么办？</b></p>
                                <p>答：建议使用Microsoft IE浏览器。点击IE的菜单“工具”—“internet选项”—“安全”—将安全中的各项设置恢复到默认级别</p>
                                <div class="height_48"></div>
                                <p><b>我网上银行重复多次付款了该怎么办？</b></p>
                                <p>答：由于您的支付信息银行没有即使传输给创意世界，造成您的银行重复扣款。<br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;不过请放心，我们会在第二个工作日和银行对账，确认您的汇款后，重复支付款项会直接充值到您的现金账户。</p>
                                <div class="height_54"></div>
                                <p><a href="javascript:;" class="blueBtn">重新支付</a></p>
                        </div>
                </div>
        </div>
</div>