<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
    'style.1.0.3',
    'jquery.fancybox',
);

$load_js = array(
    'jquery',
    'jquery.pstrength-min.1.2',
    'jquery.html5uploader',
    'uploader_config_single',
    'jquery.fancybox.min',
    'common',
    'detail',
    'login'
);

$title_text = '注册创创号';

$navigation_text = array(

);

include('modules/header.php');
include('modules/ccz-logout.php');
include('modules/navigation.php');
include('modules/sidebar.php');

include('blocks/register.php');

include('modules/footer.php');
?>

