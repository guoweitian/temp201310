<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
    'style.1.0.3',
    'jquery.fancybox',
);

$load_js = array(
    'jquery',
    'jquery.html5uploader',
    'uploader_config_single',
    'jquery.fancybox.min',
    'common',
    'detail'
);

$title_text = '找回密码邮件已发送';

$navigation_text = array(

);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation.php');
include('modules/sidebar.php');

include('blocks/send-email-to-find-password.php');

include('modules/footer.php');
?>

