<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
        'style.1.0.3',
        'redactor',
        'dp',
        'jquery.fancybox',
        'jquery.fancybox-thumbs'
);

$load_js = array(
        'jquery',
        'vendor/custom.modernizr',
        'masonry.min',
        'imagesloaded.min',
        'foundation.min',
        'jquery.html5uploader',
        'uploader_config_single',
        'uploader_config_more',
        'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'tCity',
        'city_select',
        'tabs',
        'jquery.datepicker',
        'datepicker_lang_CN',
        'common',
        'publish',
        'hiring_publish',
        'redactor',
        'redactor_zh_cn',
        'redactor_config',
        'jquery.fancybox',
);

$title_text = '创意公益明星比赛发布';

$navigation_text = array(
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-hire.php');
include('modules/sidebar.php');

include('blocks/hire-publish.php');

include('modules/footer.php');
?>