<?php

/*
 *  Define Navigation Text
 */
$scroll = 1;
$load_css = array(
        'style.1.0.3',
        'redactor',
        'jquery-ui',
        'dp',
        'jquery.fancybox',
);

$load_js = array(
        'jquery',
        'jquery.html5uploader',
        'uploader_config_single',
        'jquery.fancybox.min',
        'detail',
        'publish',
        'theme',
        'common',
        'redactor',
        'redactor_zh_cn',
        'redactor_config',
        'jquery.fancybox',
);

$title_text = '创意世界交易所';

$navigation_text = array(
        '0' => '<a class="on" href="javascript:;">交易信息</a>',
        '1' => '<a href="javascript:;">已购买用户</a>',
        '2' => '<a href="javascript:;">专家点评</a>',
        '3' => '<a href="javascript:;">讨论区</a>',
        '4' => '<a href="javascript:;">交易竞猜</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-trade.php');
include('modules/sidebar.php');

include('blocks/trade-success.php');

include('modules/footer.php');
?>