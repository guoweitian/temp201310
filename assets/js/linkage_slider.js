$(function() {
	var obj = $( ".Js_h" );
	var sliderBar = $('.Js_slider');
	var dragNum = 0;
	var testData = getPercentMoney.percent;
	var testMoney = getPercentMoney.money;
	sliderBar.each(function(m,dom){
		$(dom).slider({
			range: "min",
			value: testData[m],
			min: 0,
			max: 100,
			slide: function( event, ui ) {
				var totle = 100;
				var moneyTotal = testMoney;
				$(this).next().text(ui.value+'%').next().val(ui.value);	
				$(this).parent().find('.Js_deposit_money').text(parseInt(testMoney*ui.value / 100,10));
				if(m==0){
					$('.Js_slider').eq(1).next().text((totle-ui.value)+'%').next().val((totle-ui.value));	
					$('.Js_slider').eq(1).parent().find('.Js_deposit_money').text(moneyTotal - parseInt(testMoney*ui.value / 100,10));
					$('.Js_slider').eq(1).slider( "option", "value", (totle-ui.value));
					testData = [ui.value,totle-ui.value];
				}
				
				if(m==1){
					$('.Js_slider').eq(0).next().text((totle-ui.value)+'%').next().val((totle-ui.value));	
					$('.Js_slider').eq(0).parent().find('.Js_deposit_money').text(moneyTotal - parseInt(testMoney*ui.value / 100,10));
					$('.Js_slider').eq(0).slider( "option", "value", (totle-ui.value));	
					testData = [totle-ui.value,ui.value];
				}
			},
			stop:function(){
				dragNum = 0;
			}
		})
	})
	initSlider();
	
	function initSlider(){
		initData(testMoney);
		$('.Js_enterNum').keyup(function(e){
			if(e.keyCode<37 || (e.keyCode>40&&e.keyCode<48) || (e.keyCode>57&&e.keyCode<96) || e.keyCode > 105){
				if(e.keyCode!=8)
					return false;
			}
			
				var reg = /^[0-9]*$/;
				if($(this).val().match(reg)){
					testMoney = parseInt($('.Js_enterNum').val(),10);
					testMoney = isNaN(testMoney) ? 0 : testMoney;
					initData(testMoney);
				}
		}).keydown(function(e){
			if(e.keyCode<37 || (e.keyCode>40&&e.keyCode<48) || (e.keyCode>57&&e.keyCode<96) || e.keyCode > 105){
				if(e.keyCode!=8)
					return false;	
			}
		})
		
		function initData(money){
			obj.each(function(i,dom){
				var moneyText = getIntMoney(i,testData);
				$(dom).parents('.sliderBarDiv').find('.Js_deposit_money').text(moneyText);
				var t = moneyText==0 ? 0 : moneyText==money ? 100 : testData[i];
				$(dom).text(t+'%').parent().next().val(t);
			});
		}
	}
	
	function delPointNum(arr){
		var num = 0;
		for(var i=0; i<arr.length; i++){
			if(arr[i]>0){
				num = i;	
			}	
		}
		return num;	
	}
	
	function getPoint(arr){
		var num = 0;
		for(var i = 0; i<arr.length; i++){
			var str = (arr[i]*testMoney/100).toString();
			if(str.indexOf('.') >= 0){
				num += Number('0.' + str.split('.')[1]);
			}
		}	
		return num;
	}
	
	function getIntMoney(i,arr){
		var moneyNum = Math.floor(testMoney*testData[i] / 100);
		return i == delPointNum(arr) ? moneyNum+parseInt(getPoint(arr),10) : moneyNum;
	}
});