$(function(){
	InitUploadAvatar();
	//头像上传流程
	function InitUploadAvatar(){
		if($(".uploadbox").length>0){
			var fileTemplate = "<div class=\"fbox\" id=\"{{id}}\">";
			//fileTemplate += "<div class=\"progressbar\"><span>0%</span></div>";
			//fileTemplate += "<div class=\"preview\"></div>";
			fileTemplate += "<div class=\"filename\"></div>";
			fileTemplate += "</div>";

			function slugify(text) {
				text = text.replace(/[^-a-zA-Z0-9,&\s]+/ig, '');
				text = text.replace(/-/gi, "_");
				text = text.replace(/\s/gi, "-");
				return text
			}

			function getImg(newname){
				var img = $("#"+newname).find("img");
				var iw = img.width(),
					ih = img.height();
				return {w:iw,h:ih}
			}
			uploading = 0;//0表示未上传或上传完毕，1表示正在上传中
			
			$(".uploadbox , .file_uploadbox").html5Uploader({
				name: 'userfile',
				postUrl: '',
				width:100,
				height:100,
				onClientAbort: function(){
					alert("上传中断")
				},
				onClientLoadStart: function(e, file,newname) {
					var upload = $("#upload");
					if (upload.is(":hidden")) {
						upload.show()
					};
					//$(".upload-area").hide();//隐藏上传区域
					//$(".upload").hide();
					if(uploading == 0){
						var d = fileTemplate.replace(/{{id}}/g, slugify(newname));
						upload.append(d);
						$('.Js_uploadbox').css('background','#cf6e6e').text('头像上传中 0%').show().prev().hide();
						click_ = true;
						uploading = 1;
					}
				},
				onClientLoad: function(e, file,newname) {
					var i = newname.charAt(newname.length-1);
					$("#" + slugify(newname)).find(".preview").append("<img src=\"" + e.target.result + "\" alt=\"\">");
					if(getImg(slugify(newname)).w<250||getImg(slugify(newname)).h<141){
						return false;
					}else{
						$("#" + slugify(newname)).find(".progressbar").height(getImg(slugify(newname)).h);
					}
				},
				onServerLoadStart: function(e, file,newname) {
					$("#upload").children().not("#" + slugify(newname)).remove();
					$("#" + slugify(newname)).find(".progressbar").height(getImg(slugify(newname)).h);
				},
				onServerProgress: function(e, file,newname) {
					if (e.lengthComputable) {
						//var percentComplete = e.loaded / e.total;
						var precentCompleteNum = parseInt((parseInt(e.loaded) / parseInt(e.total))*100);
						$('.Js_uploadbox').text('头像上传中 '+precentCompleteNum+'%');
					}
				},
				onServerLoad: function(e, file,newname) {
				},
				onSuccess:function(e, file,newname,data){
					$("#" + slugify(newname)).find(".progressbar").hide();
					var data = eval ('('+ data +')')
					var status = data.status;
					
					if(status == '0'){
						switch (data.ext){
							case 'doc':
								
								break
							case 'pdf':

								break
							default:
								var path = data.content;
								//var url = data.url;
								//$('.fbox').not(':last').remove();
								//$('.fbox').find('img').attr('src',url)
								var img = new Image();
								img.onload = function(){
									console.log(base_url);
									$('.Js_userfile_box').show();
									$('.Js_file_loading').hide();
									var tHeight = this.height>=317?317:this.height
									var sTop = (tHeight - 100) / 2;
									var chtml = '<div class="tit">请选取头像显示区域</div>'
											+'	<div class="clearfix">'
											+'   <div class="Js_uploadArea j-crop">'
											+'		<img id="faceImg" src="'+this.src+'" alt="" style="z-index:10" />'
											+'</div>'
											+'<div class="Js_facepreview w100 simg" style="margin-top:'+sTop+'px"><img src="" alt=""></div>'
											+'</div>'
											+'<div style="height:25px;"></div>'
											+'<div class="p-warp"><a href="#" class="btn upfile h8 mr10 Js_reUpload">取消</a><a href="#" class="btn upfile b73 Js_okUpload">确认</a></div><div class="height_8"></div>'
									
									$("#upload").find('.upload_success_cover').remove();
									$("#" + slugify(newname)).append('<input name="cover" class="upload_success_cover" type="hidden" value="'+path+'" />');
									
									$(".Js_user_head").append(chtml);
									$(".Js_facepreview.w100").html('<img src="'+base_url+path+'" id="preview100" alt="Preview" class="jcrop-preview" />');
									$("#file_path").val(path);
									$(".upload").hide();
									initJcrop();
								}
								
								img.src = base_url+path;
								console.log(img.src);
								
						}
						click_ = false;
						uploading = 0;
					}
				}
			});
			$(".uploadbox").click(function(){
				$(".file_uploadbox").click()
			});
		};
	};

	$(".Js_reUpload").live("click",function(){
		//$.fancybox.close();
		//$(".Js_avatar").click();
		$('.Js_uploadbox').text('头像上传中 0%').hide().prev().show();
		$('.Js_userfile_box').hide().find('.Js_user_head').children().remove();
	});

	$(".Js_okUpload").live("click",function(){
		if (parseInt($('#w').val())){
			var data ={file_name:$("#file_path").val(),start_x:$("#x").val(),start_y:$("#y").val(),end_x:(Number($("#x").val())+Number($("#w").val())),end_y:(Number($("#y").val())+Number($("#h").val()))}
			//ajax_(resize_user_avatar_url,data,resize_user_avatar_c);这个是以前的ajax提交函数，现在的用request函数
			//data提交的是选取图片的4点坐标
			//ajax函数
			alert($("#file_path").val())
			alert($("#x").val())
			alert($("#y").val())
			alert((Number($("#x").val())+Number($("#w").val())))
			alert((Number($("#y").val())+Number($("#h").val())))
			return true;
		}else{
			alert('请选择头像区域');
			return false;
		} 
	});

	function resize_user_avatar_c(data1){
		var data={avatar:data1.fileName}
		$(".user-pic img").attr("src",base_url+data1.size_100_100);
		ajax_(modify_user_avatar_url,data,modify_user_avatar_c);
	}

	function modify_user_avatar_c(data2){
		$.fancybox.close();
		//window.location.reload();
	}

	//选择头像区域
	function initJcrop(){
		$(".ava-title .ava1").text("裁剪新头像").width(310);
		$(".ava-title .ava2").text("预览新头像");
		$(".Js_avaUploadBtnArea").show();


		var jcrop_api, boundx, boundy;
		
		$('#faceImg').Jcrop({
			onChange: updatePreview,
			onSelect: updatePreview,
			setSelect: [ 0, 0, 100, 100 ],
			aspectRatio: 1
		},function(){
			// Use the API to get the real image size
			var bounds = this.getBounds();
			boundx = bounds[0];
			boundy = bounds[1];
			// Store the API in the jcrop_api variable
			jcrop_api = this;
			updatePreview({h:100,w:100,x:0,x2:100,y:0,y2:100});
			//this.release()
		});

		function updatePreview(c)
		{
			if (parseInt(c.w) > 0)
			{
				var rx2 = 100 / c.w;
				var ry2 = 100 / c.h;

				$('#preview100').css({
					width: Math.round(rx2 * boundx) + 'px',
					height: Math.round(ry2 * boundy) + 'px',
					marginLeft: '-' + Math.round(rx2 * c.x) + 'px',
					marginTop: '-' + Math.round(ry2 * c.y) + 'px'
				});
			};

			$('#x').val(c.x);
			$('#y').val(c.y);
			$('#w').val(c.w);
			$('#h').val(c.h);

		};
	};
})