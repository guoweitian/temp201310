$(function(){
	$(".Js_s_one").click(function(){
		$("#tab-t .two a").click();
		scrollTopFn($(this))
	});

	$(".Js_s_two_pre,.Js_goto_one").live("click",function(){
		$("#tab-t .one a").click()
		scrollTopFn($(this))
	});

	$(".Js_s_two_next,.Js_goto_three").live("click",function(){
		$("#tab-t .three a").click()
		scrollTopFn($(this))
	});

	$(".Js_s_three_pre,.Js_goto_two").live("click",function(){
		$("#tab-t .two a").click()
		scrollTopFn($(this))
	});

	$(".Js_s_three_next,.Js_goto_two").live("click",function(){
		$("#tab-t .four a").click()
		scrollTopFn($(this))
	});

	$(".Js_s_four_pre").click(function(){
		$("#tab-t .three a").click()
		scrollTopFn($(this))
	});


	function scrollTopFn(obj){
		if(obj.parents(".js_detailForm").length>0){/*blocks/project-going.php里是js_detailForm，而不是Js_detailForm*/
			$(document).scrollTop(470);//470
		}
	}
}) 


function tabs(id, tab, m, trigger) {
	//var tabsBtn = document.getElementById(id).getElementsByTagName('ul')[0].getElementsByTagName('a');
	var tabsBtn = $("#"+id).find("#"+tab).find("a");
	//var tabsContent = document.getElementById(id).document.getElementById("").getElementsByTagName('p');
	var tabsContent = $("#"+id).find(".tabs-c");
	tabsContent.hide();
	tabsContent.eq(m-1).show();

	for (var i = 0, len = tabsBtn.length; i < len; i++) {
		tabsBtn[i].index = i;
		if (trigger == 'click') {
			tabsBtn.click(function () {
				clearClass();
				$(this).addClass('selected');
				$(this).parent().removeClass("ok").removeClass("notFill").addClass("current");
				$(this).parent().prevAll("li").removeClass("ok").removeClass("notFill").removeClass("current").addClass("ok");
				$(this).parent().nextAll("li").removeClass("ok").removeClass("current").addClass("notFill");
				showContent(this.index);
			})
		} else if (trigger == 'mouseover') {
			tabsBtn[i].onmouseover = function () {
				clearClass();
				this.className = 'selected';
				showContent(this.index);
			}
		}
	}

	function showContent(n) {
		tabsContent.hide();
		tabsContent.eq(n).show()
	}

	function clearClass() {
		for (var i = 0, len = tabsBtn.length; i < len; i++) {
			tabsBtn[i].className = '';
		}
	}
	//判断当前的step
	getPublishStep()
}

/*
获取发布框当前步骤
 */
function getPublishStep(){
	var url = window.location.href;
	var theRequest;
	var x = $(".Js_detailPublish");
	if (url.indexOf("#") != -1) {
		var str = url.substr(1);
		strs = str.split("#");
		theRequest = strs[strs.length-1]
	};
	switch(theRequest){
	case "step-one":
		$(".form-wrap").show();
		$("#tab-t .one a").click();
		break;
	case "step-two":
		$(".form-wrap").show();
		$("#tab-t .two a").click();
		break;
	case "step-three":
		$(".form-wrap").show();
		$("#tab-t .three a").click();
		break;
	case "step-four":
		$(".form-wrap").show();
		$("#tab-t .four a").click();
		break;
	default:
		$("#tab-t .one a").click();
		break;
	}
}