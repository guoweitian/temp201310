$(function() {
	/*
	   明星比赛添加奖项
	*/
    function addCommas(nStr){
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    if($('.Js_add_newawards').length>0){
        $('.Js_add_newawards').click(function(){
            if($('.Js_addawards_form').is(':hidden')){
                $('.Js_addawards_form').show();
            }else{
                $('.Js_addawards_form').hide();
            }
        })
    }

    var mxzb_index = null;
    $('.Js_add_awards_ok').click(function(){
       // alert(strAdd)
        var step = '';
        if($('.Js_add_ngawards').val()==''){
            $('.Js_add_ngawards').parents('.tools').next().show();
            step = 'one';
        }else{
            $('.Js_add_ngawards').parents('.tools').next().hide();
        }

        if($('.Js_add_ngawardsnum').val()==0){
            $('.Js_add_ngawardsnum').parents('.tools').next().show();
            step = 'one';
        }else{
            $('.Js_add_ngawardsnum').parents('.tools').next().hide();
        }
        if(step=='one') return;
        if($('.Js_add_awards_ok').text()=='确认添加'){
            var chtml = '<div class="n-bank-list Js_n_awards_list">'
                +'		<div class="clearfix">'
                +'			<span style="float:left;" city_id="">头衔：<span class="Js_ngawardsname">'+$('.Js_add_ngawards').val()+'</span></span>'
                +'			<a href="javascript:;" class="n-del Js_add_awardsdel">删除</a>'
                +'			<a href="javascript:;" class="n-xg Js_addawards_xg">修改</a>'
                +'		</div>'
                +'		<div class="clearfix mt15">'
                +'			<div style="float:left;" b_ndsbb="">奖金：<span class="Js_awardsnum"  style="font-family: arial, sans-serif;font-size:14px; font-weight:bold; margin-right: 4px">'+$('.Js_add_ngawardsnum').val().replace(/\b(0+)/gi, "")+'</span>元</div>'
                +'		</div>'
                +'	</div>'
            $('.Js_add_awardslist').before(chtml).show();
            $('.Js_add_awards_btn,.Js_addawards_form').hide();
            $('#Js_title_num').text($('.Js_n_awards_list').length);
            var r_num = strAdd($('#Js_awards_money').attr('target_money_num'),$('.Js_add_ngawardsnum').val().replace(/\b(0+)/gi, ""))
            var f_num = addCommas(r_num);
            $('#Js_awards_money').text(f_num).attr('target_money_num',r_num);
        }else if($('.Js_add_awards_ok').text()=='确认修改'){
            $('.Js_ngawardsname').eq(mxzb_index).text($('.Js_add_ngawards').val());
            $('.Js_awardsnum').eq(mxzb_index).text($('.Js_add_ngawardsnum').val().replace(/\b(0+)/gi, ""));
            $('.Js_add_awards_cancle').click();
            var update_num = '0';
            $('.Js_awardsnum').each(function(){
                update_num = strAdd(update_num,$(this).text());
            })
            var f_num = addCommas(update_num);
            $('#Js_awards_money').text(f_num).attr('target_money_num',update_num);
        }
    })

    $('.Js_add_awardslist').find('a').click(function(){
        $('.Js_add_awards_cancle').click();
        $('.Js_add_newawards').click();
    })

    $('.Js_add_awards_cancle').click(function(){
        $(this).parents('.Js_addawards_form').hide().find('input').each(function(){
            $(this).val('');
        });
        $('.Js_add_awards_cancle').text('取消添加');
        $('.Js_add_awards_ok').text('确认添加');
    })

    $('.Js_add_awardsdel').live('click',function(){
        if($('.Js_add_awardsdel').index($(this))==mxzb_index){
            $('.Js_add_awardslist').find('a').click();
        }else if($('.Js_add_awardsdel').index($(this))<mxzb_index){
            mxzb_index--;
        }
        $(this).parents('.Js_n_awards_list').remove();
        if($('.Js_n_awards_list').length==0){
            $('.Js_add_awards_cancle').click();
            $('.Js_add_awardslist').hide();
            $('.Js_add_awards_btn').show();
        }
        $('#Js_title_num').text($('.Js_n_awards_list').length);
        //var r_num = subtract($('#Js_awards_money').attr('target_money_num'),$(this).parent().next().find('.Js_awardsnum').text());
        //var r_num = parseInt(,10)-parseInt($(this).parent().next().find('.Js_awardsnum').text(),10)
        var update_num = '0';
        $('.Js_awardsnum').each(function(){
            update_num = strAdd(update_num,$(this).text());
        })
        var f_num = addCommas(update_num);
        $('#Js_awards_money').text(f_num).attr('target_money_num',update_num);
    })

    $('.Js_addawards_xg').live('click',function(){
        $('.Js_add_ngawards').val($(this).parent().find('.Js_ngawardsname').text());
        $('.Js_add_ngawardsnum').val($(this).parent().next().find('.Js_awardsnum').text());
        $('.Js_addawards_form').show();
        $('.Js_add_awards_cancle').text('取消修改');
        $('.Js_add_awards_ok').text('确认修改');
        mxzb_index = $('.Js_addawards_xg').index($(this));
    })
	/*
	删除邀请了的专家
	 */
	$(".Js_exp_del").live("click",function(){
		$(this).parent().remove();
		return false
	});

	
	$('.Js_ccbInput').keyup(function(){
		if($(this).parents('.tools').next().length>0)
			$(this).parents('.tools').next().text($(this).val()/100+'元');
	}).click(function(){
		$(this).parents('.ft-style').find('.js_radio').eq(3).mousedown();	
	})
	
	$('.Js_input_text').keydown(function(e){
		if(e.keyCode<48 || (e.keyCode>57&&e.keyCode<96) || e.keyCode > 105){
			if(e.keyCode!=8)
					return false;		
		}
	})
	
	/*$('.Js_cityList').click(function(){
		var obj = [{address:'中国四川省成都市'},{address:'中国四川省绵阳市'},{address:'中国四川省南充市'}]
		var o = $(this).find('input');
		var _this = $(this);
		if(o.next().is(':visible')) return false;
		o.focus().next().show();
		$(this).find('s').hide().next().css('margin-left','15px');
		o.next().css('left','-13px');
		o.next().find('a').unbind('click').click(function(){
			if($(this).text()=="获取当前地理位置"){
				if(navigator.geolocation){
					navigator.geolocation.getCurrentPosition(function(position){
						var coords = position.coords;
						//var latlng = new google.maps.LatLng(30.657358499999994, 104.049977);
						var latlng = new google.maps.LatLng(coords.latitude, coords.longitude);
						var geocoder = new google.maps.Geocoder();
						geocoder.geocode({'latLng': latlng}, function(results, status) {
							if (status == google.maps.GeocoderStatus.OK) {
								if (results[1]) {
									_this.find('s').show().next().css('margin-left','55px');
									o.next().css('left','-43px');
									o.val(results[1].formatted_address).next().hide();
								}
							} else {
								alert("Geocoder failed due to: " + status);
							}
						});
					},function(error){
						var errorTypes = {
							1 : '位置服务被拒绝',
							2 : '获取不到的位置信息',
							3 : '获取信息超时'	
						}
						_this.find('s').show().next().css('margin-left','55px');
						o.next().css('left','-43px');
						o.val("").next().hide();
						alert(errorTypes[error.code] + "不能确定你的当前地理位置。");
					});
				}else{
					alert('浏览器不支持该功能');	
				}
			}else{
				_this.find('s').show().next().css('margin-left','55px');
				o.next().css('left','-43px');
				o.val($(this).text()).next().hide();
			}
			return false;	
		})
		o.keyup(function(){
			o.focus().next().show();
			o.next().find('ul').remove();
			if(o.val()){
				var chtml = $('<ul></ul>');
				for(var i=0; i<obj.length; i++){
					var item = $('<li><a href="javascript:void(0)">'+obj[i].address+'</a></li>');
					chtml.append(item);
				}
				o.next().append(chtml).find('a').unbind('click').click(function(){
					_this.find('s').show().next().css('margin-left','55px');
					o.next().css('left','-43px');
					o.val($(this).text()).next().hide();
					return false;
				});
				$(document).unbind('click',pClick).one('click',pClick)
			}
		});
		
		$(document).one('click',pClick);
		
		function pClick(){
			_this.find('s').show().next().css('margin-left','55px');
			o.next().css('left','-43px');
			o.next().hide();	
		}
		
		if($(".bbit-dp").length>0){
			$(".bbit-dp").css({"visibility":"hidden","display":"none"})
		}
		return false;
	});*/
	
	$('.Js_expertList').live('click',function(){
        var obj = [{img:'assets/temp/2.png',name:'您好成都',classType:'tUserName'},{img:'assets/temp/2.png',name:'您好成都',classType:'tUserName'},{img:'assets/temp/2.png',name:'马双',type:'朋友',classType:'circleName'},{img:'assets/temp/2.png',name:'刘兆宇',type:'成都电子科技大学',classType:'circleName'}]
		var o = $(this).find('input');
		if(o.next().is(':visible')) return false;
		o.focus().next().show();
		if(!o.val()){o.next().find('ul').remove()}
		o.keyup(function(){
			$('.Js_expertList').find('.Js_search_txt').text($(this).val())
			o.focus().next().show();
			o.next().find('ul').remove();
			if(!o.val()){return false}
			var chtml = $('<ul></ul>');
			for(var i=0; i<obj.length; i++){
				var bspw = (i==0) ? '<i class="bspw">比赛评委</i>' : '';
				var uType = obj[i].type ? '<p class="Js_w_type '+(obj[i].type=='circleName'?'circleType':'mType')+'">'+obj[i].type+'</p>' : '';
				var item = $('<li class="clearfix"><div class="expertListDiv"><a href="javascript:void(0)"><img src="'+obj[i].img+'" /></a><div class="rightArea"><p class="Js_w_name '+obj[i].classType+'">'+obj[i].name+bspw+'</p>'+uType+'</div></div></li>');
				chtml.append(item);
			}
			o.next().append(chtml).find('li').unbind('click').click(function(){
			 	var pdata = '			<li class="clearfix">'
							+'				<a href="#" class="leftArea"><img src="assets/temp/5.png" alt=""></a>'
							+'				<div>'
							+'					<p class="rightName">'+$(this).find('.Js_w_name').text()+'</p>'
							+'					<p class="rightDis"><a href="#">'+$(this).find('.Js_w_type').text()+'</a></p>'
							+'				</div>'
							+'				<span class="expert-del Js_exp_del"></span>'
							+'			</li>';

			 	$(".Js_invitedexplist").append(pdata)
				o.next().hide();
				return false;
			});
		});
		$(document).unbind('click',hidelist).bind('click',hidelist)
		function hidelist(){
			o.next().hide();
		}
		return false;
	});
	$('.Js_recommend_expert').live('click',function(){
		var index = 0;
		var chtml = '<form name="experts_recommend"><div class="boxDivParent"><div class="boxDiv">'
			chtml	+= '<h2 class="mt0">推荐站外专家</h2>'
			chtml	+='<div class="warp clearfix"><p class="mc">推荐专家名称</p><div class="tools text left"><div class="inputWarp empty"><input type="text" id="Js_zj_name" name="expertint"></div></div><div class="errorMsg left" style="display:none;">错误提示内容</div></div>'
			chtml	+='<div class="warp clearfix"><p class="mc">推荐专家简介</p><div id="tzzj_toolbar" class="toolbar-wrap" style="width: 536px;"></div><div class="height_15"></div><div class="toolbar-content left"><textarea id="tzzj_content" name="content" style="display:none"></textarea></div><div class="errorMsg left" style="display:none;">错误提示内容</div></div>'
			chtml	+='<div class="warp clearfix"><p class="mc">选择专家行业</p><div class="optDiv Js_experts_trade left"><div class="tools text pr"><div class="inputWarp opt"><span id="Js_nominate_con">请您选择推荐专家行业</span><a href="javascript:void(0)" class="prev"></a><a href="javascript:void(0)" class="next"></a></div></div><ul><li><a href="javascript:void(0)">领导人</a></li><li><a href="javascript:void(0)">科学家</a></li></ul><select style="display:none;" name="sel_type"><option>领导人</option><option>科学家</option></select></div><div class="errorMsg left" style="display:none;">错误提示内容</div></div>'
			chtml	+='<div class="warp clearfix"><p class="mc">推荐专家的邮箱地址</p><div class="tools text left"><div class="inputWarp empty"><input type="text" id="Js_nominate_addr" name="experts_email"></div></div><div class="errorMsg left" style="display:none;">错误提示内容</div></div>'
			chtml	+='<div class="warp clearfix"><p class="mc">推荐专家的电话号码</p><div class="tools text left"><div class="inputWarp empty"><input type="text" id="Js_nominate_phone" name="experts_phone"></div></div><div class="errorMsg left" style="display:none;">错误提示内容</div></div>'
			chtml	+='<div class="warp clearfix"><p class="mc">推荐专家的联系方式</p><div class="tools text left"><div class="inputWarp empty"><input type="text" id="Js_nominate_contact" name="experts_style"></div></div><div class="errorMsg left" style="display:none;">错误提示内容</div></div>'
			chtml	+='<div class="warp"><a href="javascript:void(0)" class="grayBtn Js_cencle_fancybox">取消推荐</a><a href="javascript:;" class="blueBtn ml20 Js_nominate_ok">确认推荐</a></div>'
            chtml	+='</div></div></form>';
		$.fancybox({
			fitToView	: false,
			autoHeight	: true,
			autoSize	: false,
			width		: 930,
			content    : chtml,
			padding    : 0,
			beforeShow 	: function () {
				$('.Js_experts_trade').click(function(){
					$(this).find('ul').show();
					$(document).one('click',function(){
						$('.Js_experts_trade ul').hide();
					})
					return false;
				});
				
				$('.Js_experts_trade ul li').click(function(){
					var n = $('.Js_experts_trade ul li').index($(this));
                    index = n;
					$(this).parent().next().children().eq(n)[0].selected = true;
					$('.Js_experts_trade').find('span').text($(this).text());
					$(this).parent().hide();
					return false;
				});
				$('.Js_experts_trade .prev').click(function(){
					var lList = $('.Js_experts_trade ul li');
					var txtObj = $('.Js_experts_trade').find('span');
					txtObj.text(lList.eq(index).text());
					$('.Js_experts_trade').find('ul').next().children().eq(index)[0].selected = true;
					index--;
					index = index < 0 ? lList.length-1 : index;
					return false;
				});
				
				$('.Js_experts_trade .next').click(function(){
					var lList = $('.Js_experts_trade ul li');
					var txtObj = $('.Js_experts_trade').find('span')
					txtObj.text(lList.eq(index).text());
					$('.Js_experts_trade').find('ul').next().children().eq(index)[0].selected = true;
					index++;
					index = index >= lList.length ? 0 : index;
					return false;
				});
				
				$('.Js_nominate_ok').click(function(){
					var step = '',
						zj_name = $('#Js_zj_name').val(),
						tzzj_content = $('#tzzj_content').val(),
						nominate_emailAddr = $('#Js_nominate_addr').val(),
						nominate_phone = $('#Js_nominate_phone').val(),
						nominate_contact = $('#Js_nominate_contact').val();
					if(zj_name==''){
						$('#Js_zj_name').parents('.tools').addClass('error').next().show();
						step = 'one';	
					}else{
						$('#Js_zj_name').parents('.tools').removeClass('error').next().hide();
					}
					
					if(tzzj_content==""){
						$('#tzzj_content').parents('.toolbar-content').next().show();
						$('#tzzj_content').parents('.toolbar-content').find('.redactor_box').addClass('error');
						step = 'one';
					}else{
						$('#tzzj_content').parents('.toolbar-content').next().hide();
						$('#tzzj_content').parents('.toolbar-content').find('.redactor_box').removeClass('error');
					}
					
					if($('#Js_nominate_con').text()=='请您选择推荐专家行业'){
						$('#Js_nominate_con').parents('.Js_experts_trade ').next().show();
						$('#Js_nominate_con').parents('.tools').addClass('error');
						step = 'one';
					}else{
						$('#Js_nominate_con').parents('.Js_experts_trade ').next().hide();
						$('#Js_nominate_con').parents('.tools').removeClass('error');	
					}	
					
					if(!checkTextForm.email(nominate_emailAddr)){
						$('#Js_nominate_addr').parents('.tools').addClass('error').next().show();
						step = 'one';	
					}else{
						$('#Js_nominate_addr').parents('.tools').removeClass('error').next().hide();
					}
					
					if(!(checkTextForm.cellphone(nominate_phone) || checkTextForm.telphone(nominate_phone))){
						$('#Js_nominate_phone').parents('.tools').addClass('error').next().show();
						step = 'one';	
					}else{
						$('#Js_nominate_phone').parents('.tools').removeClass('error').next().hide();
					}
					
					if(nominate_contact==''){
						$('#Js_nominate_contact').parents('.tools').addClass('error').next().show();
						step = 'one';	
					}else{
						$('#Js_nominate_contact').parents('.tools').removeClass('error').next().hide();
					}
					
					if(step==''){
						$.ajax({
							url:'test.php',
							dataType : 'json',
							type : 'post',
							success : function(){
								$('.Js_cencle_fancybox').click();
							}
						})
					}
					
				})
				
				$('.Js_cencle_fancybox').click(function(){
					$.fancybox.close();
				});
                $('.Js_tj_ok').click(function(){
                    $.fancybox.close();
                })
				$('#tzzj_content').redactor({ 
					imageUpload: 'editor_post_pic.php', 
					lang: 'zh_cn' , 
					buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' , 'video' , 'link'], 
					toolbarExternal: '#tzzj_toolbar'
				});	
			}
		})	
	})
});


