function citySelect(_province,_city,_county,fn){
	var city_select = {
		id : '1001', //最终选择的id 
		
		init : function(){//初始化数据
			var self = this;
			self.province = $('#'+_province);
			self.city = $('#'+_city);
			self.county = $('#'+_county);
			self.data = o;
			self.yourSelf = fn;
			self.toSelectData();
			/*$.getJSON("js/city.min.js",function(json){
				self.province = $('#'+_province);
				self.city = $('#'+_city);
				self.county = $('#'+_county);
				self.data = json;
				self.yourSelf = fn;
				self.toSelectData();
			});*/
		},
		
		toSelectData : function(){//向select中添加json数据
			var self = this;
			this.cityArr = [parseInt(this.id.substr(0,2),10),parseInt(this.id.substr(2,2),10),parseInt(this.id.substr(4,2),10)];
			this.province.unbind('change').html('');
			this.city.unbind('change').html('');
			this.county.unbind('change').html('');
			this.province.prev().children().remove();
			this.city.prev().children().remove();
			this.county.prev().children().remove();
			for(var i=0; i<this.data.citylist.length; i++){
				var po = this.data.citylist[i];
				this.province.append('<option>'+po.p[0]+'</option>');
				var o = $('<li><a href="javascript:void(0)">'+po.p[0]+'</a></li>');
				o.click(function(){
					var num = $(this).parent().find('li').index($(this));
					$(this).parent().next().children().eq(num).attr('selected','selected');
					self.runSelectEvent($(this).parent().next());
					self.province.prev().prev().find('span').text($(this).text());
					self.province.prev().hide();
					return false;
				})
				this.province.prev().append(o);
				this.province.prev().prev().find('span').text(this.data.citylist[this.cityArr[0]-10].p[0]);
			}
			this.province.children().eq(this.cityArr[0]-10).attr('selected',true);
			for(var j=0; j<this.data.citylist[this.cityArr[0]-10].c.length; j++){
				var co = this.data.citylist[this.cityArr[0]-10].c[j];
				this.city.append('<option>'+co.n[0]+'</option>');
				var o = $('<li><a href="javascript:void(0)">'+co.n[0]+'</a></li>');
				o.click(function(){
					var num = $(this).parent().find('li').index($(this));
					$(this).parent().next().children().eq(num).attr('selected','selected');
					self.runSelectEvent($(this).parent().next());
					self.city.prev().prev().find('span').text($(this).text());
					self.city.prev().hide();
					return false;	
				})
				this.city.prev().append(o);
				this.city.prev().prev().find('span').text(this.data.citylist[this.cityArr[0]-10].c[this.cityArr[1]-1].n[0]);
			}
			this.city.children().eq(this.cityArr[1]-1).attr('selected',true);
			if(this.county.length==0) return;
			if(this.data.citylist[this.cityArr[0]-10].c[this.cityArr[1]-1].a){
				for(var k=0; k<this.data.citylist[this.cityArr[0]-10].c[this.cityArr[1]-1].a.length; k++){
					var cio = this.data.citylist[this.cityArr[0]-10].c[this.cityArr[1]-1].a[k];
					this.county.append('<option>'+cio.s[0]+'</option>');
					var o = $('<li><a href="javascript:void(0)">'+cio.s[0]+'</a></li>');
					o.click(function(){
						var num = $(this).parent().find('li').index($(this));
						$(this).parent().next().children().eq(num).attr('selected','selected');
						self.runSelectEvent($(this).parent().next());
						self.county.prev().prev().find('span').text($(this).text());
						self.county.prev().hide();
						return false;	
					})
					this.county.prev().append(o);
					this.county.prev().prev().find('span').text(this.data.citylist[this.cityArr[0]-10].c[this.cityArr[1]-1].a[0].s[0]);
				}
				this.county.children().eq(this.cityArr[2]-1).attr('selected',true);
				//this.county.show();
				this.county.prev().prev().show();
			}else{
				//this.county.hide();	
				this.county.prev().prev().hide();
			}
			this.addSelectEvent();
		},
		
		addSelectEvent : function(){//绑定select的change事件
			var self = this;
			this.province.change(function(){self.runSelectEvent($(this))});
			this.city.change(function(){self.runSelectEvent($(this))});
			this.county.change(function(){self.runSelectEvent($(this))});
		},
		
		runSelectEvent : function(my){//事件触发
			//alert(my.find("option").length)
			this.callbackEvent = {};
			if(this.province.children().index(my.find("option:selected"))>=0){
				var index = this.province.children().index(my.find("option:selected"));
				var pro = index+10;
				this.callbackEvent['province'] = this.data.citylist[index].p[0];
				this.callbackEvent['city'] = this.data.citylist[index].c[0].n[0];
				if(this.data.citylist[index].c[0].a){
					this.callbackEvent['county'] = this.data.citylist[index].c[0].a[0].s[0];	
				}
				this.id = pro.toString() + '0101';
			}
			if(this.city.children().index(my.find("option:selected"))>=0){
				var index = this.city.children().index(my.find("option:selected"));
				var city = index+1;
				this.callbackEvent['province'] = this.data.citylist[this.cityArr[0]-10].p[0];
				this.callbackEvent['city'] = this.data.citylist[this.cityArr[0]-10].c[index].n[0];
				if(this.data.citylist[this.cityArr[0]-10].c[0].a){
					this.callbackEvent['county'] = this.data.citylist[this.cityArr[0]-10].c[index].a[0].s[0];	
				}
				this.id = this.checkStrFormat(this.cityArr[0]) + this.checkStrFormat(city) + '01';
			}
			if(this.county.children().index(my.find("option:selected"))>=0){
				var index = this.county.children().index(my.find("option:selected"));
				var county = index+1;
				this.callbackEvent['province'] = this.data.citylist[this.cityArr[0]-10].p[0];
				this.callbackEvent['city'] = this.data.citylist[this.cityArr[0]-10].c[this.cityArr[1]-1].n[0];
				if(this.data.citylist[this.cityArr[0]-10].c[0].a){
					this.callbackEvent['county'] = this.data.citylist[this.cityArr[0]-10].c[this.cityArr[1]-1].a[index].s[0];	
				}
				this.id = this.checkStrFormat(this.cityArr[0]) + this.checkStrFormat(this.cityArr[1]) + this.checkStrFormat(county);
			}
			if(this.yourSelf){
				var self = this;
				var ev = {cityName:self.callbackEvent,cityId:self.id};
				this.yourSelf(ev);
			}
			this.toSelectData();
		},
		
		checkStrFormat : function(num){//判断字符串小于10则前面添0
			if(num<10){
				return '0' + num.toString();	
			}else{
				return num.toString();	
			}
		}
	}
	return city_select;
}

var search_input = {
	init : function(obj){
		this.searchInput = obj.find('input');
		this.searchResult = obj.find('ul');
		this.clearContent();
		this.registerEvent();
	},
	
	registerEvent : function(){
		var self = this;
		self.searchResult.show().css({'top':self.searchInput.outerHeight(),'width':self.searchInput.innerWidth()});
		if(!self.searchResult.is(':hidden')){
			$(document).one('click',function(){
				self.searchResult.hide();
			})
		}
		
	},
	
	addContent : function(html,enter,out){
		var self = this;
		this.searchResult.append(html);
		this.searchResult.children().mouseenter(function(){
			$(this).addClass(enter);	
		}).mouseout(function(){
			$(this).removeClass(enter);	
		}).click(function(){
			self.searchInput.val(self.searchResult.find(out).html());
		})
	},
	
	setStyle : function(name){
		this.searchResult.children().attr('class',name);
	},
	
	clearContent : function(){
		this.searchResult.children().remove();
	}
}
 