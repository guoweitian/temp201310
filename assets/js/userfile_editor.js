﻿$(function(){
	$('.Js_user_intro').click(function(){
		var val = $('.Js_userfile_ok').text();
		$('.Js_userfile_txt').show().find('textarea').val(val);
		$('.Js_userfile_ok').hide();
	});
	
	$('.Js_userfile_cancle').click(function(){
		$('.Js_userfile_txt').hide();
		$('.Js_userfile_ok').show();
	});
	
	$('.Js_userfile_update').click(function(){
		var data = {"val":$('.Js_userfile_txt').find('textarea').val()};
		$.ajax({
			url:'test.php',
			dataType : 'json',
			type : 'post',
			data : data,
			success : function(){
				var val = $('.Js_userfile_txt').find('textarea').val();
				$('.Js_userfile_txt').hide();
				$('.Js_userfile_ok').text(val).show();
			}
		})	
	});
	
	$('.Js_add_experience').click(function(){
		$('.Js_file_add').show();	
	});
	
	$('.Js_add_edu').click(function(){
		$('.Js_edu_add').show();	
	});
	
	$('.Js_cancle_add').click(function(){
		$('.Js_file_add').hide();
	});
	
	$('.Js_cancle_edu').click(function(){
		$('.Js_edu_add').hide();
	});
	
	$('.Js_confirm_edu').click(function(){
		var step = '';
		if($('#Js_school_type').text()==''){
			$('#Js_school_type').parents('.tools').addClass('error').parents('.Js_state').next().show();
			step = 'one';	
		}else{
			$('#Js_school_type').parents('.tools').removeClass('error').parents('.Js_state').next().hide();
		}
		
		if($('#Js_school_name').val()==''){
			$('#Js_school_name').parents('.tools').addClass('error').next().show();
			step = 'one';	
		}else{
			$('#Js_school_name').parents('.tools').removeClass('error').next().hide();
		}
		
		if($('#Js_school_yx').val()==''){
			$('#Js_school_yx').parents('.tools').addClass('error').next().show();
			step = 'one';	
		}else{
			$('#Js_school_yx').parents('.tools').removeClass('error').next().hide();
		}
		
		if($('#Js_s_oldYear').text()==''){
			$('#Js_s_oldYear').parents('.tools').addClass('error').parents('.Js_user_school_div').next().show();
			step = 'one';	
		}else{
			$('#Js_s_oldYear').parents('.tools').removeClass('error');	
		}
		
		if($('#Js_s_oldMonth').text()==''){
			$('#Js_s_oldMonth').parents('.tools').addClass('error').parents('.Js_user_school_div').next().show();
			step = 'one';	
		}else{
			$('#Js_s_oldMonth').parents('.tools').removeClass('error');	
		}
		
		if($('#Js_s_newYear').text()==''){
			$('#Js_s_newYear').parents('.tools').addClass('error').parents('.Js_user_school_div').next().show();
			step = 'one';	
		}else{
			$('#Js_s_newYear').parents('.tools').removeClass('error');	
		}
		
		if($('#Js_s_newMonth').text()==''){
			$('#Js_s_newMonth').parents('.tools').addClass('error').parents('.Js_user_school_div').next().show();
			step = 'one';	
		}else{
			$('#Js_s_newMonth').parents('.tools').removeClass('error');	
		}
		
		if($('#Js_s_oldYear').text()&&$('#Js_s_oldMonth').text()&&$('#Js_s_newYear').text()&&$('#Js_s_newMonth').text()){
			$('#Js_s_oldYear').parents('.tools').removeClass('error');
			$('#Js_s_oldMonth').parents('.tools').removeClass('error');
			$('#Js_s_newYear').parents('.tools').removeClass('error');
			$('#Js_s_newMonth').parents('.tools').removeClass('error');
			$('#Js_s_newMonth').parents('.Js_user_school_div').next().hide();	
		}
		
		if(step==''){
        var schoolType = $('#Js_school_type').text();
		var schoolName = $('#Js_school_name').val();
		var schoolYx = $('#Js_school_yx').val();
		var oldYear = $('#Js_s_oldYear').text();
		var oldMonth = $('#Js_s_oldMonth').text();
		var newYear = $('#Js_s_newYear').text();
		var newMonth = $('#Js_s_newMonth').text();
		var chtml = '<div class="pro-wrap">'
				+'		<p class="post clearfix">'
                +'              <span class="left mr20 Js_school_type_list" style="line-height:20px;">'+schoolType+'</span>'
				+'				<span class="left mr40 Js_company_rname">'+schoolName+'</span>'
				+'				<span class="editorBtnList">'
				+'					<a href="javascript:;" class="editor bqh Js_editor_btn">编辑资料</a>'
				+'					<a href="javascript:;" class="editor bfs Js_file_del">删除</a>'
				+'					<a href="javascript:;" class="editor bfs Js_del_ok" style="display:none;">确认删除</a>'
				+'					<a href="javascript:;" class="editor bshy Js_file_cancel" style="display:none;">取消删除</a>'
				+'				</span>'
				+'			</p>'
				+'		<p class="company Js_job_rname">'+schoolYx+'</p>'
				+'		<p class="time-area"><span class="Js_roldYear">'+oldYear+'</span><span class="Js_roldMonth">'+oldMonth+'</span><span class="spacing">-</span><span class="Js_rnewYear">'+newYear+'</span><span class="Js_rnewMonth">'+newMonth+'</span></p>'
				+'		<div class="Js_editor_file" style="display:none;">'
				+'				<div class="height_22"></div>'
				+'				<div class="rowblock clearfix">'
				+'					<div class="pt">学校类型</div>'
				+'					<div class="optDiv Js_state left" style="width:108px;">'
				+'						<div class="tools text pr" style="width:auto">'
				+'							<div class="inputWarp opt">'
				+'								<span></span>'
				+'								<a href="javascript:void(0)" class="showMenu"></a>'
				+'							</div>'
				+'						</div>'
				+'						<ul>'
				+'							<li><a href="javascript:void(0)">大学</a></li>'
				+'							<li><a href="javascript:void(0)">中学</a></li>'
				+'							<li><a href="javascript:void(0)">小学</a></li>'
				+'						</ul>'
				+'						<select style="display:none;">'
				+'							<option>类型一</option>'
				+'							<option>类型二</option>'
				+'						</select>'
				+'					</div>'
				+'				</div>'
				+'				<div class="height_18"></div>'
				+'				<div class="rowblock clearfix">'
				+'					<div class="pt">学校名称</div>'
				+'					<div class="tools text left">'
				+'						<div class="inputWarp empty">'
				+'							<input type="text" class="Js_company_name">'
				+'						</div>'
				+'					</div>'
				+'				</div>'
				+'				<div class="height_18"></div>'
				+'				<div class="rowblock clearfix">'
				+'					<div class="pt">院系</div>'
				+'					<div class="tools text left">'
				+'						<div class="inputWarp empty">'
				+'							<input type="text" class="Js_job_name">'
				+'						</div>'
				+'					</div>'
				+'				</div>'
				+'				<div class="height_18"></div>'
				+'				<div class="rowblock clearfix">'
				+'					<div class="pt">入学年份</div>'
				+'					<div class="optDiv Js_state left mr10" style="width:148px;">'
				+'						<div class="tools text pr" style="width:auto">'
				+'							<div class="inputWarp opt">'
				+'								<span class="Js_oldYear"></span>'
				+'								<a href="javascript:void(0)" class="showMenu"></a>'
				+'							</div>'
				+'						</div>'
				+'						<ul>'
				+'							<li><a href="javascript:void(0)">2010年</a></li>'
				+'							<li><a href="javascript:void(0)">2011年</a></li>'
				+'						</ul>'
				+'						<select style="display:none;">'
				+'							<option>2010年</option>'
				+'							<option>2011年</option>'
				+'						</select>'
				+'					</div>'
				+'					<div class="optDiv Js_state left" style="width:148px;">'
				+'						<div class="tools text pr" style="width:auto">'
				+'							<div class="inputWarp opt">'
				+'								<span class="Js_oldMonth"></span>'
				+'								<a href="javascript:void(0)" class="showMenu"></a>'
				+'							</div>'
				+'						</div>'
				+'						<ul>'
				+'							<li><a href="javascript:void(0)">1月</a></li>'
				+'							<li><a href="javascript:void(0)">2月</a></li>'
				+'						</ul>'
				+'						<select style="display:none;">'
				+'							<option>1月</option>'
				+'							<option>2月</option>'
				+'						</select>'
				+'					</div>'
				+'					<s class="time-fh"></s>'
				+'					<div class="optDiv Js_state left mr10" style="width:148px;">'
				+'						<div class="tools text pr" style="width:auto">'
				+'							<div class="inputWarp opt">'
				+'								<span class="Js_newYear"></span>'
				+'								<a href="javascript:void(0)" class="showMenu"></a>'
				+'							</div>'
				+'						</div>'
				+'						<ul>'
				+'							<li><a href="javascript:void(0)">2010年</a></li>'
				+'							<li><a href="javascript:void(0)">2011年</a></li>'
				+'						</ul>'
				+'						<select style="display:none;">'
				+'							<option>2010年</option>'
				+'							<option>2011年</option>'
				+'						</select>'
				+'					</div>'
				+'					<div class="optDiv Js_state left" style="width:148px;">'
				+'						<div class="tools text pr" style="width:auto">'
				+'							<div class="inputWarp opt">'
				+'								<span class="Js_newMonth"></span>'
				+'								<a href="javascript:void(0)" class="showMenu"></a>'
				+'							</div>'
				+'						</div>'
				+'						<ul>'
				+'							<li><a href="javascript:void(0)">1月</a></li>'
				+'							<li><a href="javascript:void(0)">2月</a></li>'
				+'						</ul>'
				+'						<select style="display:none;">'
				+'							<option>1月</option>'
				+'							<option>2月</option>'
				+'						</select>'
				+'					</div>'
				+'				</div>'
				+'				<div class="height_50"></div>'
				+'				<div class="p-warp">'
				+'					<a href="javascript:;" class="btn h8 mr5 font14 Js_cancle_revise">取消修改</a>'
				+'					<a href="javascript:;" class="btn b73 font14 Js_ok_revise">确认修改</a>'
				+'				</div>'
				+'			</div>'
				+'		</div>'
				+'		<div class="height_40"></div>'
			$.ajax({
				url:'test.php',
				dataType : 'json',
				type : 'post',
				success : function(){
					$('.Js_edu').append(chtml);	
					$('.Js_cancle_edu').click();
				}
			})
		}
	})
	
	$('.Js_confirm_add').click(function(){
		var step = '';
		if($('#Js_company_name').val()==''){
			$('#Js_company_name').parents('.tools').addClass('error').next().show();
			step = 'one';	
		}else{
			$('#Js_company_name').parents('.tools').removeClass('error').next().hide();
		}
		
		if($('#Js_job_name').val()==''){
			$('#Js_job_name').parents('.tools').addClass('error').next().show();
			step = 'one';	
		}else{
			$('#Js_job_name').parents('.tools').removeClass('error').next().hide();
		}
		
		if($('#Js_work_place').val()==''){
			$('#Js_work_place').parents('.tools').addClass('error').next().show();
			step = 'one';	
		}else{
			$('#Js_work_place').parents('.tools').removeClass('error').next().hide();
		}
		
		if($('#Js_disText').val()==''){
			$('#Js_disText').parents('.tools').addClass('error').next().show();
			step = 'one';	
		}else{
			$('#Js_disText').parents('.tools').removeClass('error').next().hide();
		}
		
		if($('#Js_oldYear').text()==''){
			$('#Js_oldYear').parents('.tools').addClass('error').parents('.Js_user_sel_div').next().show();
			step = 'one';	
		}else{
			$('#Js_oldYear').parents('.tools').removeClass('error');	
		}
		
		if($('#Js_oldMonth').text()==''){
			$('#Js_oldMonth').parents('.tools').addClass('error').parents('.Js_user_sel_div').next().show();
			step = 'one';	
		}else{
			$('#Js_oldMonth').parents('.tools').removeClass('error');	
		}
		
		if($('#Js_newYear').text()==''){
			$('#Js_newYear').parents('.tools').addClass('error').parents('.Js_user_sel_div').next().show();
			step = 'one';	
		}else{
			$('#Js_newYear').parents('.tools').removeClass('error');	
		}
		
		if($('#Js_newMonth').text()==''){
			$('#Js_newMonth').parents('.tools').addClass('error').parents('.Js_user_sel_div').next().show();
			step = 'one';	
		}else{
			$('#Js_newMonth').parents('.tools').removeClass('error');	
		}
		
		if($('#Js_oldYear').text()&&$('#Js_oldMonth').text()&&$('#Js_newYear').text()&&$('#Js_newMonth').text()){
			$('#Js_oldYear').parents('.tools').removeClass('error');
			$('#Js_oldMonth').parents('.tools').removeClass('error');
			$('#Js_newYear').parents('.tools').removeClass('error');
			$('#Js_newMonth').parents('.tools').removeClass('error');
			$('#Js_newMonth').parents('.Js_user_sel_div').next().hide();	
		}
		
		
		if(step==''){
		
		var companyName = $('#Js_company_name').val();
		var jobName = $('#Js_job_name').val();
		var workPlace = $('#Js_work_place').val();
		var oldYear = $('#Js_oldYear').text();
		var oldMonth = $('#Js_oldMonth').text();
		var newYear = $('#Js_newYear').text();
		var newMonth = $('#Js_newMonth').text();
		var disText = $('#Js_disText').val();
		var cYear = (parseInt(newYear,10) - parseInt(oldYear,10))*12;
		var cMonth = parseInt(newMonth,10) - parseInt(oldMonth,10);
		var allMonth = cYear + cMonth;
		var cYear = Math.floor(allMonth / 12) <= 0 ? '' : Math.floor(allMonth / 12) + '年';
		var cMonth = allMonth % 12 <= 0 ? '' : allMonth % 12;
		var chtml = '<div class="height_50"></div>'
				+'	<div class="pro-wrap">'
				+'			<p class="post clearfix">'
				+'				<span class="left mr40 Js_job_rname">'+jobName+'</span>'
				+'				<span class="editorBtnList">'
				+'					<a href="javascript:;" class="editor bqh Js_editor_btn">编辑资料</a>'
				+'					<a href="javascript:;" class="editor bfs Js_file_del">删除</a>'
				+'					<a href="javascript:;" class="editor bfs Js_del_ok" style="display:none;">确认删除</a>'
				+'					<a href="javascript:;" class="editor bshy Js_file_cancel" style="display:none;">取消删除</a>'
				+'				</span>'
				+'			</p>'
				+'			<p class="company mt6 Js_company_rname">'+companyName+'</p>'
				+'			<p class="time-area"><span class="Js_roldYear">'+oldYear+'</span><span class="Js_roldMonth">'+oldMonth+'</span><span class="spacing">-</span><span class="Js_rnewYear">'+newYear+'</span><span class="Js_rnewMonth">'+newMonth+'</span><span class="Js_cMonth">（'+cYear+cMonth+'个月）</span><span class="spacing">|</span> <span class="Js_work_rpalce">'+workPlace+'</span></p>'
				+'			<p class="des Js_rdisText">成立了联想集团客户支持部，处理客户售后问题。</p>'
				+'			<div class="Js_editor_file" style="display:none;">'
				+'				<div class="height_22"></div>'
				+'				<div class="rowblock clearfix">'
				+'					<div class="pt">公司名称</div>'
				+'					<div class="tools text left">'
				+'						<div class="inputWarp empty">'
				+'							<input type="text" class="Js_company_name">'
				+'						</div>'
				+'					</div>'
				+'				</div>'
				+'				<div class="height_18"></div>'
				+'				<div class="rowblock clearfix">'
				+'					<div class="pt">职务名称</div>'
				+'					<div class="tools text left">'
				+'						<div class="inputWarp empty">'
				+'							<input type="text" class="Js_job_name">'
				+'						</div>'
				+'					</div>'
				+'				</div>'
				+'				<div class="height_18"></div>'
				+'				<div class="rowblock clearfix">'
				+'					<div class="pt">工作地点</div>'
				+'					<div class="tools text left">'
				+'						<div class="inputWarp empty">'
				+'							<input type="text" class="Js_work_place">'
				+'						</div>'
				+'					</div>'
				+'				</div>'
				+'				<div class="height_18"></div>'
				+'				<div class="rowblock clearfix">'
				+'					<div class="pt">时间周期</div>'
				+'					<div class="optDiv Js_state left mr10" style="width:148px;">'
				+'						<div class="tools text pr" style="width:auto">'
				+'							<div class="inputWarp opt">'
				+'								<span class="Js_oldYear"></span>'
				+'								<a href="javascript:void(0)" class="showMenu"></a>'
				+'							</div>'
				+'						</div>'
				+'						<ul>'
				+'							<li><a href="javascript:void(0)">2010年</a></li>'
				+'							<li><a href="javascript:void(0)">2011年</a></li>'
				+'						</ul>'
				+'						<select style="display:none;">'
				+'							<option>2010年</option>'
				+'							<option>2011年</option>'
				+'						</select>'
				+'					</div>'
				+'					<div class="optDiv Js_state left" style="width:148px;">'
				+'						<div class="tools text pr" style="width:auto">'
				+'							<div class="inputWarp opt">'
				+'								<span class="Js_oldMonth"></span>'
				+'								<a href="javascript:void(0)" class="showMenu"></a>'
				+'							</div>'
				+'						</div>'
				+'						<ul>'
				+'							<li><a href="javascript:void(0)">1月</a></li>'
				+'							<li><a href="javascript:void(0)">2月</a></li>'
				+'						</ul>'
				+'						<select style="display:none;">'
				+'							<option>1月</option>'
				+'							<option>2月</option>'
				+'						</select>'
				+'					</div>'
				+'					<s class="time-fh"></s>'
				+'					<div class="optDiv Js_state left mr10" style="width:148px;">'
				+'						<div class="tools text pr" style="width:auto">'
				+'							<div class="inputWarp opt">'
				+'								<span class="Js_newYear"></span>'
				+'								<a href="javascript:void(0)" class="showMenu"></a>'
				+'							</div>'
				+'						</div>'
				+'						<ul>'
				+'							<li><a href="javascript:void(0)">2010年</a></li>'
				+'							<li><a href="javascript:void(0)">2011年</a></li>'
				+'						</ul>'
				+'						<select style="display:none;">'
				+'							<option>2010年</option>'
				+'							<option>2011年</option>'
				+'						</select>'
				+'					</div>'
				+'					<div class="optDiv Js_state left" style="width:148px;">'
				+'						<div class="tools text pr" style="width:auto">'
				+'							<div class="inputWarp opt">'
				+'								<span class="Js_newMonth"></span>'
				+'								<a href="javascript:void(0)" class="showMenu"></a>'
				+'							</div>'
				+'						</div>'
				+'						<ul>'
				+'							<li><a href="javascript:void(0)">1月</a></li>'
				+'							<li><a href="javascript:void(0)">2月</a></li>'
				+'						</ul>'
				+'						<select style="display:none;">'
				+'							<option>1月</option>'
				+'							<option>2月</option>'
				+'						</select>'
				+'					</div>'
				+'				</div>'
				+'				<div class="height_5"></div>'
				+'				<p class="isAgree">'
				+'					<span class="Js_checkBox" data-name="chk"><label><i style="display: none;"></i></label>我当前还在这里工作<input type="checkbox" name="chk"></span>'
				+'				</p>'
				+'				<div class="height_38"></div>'
				+'				<div class="rowblock">'
				+'					<div class="pt" style="line-height:12px;">描述</div>'
				+'					<div class="height_15"></div>'
				+'					<div class="tools textArea" style="width:640px; height:130px;">'
				+'						<textarea style="font-size: 15px; line-height: 1.5; height:115px;" class="Js_disText"></textarea>'
				+'					</div>'
				+'				</div>'
				+'				<div class="height_40"></div>'
				+'				<div class="p-warp">'
				+'					<a href="javascript:;" class="btn h8 mr5 font14 Js_cancle_revise">取消修改</a>'
				+'					<a href="javascript:;" class="btn b73 font14 Js_ok_revise">确认修改</a>'
				+'				</div>'
				+'			</div>'
				+'		</div>'
			$.ajax({
				url:'test.php',
				dataType : 'json',
				type : 'post',
				success : function(){
					$('.Js_file_add_context').append(chtml);
					$('.Js_file_add').hide();
				}
			})	
		}
	});
	
	$('.Js_editor_btn').live('click',function(){
		var parent = $(this).parents('.pro-wrap');
		var companyName = parent.find('.Js_company_rname').text();
		var jobName = parent.find('.Js_job_rname').text()
		var workPlace = parent.find('.Js_work_rpalce').text()
		var oldYear = parent.find('.Js_roldYear').text();
		var oldMonth = parent.find('.Js_roldMonth').text();
		var newYear = parent.find('.Js_rnewYear').text();
		var newMonth = parent.find('.Js_rnewMonth').text();
		var disText = parent.find('.Js_rdisText').text();
		parent.find('.Js_company_name').val(companyName);
		parent.find('.Js_job_name').val(jobName);
		parent.find('.Js_work_place').val(workPlace);
		parent.find('.Js_oldYear').text(oldYear);
		parent.find('.Js_oldMonth').text(oldMonth);
		parent.find('.Js_newYear').text(newYear);
		parent.find('.Js_newMonth').text(newMonth);
		parent.find('.Js_disText').val(disText);
		parent.find('.Js_oldYear').parents('.Js_state').find('option').each(function(){
			if(oldYear==$(this).text()){
				$(this)[0].selected = true;
			}
		});
		parent.find('.Js_oldMonth').parents('.Js_state').find('option').each(function(){
			if(oldYear==$(this).text()){
				$(this)[0].selected = true;
			}
		})
		parent.find('.Js_newYear').parents('.Js_state').find('option').each(function(){
			if(oldYear==$(this).text()){
				$(this)[0].selected = true;
			}
		})
		parent.find('.Js_newMonth').parents('.Js_state').find('option').each(function(){
			if(oldYear==$(this).text()){
				$(this)[0].selected = true;
			}
		})
		$(this).parents('.pro-wrap').find('.Js_editor_file').show();	
	});
	
	$('.Js_ok_revise').live('click',function(){
		var parent = $(this).parents('.pro-wrap');
		var companyName = parent.find('.Js_company_name').val();
		var jobName = parent.find('.Js_job_name').val()
		var workPlace = parent.find('.Js_work_place').val()
		var oldYear = parent.find('.Js_oldYear').text();
		var oldMonth =parent.find('.Js_oldMonth').text();
		var newYear = parent.find('.Js_newYear').text();
		var newMonth = parent.find('.Js_newMonth').text();
		var disText = parent.find('.Js_disText').val();
		var cYear = (parseInt(newYear,10) - parseInt(oldYear,10))*12;
		var cMonth = parseInt(newMonth,10) - parseInt(oldMonth,10);
		var allMonth = cYear + cMonth;
		var cYear = Math.floor(allMonth / 12) <= 0 ? '' : Math.floor(allMonth / 12);
		var cMonth = allMonth % 12 <= 0 ? '' : allMonth % 12;
		parent.find('.Js_company_rname').text(companyName);
		parent.find('.Js_job_rname').text(jobName);
		parent.find('.Js_work_rpalce').text(workPlace);
		parent.find('.Js_roldYear').text(oldYear);
		parent.find('.Js_roldMonth').text(oldMonth);
		parent.find('.Js_rnewYear').text(newYear);
		parent.find('.Js_rnewMonth').text(newMonth);
		parent.find('.Js_rdisText').text(disText);
		parent.find('.Js_cMonth').text('（'+cYear+'年'+cMonth+'个月'+'）');
		$('.Js_cancle_revise').click();
	})
	
	$('.Js_cancle_revise').live('click',function(){
		$(this).parents('.Js_editor_file').hide();	
	});
	
	$('.Js_jnset_btn').click(function(){
		if($('.Js_jn_set').is(':hidden')){
			var jnList = $('.Js_jn_list').hide().find('em');
			var jnsetList = $('.Js_jn_set').find('ul');
			jnList.each(function(){
				var chtml = '<li>'+$(this).text()+'<a class="Js_jn_del"></a></li>';
				jnsetList.append(chtml);
			})
			$('.Js_jn_set').show();	
		}
	});
	
	$('.Js_jn_add').click(function(){
		var flag = true;
		var val = $(this).prev().find('input').val();
		var chtml = '<li>'+val+'<a class="Js_jn_del"></a></li>';
		var self = $(this);
		$('.Js_jn_set').find('ul li').each(function(){
			if($(this).text()==val || val==""){
				flag = false;
				return false;	
			}
		})
		if(flag){
			$.ajax({
			url:'test.php',
			dataType : 'json',
			type : 'post',
			data : val,
			success : function(){
				$('.Js_jn_set').find('ul').append(chtml);
				self.prev().find('input').val('');
			}
		})	
		}
	})
	
	$('.Js_jn_del').live('click',function(){
		$(this).parent().remove();	
	});
	
	$('.Js_jn_cancle').click(function(){
		$('.Js_jn_set').hide().find('ul').html('');	
		$('.Js_jn_list').show();	
	});
	
	$('.Js_jn_ok').click(function(){
		var jnList = $('.Js_jn_list').hide().find('em');
		var jnsetList = $('.Js_jn_set').find('ul li');
		var arr = [];
		jnsetList.each(function(){
			var chtml = '<div class="skill-tag mr16 left"><span>0</span><em>'+$(this).text()+'</em></div>';
			arr.push($(chtml));
		})
		
		jnsetList.each(function(i){
			var self = $(this);
			jnList.each(function(j){
				if($(this).text()==self.text()){
					arr.splice(i,1,$(this).parent());	
				}
			})	
		});
		
		$('.Js_jn_list').children().remove();
		for(var i in arr){
			$('.Js_jn_list').append(arr[i]);
		}
		$('.Js_jn_cancle').click();
	});
	
	
	$('.Js_file_del').live('click',function(){
		$(this).hide();
		$(this).next().show().next().show();
	});
	
	$('.Js_del_ok').live('click',function(){
		if($(this).parents('.pro-wrap').next().length>0 && $(this).parents('.pro-wrap').prev().length>0){
			$.ajax({
				url:'test.php',
				dataType : 'json',
				type : 'post',
				data : data,
				success : function(){
					$(this).parents('.pro-wrap').next().remove();
					$(this).parents('.pro-wrap').remove();
				}
			})
			return
		}
		
		if($(this).parents('.pro-wrap').next().length==0 && $(this).parents('.pro-wrap').prev().length==0){
			$(this).parents('.pro-wrap').remove();
			return
		}
		
		if($(this).parents('.pro-wrap').next().length>0){
			$(this).parents('.pro-wrap').next().remove();
			$(this).parents('.pro-wrap').remove();
			return	
		}
		
		if($(this).parents('.pro-wrap').prev().length>0){
			$(this).parents('.pro-wrap').prev().remove();
			$(this).parents('.pro-wrap').remove();
			return	
		}
	});

	$('.Js_file_cancel').live('click',function(){
		$(this).hide().prev().hide();
		$('.Js_file_del').show();
	})
})