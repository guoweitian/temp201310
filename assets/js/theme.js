$(function() {
    /*
     帖子点击事件
     */
    $(".Js_theme_list").live("click",function(){
		var lindex = $($(".Js_theme_list")).index($(this));
        var that = $(this);
        var themeH = '<div class="theme-box" style="display:none;">'
            +'	<div class="theme-wrap">'
            +'		<div class="theme-item">'
            +'			<div class="theme-avatar"><a href="#"><img class="avatar" src="assets/temp/2.png"></a></div>'
            +'			<div class="theme-con">'
            +'				<p class="uname"><a href="#">我不是郦道元</a></p>'
            +'				<div>6月20日起，华中师范大学开放配有空调的会议室、报告厅、实验室、资料室、教室、食堂供学生们学习，并且将关闭时间延长至晚间22:30。各食堂还免费提供绿豆汤等消暑饮料！你们学校有神马应对高温的举措</div>'
            +'				<div class="time-bar">5分钟前</div>'
            +'			</div>'
            +'		</div>'
            +'		<div class="theme-item">'
            +'			<div class="theme-avatar"><a href="#"><img class="avatar" src="assets/temp/2.png"></a></div>'
            +'			<div class="theme-con">'
            +'				<p class="uname"><a href="#">我不是郦道元</a></p>'
            +'				<div>6月20日起，华中师范大学开放配有空调的会议室、报告厅、实验室、资料室、教室、食堂供学生们学习，并且将关闭时间延长至晚间22:30。各食堂还免费提供绿豆汤等消暑饮料！你们学校有神马应对高温的举措</div>'
            +'				<div class="time-bar">6分钟前</div>'
            +'			</div>'
            +'		</div>'
            +'		<div class="theme-item theme-postbox clearfix">'
            +'			<div class="theme-avatar"><a href="#"><img cclass="avatar" src="assets/temp/2.png"></a></div>'
//            +'			<div id="reply_toolbar'+lindex+'" class="toolbar-wrap" style="width: 590px;"></div>'
            +'			<div class="toolbar-content" style="width: 590px; padding-left: 60px;">'
            +'				<textarea id="reply_content'+lindex+'" name="content"></textarea>'
            +'			</div>'
            +'			<div class="height_20"></div>'
            +'			<div class="theme-post-bar">'
            +'				<a class="sub-sbtn left Js_AddReply" style=" text-align: center; width: 100px; margin-left: 60px;" href="javascript:;">回复</a>'
            +'			</div>'
            +'			<div class="height_10"></div>'
            +'		</div>'
            +'	</div>'
            +'	<div style=" margin:0 auto; padding-left:90px; width:937px;">'
            +'		<div class="sp-line" style="top:0">'
            +'			<div class="line-left"></div>'
            +'			<div class="line-right"></div>'
            +'		</div>'
            +'	</div>'
            +'</div>';


        if(that.hasClass("open")){
            that.next().slideUp(300,function(){
                that.removeClass("open");
                that.next(".theme-box").remove();
                that.find('i').hide();
            });
        }else{
            that.addClass("open");
            that.after(themeH);
            that.find('i').show();
			that.find('.conTxt p').text(parseInt(that.find('.conTxt p').text(),10)+1)
            that.next().slideDown(300,function(){
                that.next().removeAttr('style');
            });
        }

        $('#reply_content'+lindex).redactor({
            imageUpload: '../editor_upload_pic.php',
            lang: 'zh_cn' ,
            buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' ,'video' , 'link'],
            toolbarExternal: '#reply_toolbar'+lindex
        });
    })
	
	
	$(".Js_AddReply").live("click", function() {
		var scope = $(this);
		data = scope.parents('.theme-item').find('textarea').val();
		if(data=='') return false;
		var themeH = '<div class="theme-item">'
            +'			<div class="theme-avatar"><a href="#"><img class="avatar" src="assets/temp/2.png"></a></div>'
            +'			<div class="theme-con">'
            +'				<p class="uname"><a href="#">我不是郦道元</a></p>'
            +'				<div>'+data+'</div>'
            +'				<div class="time-bar">5分钟前</div>'
            +'			</div>'
            +'		</div>'
		$.ajax({
			url:'test.php',
			dataType : 'json',
			type : 'post',
			data : {"con":data},
			success : function(){
				scope.parents('.theme-item').before(themeH);
    			scope.parents('.theme-item').find('textarea').val('').prev().html('<p>&#8203</p>');
			}
		})		
		
	});
	
	$('.Js_send_lt').live('click',function(){
		var titval = $(this).parents('#form_publish_topic').find('#community_topic_title_input').val();
		var cval = $(this).parents('#form_publish_topic').find('#post_content').val();
		var step = '';
        if(titval==""){
			$(this).parents('#form_publish_topic').find('#community_topic_title .errorMsg').show();
			step = 'one';
		}else{
			$(this).parents('#form_publish_topic').find('.errorMsg').hide()
		}
        if(cval==""){
			$(this).parents('#form_publish_topic').find('#community_topic_content .errorMsg').show();
			step = 'one';
		}else{
			$(this).parents('#form_publish_topic').find('.errorMsg').hide()
		}
		
		if(step=='one') return false;
		var self = $(this);
        $.post(
            './modules/lajax.php',
            {
                'forumtitle':titval,
                'forumcontent':cval
            },
            function(data){
					var thtml = '<li>'
					+'  <div class="clearfix communityDiv Js_theme_list">'
					+'	<div class="nr">'
					+'	  <a href="javascript:;">'+titval+'</a>'
					+'	  <span>李原早 3月21日发布</span>'
					+'	</div>'
					+'	<div class="thread">'
					+'	  <p>4</p>'
					+'	  <a href="#">跟帖</a>'
					+'	</div>'
					+'	<div class="conTxt">'
					+'	  <p>299</p>'
					+'	  <a href="#">查看</a>'
					+'	</div>'
					+'	<div class="ftr">'
					+'	  <p>马双</p>'
					+'	  <span href="#">13小时前 最后回复</span>'
					+'	</div>'
					+' </div>'
					+'</li>'
					
				$('.Js_theme_list').eq(0).before(thtml);
				self.parents('#form_publish_topic').find('#post_content').val('').prev().html('<p>&#8203</p>');
				self.parents('#form_publish_topic').find('#community_topic_title_input').val('');
				$('.Js_tsubpost_topics').click();	
			}
        )
	})
});
