﻿$(function(){
	JS_PAGECIRCLE.init();
	JS_FINDFRIEND.init();
	JS_ADDCIRCLE.init();
	JS_SEARCHFIRENDCIRCLE.init();
    $('.circle-itemDiv').find('.circle-item').live('mouseenter',function(){
        $(this).css({width:'132px',height:'132px',top:'8px',borderRadius:'66px',left:'10px'});
        $(this).find('.item-wrap').css({height:'132px'});
    }).live('mouseleave',function(){
            $(this).css({width:'112px',height:'112px',top:'20px',borderRadius:'56px',left:'20px'});
            $(this).find('.item-wrap').css({height:'112px'});
        })
})

var JS_ADDCIRCLE = {
	init : function(){
		this.initDomEvent();
	},
	
	createBox : function(){/*
		var chtml = '<div class="addCircleBox Js_location_box">'
				+'		<div class="addCircleBoxDiv">'
				+'			<div class="sDiv">'
				+'				<div class="sHead">'
				+'					<p><input type="text" placeholder="圈子名称" id="Js_circle_name" maxlength="28"/></p>'
//				+'					<span class="tit">点击这里可以添加说明</span>'
//				+'					<div class="mt25 clearfix">'
//				+'						<ul class="left clearfix Js_addList_tag">'
//				+'							<li class="left mr30 on">要添加的人</li>'
//				+'							<li class="left" id="Js_pitchNum" style="display:none;">选中的人 (<span>0</span>个)</li>'
//				+'						</ul>'
//				+'						<div class="onList clearfix"  style="display:none;">'
//				+'							<s></s>'
//				+'							<div style="padding-left:25px;">'
//				+'								<input type="text"  placeholder="在此圈子中搜索" class="Js_circle_search" />'
//				+'							</div>'
//				+'						</div>'
//				+'					</div>'
				+'				</div>'
//				+'				<div class="sMid">'
//				+'					<a class="delBtn" href="javascript:;">删除</a>'
//				+'					<div class="sMidDiv">'
//				+'						<a href="javascript:;" class="Js_cd_sort">按相关程度排序</a>'
//				+'						<div class="pxListDiv">'
//				+'							<ul>'
//				+'								<li><a href="javascript:;">按名字排序列</a></li>'
//				+'								<li><a href="javascript:;">按姓氏排序</a></li>'
//				+'								<li><a href="javascript:;">按相关程度排序</a></li>'
//				+'								<li><a href="javascript:;">按相关程度排序</a></li>'
//				+'							</ul>'
//				+'						</div>'
//				+'					</div>'
//				+'					<div class="circleDiv Js_search_result">'
//				+'						<span class="nobody">此圈子中还没人</span>'
//				+'						<ul class="clearfix">'
//				+'							<li class="left Js_circle_add">'
//				+'								<div class="circleList clearfix">'
//				+'									<div class="circleLeft"></div>'
//				+'									<span class="circleRight">加入</span>'
//				+'								</div>'
//				+'							</li>'
//				+'						</ul>'
//				+'					</div>'
//				+'				</div>'
				+'				<div class="ml40 mt20">'
				+'					<a href="javascript:;" class="mr16 emptyCircle Js_create_circle">创建空圈子</a>'
				+'					<a href="javascript:;" class="cirCancle Js_add_py_cancle">取消</a>'
				+'				</div>'
				+'			</div>'
				+'		</div>'
				+'	</div>	';
				
				$('.Js_box_parent').append(chtml);*/
        var chtml = '<div>'
            +'		<p style="text-align:center"><input type="text" placeholder="圈子名称" id="Js_circle_name" style="width:730px; height:30px; line-height:1; margin-top:35px; font-family:simsun; color:#c0c0c0; border:1px solid #c0c0c0; font-size:14px; padding:5px;" maxlength=28/></p>'
            +'				<div style="margin:20px 0 0 40px;">'
            +'					<a href="javascript:;" class="mr16 emptyCircle Js_create_circle">创建圈子</a>'
            +'					<a href="javascript:;" class="cirCancle Js_add_py_cancle">取消</a>'
            +'				</div>'
            +'	</div>';
        $.fancybox({
            fitToView	: false,
            autoHeight	: false,
            autoSize	: false,
            width		: 810,
            height		: 140,
            content     : chtml,
            padding		: 0,
            beforeShow 	: function () {
                //$('.Js_create_circle').click($.proxy(self.treeDomEvent.test,self));
            }
        })
	},
	
	initDomEvent : function(){
		var _this = this;
		$('.Js_add_py_circle').click($.proxy(this.treeDomEvent.addPyCircle,this));
		$('.Js_add_py_cancle').live('click',this.treeDomEvent.addPyCancleEvent);
		$('.Js_create_circle').live('click',$.proxy(this.treeDomEvent.createCircleEvent,this));
		$('.Js_cd_sort').live('click',this.treeDomEvent.cdSortEvent);
		$('.Js_cd_sort + div li').live('click',this.treeDomEvent.cdSortNextLiEvent);
		$('.Js_location_box').live('click',this.treeDomEvent.locationBoxEvent);
		$('.Js_circle_search').live('keyup',this.treeDomEvent.circleSearchEvent);
		$('.Js_circle_add').live('click',this.treeDomEvent.circleAddEvent);
		$('#Js_closeBtn').live('click',this.treeDomEvent.addSearchClose);
		$('#Js_searchInput').live('keyup',this.treeDomEvent.addSearchInput);
		$('.Js_searchResultList li').live('click',this.treeDomEvent.searchResultList);
		$('.searchFriendDiv').live('click',function(){return false;});
		$('.Js_search_result li:gt(0)').live('click',this.treeDomEvent.addListItem).live('mousedown',function(){return false;});
		$('.Js_addList_tag li').live('click',this.treeDomEvent.addListTag);
		$('.delBtn').live('click',this.treeDomEvent.delAddUser);
        $('.Js_find_friend').click(function(){return false;}).mouseup(this.treeDomEvent.cczFocus);
	},
	
	createCircleBoxColor : {
		initBoxColor : ['#c0c0c0','#7bab73','#3b86c4','#abae8f','#cf6e6e'],
		randomColor : function(){
			var color = '#' + (function(color){
								return (color +=  '0123456789abcdef'[Math.floor(Math.random()*16)])
								  && (color.length == 6) ?  color : arguments.callee(color);
							  })('');
							  
			if($.inArray(color,this.initBoxColor)<0){
				this.initBoxColor.push(color);
				return color;
			}else{
				this.randomColor();
			}
		}
	},
	
	treeDomEvent : {
		addPyCircle : function(globalJson){
			this.createBox();
			$('#Js_circle_name').focus();
		},
        cczFocus : function(){
            $('.Js_cczsearch').focus().click();
        },
		addPyCancleEvent : function(){
			//$('.Js_location_box').remove();
            $.fancybox.close();
		},
		
		createCircleEvent : function(){
			var val = $('#Js_circle_name').val();
			var color = this.createCircleBoxColor.randomColor();
			var chtml = '<div class="circle-itemDiv"><div class="circle-item normal" style="background:'+color+'">'
					+'		<div class="item-wrap">'
					+'			<div class="item-td">'
					+'				<span class="c-name" data-type="ncr">'+val+'</span>'
					+'				<span class="c-number">'+$('.Js_search_result li:gt(0)').length+'</span>'
					+'			</div>'
					+'		</div>'
					+'	</div></div>'
			if(val==""){
				alert('请输入圈子名称');
				$('#Js_circle_name').focus();
			}else{
				$('.Js_circle_item_wrap').append(chtml);
				$('.Js_add_py_cancle').click();	
			}	
		},
		
		cdSortEvent :　function(){
			var o = $(this).next();
			if(o.is(":hidden")){
				o.show();	
			}else{
				o.hide();	
			}
			return false;	
		},
		
		locationBoxEvent : function(){
			$('.Js_cd_sort').next().hide();	
		},
		
		cdSortNextLiEvent : function(){
			var val = $(this).text();
			$('.Js_cd_sort').text(val);
			$(this).parent().children().find('a').css('background-position','0 -22px');
			$(this).find('a').css('background-position','0 -1px');
		},
		
		circleSearchEvent : function(){
			var val = $(this).val();
			$('.Js_search_result span').text();
		},
		
		circleAddEvent : function(){
			if($('.searchFriendDiv').length>0) return false;
			var chtml = '<div class="searchFriendDiv">'
					+'		<s class="sj"></s>'
					+'		<s class="closeBtn" id="Js_closeBtn"></s>'
					+'		<div class="prompt">已添加<span></span>。还要添加其他人吗？</div>'
					+'		<div class="searchDiv">'
					+'			<input type="text" id="Js_searchInput" />'
					+'			<ul class="Js_searchResultList">'
					+'			</ul>'
					+'		</div>'
					+'	</div>'
			$(this).parents('.sDiv').append(chtml);
			$('#Js_searchInput').focus();
			$(document).one('click.hideSearchInput',function(){$('#Js_closeBtn').click()});
			return false;
		},
		
		addSearchClose : function(){
			$(this).parents('.searchFriendDiv').remove();	
		},
		
		addSearchInput : function(){
			$('.Js_searchResultList').html('<li>数据加载中...</li>');
			var json = [
				{name : '马云',circleArr : ['朋友'],id:"my"},
				{name : '马克斯',circleArr : ['朋友','家人'],id:"mxs"},
				{name : '马修',circleArr : ['朋友','家人','认识的人'],id:"mx"}
			];
			var s = '';
			for(var i=0; i<json.length; i++){
				var o = json[i];
				var txt = o.circleArr.length > 1 ? o.circleArr.length + ' 个圈子' : o.circleArr[0];
				s+=	'<li class="clearfix" data-type="'+json[i].id+'"><img src="assets/temp/c1.png" alt="" class="userImg" /><div class="menuDiv"><p>'+json[i].name+'</p><span>'+txt+'</span></div></li>';
			}
			$('.Js_searchResultList').html(s);
			var val = $(this).val();
			var o = $(this).parent().find('ul')
			if(val=="") o.hide();
			else o.show();
		},
		
		searchResultList : function(){
			var len = $('.Js_search_result li:gt(0)').filter('[data-flag=1]').length;
			var txt = $('.Js_pitchNum').find('span').text();
			var val = $(this).find('p').text();
			var len = $('.Js_search_result').find('li').length;
			var chtml = '<li class="left" data-flag="1" data-type="'+$(this).attr('data-type')+'"><div class="circleList clearfix" style="background:#3c86c5"><div class="circleLeft" style="border-right:none;"><img src="assets/temp/d1.png" /></div><span class="circleRight" style="color:#fff">'+val+'</span></div></li>';
			for(var i=0; i<len; i++){
				if($(this).attr('data-type')==$('.Js_search_result li:gt(0)').eq(i).attr('data-type')){
					$('#Js_closeBtn').click();
					$('.Js_circle_add').click();
					$('.prompt').find('span').text(val).end().show();
					return false;
					break;
				}
			}
			$('.Js_search_result').find('ul').append(chtml);
			$('.Js_search_result').find('.nobody').hide();
			$('#Js_closeBtn').click();
			$('.Js_circle_add').click();
			$('.prompt').find('span').text(val).end().show();
			$('.Js_create_circle').text('创建包含'+len+'个人的圈子');
			var txt = $('#Js_pitchNum').find('span').text();
			$('#Js_pitchNum').find('span').text(parseInt(txt,10)+1);
			$('.delBtn').show();
			$('#Js_pitchNum').show();
		},
		
		addListItem : function(){
			var txt = $('#Js_pitchNum').find('span').text();
			if($(this).attr('data-flag')=='1'){
				$(this).find('.circleList').css({'background':'#fff'}).find('.circleRight').css('color','#372f2b');
				$(this).attr('data-flag','0');
				$('#Js_pitchNum').find('span').text(parseInt(txt,10)-1);
			}else{
				$(this).find('.circleList').css({'background':'#3c86c5'}).find('.circleRight').css('color','#fff');		
				$(this).attr('data-flag','1');
				$('#Js_pitchNum').find('span').text(parseInt(txt,10)+1);
			}
			var len = $('.Js_search_result li:gt(0)').filter('[data-flag=1]').length;
			if(len>0){
				$('.delBtn').show();
				$('#Js_pitchNum').show();
			}else{
				$('.delBtn').hide();
				if($('.Js_addList_tag li').eq(1).hasClass('on')) return;
				$('#Js_pitchNum').hide();	
			}
		},
		
		addListTag : function(){
			$('.Js_addList_tag li').removeClass('on');
			$(this).addClass('on');
			if($('.Js_addList_tag li').index($(this))==0){
				$('.Js_search_result li').show();
			}else{
				$('.Js_search_result li').hide().filter('[data-flag=1]').show();
			}
		},
		
		delAddUser : function(){
			$('.Js_search_result li:gt(0)').filter('[data-flag=1]').remove();
			$(this).hide();
			$('.Js_addList_tag li').eq(0).click();
			$('.Js_addList_tag li').eq(1).html('<li class="left" id="Js_pitchNum" style="">选中的人 (<span>0</span>个)</li>').hide();
			var len = $('.Js_search_result').find('li').length-1;
			$('.Js_create_circle').text('创建包含'+len+'个人的圈子');
		}
	}
	
}


var JS_FINDFRIEND = {
	init:function(){
		this.DomEvent();
		this.bindData();	
	},
	
	createBox : function(){
		var chtml = '<div class="Js_add_friendbox addFriend-box">'
				+'		<div class="addFriendDiv">'
				+'		<ul id="Js_circleList">'
				+'			<li class="clearfix">'
				+'				<s><b></b></s>'
				+'				<input type="checkbox" />'
				+'				<span>朋友</span>'
				+'				<i>8</i>'
				+'			</li>'
				+'			<li class="clearfix">'
				+'				<s><b></b></s>'
				+'				<input type="checkbox" />'
				+'				<span>家庭</span>'
				+'				<i>8</i>'
				+'			</li>'
				+'			<li class="clearfix">'
				+'				<s><b></b></s>'
				+'				<input type="checkbox" />'
				+'				<span>认识的人</span>'
				+'				<i>8</i>'
				+'			</li>'
				+'			<li class="clearfix">'
				+'				<s><b></b></s>'
				+'				<input type="checkbox" />'
				+'				<span>关注对象</span>'
				+'				<i>8</i>'
				+'			</li>'
				+'			<li class="clearfix">'
				+'				<s><b></b></s>'
				+'				<input type="checkbox" />'
				+'				<span>摄影和美术235252352352352523525252</span>'
				+'				<i>8888</i>'
				+'			</li>'
				+'		</ul>'
				+'		<div class="addformDiv">'
				+'			<a href="javascript:;" class="lys Js_add_circle">创建新的圈子</a>'
				+'			<div style="display:none;">'
				+'				<input type="text" />'
				+'				<a href="javascript:;" class="addBtn Js_addBtn">创建</a>'
				+'			</div>'
				+'		</div>'
				+'		</div>'
				+'	</div>'
			$('body').append(chtml);	
	},
	
	bindData　: function(){
		$('.Js_friend_add').each(function(){
			$(this)[0].arr = [];
			$(this)[0].addItem = [];
		})	
	},
	
	DomEvent : function(){
		var _this = this;
		if($('.Js_add_friendbox').length>0){
			$(document).mousemove(function(e){
				if(e.pageX > ($('.Js_add_friendbox').position().left + $('.Js_add_friendbox').outerWidth()) || e.pageX < $('.Js_add_friendbox').position().left){
					$('.Js_add_friendbox').remove();
				}
				if(e.pageY > ($('.Js_add_friendbox').position().top + $('.Js_add_friendbox').outerHeight()) || e.pageY < $('.Js_add_friendbox').position().top){
					$('.Js_add_friendbox').remove();
				}
			})
		}
		
		$('.Js_friend_add').mouseenter(function(){
			_this.mouseEnter(_this,$(this));
		})
		
		$('.Js_add_friendbox').live('mouseleave',function(){
			$(this).remove();	
		})
		
		$('.Js_add_circle').live('click',function(){
			$(this).hide();
			$(this).next().show();
		});
		
		$('.Js_addBtn').live('click',function(){
			_this.addCircleItem(_this,$(this));
		});
		
		$('#Js_circleList li').live('click',function(){
			var o = $(this).find('b');
			if(o.is(':hidden')){
				o.show().parent().next().attr('checked',true);
				$(this).find('i').text(parseInt($(this).find('i').text(),10)+1);
				_this.box[0].arr.push($(this));
			}else{
				o.hide().parent().next().attr('checked',false);
				$(this).find('i').text(parseInt($(this).find('i').text(),10)-1);
				for(var i=0; i<_this.box[0].arr.length; i++){
					if(_this.box[0].arr[i].find('span').text()===$(this).find('span').text()){
						_this.box[0].arr.splice(i,1);
					}
				}
			}
			if(_this.box[0].arr.length>0){
				if(_this.box[0].arr.length==1){
					_this.box.css({'background':'#7bab73','border-bottom':'1px solid #7bab73'}).children().text(_this.box[0].arr[0].find('span').text()).addClass('circle-addtoed');
				}else{
					_this.box.css({'background':'#7bab73','border':'1px solid #7bab73'}).children().text(_this.box[0].arr.length + "个圈子").addClass('circle-addtoed');	
				}
			}else{
				_this.box.css({'background':'#fff','border':'1px solid #d9d9d9'}).children().text('添加').removeClass('circle-addtoed');	
			}
		})
	},
	
	sCheckBox : function(){
		for(var i=0; i<this.box[0].arr.length; i++){
			var o = this.box[0].arr[i];
			$('#Js_circleList li').each(function(){
				console.log($(this).find('span').text() + ":" + o.find('span').text())
				if($(this).find('span').text()==o.find('span').text()){
					$(this).find('b').show();
					$(this).find('i').text(o.find('i').text());
				}
			})	
		}	
	},
	
	mouseEnter : function(self,me){
		if($('.Js_add_friendbox ').length>0) $('.Js_add_friendbox ').remove();
		var h = document.documentElement.clientHeight + document.body.scrollTop;
		var offset = me.offset();
		self.box = me;
		self.createBox();
		if(h<$('.Js_add_friendbox').height() + offset.top){
			var y = offset.top-$('.Js_add_friendbox').height()+me.outerHeight();
			$('.Js_add_friendbox').css({left:offset.left,top:y});
		}else{
			console.log(offset.top)
			$('.Js_add_friendbox').css({left:offset.left,top:offset.top});
		}
		this.sCheckBox();
		for(var i=0; i<self.box[0].addItem.length; i++){
			$('#Js_circleList').append(self.box[0].addItem[i]);	
		}
	},
	
	addCircleItem : function(self,me){
		var o = me.prev();
		var isHaveCircle = true;
		if(o.val()=="") return;
		$('#Js_circleList li').each(function(){
			if($(this).find('span').text()==o.val()){
				alert('已有该圈子');
				isHaveCircle = false;
				return false;	
			}	
		})
		if(!isHaveCircle) return;
		var chtml = $('<li class="clearfix">'
				+'				<s><b></b></s>'
				+'				<input type="checkbox" />'
				+'				<span>'+o.val()+'</span>'
				+'				<i>1</i>'
				+'			</li>');
		chtml.find('b').show();
		chtml.find('input').attr('checked',true);
		o.val('').parent().hide().prev().show();
		$('#Js_circleList').append(chtml);
		this.box[0].addItem.push(chtml);
		this.box[0].arr.push(chtml);
		if(self.box[0].arr.length>0){
			if(self.box[0].arr.length==1){
				self.box.css({'background':'#7bab73','border':'1px solid #7bab73'}).children().text(self.box[0].arr[0].find('span').text()).addClass('circle-addtoed');
			}else{
				self.box.css({'background':'#7bab73','border':'1px solid #7bab73'}).children().text(self.box[0].arr.length + "个圈子").addClass('circle-addtoed');	
			}
		}else{
			self.box.css({'background':'#fff','border':'1px solid #d9d9d9'}).children().text('添加').removeClass('circle-addtoed');	
		}
	}
}


var JS_PAGECIRCLE = {
	init : function(){
		this.u_list = $('.Js_circleList li');
		this.q_list = $('.Js_add');
		this.u_list.mousedown(function(ev){
			this.target = $(this);
			JS_PAGECIRCLE.u_list_mousedown(ev,$(this));
			return false;
		});
		
		$('.s-delBtn').click(function(){
			$(this).parents('li').remove();	
		});
		
		$('[data-focus="true"]').prev().click(function(){return false;})
		$('.Js_editBtn').click(function(){
			$('[data-focus="true"]').hide().prev().show().focus();
			$(document).unbind('keydown.edit').bind('keydown.edit',function(e){
				if(e.keyCode==13){
					var val = $('[data-focus="true"]').prev().val();
					if(val==""){
						$('[data-focus="true"]').show().prev().hide();
						$(document).unbind('keydown.edit');
						return	
					}
					var o = $('.Js_circle_item_wrap').find('.c-name').not('[data-focus="true"]');
					for(var i=0; i<o.length; i++){
						if(val==o.eq(i).text()){
							alert("圈子名字重复，请重新输入");
							return false;
							break;
						}
					}
					
					$('[data-focus="true"]').text(val).show().prev().hide();
					$(document).unbind('keydown.edit');
				}
			});
			$(document).unbind('click.edit').bind('click.edit',function(){
				var val = $('[data-focus="true"]').prev().val();
				if(val==""){
					$('[data-focus="true"]').show().prev().hide();
					$(document).unbind('click.edit');
					return	
				}
				var o = $('.Js_circle_item_wrap').find('.c-name').not('[data-focus="true"]');
				for(var i=0; i<o.length; i++){
					if(val==o.eq(i).text()){
						alert("圈子名字重复，请重新输入");
						return false;
						break;
					}
				}
				$('[data-focus="true"]').text(val).show().prev().hide();
				$(document).unbind('click.edit');
			});
			return false;
		});
	},
	u_list_mousedown : function(ev,o){
		this.target = o;
		this.disX = ev.pageX - o.position().left;
		this.disY = ev.pageY - o.position().top;	
		o.css({position:'absolute',top:o.offset().top,left:o.offset().left,zIndex:1});
		o.before('<li style="width:213px; height:80px;"></li>');
		if($('.visitCard-box').length > 0) $('.visitCard-box').remove();
		this.target.removeClass('Js_visitCard');
		this.target.removeClass('Js_friend_cList');
		$(document).mousemove($.proxy(function(ev){
			if($('.visitCard-box').length > 0) $('.visitCard-box').remove();
			this.u_list_mousemove(ev);
		},this));
		
		$(document).mouseup($.proxy(function(ev){
			this.u_list_mouseup(ev);
		},this));
	},
	u_list_mousemove : function(ev){
		var _x = ev.pageX - this.disX;
		var _y = ev.pageY - this.disY;
		this.target.css({'left':_x,'top':_y});
		for(var i=0; i<this.q_list.length; i++){
			if(ev.pageX > this.q_list.eq(i).offset().left && ev.pageX < this.q_list.eq(i).offset().left + this.q_list.eq(i).width()&&ev.pageY > this.q_list.eq(i).offset().top && ev.pageY < this.q_list.eq(i).offset().top + this.q_list.eq(i).height()){
				var index = i;
			}	
		}
        $('.Js_add').removeClass('iscircle').css({width:'112px',height:'112px',top:'20px',borderRadius:'56px',left:'20px'});
        $('.Js_add').find('.item-wrap').removeClass('iswrapcircle').css({height:'112px'});
		if(index>=0){
            $('.Js_add').eq(index).addClass('iscircle').css({width:'132px',height:'132px',top:'8px',borderRadius:'66px',left:'10px'});
            $('.Js_add').eq(index).find('.item-wrap').addClass('iswrapcircle').css({height:'132px'});
			
		}
		
	},
	u_list_mouseup : function(ev){
		this.target.addClass('Js_visitCard');
		this.target.addClass('Js_friend_cList');
		for(var i=0; i<this.q_list.length; i++){
			if(ev.pageX > this.q_list.eq(i).offset().left && ev.pageX < this.q_list.eq(i).offset().left + this.q_list.eq(i).width()&&ev.pageY > this.q_list.eq(i).offset().top && ev.pageY < this.q_list.eq(i).offset().top + this.q_list.eq(i).height()){
				var index = i;
			}	
		}
		if(index>=0){
			var mtStrArr = this.target.attr('data-type') ? this.target.attr('data-type').split("|") : [];
			if(this.target.attr('data-type') && $.inArray(this.q_list.find('.c-name').eq(index).attr('data-type'),mtStrArr)>=0){
				this.target.css({position:'static'}).prev().remove();	
			}else{
				var o = this.q_list.find('.c-number').eq(index);
				this.target.css({position:'static'}).prev().remove();
				o.text(parseInt(o.text(),10)+1);
				if(!this.target.attr('data-type')){
					this.target.attr('data-type',this.q_list.find('.c-name').eq(index).attr('data-type'));
				}else{
					var wb = this.target.attr('data-type');
					this.target.attr('data-type',wb + "|" + this.q_list.find('.c-name').eq(index).attr('data-type'));	
				}
				//this.target.remove();
			}
		}else{
			this.target.css({position:'static'}).prev().remove();	
		}
		$('.Js_add').removeClass('iscircle');
		$('.Js_add').find('.item-wrap').removeClass('iswrapcircle');
		$(document).unbind('mousemove');
		$(document).unbind('mouseup');	
	}
}

JS_SEARCHFIRENDCIRCLE = {
	init : function(){
		this.domEvent();	
	},
	domEvent : function(){
		$('.Js_friend_cList').mouseenter(function(){
			if(!$(this).hasClass('Js_friend_cList')) return;
			if($(this).attr('data-type')){
				var dataType = $(this).attr('data-type');
				var typeArr = dataType.split('|');
				console.log(typeArr)
				for(var i=0; i<typeArr.length; i++){
					for(var j=0; j<$('.Js_add').length; j++){
						if(typeArr[i]==$('.Js_add').eq(j).find('.c-name').attr('data-type')){
							$('.Js_add').eq(j).addClass('iscircle');
							$('.Js_add').eq(j).find('.item-wrap').addClass('iswrapcircle');
						}	
					}	
				}
			}
		}).mouseleave(function(){
			if(!$(this).hasClass('Js_friend_cList')) return;
			$('.Js_add').removeClass('iscircle');
			$('.Js_add').find('.item-wrap').removeClass('iswrapcircle');
		})
	}
}