$(function(){
    pro_type_change();
	msg_error_check();
})

/*永久和限制时间类别选择*/
function pro_type_change(){
    $('.Js_forever').click(function(){
        $('.Js_limit_content').hide();
    })
    $('.Js_limit_time').click(function(){
        $('.Js_limit_content').show();
    })
}

function msg_error_check(){
	$('.Js_send_project').click(function(){
		var step = '';

        var form_name = $('#form-send-hire');
        var form_data = form_name.serialize();
        var form_post_url = form_name.attr('action');


        if(!$('#Js_readed_agreement').is(':checked')){
			$('#Js_readed_agreement').parents('p').next().show();
			step = 'four';
		}else{
			$('#Js_readed_agreement').parents('p').next().hide();	
		}
		
		if(!$('#Js_signature').val()){
			$('#Js_signature').parent().parent().next().show();
			step = 'four';
		}else{
			$('#Js_signature').parent().parent().next().hide();	
		}
		
		if($('#Js_find_money').val()==""){
			$('#Js_find_money').parents('.tools').addClass('error').next().show();
			step = 'two';
		}else{
			$('#Js_find_money').parents('.tools').removeClass('error').next().hide();
		}
		
		if($('#xb_content').val()==""){
			$('#xb_content').parents('.toolbar-content').next().show();
			$('#xb_content').parents('.toolbar-content').find('.redactor_box').addClass('error');
			step = 'two';
		}else{
			$('#xb_content').parents('.toolbar-content').next().hide();
			$('#xb_content').parents('.toolbar-content').find('.redactor_box').removeClass('error');
		}
		
		if($('#tz_content').val()==""){
			$('#tz_content').parents('.toolbar-content').next().show();
			$('#tz_content').parents('.toolbar-content').find('.redactor_box').addClass('error');
			step = 'two';
		}else{
			$('#tz_content').parents('.toolbar-content').next().hide();
			$('#tz_content').parents('.toolbar-content').find('.redactor_box').removeClass('error');
		}
		
		if($('#Js_hiring_name_input').val()==""){
			$('#Js_hiring_name_input').parents('.tools').addClass('error').next().show();
			step = 'one';
		}else{
			$('#Js_hiring_name_input').parents('.tools').removeClass('error').next().hide();
		}
		
		if($('#Js_loc_input').val()==""){
			$('#Js_loc_input').parents('.tools').addClass('error').next().show();
			step = 'one';
		}else{
			$('#Js_loc_input').parents('.tools').removeClass('error').next().hide();
		}
		
		if($('#date1').val()==""){
			$('#date1').parent().addClass('error').next().show();
			step = 'one';
		}else{
			$('#date1').parent().removeClass('error').next().hide();
		}
		
		if($('.Js_upload_more').children().length<=0){
			$('.Js_upload_more').parent().find('.errorMsg').show();
			step = 'one';
		}else{
			$('.Js_upload_more').parent().find('.errorMsg').hide();
		}

		if(step=='one'){
			$("#tab-t .one a").click();
		}else if(step=='two'){
			$("#tab-t .two a").click();
		}else if(step=='four'){
			$("#tab-t .four a").click();
		}else if(step==''){

            $.ajax({
                url:"test.php",
                dataType : 'json',
                type : 'post',
                //data : form_data,
                success : function(responds){
                    if(responds.status == '1'){
                        window.location.href="hire-going.php";
                    }
                    else{
                        alert('ERROR_CODE:' + responds);
                    }
                }
            })
		}
		
	})	
}