var chtml = '<div class="map_control_box" id="Js_gmaps">'
		+'		<p>显示内容筛选</p>'
		+'		<ul>'
		+'			<li><input class="ib vm" type="checkbox" data-type="1" checked="checked" /> <label class="ib vm">创意投资项目比赛</label></li>'
		+'			<li><input class="ib vm" type="checkbox" data-type="2" checked="checked" /> <label class="ib vm">创意任务人才比赛</label></li>'
		+'			<li><input class="ib vm" type="checkbox" data-type="3" checked="checked" /> <label class="ib vm">创意公益明星比赛</label></li>'
		+'			<li><input class="ib vm" type="checkbox" data-type="4" checked="checked" /> <label class="ib vm">创意世界交易</label></li>'
		+'			<li><input class="ib vm" type="checkbox" data-type="5" checked="checked" /> <label class="ib vm">创意世界智库专家</label></li>'
		+'			<li><input class="ib vm" type="checkbox" data-type="6" checked="checked" /> <label class="ib vm">创意世界普通用户</label></li>'
		+'		</ul>'
		+'		<span class="jt"></span>'
		+'	</div>'
$(function(){
	var controlsFlag = true;
	
	var _div = document.createElement('div');
	
	var map = new google.maps.Map(document.getElementById('map'),{
		center:new google.maps.LatLng(30.657358499999994, 104.049977),
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		zoom: 12
	})
	
	var latlng = new google.maps.LatLng(30.657358499999994, 104.049977);
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({'latLng': latlng}, function(results, status) {
	  if (status == google.maps.GeocoderStatus.OK) {
		if (results[1]) {
			var address = results[1].formatted_address + "<br />";
			address = results[0].formatted_address + "<br />";
			address += "纬度：" + latlng.lat() + "<br />";
			address += "经度：" + latlng.lng();
			$('#search').val(results[1].formatted_address);
		}
	  } else {
		alert("Geocoder failed due to: " + status);
	  }
	});
	
	google.maps.event.addListener(map,'click',function(){
		$('#Js_gmaps').hide();
		controlsFlag = true;	
	})
	
	var homeControlDiv = document.createElement('div');
	var homeControl = new HomeControl(homeControlDiv, map);
	homeControlDiv.index = 1;
	map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(homeControlDiv);

var searchBox = new google.maps.places.SearchBox(document.getElementById('search'));
var markers = [];
google.maps.event.addListener(searchBox, 'places_changed', function(){
	var places = searchBox.getPlaces();	
	for (var i = 0, marker; marker = markers[i]; i++) {
	  marker.setMap(null);
	}
	markers = [];
	var bounds = new google.maps.LatLngBounds();
	for (var i = 0, place; place = places[i]; i++) {
		
		var image = {
			url: place.icon,
			size: new google.maps.Size(71, 71),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(17, 34),
			scaledSize: new google.maps.Size(25, 25),
		};
		
		var marker = new google.maps.Marker({
			map: map,
			//icon: image,
			title: place.name,
			position: place.geometry.location
		});
		
		markers.push(marker);
		
		bounds.extend(place.geometry.location);
	}
	map.fitBounds(bounds);
})
google.maps.event.addListener(map, 'bounds_changed', function() {
	var bounds = map.getBounds();
	searchBox.setBounds(bounds);
});
//1fEA1zzN2iFs4Cpm5na91NWgRqYbxUJYWl3hHxY


//30.657358499999994,104.049977
//41.850033,-87.6500523
//-12.043333,-77.028333 
$('#geolocation').click(function(){
	if(navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(function(position) {
		var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		var infowindow = new google.maps.InfoWindow({
			map: map,
			position: pos,
			content: 'this is your location address'
		});
	
		map.setCenter(pos);
		map.setZoom(15);
		var mk = new google.maps.Marker({
			map:map,
			position:new google.maps.LatLng(position.coords.latitude,position.coords.longitude)
		})
		markers.push(mk);
		markers.push(infowindow);
	}, function() {
		handleNoGeolocation(true);
	});
	} else {
		// Browser doesn't support Geolocation
		handleNoGeolocation(false);
	}	
})

function handleNoGeolocation(errorFlag) {
	if (errorFlag) {
		var content = 'Error: The Geolocation service failed.';
	} else {
		var content = 'Error: Your browser doesn\'t support geolocation.';
	}

	var options = {
		map: map,
		position: new google.maps.LatLng(60, 105),
		content: content
	};

	var infowindow = new google.maps.InfoWindow(options);
	map.setCenter(options.position);
}

var bs = new google.maps.FusionTablesLayer({
		query: {
			from: '15wQvuZ_0mOLRZdEbtH0c_Sa-1SotVv0vMduoMrw'
		}
	}) 
bs.setMap(map);	
	
$('#Js_gmaps :checkbox').live('click',function(){
	bs.setMap(null);
	if($('#Js_gmaps :checked').length==0) return;
	var whereArr = [];
	for(var i=0; i<$('#Js_gmaps :checked').length; i++){
		var o = $('#Js_gmaps :checked').eq(i);
		whereArr.push(o.attr('data-type'));
	}
	where = whereArr.toString();
	console.log(where)
	
	bs = new google.maps.FusionTablesLayer({
		query: {
			from: '15wQvuZ_0mOLRZdEbtH0c_Sa-1SotVv0vMduoMrw',
			where: 'Number in('+where+')'
		}
	}) 
	bs.setMap(map);
})

function HomeControl(controlDiv, map) {
	var _div = document.createElement('div');
	_div.style.width = '32px';
	_div.style.height = '32px';
	_div.style.marginRight = '20px';
	_div.style.background = 'url(../../images/icon.png)';
	_div.style.marginBottom = '20px';
	_div.style.cursor = 'pointer';
	controlDiv.appendChild(_div);
	var ct = $(chtml)[0];
	map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(ct);
	google.maps.event.addDomListener(_div, 'click', function() {
		if(controlsFlag){
			$('#Js_gmaps').show();
		}else{
			$('#Js_gmaps').hide();	
		}
		controlsFlag = !controlsFlag;
	});
}
})

