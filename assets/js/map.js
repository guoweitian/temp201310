/*
+'					<li>'
				+'						<a href="#" class="itemNumber">1</a>'
				+'						<a href="#" class="itemAddress">青羊区</a>'
				+'						<a href="#" class="zs_itemNumber">222,132,234</a>'
				+'					</li>'
				+'					<li>'
				+'						<a href="#" class="itemNumber">2</a>'
				+'						<a href="#" class="itemAddress">武侯区</a>'
				+'						<a href="#" class="zs_itemNumber">222,132,234</a>'
				+'					</li>'
				+'					<li>'
				+'						<a href="#" class="itemNumber">3</a>'
				+'						<a href="#" class="itemAddress">高新区</a>'
				+'						<a href="#" class="zs_itemNumber">222,132,234</a>'
				+'					</li>'
				+'					<li>'
				+'						<a href="#" class="itemNumber">4</a>'
				+'						<a href="#" class="itemAddress">金牛区</a>'
				+'						<a href="#" class="zs_itemNumber">222,132,234</a>'
				+'					</li>'
				+'					<li>'
				+'						<a href="#" class="itemNumber">5</a>'
				+'						<a href="#" class="itemAddress">锦江区</a>'
				+'						<a href="#" class="zs_itemNumber">222,132,234</a>'
				+'					</li>'
				+'					<li>'
				+'						<a href="#" class="itemNumber">6</a>'
				+'						<a href="#" class="itemAddress">成华区</a>'
				+'						<a href="#" class="zs_itemNumber">222,132,234</a>'
				+'					</li>'
				+'					<li>'
				+'						<a href="#" class="itemNumber">7</a>'
				+'						<a href="#" class="itemAddress">新都区</a>'
				+'						<a href="#" class="zs_itemNumber">222,132,234</a>'
				+'					</li>'
				+'					<li>'
				+'						<a href="#" class="itemNumber">8</a>'
				+'						<a href="#" class="itemAddress">青白江区</a>'
				+'						<a href="#" class="zs_itemNumber">222,132,234</a>'
				+'					</li>'
				+'					<li>'
				+'						<a href="#" class="itemNumber">9</a>'
				+'						<a href="#" class="itemAddress">双流区</a>'
				+'						<a href="#" class="zs_itemNumber">222,132,234</a>'
				+'					</li>'
				+'					<li>'
				+'						<a href="#" class="itemNumber">10</a>'
				+'						<a href="#" class="itemAddress">温江区</a>'
				+'						<a href="#" class="zs_itemNumber">222,132,234</a>'
				+'					</li>'
				+'					<li>'
				+'						<a href="#" class="itemNumber">8</a>'
				+'						<a href="#" class="itemAddress">郫县</a>'
				+'						<a href="#" class="zs_itemNumber">222,132,234</a>'
				+'					</li>'
*/

var map = {};
var country = {
	school : [
				{
					sName : '剑桥大学',
					itemNum : 0,
					createIndex : '100',
				},{
					sName : '麻省理工大学',
					itemNum : 1,
					createIndex : '10',
				},{
      sName : '麻省理工大学',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '麻省理工大学',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '麻省理工大学',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '麻省理工大学',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '麻省理工大学',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '麻省理工大学',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '麻省理工大学',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '麻省理工大学',
      itemNum : 1,
      createIndex : '10',
    }
	],
	qy : [
				{
					sName : '苹果',
					itemNum : 0,
					createIndex : '100',
				},{
					sName : '微软',
					itemNum : 1,
					createIndex : '10',
				},{
      sName : '微软',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '微软',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '微软',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '微软',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '微软',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '微软',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '微软',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '微软',
      itemNum : 1,
      createIndex : '10',
    },{
      sName : '微软',
      itemNum : 1,
      createIndex : '10',
    }
	],
	global : [{
	itemNum : 0,
	createIndex : '1,100',
	cName:'中国',
	school : [{
		sName : '清华大学',
		itemNum : 0,
		createIndex : '100',
	},{
		sName : '北京大学',
		itemNum : 1,
		createIndex : '10',
	},],
	qy : [
				{
					sName : '长虹',
					itemNum : 0,
					createIndex : '100',
				},{
					sName : '长城',
					itemNum : 1,
					createIndex : '10',
				}
	],
	province:[
		{
			itemNum : 0,
			createIndex : '800',
			cProvince:'四川',
			school:[{
					sName : '四川大学',
					itemNum : 0,
					createIndex : '100',
				},{
					sName : '川北医学院',
					itemNum : 1,
					createIndex : '10',
				}],
				qy : [
						{
							sName : '四川企业1',
							itemNum : 0,
							createIndex : '100',
						},{
							sName : '四川企业2',
							itemNum : 1,
							createIndex : '10',
						}
			],
			city:[
				{
					itemNum : 0,
					createIndex : '400',
					cCity:'成都',
					school:[{
						sName : '四川大学',
						itemNum : 0,
						createIndex : '100',
					},{
						sName : '电子科技大学',
						itemNum : 1,
						createIndex : '10',
					}],
					qy : [
							{
								sName : '成都企业1',
								itemNum : 0,
								createIndex : '100',
							},{
								sName : '成都企业2',
								itemNum : 1,
								createIndex : '10',
							}
					],
					area:[
						{
							itemNum : 0,
							createIndex : '100',
							name:'锦江区',
							flag:true,
							school:[{
								sName : '锦江区大学',
								itemNum : 0,
								createIndex : '100',
							}],
							qy : [
										{
											sName : '锦江区企业1',
											itemNum : 0,
											createIndex : '100',
										},{
											sName : '锦江区企业2',
											itemNum : 1,
											createIndex : '10',
										}
							],
						},
						{
							itemNum : 1,
							createIndex : '50',
							name:'金牛区',
							flag:true,
							school:[{
								sName : '金牛区大学',
								itemNum : 0,
								createIndex : '100',
							}]
						}
					]	
				},
				{
					itemNum : 1,
					createIndex : '300',
					cCity:'南充',
					area:[
						{
							itemNum : 0,
							createIndex : '100',
							name:'营山',	
						},
						{
							itemNum : 1,
							createIndex : '50',
							name:'蓬安',	
						}
					]	
				}
			]	
		},
		{
			itemNum : 1,
			createIndex : 10,
			cProvince:'北京',
			city:[
				{
					itemNum : 0,
					createIndex : '300',
					cCity:'北京区'
				}
			]	
		}
	],
},{
	itemNum : 1,
	createIndex : '1,200',
	cName:'美国',
	province:[
		{
			cProvince:'加州',
			city:[
				{
					cCity:'abc',
					area:[
						'a','b'
					]	
				}
			]	
		}
	]
},{
  itemNum : 1,
    createIndex : '1,200',
    cName:'美国',
    province:[
    {
      cProvince:'加州',
      city:[
        {
          cCity:'abc',
          area:[
            'a','b'
          ]
        }
      ]
    }
  ]
},{
  itemNum : 1,
    createIndex : '1,200',
    cName:'美国',
    province:[
    {
      cProvince:'加州',
      city:[
        {
          cCity:'abc',
          area:[
            'a','b'
          ]
        }
      ]
    }
  ]
},{
  itemNum : 1,
    createIndex : '1,200',
    cName:'美国',
    province:[
    {
      cProvince:'加州',
      city:[
        {
          cCity:'abc',
          area:[
            'a','b'
          ]
        }
      ]
    }
  ]
},{
  itemNum : 1,
    createIndex : '1,200',
    cName:'美国',
    province:[
    {
      cProvince:'加州',
      city:[
        {
          cCity:'abc',
          area:[
            'a','b'
          ]
        }
      ]
    }
  ]
},{
  itemNum : 1,
    createIndex : '1,200',
    cName:'美国',
    province:[
    {
      cProvince:'加州',
      city:[
        {
          cCity:'abc',
          area:[
            'a','b'
          ]
        }
      ]
    }
  ]
},{
  itemNum : 1,
    createIndex : '1,200',
    cName:'美国',
    province:[
    {
      cProvince:'加州',
      city:[
        {
          cCity:'abc',
          area:[
            'a','b'
          ]
        }
      ]
    }
  ]
},{
  itemNum : 1,
    createIndex : '1,200',
    cName:'美国',
    province:[
    {
      cProvince:'加州',
      city:[
        {
          cCity:'abc',
          area:[
            'a','b'
          ]
        }
      ]
    }
  ]
},{
  itemNum : 1,
    createIndex : '1,200',
    cName:'美国',
    province:[
    {
      cProvince:'加州',
      city:[
        {
          cCity:'abc',
          area:[
            'a','b'
          ]
        }
      ]
    }
  ]
},{
  itemNum : 1,
    createIndex : '1,200',
    cName:'美国',
    province:[
    {
      cProvince:'加州',
      city:[
        {
          cCity:'abc',
          area:[
            'a','b'
          ]
        }
      ]
    }
  ]
}]
}

function getCountryList(arr){
	var s = '';
	var school = '';
	var qy = '';
	switch(arr.length){
		case 0:
			for(var i=0; i<country.global.length; i++){
				var o = country.global[i];
				var f = country.global[i].flag;
				if(f){
					s+='<li data-level='+o.itemNum+' class="Js_getIn" data-s="1" isflag="1"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.cName+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>'
				}else{
					s+='<li data-level='+o.itemNum+' class="Js_getIn" data-s="1"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.cName+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>'
				}
				
			}
			for(var i=0; i<country.school.length; i++){
				var o = country.school[i];
				school+='<li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.sName+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>';
			}
			for(var i=0; i<country.qy.length; i++){
				var o = country.qy[i]
				qy+='<li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.sName+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>';
			}
			break;
		case 1:
			for(var i=0; i<country.global[arr[0]].province.length; i++){
				var o = country.global[arr[0]].province[i];
				var f = country.global[arr[0]].province[i].flag;
				if(f){
					s+='<li data-level='+arr[0] + '|' +o.itemNum+' class="Js_getIn" data-s="2" isflag="1"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.cProvince+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>'
				}else{
					s+='<li data-level='+arr[0] + '|' +o.itemNum+' class="Js_getIn" data-s="2"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.cProvince+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>'
				}
			}
			for(var i=0; i<country.global[arr[0]].school.length; i++){
				var o = country.global[arr[0]].school[i]
				school+='<li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.sName+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>';
			}
			for(var i=0; i<country.global[arr[0]].qy.length; i++){
				var o = country.global[arr[0]].qy[i]
				qy+='<li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.sName+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>';
			}
			break;
		case 2:
			for(var i=0; i<country.global[arr[0]].province[arr[1]].city.length; i++){
				var o = country.global[arr[0]].province[arr[1]].city[i];
				var f = country.global[arr[0]].province[arr[1]].city[i].flag;
				if(f){
					s+='<li data-level='+arr[0] + '|' + arr[1] + '|' + o.itemNum+' class="Js_getIn" data-s="3" isflag="1"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.cCity+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>'
				}else{
					s+='<li data-level='+arr[0] + '|' + arr[1] + '|' + o.itemNum+' class="Js_getIn" data-s="3"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.cCity+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>'
				}
			}
			for(var i=0; i<country.global[arr[0]].province[arr[1]].school.length; i++){
				var o = country.global[arr[0]].province[arr[1]].school[i];
				school+='<li class="Js_getIn" data-s="4"><a href="#" class="itemNumbe">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.sName+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>';
			}
			for(var i=0; i<country.global[arr[0]].province[arr[1]].qy.length; i++){
				var o = country.global[arr[0]].province[arr[1]].qy[i]
				qy+='<li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.sName+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>';
			}
			break;
		case 3:
			for(var i=0; i<country.global[arr[0]].province[arr[1]].city[arr[2]].area.length; i++){
				var o = country.global[arr[0]].province[arr[1]].city[arr[2]].area[i];
				var f = country.global[arr[0]].province[arr[1]].city[arr[2]].area[i].flag;
				if(f){
					s+='<li data-level='+arr[0] + '|' + arr[1] + '|' + arr[2] + '|' + o.itemNum+' data-s="4" isflag="1"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.name+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>'
				}else{
					s+='<li data-level='+arr[0] + '|' + arr[1] + '|' + arr[2] + '|' + o.itemNum+' data-s="4"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.name+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>'
				}
			}
			for(var i=0; i<country.global[arr[0]].province[arr[1]].city[arr[2]].school.length; i++){
				var o = country.global[arr[0]].province[arr[1]].city[arr[2]].school[i]
				school+='<li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.sName+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>';
			}
			for(var i=0; i<country.global[arr[0]].province[arr[1]].city[arr[2]].qy.length; i++){
				var o = country.global[arr[0]].province[arr[1]].city[arr[2]].qy[i]
				qy+='<li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">'+(o.itemNum+1)+'</a><a href="#" class="itemAddress">'+o.sName+'</a><a href="#" class="zs_itemNumber">'+o.createIndex+'</a></li>';
			}
			break;
	}
	
	return {s:s,school:school,qy:qy};
}

$(function(){
				var o = getCountryList([]);
				var chtml = '<div class="map Js_map">'
				+'	<div class="mapHead Js_mapHead"><a class="m-head" href="#">创意世界地区排行榜<s></s></a></div>'
				+'	<div class="mapDiv Js_mapDiv">'
				+'		<div class="areaTag Js_areaTag"><span><a href="#" class="Js_getInTag" data-s="0" style="color:#3e85c3">全球</a></span></div>'
				+'		<ul class="clearfix mapTab Js_mapTab">'
				+'			<li class="current"><a href="#" class="item-list-area">地区</a></li>'
				+'			<li><a href="#" class="item-list-area">学校</a></li>'
				+'			<li><a href="#" class="item-list-area">企业</a></li>'
				+'		</ul>'
				+'		<div class="Js_contextList">'
				+'			<div class="Js_contextListItem">'
				+'				<div class="clearfix mapTabItemTit">'
				+'					<span class="pm" class="item-list-type">排名</span>'
				+'					<span class="dqmc" class="item-list-area">地区名称</span>'
				+'					<span class="cyzs" class="item-list-area">地区创意指数</span>'
				+'				</div>'
				+'				<ul class="mapTabItem">'
				+					o.s
				+'				</ul>'
				+'			</div>'
				+'			<div class="Js_contextListItem listHide">'
				+'				<div class="clearfix mapTabItemTit">'
				+'					<span class="pm">排名</span>'
				+'					<span class="dqmc">学校名称</span>'
				+'					<span class="cyzs">学校创意指数</span>'
				+'				</div>'
				+'				<ul class="mapTabItem" style="height:280px;">'
				+					o.school
				+'				</ul>'
				
				+'			</div>'
				+'			<div class="Js_contextListItem listHide">'
				+'				<div class="clearfix mapTabItemTit">'
				+'					<span class="pm">排名</span>'
				+'					<span class="dqmc">企业名称</span>'
				+'					<span class="cyzs">企业创意指数</span>'
				+'				</div>'
				+'				<ul class="mapTabItem">'
				+					o.qy
				+'				</ul>'
				+'			</div>'
				+'		</div>'
				+'	</div>'
				+'	<div class="schoolSelect Js_schoolSelect">'
				+'		<ul class="clearfix">'
				+'			<li class="dx" data-type="0">大学</li>'
				+'			<li class="zx" data-type="0">中学</li>'
				+'			<li class="xx" data-type="0">小学</li>'
				+'		</ul>'
				+'	</div>'
				+'	<ul class="itemUl Js_itemUl">'
				+'		<li class="first on"><a href="javascript:;" altname="用户" class="Js_intro" data-type="5" data-flag="1"></a></li>'
				+'		<li class="second on"><a href="javascript:;" altname="专家" class="Js_intro" data-type="6" data-flag="1"></a></li>'
				+'		<li class="third on"><a href="javascript:;" altname="创意投资项目比赛" class="Js_intro" data-type="1" data-flag="1"></a></li>'
				+'		<li class="forth on"><a href="javascript:;" altname="创意任务人才比赛" class="Js_intro" data-type="2" data-flag="1"></a></li>'
				+'		<li class="fivth on"><a href="javascript:;" altname="创意公益明星比赛" class="Js_intro" data-type="3" data-flag="1"></a></li>'
				+'		<li class="last on"><a href="javascript:;" altname="创意世界交易所" class="Js_intro" data-type="4" data-flag="1"></a></li>'
				+'	</ul>'
				+'</div>'
		map.map = new google.maps.Map(document.getElementById('map'),{
		center:new google.maps.LatLng(30.657358499999994, 104.049977),
		mapTypeControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		zoom: 12
	})	
	google.maps.event.addListener(map.map,'click',function(){
		infowindow.close();	
	})
	var homeControlDiv = document.createElement('div');
	var homeControl = new HomeControl(homeControlDiv, map.map);
	homeControlDiv.index = 1;
	map.map.controls[google.maps.ControlPosition.RIGHT_TOP].push(homeControlDiv);
	
	
	map.geocoder = new google.maps.Geocoder();

	map.latNorthEast = 0;
	map.lngNorthEast = 0;
	map.latSouthWest = 0;
	map.lngSouthWest = 0;
	map.type='123456';
	map.cache=[];
	map.back={};
	map.back;
	map.record="";
	google.maps.event.addListener(map.map, 'bounds_changed', function(){
		var latNorthEast = map.map.getBounds().getNorthEast().lat();
		var lngNorthEast = map.map.getBounds().getNorthEast().lng();
		var latSouthWest = map.map.getBounds().getSouthWest().lat();
		var lngSouthWest = map.map.getBounds().getSouthWest().lng();
    	if(latNorthEast >= map.latNorthEast || lngNorthEast >= map.lngNorthEast || latSouthWest <= map.latSouthWest || lngSouthWest <= map.lngSouthWest){
    		//if(latNorthEast >= map.latNorthEast){map.latNorthEast = latNorthEast;}
    		//if(lngNorthEast >= map.lngNorthEast){map.lngNorthEast = lngNorthEast;}
    		//if(latSouthWest <= map.latSouthWest){map.latSouthWest = latSouthWest;}
    		//if(lngSouthWest <= map.lngSouthWest){map.lngSouthWest = lngSouthWest;}
    		map.latNorthEast = latNorthEast;
    		map.lngNorthEast = lngNorthEast;
    		map.latSouthWest = latSouthWest;
    		map.lngSouthWest = lngSouthWest;
	    	clearTimeout(map.time)
	    	map.time = setTimeout(function(){
				//if (map.markerCluster) {
				//	map.markerCluster.clearMarkers();
				//}
				ajaxMap();
	    	},1000)
    	}
    });
	
	
	function HomeControl(controlDiv, map) {
		controlDiv.appendChild($(chtml)[0]);
		var index = 0;
		var map = $(controlDiv).find('.Js_map');
		var head = $(controlDiv).find('.Js_mapHead');
		var typeList = $(controlDiv).find('.Js_mapTab li');
		var mapTabItem = $(controlDiv).find('.mapTabItem li');
		var contextList = $(controlDiv).find('.Js_contextList .Js_contextListItem');
		var schoolList = $('.Js_schoolSelect').find('li');
		contextList.eq(index).show();
		head.click(function(){
			if($('.Js_mapDiv').is(':hidden')){
				$('.Js_mapDiv,.Js_itemUl,.Js_schoolSelect').show();
				map.css({'marginTop':'17px'});
				head.find('s').css({"borderBottom":'5px solid #fff','borderTop':'5px solid transparent','top':'16px'});
			}else{
				$('.Js_mapDiv,.Js_itemUl,.Js_schoolSelect').hide();	
				map.css({'marginTop':'0px'});
				head.find('s').css({"borderTop":'5px solid #fff','borderBottom':'5px solid transparent','top':'21px'});
			}	
		})
		
		schoolList.live('click',function(){
			if($(this).text()=="大学"){
				var chtml = '<li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">1</a><a href="#" class="itemAddress">西南交大</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">2</a><a href="#" class="itemAddress">西南财大</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">3</a><a href="#" class="itemAddress">西华大学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">4</a><a href="#" class="itemAddress">四川师范大学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">5</a><a href="#" class="itemAddress">成都理工</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">6</a><a href="#" class="itemAddress">电子科大</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">7</a><a href="#" class="itemAddress">成都信工院</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">8</a><a href="#" class="itemAddress">西南民族大学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">9</a><a href="#" class="itemAddress">成都大学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">10</a><a href="#" class="itemAddress">华西师大</a><a href="#" class="zs_itemNumber">10</a></li>'
			}else if($(this).text()=="中学"){
				var chtml = '<li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">1</a><a href="#" class="itemAddress">石室中学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">2</a><a href="#" class="itemAddress">成都七中</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">3</a><a href="#" class="itemAddress">树德中学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">4</a><a href="#" class="itemAddress">四川师大附中</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">5</a><a href="#" class="itemAddress">双流中学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">6</a><a href="#" class="itemAddress">温江中学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">7</a><a href="#" class="itemAddress">棠湖中学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">8</a><a href="#" class="itemAddress">华西中学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">9</a><a href="#" class="itemAddress">玉林中学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">10</a><a href="#" class="itemAddress">崇庆中学</a><a href="#" class="zs_itemNumber">10</a></li>'	
			}else{
				var chtml = '<li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">1</a><a href="#" class="itemAddress">磨子桥小学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">2</a><a href="#" class="itemAddress">盐道街小学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">3</a><a href="#" class="itemAddress">天涯石小学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">4</a><a href="#" class="itemAddress">红专西路小学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">5</a><a href="#" class="itemAddress">驸马小学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">6</a><a href="#" class="itemAddress">锦江小学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">7</a><a href="#" class="itemAddress">花果小学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">8</a><a href="#" class="itemAddress">草堂小学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">9</a><a href="#" class="itemAddress">泡桐树小学</a><a href="#" class="zs_itemNumber">10</a></li><li class="Js_getIn" data-s="4"><a href="#" class="itemNumber">10</a><a href="#" class="itemAddress">江路小学</a><a href="#" class="zs_itemNumber">10</a></li>'	
			}
			$('.Js_contextListItem').eq(1).find('ul').html(chtml);	
			$('.Js_schoolSelect').find('li').removeClass('on').attr('data-type','0');
			$(this).addClass('on').attr('data-type','1');
		}).live('mouseenter',function(){
			$(this).addClass('on');
		}).live('mouseleave',function(){
			if($(this).attr('data-type')=="0")
				$(this).removeClass('on');	
		})
		
		typeList.click(function(){
			var num = typeList.index((this));
			typeList.removeClass('current');
			typeList.eq(num).addClass('current');
			contextList.hide();
			contextList.eq(num).show();
			if($(this).find('a').text()=="学校"){
				$('.Js_schoolSelect').show();
        $('.Js_mapDiv').height(382);
			}else{
				$('.Js_schoolSelect').hide();
        $('.Js_mapDiv').height(420);
			}
			return false;
		});
		
		mapTabItem.live('click',function(){
			var txt = $(this).find('.itemAddress').text();
			var sNum = $(this).attr('data-s');
			$('.mapTabItem li').removeClass('on');
			$(this).addClass('on');
			if(sNum>0){
				searchMap(txt);
			}else{
				map.map.setZoom(3);
			}
      $('#Js_c_area').text(txt);
      var zjhtml = '<li>'
        +'			<a href="#">'
        +'				<img src="assets/temp/tp5.jpg" alt="">'
        +'				<dl>'
        +'					<dt class="Js_visitCard">孙正义1</dt>'
        +'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
        +'				</dl>'
        +'			</a>'
        +'		</li>'
        +'		<li>'
        +'			<a href="#">'
        +'				<img src="assets/temp/tp6.jpg" alt="">'
        +'				<dl>'
        +'					<dt class="Js_visitCard">孙正义2</dt>'
        +'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
        +'				</dl>'
        +'			</a>'
        +'		</li>'
        +'		<li>'
        +'			<a href="#">'
        +'				<img src="assets/temp/tp7.jpg" alt="">'
        +'				<dl>'
        +'					<dt class="Js_visitCard">孙正义3</dt>'
        +'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
        +'				</dl>'
        +'			</a>'
        +'		</li>'
        +'		<li>'
        +'			<a href="#">'
        +'              <img src="assets/temp/tp8.jpg" alt="">'
        +'				<dl>'
        +'					<dt class="Js_visitCard">孙正义4</dt>'
        +'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
        +'				</dl>'
        +'			</a>'
        +'		</li>'
        +'		<li>'
        +'			<a href="#">'
        +'				<img src="assets/temp/tp8.jpg" alt="">'
        +'				<dl>'
        +'					<dt class="Js_visitCard">孙正义5</dt>'
        +'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
        +'				</dl>'
        +'			</a>'
        +'		</li>'
        +'		<li>'
        +'			<a href="#">'
        +'				<img src="assets/temp/tp7.jpg" alt="">'
        +'				<dl>'
        +'					<dt class="Js_visitCard">孙正义6</dt>'
        +'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
        +'				</dl>'
        +'			</a>'
        +'		</li>'
        +'		<li>'
        +'			<a href="#">'
        +'				<img src="assets/temp/tp6.jpg" alt="">'
        +'				<dl>'
        +'					<dt class="Js_visitCard">孙正义7</dt>'
        +'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
        +'				</dl>'
        +'			</a>'
        +'		</li>'
        +'		<li>'
        +'			<a href="#">'
        +'				<img src="assets/temp/tp5.jpg" alt="">'
        +'				<dl>'
        +'					<dt class="Js_visitCard">孙正义8</dt>'
        +'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
        +'				</dl>'
        +'			</a>'
        +'		</li>';
      $('.Js_expert_list').html(zjhtml);
			if($(this).attr('data-level')){
				var dataLevel = $(this).attr('data-level');
				var levelArr = dataLevel.split('|');
				var nArr = [];
				for(var i=0; i<levelArr.length; i++){
					nArr.push(parseInt(levelArr[i],10));	
				}
			
				if($(this).attr('isflag')=='1') return;
				var o = getCountryList(nArr);
				$('.mapTabItem').each(function(a,b){
					switch(a){
						case 0:
						$('.Js_getInTag').css('color','#727272');
						$('.Js_areaTag').append('<span>&nbsp;&nbsp; > &nbsp;&nbsp;<a href="#" class="Js_getInTag" data-s="'+($('.Js_getInTag').length)+'" style="color:#3e85c3" data-level="'+dataLevel+'">'+txt+'</a></span>');
						$(this).html(o.s);
						break
						case 1:
						$(this).html(o.school);
						break
						case 2:
						$(this).html(o.qy);
						break	
					}
				})
			}
		})
	}
	
	$('.Js_getInTag').live('click',function(){
		var txt = $(this).text();
		var sNum = $(this).attr('data-s');
		var index = $('.Js_getInTag').index($(this));
		$('.Js_getInTag').parent().filter(':gt('+index+')').remove();
		$(this).css('color','#3e85c3');
		var dataLevel = $(this).attr('data-level');
		var levelArr = $(this).attr('data-level') ? dataLevel.split('|') : [];
		var nArr = [];
		if(sNum>0){
			searchMap(txt);
		}else{
			map.map.setZoom(3);
		}
		for(var i=0; i<levelArr.length; i++){
			nArr.push(parseInt(levelArr[i],10));	
		}
		
		var o = getCountryList(nArr);
		$('.mapTabItem').each(function(a,b){
			switch(a){
				case 0:
				$(this).html(o.s);
				break
				case 1:
				$(this).html(o.school);
				break
				case 2:
				$(this).html(o.qy);
				break	
			}
		})
		
		$('#Js_c_area').text(txt);
        var zjhtml = '     <li>'
                    +'			<a href="#">'
					+'				<img src="assets/temp/tp5.jpg" alt="">'
					+'				<dl>'
					+'					<dt class="Js_visitCard">孙正义1</dt>'
					+'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
					+'				</dl>'
					+'			</a>'
					+'		</li>'
                    +'	<li>'
					+'			<a href="#">'
					+'				<img src="assets/temp/tp6.jpg" alt="">'
					+'				<dl>'
					+'					<dt class="Js_visitCard">孙正义2</dt>'
					+'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
					+'				</dl>'
					+'			</a>'
					+'		</li>'
                    +'      <li>'
					+'			<a href="#">'
					+'				<img src="assets/temp/tp7.jpg" alt="">'
					+'				<dl>'
					+'					<dt class="Js_visitCard">孙正义3</dt>'
					+'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
					+'				</dl>'
					+'			</a>'
				    +'      </li>'
					+'		<li>'
					+'			<a href="#">'
					+'				<img src="assets/temp/tp8.jpg" alt="">'
					+'				<dl>'
					+'					<dt class="Js_visitCard">孙正义4</dt>'
					+'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
					+'				</dl>'
                    +'			</a>'
					+'		</li>'
					+'		<li>'
					+'			<a href="#">'
					+'				<img src="assets/temp/tp8.jpg" alt="">'
					+'				<dl>'
					+'					<dt class="Js_visitCard">孙正义5</dt>'
					+'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
					+'				</dl>'
                    +'			</a>'
					+'		</li>'
					+'		<li>'
					+'			<a href="#">'
					+'				<img src="assets/temp/tp7.jpg" alt="">'
					+'				<dl>'
					+'					<dt class="Js_visitCard">孙正义6</dt>'
					+'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
					+'				</dl>'
                    +'			</a>'
					+'		</li>'
					+'		<li>'
					+'			<a href="#">'
					+'				<img src="assets/temp/tp6.jpg" alt="">'
					+'				<dl>'
					+'					<dt class="Js_visitCard">孙正义7</dt>'
					+'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
					+'			    </dl>'
                    +'			</a>'
					+'		</li>'
					+'		<li>'
					+'			<a href="#">'
					+'				<img src="assets/temp/tp5.jpg" alt="">'
					+'				<dl>'
					+'					<dt class="Js_visitCard">孙正义8</dt>'
					+'					<dd>新闻集团核心大股东，董事长兼行政总裁</dd>'
					+'				</dl>'
					+'			</a>'
					+'		</li>';
        $('.Js_expert_list').html(zjhtml);
	})
	
	$('.Js_intro').live('mouseenter',function(e){
			if($(this).attr('data-flag')=="0"){
				$(this).parent().addClass('on');
				var altname = "在地图上显示"+$(this).attr('altname')+"信息";
			}else{
				var altname = "隐藏地图上的"+$(this).attr('altname')+"信息"
			}
			var boxY = e.pageY+20;
			var boxX = e.pageX+10;
			var w = altname.length*14 + 14*2;
			var bodyW = $('body').width();
			if(boxX+w>bodyW){
				var ht = '<div class="Js_map_sBox" style="font-size:14px; color:#888888; padding:4px 13px; border:1px solid #d2d2d2; background:#fff; position:absolute; top:'+boxY+'px; right:0px; width:'+w+'px;">'+altname+'</div>';
			}else{
			var ht = '<div class="Js_map_sBox" style="font-size:14px; color:#888888; padding:4px 13px; border:1px solid #d2d2d2; background:#fff; position:absolute; top:'+boxY+'px; left:'+boxX+'px; width:'+w+'px;">'+altname+'</div>';
			}
			$('body').append(ht);
		}).live('mousemove',function(e){
			var boxY = e.pageY+20;
			var boxX = e.pageX+10;
			var boxW = $('.Js_map_sBox').outerWidth();
			var bodyW = $('body').width();
			if(boxX+boxW>bodyW){
				$('.Js_map_sBox').css({right:0,top:boxY});
			}else{
				$('.Js_map_sBox').css({left:boxX,top:boxY});
			}
		}).live('mouseleave',function(){
			if($(this).attr('data-flag')=="0"){
				$(this).parent().removeClass('on');
			}
			$('.Js_map_sBox').remove();	
		}).live('click',function(){
			var type = $(this).attr('data-type'),addmarks_ =[],removeMarkers_ = [];
			if($(this).attr('data-flag')=='1'){
				if($('.Js_map_sBox').length>0){
					$('.Js_map_sBox').text("在地图上显示"+$(this).attr('altname')+"信息")
				}
				for(var i=0,len=map.cache.length;i<len;i++){
					if(map.cache[i].type==type){
						map.cache[i].setVisible(false);
						removeMarkers_.push(map.cache[i]);
					}
				}
				$(this).parent().removeClass('on');	
				$(this).attr('data-flag','0');
			}else if($(this).attr('data-flag')=='0'){
				if($('.Js_map_sBox').length>0){
					$('.Js_map_sBox').text("隐藏地图上的"+$(this).attr('altname')+"信息")
				}
				for(var i=0,len=map.cache.length;i<len;i++){
					if(map.cache[i].type==type){
						map.cache[i].setVisible(true);
						addmarks_.push(map.cache[i]);
					}
				}
				$(this).parent().addClass('on');
				$(this).attr('data-flag','1');		
			}
			map.markerCluster.addMarkers(addmarks_);
			map.markerCluster.removeMarkers(removeMarkers_);
		})
})
var infowindow = new google.maps.InfoWindow();
var mhtml = '<div class="clearfix">'
			+'	<a class="mapinfoimg"><img src="assets/temp/d2.png" /></a>'
			+'	<div class="mapinfoDiv">'
			+'		<div class="leftTopDiv">'
			+'			<p class="matchName"><a href="#">侏罗纪的召唤-蚀刻暴龙拼装模型</a></p>'
			+'			<s class="matchPregass"></s>'
			+'			<p class="fbName"><a href="#">发布者名字</a></p>'
			+'			<p class="money">10,000元</p>'
			+'			<p class="matchStyle">任务比赛奖金</p>'
			+'		</div>'
			+'		<div class="clearfix leftBottomDiv">'
			+'			<span class="leftTxt">剩余3天</span>'
			+'			<span class="rightTxt">3,000作品</span>'
			+'		</div>'
            +'	</div>'
			+'</div>'
			
			
var thtml = '<div class="clearfix">'
		+'		<a class="mapUserImg" href="#"><img src="assets/temp/d1.png"></a>'
		+'		<div class="mapUserDiv">'
		+'			<p><a href="#">如果我不是郦道元怎么办</a></p>'
		+'			<span>中国四川成都</span>'
		+'		</div>'
		+'	</div>'
function ajaxMap(){
	var fType = [];
	$('.Js_itemUl a').filter('[data-flag=0]').each(function(){
		fType.push($(this).attr('data-type'));
	})
	addmarks_=[],removeMarkers_=[];
	map.marks = [];
	var latNorthEast = map.map.getBounds().getNorthEast().lat();
	var lngNorthEast = map.map.getBounds().getNorthEast().lng();
	var latSouthWest = map.map.getBounds().getSouthWest().lat();
	var lngSouthWest = map.map.getBounds().getSouthWest().lng();
	var latCenter = map.map.getCenter().lat();
	var lngCenter = map.map.getCenter().lng();
	var zoom = map.map.getZoom();
	$.ajax({
		url:'map.php',
		data:{latCenter:latCenter,lngCenter:lngCenter,latSouthWest:latSouthWest,lngSouthWest:lngSouthWest,latNorthEast:latNorthEast,lngNorthEast:lngNorthEast,zoom:zoom},
		type:'POST',
		dataType:'json',
		success:function(data){
			if(data.status == 1){
				for(var i=0,len=data.latlng.lat.length;i<len;i++){
					//var marker = data.latlng.lat[i];
					var record = data.latlng.lat[i]+'@'+data.latlng.lng[i];
					if(map.record.search(record) == -1){
                        var marker = new google.maps.Marker({
							map:map.map,
							position: new google.maps.LatLng(data.latlng.lat[i], data.latlng.lng[i]),
                            icon:'assets/temp/marker-'+data.latlng.type[i]+'.png'
						});
						if(map.type == "" || map.type.search(data.latlng.type[i]) == -1) {
							//map.marks.push(marker);
							marker.setVisible(false);
						}else{
							map.marks.push(marker);
						}
						
						marker.type = data.latlng.type[i];
						marker.obj = marker;
						marker.index = i;
						map.record += '*'+record;
						map.cache.push(marker);
						if($.inArray(marker.type.toString(),fType)>=0){
							marker.setVisible(false);
							removeMarkers_.push(marker);
						}else{
							addmarks_.push(marker);	
						}
						google.maps.event.addListener(marker,"click",function(){
							infowindow.setContent(thtml);
							infowindow.open(map.map,this.obj);
						})
					}
				}
				if(map.markerCluster){map.markerCluster.addMarkers(map.marks,false)}
				else{map.markerCluster = new MarkerClusterer(map.map, map.marks,{maxZoom: 18});}
				
				//map.markerCluster.addMarkers(addmarks_);
				map.markerCluster.removeMarkers(removeMarkers_);
				
			}else if(data.status == 0){
				alert('error');
			}
		},
		error:function(){},
		complete:function(){
		},
		timeout:30000
	})
}


function searchMap(txt){
	map.geocoder.geocode( { 'address':txt}, function(results, status) {
    	if (status == google.maps.GeocoderStatus.OK) {
        	map.map.setCenter(results[0].geometry.location);
        	map.map.fitBounds(results[0].geometry.viewport);
    		//ajaxMap();
      	}
    });
}

/*PROGMAPS = {
	init : function(){
			
	}	
}*/