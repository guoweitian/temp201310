function share(){
    var arr = [];
    var uList = $('.Js_wrapList').children();
    uList.each(function(i,obj){
        if($(this).position().left==0){
            arr.push($(this));
        }
    });

    $('.Js_publick_Div').click(function(){
        $('.Js_share').slideDown(300,function(){
            $('.Js_publick_txt').focus();
        });
        $('.Js_publick_parent').slideUp(300);
        for(var i=1; i<arr.length; i++){
            if(i==1){
                arr[1].animate({'top':0},300,'linear');
            }else{
                var n = 0;
                for(var j=1; j<i; j++){
                    n+=(arr[j].height() + 20);
                }
                arr[i].animate({'top':n},300,'linear');
            }
        }
    });


    $('.Js_share_cancle').click(function(){
        var h = arr[0].height()+20;
        $('.Js_share').slideUp(300);
        $('.Js_publick_parent').slideDown(300);
        for(var i=1; i<arr.length; i++){
            if(i==1){
                arr[1].animate({'top':h},300,'linear');
            }else{
                var n = 0;
                for(var j=0; j<i; j++){
                    if(j==0){
                        n+=h;
                    }else{
                        n+=(arr[j].height() + 20);
                    }
                }
                arr[i].animate({'top':n},300,'linear');
            }
        }
    })
}



/*头部导航scroll*/
function navScroll(){
    var clickBtn = null;
    var isType = null;
    $('.Js_nav_scroll').live('click',function(){
        var num = $('.Js_nav_scroll').index($(this));
        if(clickBtn == num && isType == 'clickEvent') return;
        if($('html,body').is(":animated")) return;
        $('.Js_navMore').removeClass('on');
        $(document).unbind('scroll');
        $('html,body').animate({scrollTop:$('.Js_scroll').eq(num).offset().top - 139},'slow',function(){
            $(document).bind('scroll',function(){
                $('.Js_scroll').each(function(i){
                    if($(document).scrollTop() >= $(this).offset().top - 139 && $(document).scrollTop() <= $(this).offset().top + $(this).parents('.detailPageMid').outerHeight() - 139){
                        $('.Js_nav_scroll').children().removeClass('on');
                        $('.Js_nav_scroll').eq(i).children().addClass('on');
                        clickBtn = i;
                        isType = 'scrollEvent';
                    }
                });
                if($('.Js_nav_scroll').eq(clickBtn).parents('.Js_navMoreItem').length>0){
                    $('.Js_navMore').addClass('on');
                }else{
                    $('.Js_navMore').removeClass('on');
                }
            });
        });
        $('.Js_nav_scroll').children().removeClass('on');
        $(this).children().addClass('on');
        clickBtn = num;
        isType = 'clickEvent';
        if($('.Js_nav_scroll').eq(clickBtn).parents('.Js_navMoreItem').length>0){
            $('.Js_navMore').addClass('on');
        }
    })

    $(document).bind('scroll',function(){
        $('.Js_navMore').removeClass('on');
        $('.Js_scroll').each(function(i){
            if($(document).scrollTop() >= $(this).offset().top - 139 && $(document).scrollTop() <= $(this).offset().top + $(this).parents('.detailPageMid').outerHeight() - 139){
                $('.Js_nav_scroll').children().removeClass('on');
                $('.Js_nav_scroll').eq(i).children().addClass('on');
                clickBtn = i;
                isType = 'scrollEvent';
            }
        })

        if($('.Js_nav_scroll').eq(clickBtn).parents('.Js_navMoreItem').length>0){
            $('.Js_navMore').addClass('on');
        }else{
            $('.Js_navMore').removeClass('on');
        }
    })
}

function navGmaps(){
    $('.Js_pos_loc').click(function(){
        var _this = $(this);
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(function(position){
                var coords = position.coords;
                //var latlng = new google.maps.LatLng(30.657358499999994, 104.049977);
                var latlng = new google.maps.LatLng(coords.latitude, coords.longitude);
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({'latLng': latlng}, function(results, status) {
                    if (status == 'OK') {
                        var flag = false;
                        for(var i=0; i<results.length; i++){
                            var address = results[i];
                            for(var j=0; j<address.types.length; j++){
                                if(address.types[j]=="sublocality"){
                                    flag = true;
                                }
                            }

                            if(flag){
                                var s = address.formatted_address;
                                break;
                            }
                        }
                        _this.parents('#g-searchbox').hide().prev().text(s).show();
                        $('.Js_loc_input').val(s);
                        $('.Js_cityList').find('s').css('background','url(assets/images/dw.png) no-repeat center center')
                        $(".match-addr").css('background-position','0 22px');
                        /*if (results[1]) {
                         var s = "";
                         for(var i=results[1].address_components.length-1; i>=0; i--){
                         var o = results[1].address_components[i];
                         var flag = true;
                         for(var j=0; j<o.types.length; j++){
                         if(o.types[j]=="postal_code"){
                         flag = false;
                         break;
                         }
                         }

                         if(flag){
                         s+=o.long_name;
                         }
                         }
                         _this.parents('#g-searchbox').hide().prev().text(s).show();
                         $('.Js_loc_input').val(s);
                         $('.Js_cityList').find('s').css('background','url(assets/images/dw.png) no-repeat center center')
                         $(".match-addr").css('background-position','0 22px');
                         }*/
                    } else {
                        $(".match-addr").text('全世界').css('background-position','0 -81px');
                    }
                });
            },function(error){
                var errorTypes = {
                    1 : '位置服务被拒绝',
                    2 : '获取不到的位置信息',
                    3 : '获取信息超时'
                }
                //alert(errorTypes[error.code] + "不能确定你的当前地理位置。");
                $(".match-addr").text('全世界').css('background-position','0 -81px');
            });
        }else{
            alert('浏览器不支持该功能');
        }
    })

    $('.Js_world_all').click(function(){
        $(this).parents('#g-searchbox').hide().prev().text('全世界').css('background-position','0 -81px').show();
    })
    $('.Js_pos_loc').click();
}

window.onload = function(){
    $(".aside").height(document.documentElement.clientHeight-76);
    var l = $('.Js_ccz_left').width()+6;
    $('.Js_ccz_right').css('margin-left',l);
    //$('.Js_ccbNum').children().eq(0).css('min-width',l-11);
    // $('.Js_login_txt').css('width',l-30);
    $('.Js_ccbNum').width(l-11);
    $('.Js_ccz_right').show();
    $(window).resize(function(){
        var l = $('.Js_ccz_left').width()+5;
        $('.Js_ccz_right').css('margin-left',l);
    });

    var createIndexW = $('.Js_livemsg b').outerWidth();
    var createIndexH = $('.Js_livemsg b').outerHeight();
    var createIndexParentW = $('.Js_livemsg').width();
    var createIndexParentH = $('.Js_livemsg').height();
    if(parseInt($('.Js_livemsg b').text())==0){
        var w = 5;
        var h = 3;
    }else{
        var w = Math.floor((createIndexParentW-createIndexW)/2);
        var h = Math.ceil((createIndexParentH-createIndexH)/2);
    }
    $('.Js_livemsg b').css({'top':h,'left':w}).show();

    var url = window.location.href;
    var theRequest;
    var x = $(".Js_detailPublish");
    if (url.indexOf("#") != -1) {
        var str = url.substr(1);
        strs = str.split("#");
        theRequest = strs[strs.length-1]
    };

    switch(theRequest){
        case "step-one":
            var index = 0;
            break;
        case "step-two":
            var index = 1;
            break;
        case "step-three":
            var index = 2;
            break;
        case "step-four":
            var index = 3;
            break;
        default:
            var index = 0;
            break;
    }
    if($('.Js_tabs_items').length>0){
        $('.Js_tabs_items').each(function(){
            $(this).find('.tabs-c').hide();
            $(this).find('.tabs-c').eq(index).show();
        })
    }
}

function syUserfile(){
    if($('.Js_sy_savebtn').length>0){
        $('.Js_sy_savebtn').click(function(){
            $(this).parent().find('.init').each(function(){
                if($(this).val()==""){
                    $('.Js_sy_savebtn').show();
                    return false;
                }else{
                    $('.Js_sy_savebtn').hide();
                }
            })
            $(this).parent().find('.init').each(function(){
                if($(this).val()!=""){
                    $(this).hide().prev().find('span').text($(this).val()).end().show();
                }
            })
        })
    }

    if($('.Js_sy_fileedit').length>0){
        $('.Js_sy_fileedit').click(function(){
            if($(this).next().is(':hidden')){
                $('.Js_sy_filebtn,.Js_sy_filedel,.Js_edit_kj').hide();
                $(this).next().show().next().show().next().show();
                $(document).one('click',function(){
                    $('.Js_sy_filebtn,.Js_sy_filedel,.Js_edit_kj').hide();
                });
                $('.Js_cczsearch').one('click',function(){
                    $('.Js_sy_filebtn,.Js_sy_filedel,.Js_edit_kj').hide();
                })
            }else{
                $(this).next().hide().next().hide().next().hide();
            }
            return false;
        })
    }

    if($('.Js_sy_filedel').length>0){
        $('.Js_sy_filedel').click(function(){
            $(this).parent().children().not('span,.Js_sy_fileedit').hide();
            $(this).parent().hide().next().show();
            $(this).parent().next().val('');
            $('.Js_sy_savebtn').show();
        })
    }

    if($('.Js_sy_okbtn').length>0){
        $('.Js_sy_okbtn').click(function(){
            if($(this).prev().val()){
                $(this).parent().parent().find('span').text($(this).prev().val());
                $(this).parent().parent().next().val($(this).prev().val());
                $(this).prev().val('');
                $(this).parent().parent().children().not('span,.Js_sy_fileedit').hide();
            }
        })
    }


    if($('.Js_edit_kj').length>0){
        $('.Js_edit_kj').click(function(){return false;})
    }
}

function userfileitems(){
    dataControl('date1');
    dataControl('date2');
    if($('#t1').length>0){
        var city1 = citySelect('t1','t2','t3',callback1);
        city1.init();
    }
    if($('#c1').length>0){
        var city2 = citySelect('c1','c2','c3',callback2);
        city2.init();
    }
    if($('#s1').length>0){
        var city3 = citySelect('s1','s2','s3',callback3);
        city3.init();
    }

    if($('.Js_password').length>0){
        $('.Js_password').pstrength();
    }
    var add_obj = {cityName: {city: "东城区",province: "北京"}, cityId: "100101"}
    var bank_obj = {cityName: {city: "东城区",province: "北京"}, cityId: "100101"}
    function callback1(ev){
        if($('#cityCode').length>0){
            $('#cityCode').val(ev.cityId);
        }
        console.log(ev);
    };
    function callback2(ev){
        add_obj = ev;
    };
    function callback3(ev){
        bank_obj = ev;
    };
    var ngz_index = null;

    if($('.Js_userfile_item').length>0){
        $('.Js_userfile_item').click(function(){
            var num = $('.Js_userfile_item').index($(this));
            $('.Js_userfile_item').removeClass('current');
            $(this).addClass('current');
            $('.Js_u_listitem').hide().eq(num).show();
            $('.Js_add_form,.Js_addbank_form').hide();
        })
    }
    if($('.Js_add_newaddress').length>0){
        $('.Js_add_newaddress').click(function(){
            if($('.Js_add_form').is(':hidden')){
                $('.Js_add_form').show();
            }else{
                $('.Js_add_form').hide();
            }
        })
    }

    $('.Js_add_dress_ok').click(function(){
        var step = '';
        if($('.Js_add_ngname').val() == ''){
            $('.Js_add_ngname').parents('.tools').addClass('error').next().show();
            step = 'one';
        }else{
            $('.Js_add_ngname').parents('.tools').removeClass('error').next().hide();
        }
        if($('.Js_jd_address').val() == ''){
            $('.Js_jd_address').parents('.tools').addClass('error').next().show();
            step = 'one';
        }else{
            $('.Js_jd_address').parents('.tools').removeClass('error').next().hide();
        }
        if(!checkTextForm.zipCode($('.Js_add_ngzipcode').val())){
            $('.Js_add_ngzipcode').parents('.tools').addClass('error').next().show();
            step = 'one';
        }else{
            $('.Js_add_ngzipcode').parents('.tools').removeClass('error').next().hide();
        }
        if(!(checkTextForm.cellphone($('.Js_add_ngphone').val()) || checkTextForm.telphone($('.Js_add_ngphone')))){
            $('.Js_add_ngphone').parents('.tools').addClass('error').next().show();
            step = 'one';
        }else{
            $('.Js_add_ngphone').parents('.tools').removeClass('error').next().hide();
        }
        if(step == ''){
            var strName = '北京市东城区';
            if(add_obj.cityName.county){
                strName = add_obj.cityName.province+'省'+ add_obj.cityName.city + '市' + add_obj.cityName.county;
            }else{
                strName = add_obj.cityName.province+'市'+ add_obj.cityName.city;
            }
            if($('.Js_add_dress_ok').text()=='添加地址'){
                var chtml = '<div class="n-address-list">'
                    +'      <div class="clearfix">'
                    +'          <span style="float:left; width: 815PX; word-wrap: break-word; word-break: break-all;" city_id="'+add_obj.cityId+'">个人地址：<span class="Js_ngsxs">'+strName+'</span><span class="Js_ngaddress">'+$('.Js_jd_address').val()+'</span></span>'
                    +'          <a href="#" class="n-del Js_add_dressdel">删除</a>'
                    +'          <a href="#" class="n-xg Js_add_xg">修改</a>'
                    +'      </div>'
                    +'      <div class="clearfix mt15">'
                    +'          <div style="float:left; width: 815px; word-wrap: break-word;"><span class="Js_ngname">'+$('.Js_add_ngname').val()+'</span>&nbsp&nbsp&nbsp&nbsp联系电话：<span class="Js_ngphone">'+$('.Js_add_ngphone').val()+'</span>，邮编：<span class="Js_ngzipcode">'+$('.Js_add_ngzipcode').val()+'<span></div>'
                    +'          <a href="javascript:;" style="float:right;" class="c-add-btn Js_szw_defalut">设为默认</a>'
                    +'      </div>'
                    +'  </div>';

                $.ajax({
                    url:'test.php',
                    dataType : 'json',
                    type : 'post',
                    success : function(){
                        $('.Js_add_addresslist').before(chtml).show();
                        $('.Js_add_dress_btn,.Js_add_form').hide();
                        city2.id = '1001';
                        city2.init();
                        add_obj = {cityName: {city: "东城区",province: "北京"}, cityId: "100101"}
                    }

                })

            }else if($('.Js_add_dress_ok').text()=='修改地址'){
                $.ajax({
                    url:'test.php',
                    dataType : 'json',
                    type : 'post',
                    success : function(){
                        $('.Js_ngaddress').eq(ngz_index).text($('.Js_jd_address').val());
                        $('.Js_ngname').eq(ngz_index).text($('.Js_add_ngname').val());
                        $('.Js_ngphone').eq(ngz_index).text($('.Js_add_ngphone').val());
                        $('.Js_ngzipcode').eq(ngz_index).text($('.Js_add_ngzipcode').val());
                        $('.Js_ngsxs').eq(ngz_index).text(strName).parent().attr('city_id',add_obj.cityId);
                        $('.Js_add_dress_cancle').click();
                    }
                })

            }
        }
        
    })
    $('.Js_idverif_ok').click(function(){
        var step = '';
        var name = $('.Js_idvname').val(),
            idnum = $('.Js_idvnum').val(),
            phone = $('.Js_idvphone').val(),
            email = $('.Js_idvemail').val();
        if(name == ''){
            $('.Js_idvname').parents('.tools').addClass('error').next().show();
            step = 'one';
        }else{
            $('.Js_idvname').parents('.tools').removeClass('error').next().hide();
        }
        if(!checkTextForm.idCard(idnum)){
            $('.Js_idvnum').parents('.tools').addClass('error').next().show();
            step = 'one';
        }else{
            $('.Js_idvnum').parents('.tools').removeClass('error').next().hide();
        }
        if(!checkTextForm.cellphone(phone)){
            $('.Js_idvphone').parents('.tools').addClass('error').next().show();
            step = 'one';
        }else{
            $('.Js_idvphone').parents('.tools').removeClass('error').next().hide();
        }
        if(!checkTextForm.email(email)){
            $('.Js_idvemail').parents('.tools').addClass('error').next().show();
            step ='one';
        }else{
            $('.Js_idvemail').parents('.tools').removeClass('error').next().hide();
        }

    });

    $('.Js_add_addresslist').find('a').click(function(){
        $('.Js_add_dress_cancle').click();
        $('.Js_add_newaddress').click()
    })

    $('.Js_add_dress_cancle').click(function(){
        $(this).parents('.Js_add_form').hide().find('input').each(function(){
            $(this).val('');
        });
        city2.id = '1001';
        city2.init();
        $('.Js_add_dress_cancle').text('取消添加');
        $('.Js_add_dress_ok').text('添加地址');
    })

    $('.Js_add_dressdel').live('click',function(){
        if($('.Js_add_dressdel').index($(this))==ngz_index){
            $('.Js_add_addresslist').find('a').click();
        }else if($('.Js_add_dressdel').index($(this))<ngz_index){
            ngz_index--;
        }
        $(this).parents('.n-address-list').remove();
        if($('.n-address-list').length==0){
            $('.Js_add_addresslist').hide();
            $('.Js_add_dress_btn').show();
        }
    })

    $('.Js_add_xg').live('click',function(){
        $('.Js_jd_address').val($(this).prev().prev().find('.Js_ngaddress').text());
        $('.Js_add_ngname').val($(this).parent().next().find('.Js_ngname').text());
        $('.Js_add_ngphone').val($(this).parent().next().find('.Js_ngphone').text());
        $('.Js_add_ngzipcode').val($(this).parent().next().find('.Js_ngzipcode').text());
        city2.id = $(this).prev().prev().attr('city_id');
        city2.init();
        $('.Js_add_form').show();
        $('.Js_add_dress_cancle').text('取消修改');
        $('.Js_add_dress_ok').text('修改地址');
        ngz_index = $('.Js_add_xg').index($(this));
    })

    $('.Js_szw_defalut').live('click',function(){
        $('.Js_szw_zdefalut').replaceWith('<a href="#" style="float:right;" class="c-add-btn Js_szw_defalut">设为默认</a>');
        $(this).replaceWith('<span href="javascript:;" class="n-defaltu Js_szw_zdefalut">已设为默认</span>');
    })






    if($('#Js_account_getmoney').length>0){
        $('#Js_account_getmoney').click(function(){
            var res = 1;
            if(res==1){
                chtml = '<div class="popup">'
                    +'      <div class="popup_message">'
                    +'      <div class="popup_message_notice"><a class="popup_message_alert">您需要先完成身份验证并添加银行卡才能申请提现</a></div>'
                    +'     </div>'
                    +'      <div class="popup_button_single">'
                    +'          <a class="popup_button_close" href="javascript:;" id="Js_getmoney_return">返回</a>'
                    +'      </div>'
                    +'  </div>'
                $.fancybox({
                    fitToView	: false,
                    autoHeight	: true,
                    autoSize	: false,
                    width		: 670,
                    content     : chtml,
                    padding		: 0,
                    beforeShow 	: function () {
                        $('#Js_getmoney_return').click(function(){
                            $.fancybox.close();
                        })
                    }
                })
            }else{}
        })
    }

    if($('#Js_account_getmoney_success').length>0){
        $('#Js_account_getmoney_success').click(function(){
            var res = 1;
            if(res==1){
                chtml = '<div class="popup">'
                    +'      <div class="popup_sucess_image">'
                    +'          <img src="assets/images/popup_sucess.png"/>'
                    +'      </div>'
                    +'      <div class="popup_message">'
                    +'          <div class="popup_message_notice" style="font-size:18px">您已成功提交了提现申请</div>'
                    +'          <div class="popup_message_notice" style="font-size:18px; margin-top:6px">系统审核通过后你将会在1-2个工作日收到提现金额</div>'
                    +'      </div>'
                    +'      <div class="popup_button_single">'
                    +'          <a class="popup_button_close Js_getmoney_return" href="#" id="Js_getmoney_return">关闭</a>'
                    +'      </div>'
                    +'    </div>'
                $.fancybox({
                    fitToView	: false,
                    autoHeight	: true,
                    autoSize	: false,
                    width		: 670,
                    content     : chtml,
                    padding		: 0,
                    beforeShow 	: function () {
                        $('.Js_getmoney_return').click(function(){
                            $.fancybox.close();
                        })
                    }
                })
            }else{}
        })
    }

    if($('#Js_apply_thinktank_candidate').length>0){
        $('#Js_apply_thinktank_candidate').click(function(){
            var res = 1;
            if(res==1){
                chtml = '<div class="popup">'
                    +'      <div class="popup_message">'
                    +'      <div class="popup_message_notice"><a class="popup_message_alert">您需要先完成身份验证才能申请成为创意世界智库候选专家</a></div>'
                    +'     </div>'
                    +'      <div class="popup_button_single">'
                    +'          <a class="popup_button_close Js_apply_return" href="javascript:;">返回</a>'
                    +'      </div>'
                    +'  </div>'
                $.fancybox({
                    fitToView	: false,
                    autoHeight	: true,
                    autoSize	: false,
                    width		: 670,
                    content     : chtml,
                    padding		: 0,
                    beforeShow 	: function () {
                        $('.Js_apply_return').click(function(){
                            $.fancybox.close();
                        })
                    }
                })
            }else{}
        })
    }





    var ngzb_index = null;
    if($('.Js_add_newbank').length>0){
        $('.Js_add_newbank').click(function(){
            var res = 0;/*值为1时，弹出弹出层*/
            if(res==1){
                chtml = '<div class="popup">'
                    +'      <div class="popup_message">'
                    +'      <div class="popup_message_notice"><a class="popup_message_alert">您需要先完成身份验证才能添加银行卡</a></div>'
                    +'     </div>'
                    +'      <div class="popup_button_single">'
                    +'          <a class="popup_button_close" href="javascript:;" id="Js_bank_return">返回</a>'
                    +'      </div>'
                    +'  </div>'
                $.fancybox({
                    fitToView	: false,
                    autoHeight	: true,
                    autoSize	: false,
                    width		: 670,
                    content     : chtml,
                    padding		: 0,
                    beforeShow 	: function () {
                        $('#Js_bank_return').click(function(){
                            $.fancybox.close();
                        })
                    }
                })
            }else{
                if($('.Js_addbank_form').is(':hidden')){
                    $('.Js_addbank_form').show();
                }else{
                    $('.Js_addbank_form').hide();
                }
            }
        })
    }

    $('.Js_add_bank_ok').click(function(){
        var step = '';
        if($('.Js_bankname').text() == ''){
            $('.Js_bankname').parents('.tools').addClass('error').nextAll('.errorMsg').show();
            step = 'one';
        }else{
            $('.Js_bankname').parents('.tools').removeClass('error').nextAll('.errorMsg').hide();
        }
        if(!checkTextForm.nonZeroBegin($('.Js_add_ngbanknum').val())){
            $('.Js_add_ngbanknum').parents('.tools').addClass('error').next().show();
            step = 'one';
        }else{
            $('.Js_add_ngbanknum').parents('.tools').removeClass('error').next().hide();
        }
        if(step == ''){
            var kongstr = '';
            var banknumstr = '';
            for(var i=0; i<$('.Js_add_ngbanknum').val().length; i++){
                if(i < $('.Js_add_ngbanknum').val().length-4){
                    if(i%4 == 0 && i!=0){
                        kongstr += ' ×';
                    }else{
                        kongstr += '×';
                    }
                }else{
                    if(i==$('.Js_add_ngbanknum').val().length-4){
                        banknumstr += 	' '+$('.Js_add_ngbanknum').val()[i];
                    }else{
                        banknumstr += 	$('.Js_add_ngbanknum').val()[i];
                    }
                }
            }
            if($('.Js_add_bank_ok').text()=='确认添加'){
                var chtml = '<div class="n-bank-list">'
                    +'		<div class="clearfix">'
                    +'			<span style="float:left;" city_id="'+bank_obj.cityId+'">银行：<span class="Js_ngbankname">'+$('.Js_bankname').text()+'</span></span>'
                    +'			<a href="javascript:;" class="n-del Js_add_bankdel">删除</a>'
                    +'			<a href="javascript:;" class="n-xg Js_addbank_xg">修改</a>'
                    +'		</div>'
                    +'		<div class="clearfix mt15">'
                    +'			<div style="float:left;" b_ndsbb="'+$('.Js_add_ngbanknum').val()+'">卡号：<span style="font-size:14px; color:#cf6e6e; font-weight:bold;" class="Js_xxxx_txt">'+kongstr+'</span><span class="Js_banknum"  style="font-size:14px; font-weight:bold;"> '+banknumstr+'</span></div>'
                    +'			<a href="javascript:;" style="float:right;" class="c-add-btn Js_szwb_defalut">设为默认</a>'
                    +'		</div>'
                    +'	</div>'
                $.ajax({
                    url:'test.php',
                    dataType : 'json',
                    type : 'post',
                    success : function(){
                        $('.Js_add_banklist').before(chtml).show();
                        $('.Js_add_bank_btn,.Js_addbank_form').hide();
                        city3.id = '1001';
                        city3.init();
                        bank_obj = {cityName: {city: "东城区",province: "北京"}, cityId: "100101"}
                    }
                })
            }else if($('.Js_add_bank_ok').text()=='确认修改'){
                $.ajax({
                    url:'test.php',
                    dataType : 'json',
                    type : 'post',
                    success : function(){
                        $('.Js_ngbankname').eq(ngzb_index).text($('.Js_bankname').text());
                        $('.Js_xxxx_txt').eq(ngzb_index).text(kongstr);
                        $('.Js_banknum').eq(ngzb_index).text(banknumstr);
                        $('.Js_banknum').eq(ngzb_index).parent().attr('b_ndsbb',$('.Js_add_ngbanknum').val());
                        $('.Js_ngbankname').eq(ngzb_index).parent().attr('city_id',bank_obj.cityId);
                        $('.Js_add_bank_cancle').click();
                    }

                })
            }
        }

    })

    $('.Js_add_banklist').find('a').click(function(){
        $('.Js_add_bank_cancle').click();
        $('.Js_add_newbank').click();
    })

    $('.Js_add_bank_cancle').click(function(){
        $(this).parents('.Js_addbank_form').hide().find('input').each(function(){
            $(this).val('');
            $('.Js_bankname').text('');
        });
        city3.id = '1001';
        city3.init();
        $('.Js_add_bank_cancle').text('取消添加');
        $('.Js_add_bank_ok').text('确认添加');
    })

    $('.Js_add_bankdel').live('click',function(){
        if($('.Js_add_bankdel').index($(this))==ngzb_index){
            $('.Js_add_banklist').find('a').click();
        }else if($('.Js_add_bankdel').index($(this))<ngzb_index){
            ngzb_index--;
        }
        $(this).parents('.n-bank-list').remove();
        if($('.n-bank-list').length==0){
            $('.Js_add_banklist').hide();
            $('.Js_add_bank_btn').show();
        }
    })

    $('.Js_addbank_xg').live('click',function(){
        $('.Js_bankname').text($(this).prev().prev().find('.Js_ngbankname').text());
        $('.Js_add_ngbanknum').val($(this).parent().next().find('.Js_banknum').parent().attr('b_ndsbb'));
        city3.id = $(this).prev().prev().attr('city_id');
        city3.init();
        $('.Js_addbank_form').show();
        $('.Js_add_bank_cancle').text('取消修改');
        $('.Js_add_bank_ok').text('确认修改');
        ngzb_index = $('.Js_addbank_xg').index($(this));
    })

    $('.Js_szwb_defalut').live('click',function(){
        $('.Js_szwb_zdefalut').replaceWith('<a href="javascript:;" style="float:right;" class="c-add-btn Js_szwb_defalut">设为默认</a>');
        $(this).replaceWith('<span href="javascript:;" class="n-defaltu Js_szwb_zdefalut">已设为默认</span>');
    })
}

/*function syCookie(){
 checkCookie();
 }

 function getCookie(c_name){
 if (document.cookie.length>0){
 c_start=document.cookie.indexOf(c_name + "=")
 if (c_start!=-1){
 c_start=c_start + c_name.length+1
 c_end=document.cookie.indexOf(";",c_start)
 if (c_end==-1) c_end=document.cookie.length
 return unescape(document.cookie.substring(c_start,c_end))
 }
 }
 return ""
 }

 function setCookie(c_name,value,expiredays)
 {
 var exdate=new Date()
 exdate.setDate(exdate.getDate()+expiredays)
 document.cookie=c_name+ "=" +escape(value)+
 ((expiredays==null) ? "" : "; expires="+exdate.toGMTString())
 }

 function checkCookie(){
 username=getCookie('username');
 if (username!=null && username!=""){
 //alert('Welcome again '+username+'!');
 }else{
 var thtml = '<div class="sy-ts-div Js_sytsdiv">'
 +'		点击这里可以展开网站导航'
 +'		<s></s>'
 +'		<a href="javascript:;" id="Js_gbclose"></a>'
 +'	</div>'
 username = "gogogo";
 $('.Js_loc_ts').append(thtml).find('i').addClass('arrcur');
 $('#Js_gbclose').click(function(){
 $(this).parent().remove();
 $('.Js_loc_ts').find('i').removeClass('arrcur');
 })
 $('.Js_sytsdiv').mouseleave(function(e){
 if($(e.relatedTarget).hasClass("Js_location")){
 $('.Js_location').mouseenter();
 }
 })
 window.setTimeout(function(){
 $('.Js_sytsdiv').remove();
 $('.Js_loc_ts').find('i').removeClass('arrcur');
 },5000);
 if (username!=null && username!=""){
 setCookie('username',username,365);
 }
 }
 }*/
function indexItemChange(){
    var objA = $("#Js_lNews_tab");
    var objB = objA.find("li");
    objB.mouseenter(function(){
        var index =   objB.index($(this));
        objB.find('a').removeClass('ok');
        $(this).find('a').addClass('ok');
        $('.Js_news_box').hide().eq(index).show();
    });

    var objApp = $(".Js_app_list").find("li");
    objApp.mouseenter(function(){
        var index =   objApp.index($(this));
        console.log(index);
        objApp.find('a').removeClass('ok');
        $(this).find('a').addClass('ok');
        $('.Js_app_item').hide().eq(index).show();
    });
}

var arrivedAtBottom = function () {
    return $(document).scrollTop() + $(window).height() == $(document).height();
}
function getList() {
    var boxes = [];
    for (var i=0; i < 1; i++ ) {
        var ncthml = '<li class="posts-item" data-type="2"><div class="posts-box clearfix"><div class="tit-bq bfs">美女</div><div class="post-user-info clearfix"><a href="#"><img class="u-avatar left" src="assets/temp/9.png" alt=""></a><p class="u-name left"><a class="name Js_visitCard" href="#">如果我不是郦道元怎么办</a><span class="time">下午12:53</span></p></div><div class="post-pic"><img src="assets/temp/p5.jpg" alt=""></div><div class="post-content"><a>由JueLab发起的项目[成为一位皮革匠人第四期]上线了。</a></div><div class="psot-like clearfix"><a class="like-box zan Js_zan" href="javascript:;">464</a><a class="like-box zhuan" href="javascript:;">600</a><input class="Js_noneinput" type="text" placeholder="添加您的评论..." /></div><div class="post-comment" style="display:none;"><div class="post-c-con"><div class="p-c-title"><b>0</b> 条评论</div><ul class="Js_itemlist clearfix"></ul></div><div class="post-repeat-wrap"><div class="post-repeat-text"><input class="Js_hinput" type="text" placeholder="添加您的评论..."></div><div class="post-repeat-box Js_subtextarea"><div class="p-c-list "><img class="p-c-avatar" src="assets/temp/7.png" alt=""><div class="p-c-con"><textarea class="sub-text" name="" cols="30" rows="10"></textarea><div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div></div></div></div></div></div></div></li>'
        boxes.push(ncthml);
    }
    return boxes.join("");
};

var schollJson = ['电子科技电器大学','成都电子学院','四川大学'];

function c_scholl(str,val){
    var re = new RegExp(val,"gi");
    return str.replace(re,'<b>'+val+'</b>');
}
//获取当然日期的下个月
function jsInputDate(){
    var  a = new Date();
    var  y = a.getFullYear();
    var  m = a.getMonth()+2;
    var  d = a.getDate();
    if(m == 13){y = y+1; m =1;}
    if(d == 31 && m ==2){d = 28;}
    if(d == 31 && (m ==4 || m==6 || m ==9 || m ==11)){d=30;}
    var  x = y+"/"+m+"/"+d;
    $('.Js_input_date').val(x);
}
//个人页面技能点击增加1次
function jsSkillTag(){
    var arr =[];
    $(".skill-tag").each(function(){
        arr.push(parseInt($(this).find("span").text()));
    });
    $(".skill-tag").click(function(){
        var i = $(".skill-tag").index($(this));
        var objA = $(this).find("span");
        if(parseInt(objA.text())<= arr[i]){
            objA.text(parseInt(objA.text())+1)}

    });
}

$(function() {
    userlogin();
    user_register();
    jsSkillTag()//个人页面技能点击增加1次
    jsInputDate()//获取当然日期的下个月
    $('.Js_video_del').click(function(){
        $(this).prev().find('input').val('');
    })
    $('.Js_db_area_input').keyup(function(){
        var myself = $(this);
        $.ajax({
            url:'test.php',
            dataType : 'json',
            type : 'post',
            success : function(){
                var val = myself.val();
                var showListItem = myself.next().children();
                for(var i=0; i<showListItem.length; i++){
                    if(showListItem.eq(i).text().indexOf(val)<0){
                        showListItem.eq(i).remove();
                        if(i==showListItem.length-1){
                            myself.next().hide();
                        }
                    }
                }

                for(var i=0; i<schollJson.length; i++){
                    if(schollJson[i].indexOf(val)>=0 && val){
                        var hflag = true;
                        showListItem.each(function(){
                            if($(this).text()==schollJson[i]){
                                $(this).html(c_scholl($(this).text(),val));
                                hflag = false;
                            }
                        })
                        if(hflag){
                            if(myself.next().children().length>0){
                                var chtml = '<li class="Js_db_area_list">'+c_scholl(schollJson[i],val)+'</li>';
                            }else{
                                var chtml = '<li class="Js_db_area_list last">'+c_scholl(schollJson[i],val)+'</li>';
                            }
                            myself.next().append(chtml).show();
                        }
                    }
                }
                if(val.length==0) myself.next().html('').hide();
            }
        })
    }).focus(function(){
            var showListItem = $(this).next().children();
            $('.Js_db_area').hide();
            $(document).one('click.arealist',function(){
                $('.Js_db_area').hide();
            })
            if(showListItem.length>0)
                $(this).next().show();
        }).click(function(){return false;})

    $('.Js_db_area_list').live('click',function(){
        $(this).parent().prev().val($(this).text());
        $(this).parent().html('');
    })

    indexItemChange();
    $('.Js_zan').live('click',function(){
        if($(this).text()=='赞'){
            $(this).text('0');
        }else{
            $(this).text(parseInt($(this).text(),10)+1);
        }
    })
    if($('.Js_friendArea_filter').length>0){
        $('.Js_friendArea_filter').find('a').click(function(){
            var index = $('.Js_friendArea_filter').find('a').removeClass('current').index($(this));
            $(this).addClass('current');
            $('.Js_wrapList li').not($('.Js_wrapList li').eq(1)).hide();
            $('.Js_wrapList li[data-type='+index+']').show();
            container.masonry();

        });

        $(window).scroll(function(){

            if(arrivedAtBottom()){
                /**/
                console.log(container.html());
                var $boxes = $(getList());
                container.append($boxes).masonry('appended',$boxes);
            }
        })
    }

    $('.Js_label_ok').live('click',function(e){
        if($(this).attr('type')=='checkbox'){
            if($(this).parent().hasClass('ok')){
                $(this).parent().removeClass('ok');
            }else{
                $(this).parent().addClass('ok');
            }
        }else if($(this).find('input').attr('type')=='radio'){
            var s = $(this).find('input').attr('name');
            $('.Js_label_ok input[name='+s+']').parent().removeClass('ok');
            $(this).addClass('ok');
        }
    })


    $('.Js_labelck_ok').live('click', function(e){
        $('.Js_labelck_ok').removeClass('ok');
        if($(this).find('input').attr('type')=='radio'){
            if($(this).hasClass('ok')){
                $(this).removeClass('ok');
                $(this).find('input').attr('checked',false);
            }else{
                $(this).addClass('ok');
                $(this).find('input').attr('checked',true);
            }
            return false;
        }else if($(this).find('input').attr('type')=='radio'){
            var s = $(this).find('input').attr('name');
            $('.Js_labelck_ok input[name='+s+']').parent().removeClass('ok');
            $(this).addClass('ok');
        }

        e.stopPropagation();
    })

    $('.Js_bank_ok').live('mouseup',function(e){
        var s = $(this).find('input').attr('name');
        $('.Js_bank_ok input[name='+s+']').next().removeClass('b-cs').addClass('b-hs');
        $(this).find('img').removeClass('b-hs').addClass('b-cs');
        e.stopPropagation();
    })
    // var asideHeight = $("body").height();
    // $(".aside").css("min-height",asideHeight+"px")
    // alert(asideHeight);
    var page_list_three = 0;
    $(window).scroll(function(){
        if($(document).scrollTop()>100){
            $('.Js_toTop').parent().show();
        }else{
            $('.Js_toTop').parent().hide();
        }
        if($('.Js_page_list_item').length>0){
            if(arrivedAtBottom()) {
                //...已到达最底部
                if(page_list_three==3){
                    $('.Js_page_list_btn').show()
                    return;
                }
                for(var i=0; i<9; i++){
                    var cthml = '<div class="large-4 small-6 columns">'
                        +'  <a href="./project-going.php">'
                        +'	<div class="match-box">'
                        +'	  <div class="listItemImg">'
                        +'		<div class="rightBox heart"></div>'
                        +'		<s class="heart"></s>'
                        +'		<div class="bottomBox"></div>'
                        +'		<img src="assets/temp/item.jpg" alt="">'
                        +'	  </div>'
                        +'	  <div class="contextDiv">'
                        +'		<h3>侏罗纪的召唤-蚀刻暴龙拼装模型 -杰思模型</h3>'
                        +'		<div class="progress"></div>'
                        +'		<p class="promulgatorName Js_visitCard">发布者名字</p>'
                        +'		<p class="promulgatorMoney">10,000元</p>'
                        +'		<span class="status">当前竞拍价格</span>'
                        +'	  </div>'
                        +'	  <div class="bottomDiv clearfix">'
                        +'		<span class="left">剩余7天</span>'
                        +'		<span class="right bq">30次竞拍</span>'
                        +'	  </div>'
                        +'	</div>'
                        +' </a>'
                        +'</div>'
                    $('.Js_page_list_btn').before(cthml);
                }
                page_list_three++;
            }
        }
    })

    var page_list_btnnum = 0;
    $('.Js_page_list_next').click(function(){
        if(page_list_btnnum==$(this).prev().children().length-1){
            return;
        }
        if(page_list_btnnum==$(this).prev().children().length-2){
            $('.Js_page_list_next').each(function(){
                $(this).css('background-position','0px -18px')
            })
        }
        page_list_btnnum++;
        $('.Js_page_list_prev').css('background-position','0 -36px');
        $('.Js_page_list_next').each(function(){
            $(this).prev().children().removeClass('current');
            $(this).prev().children().eq(page_list_btnnum).addClass('current');
        })
    }).mouseenter(function(){
            if(page_list_btnnum==$(this).prev().children().length-1){
                return;
            }
            $(this).css('background-position','0 -90px');
        }).mouseleave(function(){
            if(page_list_btnnum==$(this).prev().children().length-1){
                return;
            }
            $(this).css('background-position','0 -54px');
        })

    $('.Js_page_list_prev').click(function(){
        if(page_list_btnnum==0){
            return;
        }
        if(page_list_btnnum==1){
            $('.Js_page_list_prev').each(function(){
                $(this).css('background-position','0px 0')
            })
        }
        page_list_btnnum--;
        $('.Js_page_list_next').css('background-position','0 -54px');
        $('.Js_page_list_prev').each(function(){
            $(this).next().children().removeClass('current');
            $(this).next().children().eq(page_list_btnnum).addClass('current');
        })

    }).mouseenter(function(){
            if(page_list_btnnum==0){
                return;
            }
            $(this).css('background-position','0 -72px');
        }).mouseleave(function(){
            if(page_list_btnnum==0){
                return;
            }
            $(this).css('background-position','0 0');
        })

    $('.Js_page_list_itemNum').children().click(function(){
        var l_index = $(this).parent().children().index($(this));
        page_list_btnnum = l_index;
        $('.Js_page_list_itemNum').each(function(){
            $(this).children().removeClass('current');
            $(this).children().eq(page_list_btnnum).addClass('current');
            if(page_list_btnnum==0){
                $('.Js_page_list_next').css('background-position','0 -54px');
                $('.Js_page_list_prev').css('background-position','0 0');
            }else if(page_list_btnnum==$(this).children().length-1){
                $('.Js_page_list_next').css('background-position','0 -18px');
                $('.Js_page_list_prev').css('background-position','0 -36px');
            }else{
                $('.Js_page_list_next').css('background-position','0 -54px');
                $('.Js_page_list_prev').css('background-position','0 -36px');
            }
        })
    })

    $('.Js_net_rmb_account').click(function(){
        var o = [
            {
                data:'2013/01/27 22:21:22',
                content : '淘宝购物-聚壹之家牛津百纳箱聚壹之家牛',
                money : '-20.00',
                account : '110.65',
                cz_href : 'http://www.baidu.com'
            },{
                data:'2013/01/27 22:21:22',
                content : '淘宝购物-聚之家牛1',
                money : '-20.00',
                account : '110.65',
                cz_href : 'http://www.baidu.com'
            },{
                data:'2013/01/27 22:21:22',
                content : '淘宝购物-聚纳箱聚壹2之家牛',
                money : '-20.00',
                account : '110.65',
                cz_href : 'http://www.baidu.com'
            },{
                data:'2013/01/27 22:21:22',
                content : '淘宝津百纳箱聚壹3之家牛',
                money : '-20.00',
                account : '110.65',
                cz_href : 'http://www.baidu.com'
            },{
                data:'2013/01/27 22:21:22',
                content : '淘宝家牛津百纳箱聚壹4之家牛',
                money : '-20.00',
                account : '110.65',
                cz_href : 'http://www.baidu.com'
            }
        ]
        for(var i=0; i<o.length; i++){
            var td_n = 0;
            for(var j in o[i]){
                if(td_n==4){
                    $('.Js_rmb_account_list').find('tr').eq(i+1).find('td').eq(td_n).html('<a href="'+o[i][j]+'">详情</a>');
                }else{
                    $('.Js_rmb_account_list').find('tr').eq(i+1).find('td').eq(td_n).text(o[i][j]);
                }
                td_n++;
            }
        }
    })
    //订单数据开始
    $('.Js_net_order_account').click(function(){
        var o = [
            {
                data:'2013/01/27 22:21:22',
                user: '这是个用户',
                content : '淘宝购物-聚壹之家牛津百纳箱聚壹之家牛',
                stat : '交易关闭',
                account : '110.65',
                cz_h : '详情'
            },{
                data:'2013/02/27 22:21:22',
                user: '这也是个用户',
                content : '淘宝购物-聚之家牛1',
                stat : '等待发货',
                account : '150.65',
                cz_h : '详情'
            },{
                data:'2013/03/27 22:21:22',
                content : '淘宝购物-聚纳箱聚壹2之家牛',
                stat : '等待收货',
                account : '210.00',
                cz_h : '确认收货'
            },{
                data:'2013/06/17 22:21:22',
                user: '这还是个用户',
                content : '淘宝津百纳箱聚壹3之家牛',
                stat : '交易成功',
                account : '110.65',
                cz_h : '详情'
            },{
                data:'2013/07/17 22:21:22',
                user: '这又是也是个用户',
                content : '淘宝家牛津百纳箱聚壹4之家牛',
                stat : '交易成功',
                account : '110.65',
                cz_h : '详情'
            }
        ]
        for(var i=0; i<o.length; i++){
            var td_n = 0;
            for(var j in o[i]){
                if(td_n==4){
                    $('.Js_order_account_list').find('tr').eq(i+1).find('td').eq(td_n).html('<a href="'+o[i][j]+'"></a>');
                }else{
                    $('.Js_order_account_list').find('tr').eq(i+1).find('td').eq(td_n).text(o[i][j]);
                }
                td_n++;
            }
        }
    })
    //订单数据结束

    $('.Js_createindex_account').click(function(){
        var o = [
            {
                data:'2013/01/27 22:21:22',
                content : '竞猜',
                money : '+100',
                account : '50'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+120',
                account : '150'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜',
                money : '+100',
                account : '50'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+120',
                account : '150'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜',
                money : '+100',
                account : '50'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+120',
                account : '150'
            }
        ]
        for(var i=0; i<o.length; i++){
            var td_n = 0;
            for(var j in o[i]){
                $('.Js_createIndex_account_list').find('tr').eq(i+1).find('td').eq(td_n).text(o[i][j]);
                td_n++;
            }
        }
    })


    $('.Js_time_create_filter').click(function(){
        $('.Js_time_create_filter').removeClass('current');
        $(this).addClass('current');
        var o = [
            {
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+100',
                account : '50'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+120',
                account : '150'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+100',
                account : '50'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+120',
                account : '150'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜',
                money : '+100',
                account : '50'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+120',
                account : '150'
            }
        ]
        for(var i=0; i<o.length; i++){
            var td_n = 0;
            for(var j in o[i]){
                $('.Js_createIndex_account_list').find('tr').eq(i+1).find('td').eq(td_n).text(o[i][j]);
                td_n++;
            }
        }

        var nd = new Date();
        $('#dateone').val($(this).attr('data-type'));
        $('#datetwo').val(nd.getFullYear() + '/' + (nd.getMonth()+1) + '/' + nd.getDate());
    })

    $('.Js_zs_create_filter').click(function(){
        $('.Js_zs_create_filter').removeClass('current');
        $(this).addClass('current');
        var o = [
            {
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+100',
                account : '50'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+120',
                account : '150'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+100',
                account : '50'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+120',
                account : '150'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜',
                money : '+100',
                account : '50'
            },{
                data:'2013/01/27 22:21:22',
                content : '竞猜中奖',
                money : '+120',
                account : '150'
            }
        ]
        for(var i=0; i<o.length; i++){
            var td_n = 0;
            for(var j in o[i]){
                $('.Js_createIndex_account_list').find('tr').eq(i+1).find('td').eq(td_n).text(o[i][j]);
                td_n++;
            }
        }
    })


    $('.Js_time_filter').click(function(){
        $('.Js_time_filter').removeClass('current');
        $(this).addClass('current');
        var o = [
            {
                data:'2013/01/27 22:21:22',
                content : '纳箱聚壹之家牛',
                money : '-22.00',
                account : '111.65',
                cz_href : 'http://www.baidu.com'
            },{
                data:'2013/01/27 22:21:22',
                content : '纳箱聚壹之家牛',
                money : '-22.00',
                account : '111.65',
                cz_href : 'http://www.baidu.com'
            },{
                data:'2013/01/27 22:21:22',
                content : '纳箱聚壹之家牛',
                money : '-22.00',
                account : '111.65',
                cz_href : 'http://www.baidu.com'
            },{
                data:'2013/01/27 22:21:22',
                content : '纳箱聚壹之家牛',
                money : '-22.00',
                account : '111.65',
                cz_href : 'http://www.baidu.com'
            },{
                data:'2013/01/27 22:21:22',
                content : '纳箱聚壹之家牛',
                money : '-22.00',
                account : '111.65',
                cz_href : 'http://www.baidu.com'
            }
        ]
        for(var i=0; i<o.length; i++){
            var td_n = 0;
            for(var j in o[i]){
                if(td_n==4){
                    $('.Js_rmb_account_list').find('tr').eq(i+1).find('td').eq(td_n).html('<a href="'+o[i][j]+'">详情</a>');
                }else{
                    $('.Js_rmb_account_list').find('tr').eq(i+1).find('td').eq(td_n).text(o[i][j]);
                }
                td_n++;
            }
        }
        var nd = new Date();
        $('#dateone').val($(this).attr('data-type'));
        $('#datetwo').val(nd.getFullYear() + '/' + (nd.getMonth()+1) + '/' + nd.getDate());
    })

    $('.Js_rmb_record_filter').click(function(){
        $('.Js_rmb_record_filter').removeClass('current');
        $(this).addClass('current');
        var o = [
            {
                data:'2013/01/27 22:21:22',
                content : '纳箱聚壹之家牛',
                money : '-24.00',
                account : '123.65',
                cz_href : 'http://www.baidu.com'
            },{
                data:'2013/01/27 22:21:22',
                content : '纳箱聚壹之家牛',
                money : '-212.00',
                account : '113.65',
                cz_href : 'http://www.baidu.com'
            },{
                data:'2013/01/27 22:21:22',
                content : '纳箱聚壹之家牛',
                money : '-212.00',
                account : '134.65',
                cz_href : 'http://www.baidu.com'
            },{
                data:'2013/01/27 22:21:22',
                content : '纳箱聚壹之家牛',
                money : '-23.00',
                account : '111.65',
                cz_href : 'http://www.baidu.com'
            },{
                data:'2013/01/27 22:21:22',
                content : '纳箱聚壹之家牛',
                money : '-21.00',
                account : '161.65',
                cz_href : 'http://www.baidu.com'
            }
        ]
        for(var i=0; i<o.length; i++){
            var td_n = 0;
            for(var j in o[i]){
                if(td_n==4){
                    $('.Js_rmb_account_list').find('tr').eq(i+1).find('td').eq(td_n).html('<a href="'+o[i][j]+'">详情</a>');
                }else{
                    $('.Js_rmb_account_list').find('tr').eq(i+1).find('td').eq(td_n).text(o[i][j]);
                }
                td_n++;
            }
        }
    })


    //syCookie();
    userfileitems();
    syUserfile();
    $('.Js_pageChange').each(function(i){
        pageChange($(this),'left'+i,'right'+i);
    })
    var all_pageList = $('.Js_pageChange li');
    $('.Js_match_filter').click(function(){
        $('.Js_match_filter').removeClass('on');
        $(this).addClass('on');
        var index = $('.Js_match_filter').index($(this));
        $('.Js_pageChange ul').html(all_pageList);
        $('.Js_pageChange li').removeAttr('style').not($('.Js_pageChange li[data-type="'+index+'"]')).remove();
        pageChange($('.Js_pageChange'),'left1','right1');
    })

    $('.Js_match_filter_all').click(function(){
        $('.Js_pageChange ul').html(all_pageList);
        pageChange($('.Js_pageChange'),'left1','right1');
    })
    //pageChange($('.Js_pageChange1'),'left2','right2');
    navGmaps();
    navScroll();
    share();
    JS_FINDFRIEND_COMMEN.init();
    JS_GMAP_ADDRESS_SEARCH.init();
    /*if($('.listItemImg').length>0){
     $('.listItemImg img').load(function(){
     if($(this).width()>$(this).parents('.match-box').width()){
     var l = -(($(this).width() - $(this).parents('.match-box').width()) / 2);
     $(this).css({'position':'absolute','left':l});
     }
     })
     }*/
    $('.Js_toTop').click(function(){
        if(!$('html').is(":animated")||!$('body').is(":animated"))
            $('html,body').animate({scrollTop:0},'slow',function(){});
    })

    $('.Js_invistd').click(function(){
        if($('.Js_dp').is(":animated")) return;
        if($('.Js_dp').is(':hidden')){
            $('.Js_invistd_c').show();
            $('.Js_invistd_b').show();
            $('.Js_dp').slideDown();
        }else{
            if($('.Js_dp').attr('u-data')=='Js_invistd_data'){
                $('.Js_dp').slideUp(function(){
                    $('.Js_invistd_c').hide();
                    $('.Js_invistd_b').hide();
                });
            }else{
                $('.Js_remark_c').hide();
                $('.Js_remark_b').hide();
                $('.Js_invistd_c').show();
                $('.Js_invistd_b').show();
            }
        }
        $('.Js_dp').attr('u-data','Js_invistd_data');
    })

    $('.Js_remark').click(function(){
        if($('.Js_dp').is(":animated")) return;
        if($('.Js_dp').is(':hidden')){
            $('.Js_remark_c').show();
            $('.Js_remark_b').show();
            $('.Js_dp').slideDown();
        }else{
            if($('.Js_dp').attr('u-data')=='Js_remark_data'){
                $('.Js_dp').slideUp(function(){
                    $('.Js_remark_c').hide();
                    $('.Js_remark_b').hide();
                });
            }else{
                $('.Js_invistd_c').hide();
                $('.Js_invistd_b').hide();
                $('.Js_remark_c').show();
                $('.Js_remark_b').show();
            }
        }
        $('.Js_dp').attr('u-data','Js_remark_data')
    })

    $('.Js_t_remark').live('click',function(){
        var obj = $(this).parents(".u_listItem").next(".task-expcomment");
        var that = $(this);
        obj.find('.Js_exp_c').show();
        obj.find('.Js_winner_c').hide();
        obj.find('.Js_invistd_c').hide();
        obj.find('.Js_tRemark_c').show();
        obj.find('.Js_invistd_t').hide();
        obj.css('background','#e6e6e6');
        if(obj.is(':hidden')){
            $('.Js_explist').show();
            obj.slideDown("fast");
            that.addClass("opened").parents(".workbottom").find(".Js_arr").css('right','45px').show();
        }else{
            if(obj.attr('u-data')=='Js_exp_m'){
                obj.slideUp("fast",function(){
                    that.removeClass("opened").parents(".workbottom").find(".Js_arr").hide();
                });
            }else{
                obj.find('.Js_winner_c').hide();
                obj.find('.Js_exp_remark_c').hide();
                obj.find('.Js_exp_c').show();
                that.parents(".workbottom").find(".Js_arr1").hide();
                that.parents(".workbottom").find(".Js_arr").css('right','45px').show();
                obj.css('background','#e6e6e6');
            }
        }

        obj.attr('u-data','Js_exp_m');

    })

    $('.Js_tcan_remark').live('click',function(){
        $(this).parents('.Js_tRemark_c').hide();
    })

    $('.Js_tcan_invistd').live('click',function(){
        $(this).parents('.Js_tInvistd_c').hide();
    })

    $('.Js_invistd_t').live('click',function(){
        var obj = $(this).parents(".task-expcomment");
        $(this).parents('.Js_parents').find('.Js_tRemark_c').hide();
        if($(this).parents('.Js_parents').find('.Js_invistd_c').is(':visible')){
            $(this).parents('.Js_parents').find('.Js_invistd_c').hide();
        }else{
            $(this).parents('.Js_parents').find('.Js_invistd_c').show();
        }
        obj.attr('u-data','Js_exp_s');
    })

    $('.Js_cant_invistd').live('click',function(){
        $(this).parents('.Js_tInvistd_c').hide();
    })

    $('.Js_pro_upadate').click(function(){
        if($('.js_pro_c').is(":animated")) return;
        if($('.js_pro_c').is(':hidden')){
            $('.Js_pro_b').show();
            $('.js_pro_c').slideDown();
        }else{
            if($('.Js_file_delete').length>0){
                $('.Js_file_delete').click();
            }
            $('#Js_text_con,#Js_update_tit').val('');
            $('#js_context_update').val('').prev().html('<p>&#8203</p>');
            $('.js_pro_c').slideUp(function(){
                $('.Js_pro_b').hide();
            });
        }

    })

    $('.Js_charity').click(function(){
        if($('.Js_charity_c').is(":animated")) return;
        if($('.Js_charity_c').is(':hidden')){
            $('.Js_charity_b').show();
            $('.Js_charity_c').slideDown();
        }else{
            $('.Js_charity_c').slideUp(function(){
                $('.Js_charity_b').hide();
            });
        }
    })

    $('.Js_starEx').click(function(){
        if($('.Js_starEx_c').is(":animated")) return;
        if($('.Js_starEx_c').is(':hidden')){
            $('.Js_starEx_b').show();
            $('.Js_starEx_c').slideDown();
        }else{
            $('.Js_starEx_c').slideUp(function(){
                $('.Js_starEx_b').hide();
            });
        }
    })

    $('.Js_recharge,.Js_cancle_buy').click(function(){
        if($('.Js_recharge_c').is(":animated")) return;
        if($('.Js_recharge_c').is(':hidden')){
            $('.Js_recharge_b').show();
            $('.Js_recharge_c').slideDown();
        }else{
            $('.Js_recharge_c').slideUp(function(){
                $('.Js_recharge_b').hide();
            });
        }
    })

    $('.Js_pay').mouseenter(function(){
        $(this).next().show();
    }).mouseleave(function(){
            $(this).next().hide();
        })

    $('.Js_pay_menu').mouseenter(function(){
        $(this).show();
    }).mouseleave(function(){
            $(this).hide();
        })

    $(window).resize(function(){
        $(".aside").height(document.documentElement.clientHeight-76);

        var n = parseInt((document.documentElement.clientHeight*2/3)/86)
        $(".info-box .info-list").css("max-height",86*n-6)
        //$(".info-box .info-list").css("max-height",document.documentElement.clientHeight*2/3)
        $(".search-result").css("max-height",document.documentElement.clientHeight*2/3)

    })

    var onshow = false;
    var opening = 0;
    $(".Js_location").bind("mouseenter",function(e){
        var s = e.target || e.srcElement;
        opening = 1;
        $(".aside").show();
        $(".aside").stop().animate({
            left:0
        }, 50 ,function(){
            onshow = true;
            opening = 2;
            //is_close=getCookie('isclose');
            if($('.Js_sytsdd').length==0){
                /*var thtml = '<div class="sy-ts-div Js_sytsdd" style="z-index:999999">'
                 +'		点击这里可以收回网站导航'
                 +'		<s></s>'
                 +'		<a href="javascript:;" id="Js_zkclose"></a>'
                 +'	</div>'*/
                is_close = "ok";
                //$('.Js_loc_xm').append(thtml);
                $('.Js_ipad_click').prev().css('background-position','-4px -266px');
                $('#Js_zkclose').click(function(){
                    $(this).parent().remove();
                    $('.Js_ipad_click').prev().css('background-position','-4px -245px');
                })
                window.setTimeout(function(){
                    $('.Js_sytsdd').remove();
                    $('.Js_ipad_click').prev().css('background-position','-4px -245px');
                },5000);
                $('.Js_sytsdd').css({left:$('.Js_ipad_click').prev().offset().left-33,top:$('.Js_ipad_click').prev().offset().top+28})
                /*if (username!=null && username!=""){
                 setCookie('isclose',username,365);
                 }*/
                $('#Js_silder_bar').removeClass('in').addClass('on');
            }
        });
    })
    var i_timer = null;
    $(".aside").bind("mouseleave",function(e){
        if(!i_timer){
            i_timer = window.setTimeout(function(){
                hideAside();
            },1500);
        }
    }).bind('mouseenter',function(){
            window.clearTimeout(i_timer);
            i_timer = null;
        })

    $(".Js_location").bind("mouseleave",function(e){
        var isfast = false;
        $(".aside").bind("mouseenter",function(){
            opening = 2;
            isfast = true;
            return false
        })
        setTimeout(function(){
            if(!$(".aside").is(':hidden')&&!isfast){
                //$(".aside").hide();
                //hideAside();
            }
        },1500);
    })

    $(".Js_cbtype >div").bind("click",function(){
        $(".Js_cbtype >div").removeClass("current")
        $(this).addClass("current");
    })

    function hideAside(){
        setTimeout(function(){
            $(".aside").stop().animate({
                left:-224
            }, 100 ,function(){
                //$(".aside").hide();
                onshow = false;
                opening = 0;
                $('.Js_sytsdd').remove();
                $('.Js_ipad_click').prev().css('background-position','-4px -245px');
                $('#Js_silder_bar').removeClass('on').addClass('in');
            });
            $(".aside").unbind("mouseleave",hideAside)
        },100)
    };

    $('#Js_silder_bar').click(function(){
        if($(this).hasClass('in')){
            $(".Js_location").mouseenter();
        }else{
            hideAside();
        }
    })

    $(document).click(function(e){
        if(e.button==2) return;
        if($(".information").is(":visible")){
            $(".information").hide()
        };

        if($(".search-result").is(":visible")){
            $(".search-result").hide()
        };

        if($(".Js_ccbNum").is(":visible")){
            $(".Js_ccbNum").hide();
        };

        if($(".ccz-scHead").is(":visible")){
            $(".ccz-scHead").hide();
        }

        if($(".ccz-shareDiv").is(":visible")){
            $(".ccz-shareDiv").hide();
        }

        if($(".Js_ccz_ydDiv").is(":visible")){
            $(".Js_ccz_ydDiv").hide();
        }

        if($(".Js_yjfk_div").is(":visible")){
            $(".Js_yjfk_div").hide();
        }
    });
    $(".Js_yjfk_div").live('click',function(){return false;})
    $("input[type='text'],input[type='password']").bind("focus",function(){
        var o = $(this).parents(".tools")
        if(o.length>0){
            o.addClass("focus").removeClass('error');
        }
    });
    $("input[type='text'],input[type='password']").bind("focusout",function(){
        var o = $(this).parents(".tools")
        if(o.length>0){
            o.removeClass("focus")
        }
    })


    var cardhtml = '<div class="visitCard-box Js_vc_txt">'
        +'	<a href="#"><img src="assets/temp/f8.png" alt=""></a>'
        +'		<div class="v-info">'
        +'			<div class="u-name"><a href="#">阿加西的名字有好长呢</a></div>'
        +'			<div class="txt">创意世界科技，中国成都，电子科技大学，Google资深用户体验设计师 </div>'
        +'			<div class="c-btn"><span class="v-add Js_commen_friend_add"><span>添加</span></span></div>'
        +'		</div>'
        +'		<div class="icon-add"><a class="Js_go_chat" style="display:block; height:100%;"></a></div>'
        +'		<img src="assets/temp/t8.png" alt="" style="border-radius:40px; position:absolute; top:98px; left:80px; width:81px; height:81px;">'
        +'	</div>'
    var cardhtmlObj = $(cardhtml);
    var showCardTimer;
    $('.Js_visitCard').live('mouseenter',function(ev){
        if(!$(this).hasClass('Js_visitCard')) return;
        var _self = $(this);
        var _x = ev.pageX+2;
        var _y = ev.pageY;
        $(this).mousemove(function(e){
            _x = e.pageX+2;
            _y = e.pageY;
        })
        showCardTimer = window.setTimeout(function(){
            JS_FINDFRIEND_COMMEN.cardObj = _self;
            cardhtmlObj.css({'top':_y,'left':_x});
            if(_self[0].arr.length>0){
                if(_self[0].arr.length==1){
                    cardhtmlObj.find('.Js_commen_friend_add').css({'background':'#7bab73','border':'1px solid #7bab73'}).children().text(_self[0].arr[0].find('span').text()).addClass('circle-addtoed');
                }else{
                    cardhtmlObj.find('.Js_commen_friend_add').css({'background':'#7bab73','border':'1px solid #7bab73'}).children().text(_self[0].arr.length + "个圈子").addClass('circle-addtoed');
                }
            }else{
                cardhtmlObj.find('.Js_commen_friend_add').css({'background':'#fff','border':'1px solid #d9d9d9'}).children().text('添加').removeClass('circle-addtoed');
            }
            $('body').append(cardhtmlObj);
            showCardTimer = null;
        },300)
    }).live('mouseleave',function(){
            if(!showCardTimer){
                showCardTimer = window.setTimeout(function(){
                    JS_FINDFRIEND_COMMEN.cardObj = null;
                    cardhtmlObj.remove();
                    cardhtmlObj = $(cardhtml);
                    showCardTimer = null;
                },300)
            }else{
                window.clearTimeout(showCardTimer);
                showCardTimer = null;
            }
        })

    $('.Js_vc_txt').live('mouseenter',function(){
        window.clearTimeout(showCardTimer);
    }).live('mouseleave',function(e){
            console.log($(e.relatedTarget))
            if($(e.relatedTarget).parents('.Js_commen_add_friendbox').length > 0 || $(e.relatedTarget).hasClass('Js_commen_add_friendbox')) return;
            showCardTimer = window.setTimeout(function(){
                JS_FINDFRIEND_COMMEN.cardObj = null;
                cardhtmlObj.remove();
                cardhtmlObj = $(cardhtml);
                showCardTimer = null;
            },300)
        })

    $('.Js_go_chat').live('click',function(){
        $('.Js_livemsg').click();
        $('.Js_ccz_box').hide();
        $('.Js_chat_box').show();
        $('.Js_tit_txt').text('返回查看所有消息提醒');
        $(".info-box .info-list").css("max-height",450);
        return false;
    })

    $('.Js_get_ccb').click(function(){
        $('.Js_livemsg').click();
        return false;
    })
    /*
     google地理位置搜索
     */
    $(".Js_search-box").live("click",function(){
        $(".select-field").show();
    })

    $(".match-addr").bind("mouseenter",showsearch);

    function showsearch(){
        $(".match-addr").hide();
        $("#g-searchbox").show().addClass("focus");
        $("#g-searchbox").bind("mouseleave",hidesearch)
        $(".match-addr").unbind("mouseenter")
    }

    function hidesearch(){
        $("#g-searchbox").find('input').blur();
        $("#g-searchbox").hide();
        $(".match-addr").show();
        $(".match-addr").bind("mouseenter",showsearch);
        $("#g-searchbox").unbind("mouseleave",hidesearch)
        $(".select-field").hide()
    }

    $(".Js_search-box").bind("keyup",function(){
        var val = $(this).val();
        if($(this).val()!==""){
            $(".address-field").show();
            var pla = new google.maps.places.AutocompleteService();
            pla.getPlacePredictions({input:val},function(a,b){
                if(a){
                    $(".address-field").html('');
                    for(var i=0; i<a.length; i++){
                        var tp = '<li>'+a[i].description+'</li>';
                        $(".address-field").append(tp);
                    }
                }
            })
        }else{
            $(".address-field").hide();
        }
    })
    $(".address-field li").live("click",function(){
        var tx = $(this).text();
        $(".match-addr").show();
        $(".match-addr").text(tx).css('background-position','0 22px');
        $("#g-searchbox").hide()
    })

    $('.Js_chat').live('click',function(){
        $('.Js_ccz_box').hide();
        $('.Js_chat_box').show();
        $('.Js_tit_txt').text('返回查看所有消息提醒');
        $(".info-box .info-list").css("max-height",450);
        return false;
    })

    $('.Js_no_realize').live('click',function(e){
        e.stopPropagation();
        var realizeHtml ='<div class="yd-div">'
            +'			<span class="no-realize">您的需求暂时还没有实现哦，经过对您的比赛进行分析，推荐给您一下方法，将大大提升您需求几率哦</span>'
            +'			<div class="recommend">'
            +'				<p>1.俗话说："在家靠父母，出门靠朋友"，有朋友的帮助相信任何困难都可以克服，将您的比赛分享给您朋友，他们是能最大的财富</p>'
            +'				<a href="#" class="re-btn">分享比赛</a>'
            +'			</div>'
            +'			<div class="recommend">'
            +'				<p>2.在您的比赛中邀请专家点评，有权威专家的建议，相信更多的人会信任您的比赛</p>'
            +'				<a href="#" class="re-btn">邀请专家点评</a>'
            +'			</div>'
            +'			<div class="recommend">'
            +'				<p>3.大胆将您的比赛推送到最醒目的位置吧！让所有人都能一眼就看到他</p>'
            +'				<a href="#" class="re-btn">邀请专家点评</a>'
            +'			</div>'
            +'			<div class="recommend">'
            +'				<p>4.创意世界有很多非常优秀，非常有创意的用户，他们的成功可能给您非常多的参考，看看他们的比赛，也许可以少走很多弯路哦！</p>'
            +'				<div class="moreMatch-list">'
            +'					<ul class="clearfix">'
            +'						<li><a href="#"><img src="assets/temp/h1.png" /></a></li>'
            +'						<li><a href="#"><img src="assets/temp/h2.png" /></a></li>'
            +'						<li><a href="#"><img src="assets/temp/h3.png" /></a></li>'
            +'					</ul>'
            +'					<span><a href="#">查看更多比赛</a></span>'
            +'				</div>'
            +'			</div>'
            +'			<div class="recommend last">'
            +'				<p>5.来吧，重新描述您的项目，让他变得充满吸引力！您一定会成功</p>'
            +'			</div>'
            +'			<div class="yd-ccz-btn Js_ydBtn"><a href="javascript:;">所有我要，召之即来</a></div>'
            +'		</div>'
        var obj = $(".Js_ccz_ydDiv");
        $(".search-result").hide();
        $('.Js_ccbNum').hide();
        $(".information").hide();
        $(".ccz-shareDiv").hide();
        $('.Js_yjfk_div').hide();
        if(obj.is(":hidden")){
            obj.show();
            obj.html(realizeHtml);
        }else{
            //obj.hide();
        }
        return false;

    })

    $('.Js_tit_txt').live('click',function(){
        if(!$('.Js_chat_box').is(':hidden')){
            $('.Js_ccz_box').show();
            $('.Js_chat_box').hide();
            $('.Js_tit_txt').text('标记所有消息提醒为已读');
            $(".info-box .info-list").css("max-height",600);
            return false;
        }
    })

    $('.Js_chat_send').live('click',function(){
        if($('.Js_chat_txt').val()!=''){
            $.ajax({
                url:'test.php',
                dataType : 'json',
                type : 'post',
                success : function(){
                    var chtml ='<li class="chat-right clearfix">'
                        +'				<a href="#"><img src="assets/temp/8.png" alt="" style="" /></a>'
                        +'				<div class="con">'+$('.Js_chat_txt').val()
                        +'					<s></s>'
                        +'				</div>'
                        +'			</li>'


                    $('.Js_chat_box .info-list').append(chtml);
                    $('.Js_chat_box .info-list').scrollTop($('.Js_chat_box .info-list')[0].scrollHeight);
                    $('.Js_chat_txt').val('');
                }
            })
        }
    })
    $(".Js_livemsg").bind("click",function(e){
        e.stopPropagation();

        var stadyStr = $('.Js_get_ccb').data('flag') ? '' : '						<div class="stady yd-mynews Js_stady_mynews">'
            +'							<div class="yd-cue">'
            +'								<s class="yd-mynew"></s>	'
            +'								<p class="yc-con">在创意世界，您所获得的每一点创意指数都会为您带来创创币的奖励，点击右侧的"立即领取"按钮领取您的创创币吧。当您学会使用创创召的时候，将会获得更加丰厚的奖励哦~</p>'
            +'								<a href="javascript:;" class="over-stady Js_my_overStady">我学会了</a>	'
            +'							</div>'
            +'						</div>';

        $('.Js_information').die('click').live('click',function(){return false;})
        var tdata ='<i class="dot" style="left:'+($('.Js_livemsg').position().left+13)+'px"></i>'
            +'	<div class="info-box">'
            +'		<div style="box-shadow:0 -1px 3px rgba(0,0,0,.4); border-radius:4px;">'
            +'		<div class="link-allinfo"><a href="#" class="Js_tit_txt">标记所有消息提醒为已读</a></div>'
            +'		<div class="Js_ccz_box">'
            +'			<ul class="info-list">'
            +'				<li>'
            +'					<div class="getIndexDiv clearfix">'
            +'						<div class="clearfix">'
            +'							<div class="left">'
            +'								<img src="assets/images/ddp.png" alt="" class="left">'
            +'								<div class="r-con">'
            +'									<p>您刚刚获得了<b>123</b>点创意指数</p>'
            +'									<p>可以领取<b>123</b>个创创币</p>'
            +'								</div>'
            +'							</div>'
            +'							<a href="#" class="m_btn right f-12 mt10">立即领取</a>'
            +'						</div>'
            +						stadyStr
            +'					</div>'
            +'				</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/images/g-lb.png" alt=""></a></span>'
            +'					<div class="left ydarea">'
            +'						<span>恭喜您，掌握了实现“所有我要，召之即来”的方法</span>'
            +'						<p>获得了 <b>1000</b> 点创意指数。</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/images/g-lb.png" alt=""></a></span>'
            +'					<div class="left ydarea">'
            +'						<span>恭喜您已经通过创意世界智库认证</span>'
            +'						<p>成为 <b>创意世界智库专家。</b></p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/images/g-lb.png" alt=""></a></span>'
            +'					<div class="left ydarea">'
            +'						<span>恭喜您已经成为创意世界智库候选专家</span>'
            +'						<p>您只需创意等级达到 <b> 20 </b> 级，就将成为 <b> 创意世界智库专家</b></p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList Js_no_realize">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/images/g-lb.png" alt=""></a></span>'
            +'					<div class="left ydarea">'
            +'						<span>您的需求暂时未能实现</span>'
            +'						<p>看看创意世界给您的建议吧！</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/2.png" alt=""></a></span>'
            +'					<div class="left msgarea"><a class="user-name" href="#">我不是郦元道</a>已将您加入了他的圈子</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'				<a href="#" class="item-link"></a>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/2.png" alt=""></a></span>'
            +'					<div class="left msgarea"><a class="user-name" href="#">马双</a>已将您回圈</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/2.png" alt=""></a></span>'
            +'					<div class="left msgarea">'
            +'						<a class="user-name" href="#">我不是郦元道</a>邀请您进行点评'
            +'						<p class="des">史上第一部众筹电影《十万个冷笑话》</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/2.png" alt=""></a></span>'
            +'					<div class="left msgarea">'
            +'						<a class="user-name" href="#">老专家</a>接受了您的邀请并点评了比赛'
            +'						<p class="des">十万个冷笑话</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/2.png" alt=""></a></span>'
            +'					<div class="left msgarea">'
            +'						<a class="user-name" href="#">李克强</a>点评了您关注的比赛'
            +'						<p class="des">用图片交换生活</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/2.png" alt=""></a></span>'
            +'					<div class="left msgarea">'
            +'						<a class="user-name" href="#">老彭</a>邀请您参加创意任务人才比赛'
            +'						<p class="des">60平米房屋简装方案设计</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/2.png" alt=""></a></span>'
            +'					<div class="left msgarea">'
            +'						<a class="user-name" href="#">老张</a>给您分享了创意公益明星比赛'
            +'						<p class="des">成都锦江宾馆的服务明星评比</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/3.png" alt=""></a></span>'
            +'					<div class="left msgarea">'
            +'						<span class="title-name">创意投资项目比赛已成功</span>'
            +'						<p class="des">史上第一部众筹电影《十万个为什么》</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/3.png" alt=""></a></span>'
            +'					<div class="left msgarea">'
            +'						<span class="title-name">创意任务人才比赛已成功</span>'
            +'						<p class="des">60平方米简装方案设计</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/3.png" alt=""></a></span>'
            +'					<div class="left msgarea">'
            +'						<span class="title-name">您在创意世界交易所中的出价已经被人超过</span>'
            +'						<p class="des">创意世界LOGO素描手稿</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/3.png" alt=""></a></span>'
            +'					<div class="left msgarea">'
            +'						<span class="title-name">创意公益明星比赛已成功</span>'
            +'						<p class="des">成都锦江宾馆2013服务之星</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/3.png" alt=""></a></span>'

            +'					<div class="left msgarea">'
            +'						<span class="title-name">创意世界交易所交易已成功</span>'
            +'						<p class="des">创意世界LOGO素描手稿</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/2.png" alt=""></a></span>'
            +'					<div class="left msgarea">'
            +'						<a class="user-name" href="#">老彭</a>在<a class="link-des" href="#">创意世界LOGO素描手稿</a>中提到了您'
            +'						<p class="des">"老李，你快来看这个"</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/2.png" alt=""></a></span>'
            +'					<div class="left msgarea">'
            +'						<a class="user-name" href="#">老李</a>在<a class="link-des" href="#">创意世界LOGO素描手稿</a>中回复了您'
            +'						<p class="des">"老彭，这个是绕口令嘛"</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			<li class="cczList Js_chat">'
            +'				<div class="not-single">'
            +'					<span class="left avatar"><a href="#"><img src="assets/temp/2.png" alt=""></a></span>'
            +'					<div class="left msgarea">'
            +'						<a class="user-name" href="#">老彭</a>对你说道'
            +'						<p class="des">"双哥，晚上出来吃火锅"</p>'
            +'					</div>'
            +'				</div>'
            +'				<i class="lidot"></i>'
            +'			</li>'
            +'			</ul>'
            +'		</div>'
            +'		<div class="Js_chat_box" style="display:none;">'
            +'		<div class="chat-firend">和柴先侨聊天中</div>'
            +'		<ul class="info-list">'
            +'		    <div class="chat-more-tobecancled"><a class="chat-more" style="display: block">点击查看更多聊天记录</a></div>'
            +'			<li class="chat-left clearfix">'
            +'				<a href="#"><img src="assets/temp/9.png" alt="" style="" /></a>'
            +'				<div class="con">相对于以往笨重的数码相机而言，现在的卡片机轻薄便携，但如果真要冠以“卡片”的名义，它就非得安装Plastic Logic刚研发出的传感器不可。这是一款40×40毫米传感器包含一个透射背板和ISORG有机光电探测器材料，&nbsp;&nbsp;10分钟前'
            +'					<s></s>'
            +'				</div>'
            +'			</li>'
            +'			<li class="chat-right clearfix">'
            +'				<span>2013/01/25 22:20:22</span>'
            +'				<div class="clearfix">'
            +'					<a href="#"><img src="assets/temp/9.png" alt="" /></a>'
            +'					<div class="con">蜜桃熟?全家福 第二届双溪蟠桃会欢迎您！7月13— 20日，百名摄影名家齐聚汉源双溪，为您和家人免费拍摄"最美全家福"照片,登《品味TASTE》封面，赢万元现金大奖！一等奖3000元，二等奖2000元，三等奖1000元，'
            +'						<s></s>'
            +'					</div>'
            +'				</div>'
            +'			</li>'
            +'			<li class="chat-left clearfix">'
            +'				<a href="#"><img src="assets/temp/9.png" alt="" /></a>'
            +'				<div class="con">今天有活动没'
            +'					<s></s>'
            +'				</div>'
            +'			</li>'
            +'			<li class="chat-right clearfix">'
            +'				<span>2013/01/25 22:20:22</span>'
            +'				<div class="clearfix">'
            +'					<a href="#"><img src="assets/temp/9.png" alt="" /></a>'
            +'					<div class="con">蜜桃熟?全家福 第二届双溪蟠桃会欢迎您！7月13— 20日，百名摄影名家齐聚汉源双溪，为您和家人免费拍摄"最美全家福"照片,登《品味TASTE》封面，赢万元现金大奖！一等奖3000元，二等奖2000元，三等奖1000元，'
            +'						<s></s>'
            +'					</div>'
            +'				</div>'
            +'			</li>'
            +'			<li class="chat-left clearfix">'
            +'				<a href="#"><img src="assets/temp/9.png" alt="" style="" /></a>'
            +'				<div class="con">十年窗下无人问，一举成名天下知。有了微博无需宅，一呼百应朋友遍天下，今后必当好'
            +'					<s></s>'
            +'				</div>'
            +'			</li>'
            +'		</ul>'
            +'		<div><textarea placeholder="请输入你要聊天的内容" class="chat-txt Js_chat_txt"></textarea></div>'
            +'		<div class="height_20"></div>'
            +'		<a href="javascript:;" class="chat-send Js_chat_send">发送</a>'
            +'		<div class="height_18"></div>'
            +'	</div>'
            +'	</div>'
            +'	</div>';

        var obj = $(".information");
        $(".search-result").hide();
        $('.Js_ccbNum').hide();
        $(".ccz-scHead").hide();
        $(".ccz-shareDiv").hide();
        $(".Js_ccz_ydDiv").hide();
        $('.Js_yjfk_div').hide();
        if(obj.is(":hidden")){
            obj.show();
            obj.html(tdata);
            var n = parseInt((document.documentElement.clientHeight*2/3)/86)
            $(".info-box .info-list").css("max-height",86*n-3)
        }else{
            obj.hide()
        }

        $('.Js_my_overStady').unbind('click').bind('click',function(){
            var self = $(this);
            $.ajax({
                url:'test.php',
                dataType : 'json',
                type : 'post',
                success : function(){
                    self.parents('.Js_stady_mynews').remove();
                    $('.Js_get_ccb').data('flag',true);
                }
            })
            return false;
        })
    });

    $('.Js_chat_box').live('click',function(){return false;})

    $('.Js_ydBtn').live('click',function(){
        $('.Js_cczsearch').click();
        $('.Js_cczsearch').data('flag',true);
        if(isStady){
            $('.Js_stady').hide();
        }else{
            $('.Js_stady').show();
        }
        $('.Js_ask_q').show();
        return false;
    });

    var isStady = 0;

    $('.Js_other_btn').click(function(){
        $('.Js_cczsearch').click();
        return false;
    })

    $(".Js_cczsearch").click(function(e){
        e.stopPropagation();
        $(this).next().hide();
        $(this).focus();
        var tdata = '<div class="stady top"><div class="clearfix">'
            +'				<a href="javacript:;" class="search-btn Js_search_btn">所有我要，召之即来</a>'
            +'				<span class="more-reslut">没有找到结果？马上呈现<a href="./search.php" id="Js_more_result">更多结果</a></span>'
            +'	</div></div>'
            +'		<div class="stady Js_stady" style="display:none;">'
            +'			<div class="yd-cue">'
            +'				<s></s>'
            +'				<p class="yc-con">您现在正和创创召进行互动，创创召是您在创意世界的小秘书，您需要任何的帮助，都可以通过创创召找到答案。今后您只需要进入创创召搜索您想要的结果，或点击左上角"所有我要，召之即来"的按钮，便可以轻松和创创召交流您的需求，赶快来实验一下吧。当您学会使用创创召的时候，将会获得丰厚的奖励哦~</p>'
            +'				<a href="javascript:;" class="over-stady Js_over_stady">我学会了</a>'
            +'			</div>'
            +'		</div>'
            +'		<div class="ask-q Js_ask_q" style="display:none;">'
            +'			<div>'
            +'				<p class="tit">1.您需要什么？</p>'
            +'				<a href="javascript:;" class="need-list Js_p_needList">项目资金</a>'
            +'				<a href="javascript:;" class="need-list Js_p_needList">找人帮忙</a>'
            +'				<a href="javascript:;" class="need-list Js_p_needList">出名</a>'
            +'				<a href="javascript:;" class="need-list Js_p_needList">获得奖金</a>'
            +'				<a href="javascript:;" class="need-list Js_p_needList">找人代言</a>'
            +'				<a href="javascript:;" class="need-list Js_p_needList">找项目投资</a>'
            +'				<div class="clearfix">'
            +'					<div class="user-add">'
            +'						<input placeholder="请输入其他需求" />'
            +'					</div>'
            +'					<a class="user-ok Js_p_userOk">确认</a>'
            +'				</div>'
            +'			</div>'
            +'			<div>'
            +'				<p class="tit">2.您能提供什么？</p>'
            +'				<div style="display:none;" class="Js_you_tg">'
            +'					<a href="javascript:;" class="need-list Js_r_needList">优秀项目</a>'
            +'					<a href="javascript:;" class="need-list Js_r_needList">项目资金</a>'
            +'					<a href="javascript:;" class="need-list Js_r_needList">优秀才艺</a>'
            +'					<a href="javascript:;" class="need-list Js_r_needList">合约机会</a>'
            +'					<a href="javascript:;" class="need-list Js_r_needList">技能帮助</a>'
            +'					<a href="javascript:;" class="need-list Js_r_needList">提供奖金</a>'
            +'					<div class="clearfix">'
            +'						<div class="user-add">'
            +'							<input placeholder="请输入其他能力" />'
            +'						</div>'
            +'						<a class="user-ok Js_r_userOk">确认</a>'
            +'					</div>'
            +'				</div>'
            +'			</div>'
            +'			<div>'
            +'				<p class="tit">3.等待需求实现</p>'
            +'				<div style="text-align:center; display:none;" class="yd-ccz-btn Js_demand_realize">'
            +'					<a href="javascript:;" style="font-size:16px;">看看创意世界都为您找到了什么</a>'
            +'				</div>'
            +'			</div>'
            +'		</div>'
            +'		<div class="Js_search_list_">'
            +'		</div>';
        var obj = $(".search-result");
        $(".information").hide();
        $('.Js_ccbNum').hide();
        $(".ccz-scHead").hide();
        $(".ccz-shareDiv").hide();
        $(".Js_ccz_ydDiv").hide();
        $('.Js_yjfk_div').hide();
        if(obj.is(':hidden')){
            if(!$(this).val()){
                obj.html(tdata);
            }
            $(".search-result").css("max-height",document.documentElement.clientHeight*2/3);
            obj.show();
        }
        $('.Js_stady').hide();
        $('.Js_ask_q').hide();
        $('.Js_p_needList').unbind('click').bind('click',function(){
            $('.Js_p_needList').css('background','#888888');
            $(this).css('background','#d06e6f');
            $('.Js_you_tg').show();
        });

        $('.Js_r_needList').unbind('click').bind('click',function(){
            $('.Js_r_needList').css('background','#888888');
            $(this).css('background','#d06e6f');
            $('.Js_demand_realize').show();
        });

        $('.Js_p_userOk').unbind('click').bind('click',function(){
            var self = $(this);
            $.ajax({
                url:'test.php',
                dataType : 'json',
                type : 'post',
                success : function(){
                    var val = self.prev().find('input').val();
                    var ohtml = '<a href="javascript:;" class="need-list Js_p_needList" style="background:#d06e6f">'+val+'</a>';
                    var tArr = [];
                    $('.Js_p_needList').text(function(index,con){
                        tArr.push(con);
                    })
                    if(val && $.inArray(val,tArr)<0){
                        $('.Js_p_needList').css('background','#888888');
                        $('.Js_p_needList:last').after(ohtml);
                        self.prev().find('input').val('').blur();
                        $('.Js_you_tg').show();
                    }
                    $('.Js_p_needList').unbind('click').bind('click',function(){
                        $('.Js_p_needList').css('background','#888888');
                        self.css('background','#d06e6f');
                        $('.Js_you_tg').show();
                    });
                }
            })
        });

        $('.Js_r_userOk').unbind('click').bind('click',function(){
            var val = $(this).prev().find('input').val();
            var ohtml = '<a href="javascript:;" class="need-list Js_r_needList" style="background:#d06e6f">'+val+'</a>';
            var tArr = [];
            $('.Js_r_needList').text(function(index,con){
                tArr.push(con);
            })
            if(val && $.inArray(val,tArr)<0){
                $('.Js_r_needList').css('background','#888888');
                $('.Js_r_needList:last').after(ohtml);
                $(this).prev().find('input').val('').blur();
                $('.Js_demand_realize').show();
            }
            $('.Js_r_needList').unbind('click').bind('click',function(){
                $('.Js_r_needList').css('background','#888888');
                $(this).css('background','#d06e6f');
                $('.Js_demand_realize').show();
            });
        })

        $('.Js_over_stady').unbind('click').bind('click',function(){
            $.ajax({
                url:'test.php',
                dataType : 'json',
                type : 'post',
                success : function(){
                    isStady = 1;
                    $('.Js_stady').hide();
                }
            })
        });

        $('.Js_search_btn').unbind('click').bind('click',function(){
            if(!isStady){
                $('.Js_stady').show();

            }
            $('.Js_ask_q').show();
            $('.Js_search_list_').html('');
            $('.Js_cczsearch').val('').next().show();
        })
    }).blur(function(){
            var val = $(this).val();
            if(val == ""){
                if($(this).data('flag')){
                    $(this).data('flag',false);
                    return;
                }
                $(this).next().show();
            }
        });

    $('.search-result').click(function(e){
        e.stopPropagation();

    })

    $('.Js_placeholder').click(function(){
        $(".Js_cczsearch").click();
        return false;
    });

    $(".Js_cczsearch").bind("keyup",function(e){
        e.stopPropagation();
        var val = $(this).val()
        var tdata = '	<ul class="user-list">'
            +'				<li>'
            +'					<div class="user">'
            +'						<span class="left avatar"><a href="profile-self.php"><img class="avat" src="assets/temp/av1.png" alt="用户头像"/><img class="expt" src="assets/images/expert.png" alt="专家标志"/></a></span>'
            +'						<div class="ml55">'
            +'							<div class="left msgarea">'
            +'							    <a href="profile-self.php" class="user-name" href="#">紫不语</a>'
            +'								<p class="des">亚洲美容天王</p>'
            +'							</div>'
            +'						</div>'
            +'					</div>'
            +'				</li>'
            +'				<li>'
            +'					<div class="user">'
            +'						<span class="left avatar"><a href="profile-self.php"><img class="avat" src="assets/temp/av2.png" alt="用户头像"></a></span>'
            +'						<div class="ml55">'
            +'							<div class="left msgarea">'
            +'								<a href="profile-self.php" class="user-name" href="#">马双</a>'
            +'								<p class="des frend">朋友</p>'
            +'							</div>'
            +'						</div>'
            +'					</div>'
            +'				</li>'
            +'				<li>'
            +'					<div class="user">'
            +'						<span class="left avatar"><a href="profile-self.php"><img class="avat" src="assets/temp/av3.png" alt="用户头像"/></a></span>'
            +'						<div class="ml55">'
            +'							<div class="left msgarea">'
            +'								<a href="profile-self.php" class="user-name" href="#">刘兆宇</a>'
            +'									<p class="des">成都电子科技大学</p>'
            +'							</div>'
            +'						</div>'
            +'					</div>'
            +'				</li>'
            +'				<li>'
            +'					<div class="user">'
            +'						<span class="left avatar"><a href="profile-self.php"><img class="avat" src="assets/temp/av4.png" alt="用户头像"/><img class="expt" src="assets/images/expert.png" alt="专家标志"/></a></span>'
            +'						<div class="ml55">'
            +'							<div class="left msgarea">'
            +'								<a href="profile-self.php" class="user-name" href="#">王自如</a>'
            +'									<p class="des">资深手机测评达人</p>'
            +'							</div>'
            +'						</div>'
            +'					</div>'
            +'				</li>'
            +'			</ul>'
            +'			<div class="link-allmatch"><i>创意世界大赛</i><div class="line-div-match"><span></span></div></div>'
            +'			<ul class="user-list">'
            +'				<li>'
            +'					<div class="not-single">'
            +'						<span class="left avatar"><a href="profile-self.php"><img src="assets/temp/3.png" alt=""></a></span>'
            +'						<div class="ml55">'
            +'							<div class="left msgarea">'
            +'								<a href="../project-going.php" class="title-name">成都财富论坛</a>'
            +'								<a href="../project-list.php" class="des">创意投资项目比赛</a>'
            +'							</div>'
            +'						</div>'
            +'					</div>'
            +'				</li>'
            +'				<li>'
            +'					<div class="not-single">'
            +'						<span class="left avatar"><a href="profile-self.php"><img src="assets/temp/3.png" alt=""></a></span>'
            +'						<div class="ml55">'
            +'							<div class="left msgarea">'
            +'								<a href="../project-going.php" class="title-name">生活成都，艾美成都之星</a>'
            +'								<a href="../project-list.php" class="des">创意投资项目比赛</a>'
            +'							</div>'
            +'						</div>'
            +'					</div>'
            +'				</li>'
            +'				<li>'
            +'					<div class="not-single">'
            +'						<span class="left avatar"><a href="profile-self.php"><img src="assets/temp/3.png" alt=""></a></span>'
            +'						<div class="ml55">'
            +'							<div class="left msgarea">'
            +'								<a href="../task-going.php" class="title-name">成都创意LOG设计大赛</a>'
            +'								<a href="../task-list.php" class="des">创意任务人才比赛</a>'
            +'							</div>'
            +'						</div>'
            +'					</div>'
            +'				</li>'
            +'				<li>'
            +'					<div class="not-single">'
            +'						<span class="left avatar"><a href="profile-self.php"><img src="assets/temp/3.png" alt=""></a></span>'
            +'						<div class="ml55">'
            +'							<div class="left msgarea">'
            +'								<a href="../hire-going.php" class="title-name">成都爱，爱成都</a>'
            +'								<a href="../hirie-list.php" class="des">创意公益明星比赛</a>'
            +'							</div>'
            +'						</div>'
            +'					</div>'
            +'				</li>'
            +'			</ul>'

            +'			<div class="link-allmatch"><i>创意世界交易所</i><div class="line-div-trade"><span></span></div></div>'
            +'			<ul class="user-list">'
            +'				<li>'
            +'					<div class="not-single">'
            +'						<span class="left avatar"><a href="profile-self.php"><img src="assets/temp/3.png" alt=""></a></span>'
            +'						<div class="ml55">'
            +'							<div class="left msgarea">'
            +'								<a href="../trade-buy.php" class="title-name">成都财富论坛</a>'
            +'								<a href="../trade-buy.php" class="des">创意世界交易所</a>'
            +'							</div>'
            +'						</div>'
            +'					</div>'
            +'				</li>'
            +'				<li>'
            +'					<div class="not-single">'
            +'						<span class="left avatar"><a href="profile-self.php"><img src="assets/temp/3.png" alt=""></a></span>'
            +'						<div class="ml55">'
            +'							<div class="left msgarea">'
            +'								<a href="../trade-bid.php" class="title-name">生活成都，艾美成都之星</a>'
            +'								<a href="../trade-bid.php" class="des">创意世界交易所</a>'
            +'							</div>'
            +'						</div>'
            +'					</div>'
            +'				</li>'
            +'				<li>'
            +'					<div class="not-single">'
            +'						<span class="left avatar"><a href="profile-self.php"><img src="assets/temp/3.png" alt=""></a></span>'
            +'						<div class="ml55">'
            +'						<div class="left msgarea">'
            +'							<a href="../trade-over.php" class="title-name">成都钟水饺全师傅面授课程一次</a>'
            +'							<a href="../trade-over.php" class="des">创意世界交易所</a>'
            +'						</div>'
            +'						</div>'
            +'					</div>'
            +'				</li>'
            +'				<li>'
            +'					<div class="not-single">'
            +'						<span class="left avatar"><a href="profile-self.php"><img src="assets/temp/3.png" alt=""></a></span>'
            +'						<div class="ml55">'
            +'							<div class="left msgarea">'
            +'								<a href="../trade-over.php" class="title-name">成都爱，爱成都</a>'
            +'								<a href="../trade-over.php" class="des">创意世界交易所</a>'
            +'							</div>'
            +'						</div>'
            +'					</div>'
            +'				</li>'
            +'			</ul>';
        if(val){
            $('.Js_stady,.Js_ask_q').hide();
            $('.Js_search_list_').html(tdata);
        }else{
            $('.Js_stady,.Js_ask_q').show();
            $('.Js_search_list_').html('');
        }

    });

    $('#Js_more_result').live('click',function(){
        var val = $(".Js_cczsearch").val();
        window.location.href = 'search/keyword=' + val;
    })

    $('.Js_addClass').live('click',function(){
        $('.Js_selDiv_xj').find('ul').hide();
        selShow();
        return false;
    });

    $('.Js_selDiv_xj').live('click',function(){
        if($('.Js_addClass').attr('flag')=="0"){
            $(this).find('ul').show();
        }
        return false;
    })
    $(".Js_cczScHead").live('click',function(){
        if($('.Js_sc_sel').val()==""){
            $('.Js_selDiv_xj').find('ul').hide();
            selHide();
        }
        return false
    });

    $(".Js_cczScHead li").live('click',function(){
        var val = $(this).text();
        $(this).parent().hide().prev().val(val);
        return false;
    })

    $('.Js_cczxjCancle').live('click',function(){
        $('.Js_cczScHead').hide();
    })

    $('.Js_cczxjok').live('click',function(){
        var index = -1;
        var flag = $('.Js_addClass').attr('flag');
        $('.Js_item_tit').prev().each(function(a,b){
            if($(this).val()==$('.Js_sc_sel').val()){
                index = a;
            }
        })

        if(flag=="1"){
            if(index>=0){
                alert('您已经创建了该类别！');
                return false;
            }

            if($('.Js_web_name').val()==""){
                var chtml = '<dl class="mt10">'
                    +'		<dt class="mb7"> '
                    +'			<div class="my-scDiv">'
                    +'				<div class="con clearfix">'
                    +'					<input type="text" value="'+$('.Js_sc_sel').val()+'" class="tit" disabled="">'
                    +'					<a href="javascript:;" class="Js_item_tit"></a>'
                    +'				</div>'
                    +'			</div>'
                    +'		</dt>'
                    +'	</dl>'
            }else{
                var chtml = '<dl class="mt10">'
                    +'		<dt class="mb7"> '
                    +'			<div class="my-scDiv">'
                    +'				<div class="con clearfix">'
                    +'					<input type="text" value="'+$('.Js_sc_sel').val()+'" class="tit" disabled="">'
                    +'					<a href="javascript:;" class="Js_item_tit"></a>'
                    +'				</div>'
                    +'			</div>'
                    +'		</dt>'
                    +'		<dd class="clearfix Js_item_dd">'
                    +'		<div class="my-scDiv left mr2">'
                    +'			<div class="con clearfix">'
                    +'				<img src="assets/temp/wy.png">'
                    +'				<div class="left pr">'
                    +'					<input type="text" value="'+$('.Js_web_name').val()+'" disabled="">'
                    +'					<a href="javascript:;" class="web-href n-tit">'+$('.Js_web_name').val()+'</a>'
                    +'				</div>'
                    +'				<a href="javascript:;" class="del Js_item_index"></a>'
                    +'			</div>'
                    +'		</div>'
                    +'	</dd>'
                    +'	</dl>'
            }
            $('.Js_item_div').append(chtml);
        }else{
            var chtml = '<div class="my-scDiv left mr2">'
                +'				<div class="con clearfix">'
                +'					<img src="assets/temp/ie.png">'
                +'					<div class="left pr" >'
                +'						<input type="text" value="'+$('.Js_web_name').val()+'" disabled />'
                +'						<a href="javascript:;" class="web-href n-tit">'+$('.Js_web_name').val()+'</a>'
                +'					</div>'
                +'					<a href="javascript:;" class="del Js_item_index"></a>'
                +'				</div>'
                +'			</div>'
            $('.Js_item_dd').eq(index).append(chtml);
        }
        $('.Js_cczScHead').hide();
        addWebAddress();
        return false;
    })

    $('.Js_sc_add').click(function(){
        $('.Js_collect_btn').click();
        $(".Js_add_webAddress").show();
        if($('.Js_sc_edit').text()=="返回"){
            $('.Js_sc_edit').click();
        }
        return false;
    })

    $('.my-scDiv').find('input').keyup(function(){
        if($(this).next().hasClass('web-href')){
            $(this).next().text($(this).val());
        }
    })

    $('.Js_sc_edit').click(function(){
        if($(this).text()=="编辑"){
            $(this).text('返回');
            $('.my-scDiv .con').css('border-color','#ececed').find('input').show().attr('disabled',false);
            $('.my-scDiv .con').find('.web-href').hide();
            $('.my-scDiv .con').find('.del').show();
            $(".Js_cczScHead").hide();
        }else{
            $(this).text('编辑');
            $('.my-scDiv .con').css('border-color','#fff').find('input').hide().attr('disabled',true);
            $('.my-scDiv .con').find('.tit').show();
            $('.my-scDiv .con').find('.web-href').show();
            $('.my-scDiv .con').find('.del').hide();
        }
        return false;
    })

    $('.Js_item_tit').click(function(){
        $(this).parents('dl').remove();
        addWebAddress();
    })

    $('.Js_item_index').live('click',function(){
        $(this).parent().parent().remove();
        addWebAddress();
    })

    function selShow(){
        $('.Js_sc_sel').attr('disabled',false).removeClass('sel').focus().val('').prev().hide().parent().removeClass('selDiv');
        $('.Js_addClass').attr('flag','1');
    }

    function selHide(){
        $('.Js_sc_sel').attr('disabled',true).addClass('sel').blur().prev().show().parent().addClass('selDiv');
        $('.Js_addClass').attr('flag','0');
    }

    function addWebAddress(){
        var arr = [];
        var n = 0;
        var uList = $('.Js_wrapList').children();
        uList.each(function(i,obj){
            if($(this).position().left==0){
                arr.push($(this));
            }
        });

        for(var i=0; i<arr.length; i++){
            arr[i].css('top',n);
            n+=(arr[i].height() + 20);
        }
    }
    $('.Js_collect_btn').bind('click',function(e){
        e.stopPropagation();
        var tdata = "<h3>添加收藏</h3>"
            +'		<div class="contxt">'
            +'			<div class="clearfix mt10">'
            +'				<div class="left clearfix">'
            +'					<label class="ccz-xj">分类：</label>'
            +'					<div class="ccz-xj-menuDiv selDiv Js_selDiv_xj">'
            +'						<s></s>'
            +'						<input type="text" class="sel Js_sc_sel" disabled>'
            +'						<ul>'
            +'							<li>购物</li>'
            +'							<li>门户</li>'
            +'							<li>视频</li>'
            +'							<li>创意世界</li>'
            +'						</ul>'
            +'					</div>'
            +'				</div>'
            +'				<a href="javascript:;" class="new-class Js_addClass" flag="0">新建分类</a>'
            +'			</div>'
            +'			<div class="clearfix mt10">'
            +'				<div class="left clearfix">'
            +'					<label class="ccz-xj">名称：</label>'
            +'					<div class="ccz-xj-menuDiv">'
            +'						<input type="text" class="Js_web_name">'
            +'					</div>'
            +'				</div>'
            +'				<div class="left clearfix ml10 Js_add_webAddress">'
            +'					<label class="ccz-xj">添加网址：</label>'
            +'					<div class="ccz-xj-menuDiv">'
            +'						<input type="text">'
            +'					</div>'
            +'				</div>'
            +'			</div>'
            +'			<p class="mh-btn">'
            +'				<a href="javascript:;" class="bshy Js_cczxjCancle">取消</a>'
            +'				<a href="javascript:;" class="bqh Js_cczxjok">确认</a>'
            +'			</p>'
            +'		</div>'
        var obj = $(".Js_cczScHead");
        $(".search-result").hide();
        $('.Js_ccbNum').hide();
        $(".information").hide();
        $(".ccz-shareDiv").hide();
        $(".Js_ccz_ydDiv").hide();
        $('.Js_yjfk_div').hide();
        if(obj.is(":hidden")){
            obj.show();
            obj.html(tdata);
        }else{
            //obj.hide();
        }
        $(".Js_add_webAddress").hide();
        return false;
    });

    $('.Js_yjfk').click(function(e){
        e.stopPropagation();
        var tdata = '<div class="yj-div">'
            +'		<p style="">请留下您对"创意世界"的意见和建议！（请填写）</p>'
            +'		<textarea></textarea>'
            +'		<a class="new-btn" id="Js_submit_feedback">提交反馈</a>'
            +'	</div>'
        var obj = $(".Js_yjfk_div");
        $(".search-result").hide();
        $('.Js_ccbNum').hide();
        $(".information").hide();
        $(".ccz-shareDiv").hide();
        $(".Js_ccz_ydDiv").hide();
        $(".Js_cczScHead").hide();
        $('.Js_yjfk_div').hide();
        if(obj.is(":hidden")){
            obj.show();
            obj.html(tdata);
        }else{
            //obj.hide();
        }
        return false;
    })

    $('#Js_submit_feedback').live('click',function(){
        var val = $(this).prev().val();
        var data = {'feedback':val};
        $.ajax({
            url:'feedback',
            dataType : 'json',
            type : 'post',
            data : data,
            success : function(res){
                if(res.status=='0'){

                }else{
                    alert('error');
                }
            }
        })
    })

    $('.Js_webout_share').live('click',function(){
        var o = $(this).next();
        o.show();
        $('.Js_share_menu').hide();
        $(document).one('click',function(){
            o.hide();
        })
        return false;
    })

    $('.Js_bind_webout input').live('click',function(e){
        var img = $(this).next();
        if($(this).filter('input:checked').length==1){
            img.attr('src',img.attr('src').replace('.png','_.png'));
        }else{
            img.attr('src',img.attr('src').replace('_.png','.png'));
        }
        e.stopPropagation();
    })
    $('.Js_bind_webout').live('click',function(e){
        var img = $(this).find('img');
        if($(this).find('input:checked').length==1){
            $(this).find('input').attr('checked',false);
            img.attr('src',img.attr('src').replace('_.png','.png'));
        }else{
            $(this).find('input').attr('checked',true);
            img.attr('src',img.attr('src').replace('.png','_.png'));
        }
        e.stopPropagation();
    })

    $('.Js_share_search').live('click',function(){
        var span = $(this).find('.Js_defualt_txt');
        var input = $(this).find('input');
        var menu = $('.Js_share_menu');
        span.hide();
        input.focus();
        menu.show();
        $('.webout-share').hide();
        $(document).one('click',function(){
            if(input.val()==''){
                span.show();
                menu.hide();
            }
        })
        return false;
    }).find('input').live('keydown',function(e){
            if(e.keyCode==18 || e.keyCode==9) return false;
        }).live('keyup',function(e){
            $('.Js_share_menu ul').html('');
            var lihtml = '<li class="share-circle" data-type="3"><span>马哥的圈子</span> （14）</li>';
            $('.Js_share_menu ul').append(lihtml);
        })

    if($('.Js_item_div').height()>385){
        $('.Js_open_sc').show();
    }

    $('.Js_open_sc').click(function(){
        if($(this).text()=='展开'){
            $(this).text('收起').css('background','url(assets/images/icon-arrup.png) no-repeat 0 3px');
            $('.Js_publick_parent').css({'height':'auto'});
        }else{
            $(this).text('展开').css('background','url(assets/images/icon-arrdown.png) no-repeat 0 3px');
            $('.Js_publick_parent').css('height','385px');
        }
        addWebAddress();
    })

    $('.Js_share_menu li').live('click',function(e){
        if($(this).attr('data-type')=="1"){
            var shtml = $('<span class="add-span Js_add_span">'+$(this).find('span').text()+'<a href="javascript:;" class="Js_del_span"></a></span>');
        }else if($(this).attr('data-type')=="2"){
            var shtml = $('<span class="add-span Js_add_span" style="background:#abae8f">'+$(this).find('span').text()+'<a href="javascript:;" class="Js_del_span"></a></span>');
        }else if($(this).attr('data-type')=="3"){
            var shtml = $('<span class="add-span Js_add_span" style="background:#888888">'+$(this).find('span').text()+'<a href="javascript:;" class="Js_del_span"></a></span>');
        }
        $('.Js_input_w').before(shtml);
        shtml.click(function(e){
            if($(e.target).hasClass('Js_del_span')){
                $(e.target).parent().remove();
                $('.Js_share_menu').hide();
                $('.Js_share_search').find('.Js_defualt_txt').show().prev().val('');
                shareSpanWidth();
            }
            return false;
        })
        $('.Js_share_menu').hide();
        $('.Js_share_search').find('.Js_defualt_txt').show().prev().val('');
        shareSpanWidth();
        return false;
    })


    $('.Js_add_span').each(function(){
        $(this).die('click').live('click',function(e){
            if($(e.srcElement).hasClass('Js_del_span')){
                $(e.srcElement).parent().remove();
                $('.Js_share_menu').hide();
                $('.Js_share_search').find('.Js_defualt_txt').show().prev().val('');
                shareSpanWidth();
            }
            return false;
        })
    })

    $(window).resize(function(){
        shareSpanWidth();
    })


    function shareSpanWidth(){
        if($('.Js_add_span').length>0){
            $('.Js_defualt_txt').text('添加更多人');
        }else{
            $('.Js_defualt_txt').text(' + 添加姓名、圈子或电子邮件地址');
        }
        var addSpanWidth = 0;
        $('.Js_add_span').each(function(){
            addSpanWidth += ($(this).outerWidth()+10);
            if(addSpanWidth > $('.Js_share_search').width()){
                addSpanWidth = $(this).outerWidth()+10;
            }
        })
        var delSpanWidth = $('.Js_share_search').width()-addSpanWidth<=60 ? $('.Js_share_search').width() : $('.Js_share_search').width()-addSpanWidth;
        $('.Js_input_w').width(delSpanWidth-32);
    }

    $('.Js_ccz_list li').live('click',function(){
        var r = $(this).find('span').css('background-position').split(" ")[1];
        var index =$('.Js_ccz_list').children().index($(this));
        var ccblistKids = $('.Js_ccblist_item').children();
        if(ccblistKids.eq(index).is(':hidden')){
            ccblistKids.hide();
            $('.Js_ccz_list li').each(function(a,b){
                if(a==0){
                    $(this).find('span').css('background-position','10px 2px').next().css('color','#5f5f5f');
                }else if(a==1){
                    $(this).find('span').css('background-position','10px -22px').next().css('color','#5f5f5f');
                }else{
                    $(this).find('span').css('background-position','10px -49px').next().css('color','#5f5f5f');
                }
            })
            ccblistKids.eq(index).show();
            $(this).find('span').css('background-position','-19px '+r).next().css('color','#3b86c4');
        }else{
            ccblistKids.hide();
            $(this).find('span').css('background-position','10px '+r).next().css('color','#5f5f5f');
        }
    })

    $(".Js_shareScHead").live('click',function(e){
        $('.webout-share').hide();
        if($('.Js_share_search').find('input').val()==''){
            $('.Js_share_search').find('.Js_defualt_txt').show();
            $('.Js_share_menu').hide();
        }
        e.stopPropagation();
    })
    $('.Js_share_btn').bind('click',function(e){
        e.stopPropagation();
        var tdata = '<h3 class="share-tit">分享</h3>'
            +'		<div class="ccz-enterAreaDiv">'
            +'			<a href="#" class="ccz-userImg">'
            +'				<img src="assets/temp/5.png" alt="">'
            +'			</a>'
            +'			<div class="ccz-area">'
            +'				<textarea class="publish_textarea" name="" id="" cols="30" rows="10" placeholder="分享新鲜事..." id="Js_share_contact"></textarea>'
            +'				<s></s>'
            +'			</div>'
            +'			<div class="height_10"></div>'
            +'			<div class="clearfix">'
            +'				<div class="clearfix">'
            +'					<label style="line-height:16px;">添加：</label>'
            +'					<ul class="clearfix ccz-cz_list Js_ccz_list">'
            +'						<li>'
            +'							<a href="#" class="clearfix" id="Js_share_img">'
            +'								<span style="background-position:10px 2px;"></span><s>照片</s>'
            +'							</a>'
            +'						</li>'
            +'						<li>'
            +'							<a href="#" class="clearfix" id="Js_share_video">'
            +'								<span style="background-position:10px -22px;"></span><s>视频</s>'
            +'							</a>'
            +'						</li>'
            +'						<li>'
            +'							<a href="#" class="clearfix" id="Js_share_src">'
            +'								<span style="background-position:10px -49px;"></span><s>链接</s>'
            +'							</a>'
            +'						</li>'
            +'					</ul>'
            +'				</div>'
            +'				<div class="ml98 mt5 Js_ccblist_item">'
            +'					<div class="rowblock Js_cover" style="display:none">'
            +'						<div class="uploadWrap-single dargArea left">'
            +'							<span>拖动图片进此区域，或者<a href="javascript:;" class="uploadbox">点击</a>上传</span>'
            +'							<input class="file_uploadbox-single" type="file" accept="image/gif,image/jpeg,image/x-png,image/tiff" style="width: 1px;">'
            +'						</div>'
            +'						<div class="height_10"></div>'
            +'						<div id="upload-single" class="clearfix Js_cover_div upload-single"></div>'
            +'					</div>'
            +'					<div class="clearfix" style="display:none">'
            +'						<span class="left share-videoAddress">视频地址链接：</span>'
            +'						<div class="tools text left" style="width:50%;">'
            +'							<div class="inputWarp empty">'
            +'								<input type="text" style="color:#999999; font-family:simsun; font-size:12px;">'
            +'							</div>'
            +'						</div>'
            +'						<span class="share-icon-yz"></span>'
            +'					</div>'
            +'					<div class="clearfix" style="display:none">'
            +'						<div class="clearfix">'
            +'							<span class="left share-videoAddress">网址：</span>'
            +'							<div class="tools text left" style="width:50%;">'
            +'								<div class="inputWarp empty">'
            +'									<input type="text" style="color:#999999; font-family:simsun; font-size:12px;" value="'+window.location.href+'" />'
            +'								</div>'
            +'							</div>'
            +'						</div>'
            +'						<a href="javascript:;" class="share-presentWeb Js_presentWeb">分享当前页面</a>'
            +'					</div>'
            +'				</div>'
            +'			</div>'
            +'			<div class="height_12"></div>'
            +'			<div class="clearfix ccz-share_obj">'
            +'				<label style=" line-height:38px;">分享对象：</label>'
            +'				<div style="margin:0 96px 0 82px;">'
            +'					<div class="left ml16" style="width:100%; height:auto;">'
            +'						<div class="tools text pr Js_share_search clearfix" style="width:100%; height:auto;">'
            +'							<div class="inputWarp opt font12 pr left Js_input_w">'
            +'								<input type="text" class="df-text" />'
            +'								<span class="df-text defalutTxt Js_defualt_txt"> + 请输入好友姓名或圈子名称</span>'
            +'							</div>'
            +'						</div>'
            +'						<div class="share-menu Js_share_menu">'
            +'							<ul>'
            +'								<li class="share-gk" data-type="1"><span>公开分享</span></li>'
            +'								<li class="share-circle" data-type="2"><span>全部</span>（80）</li>'
            +'								<li class="share-circle" data-type="2"><span>熟人</span>（50）</li>'
            +'								<li class="share-person" data-type="3"><img src="assets/temp/c1.png" /><span>朋友</span>（32）</li>'
            +'								<li class="share-person" data-type="3"><img src="assets/temp/c1.png" /><span>同学</span>（16）</li>'
            +'								<li class="share-person" data-type="3"><img src="assets/temp/c1.png" /><span>家人</span>（2）</li>'
            +'							</ul>'
            +'						</div>'
            +'					</div>'
            +'				</div>'
            +'				<a href="#" class="ccz-add Js_webout_share">站外分享</a>'
            +'				<div class="webout-share">'
            +'					<s></s>'
            +'					<ul>'
            +'						<li class="Js_bind_webout">'
            +'							<input type="checkbox" />'
            +'							<img src="assets/temp/xl.png">'
            +'							<span>新浪微博</span>'
            +'						</li>'
            +'						<li class="Js_bind_webout">'
            +'							<input type="checkbox" />'
            +'							<img src="assets/temp/rrw.png">'
            +'							<span>人人网</span>'
            +'						</li>'
            +'						<li class="Js_bind_webout">'
            +'							<input type="checkbox" />'
            +'							<img src="assets/temp/txw.png">'
            +'							<span>腾讯微博</span>'
            +'						</li>'
            +'						<li class="Js_bind_webout">'
            +'							<input type="checkbox" />'
            +'							<img src="assets/temp/qqkj.png">'
            +'							<span>QQ空间</span>'
            +'						</li>'
            +'					</ul>'
            +'				</div>'
            +'			</div>'
            +'			<div class="height_25"></div>'
            +'			<div class="ccz-btnDiv">'
            +'				<a href="#" class="mr5 bshy Js_share_cancel">取消</a>'
            +'				<a href="#" class="bqh Js_share_ok">分享</a>'
            +'			</div>'
            +'		</div>'

        var obj = $(".Js_shareScHead");
        $(".search-result").hide();
        $('.Js_ccbNum').hide();
        $(".information").hide();
        $('.ccz-scHead').hide();
        $(".Js_ccz_ydDiv").hide();
        $('.Js_yjfk_div').hide();
        if(obj.is(":hidden")){
            obj.show();
            //obj.html(tdata);
            shareSpanWidth();
        }else{
            //obj.hide();
        }
        return false;
    });

    $('.Js_share_cancel').live('click',function(){$(".Js_shareScHead").hide()});
    $('.Js_share_ok').live('click',function(){
        $('.Js_shareScHead').hide();
        chtml = '<div class="popup">'
            +'		<div class="popup_sucess_image">'
            +'			<img src="assets/images/popup_sucess.png" />'
            +'		</div>'
            +'		<div class="popup_message">'
            +'			<div class="popup_message_notice">恭喜您已成功分享了一条新的创博</div>'
            +'		</div>'
            +'		<div class="popup_button_single">'
            +'			<a class="popup_button_close Js_share_fancybox_close" href="javascript:;">关闭</a>'
            +'			<a class="popup_button_continue" href="profile-self.php">查看详情</a>'
            +'		</div>'
            +'	</div>'
        $.fancybox({
            fitToView	: false,
            autoHeight	: true,
            autoSize	: false,
            width		: 470,
            content     : chtml,
            padding		: 0,
            beforeShow 	: function () {
                $('.Js_share_fancybox_close').click(function(){
                    $.fancybox.close();
                })
            }
        })
    });
    $('.Js_presentWeb').live('click',function(){

    })

    $('.Js_user_select').bind('click',function(e){
        e.stopPropagation();
        var obj = $('.Js_ccbNum');
        $(".search-result").hide();
        $(".information").hide();
        $('.Js_yjfk_div').hide();
        $('.Js_shareScHead').hide();
        if(obj.is(":hidden")){
            obj.show();
        }else{
            obj.hide()
        }
    })

    initRadio();
    function initRadio(){
        $('.js_radio').mousedown(function(){
            var selected = $(this).find('i');
            var flag = selected.is(':visible');
            $('.js_radio[data-name='+$(this).attr('data-name')+']').find('i').not(selected).hide();
            if(!flag){
                selected.show();
                $(this).find('input[name='+$(this).attr("data-name")+']').attr('checked','checked');
            };
            if($(this).parent().find('.tools').length>0){
                $(this).parent().find('.Js_ccbInput').focus();
            }else{
                $('.Js_ccbInput').blur();
            }
            return false;
        });

        $('.Js_bank').mousedown(function(){
            $('.Js_bank').find('img').css('border','1px solid #dddddd');
            $(this).find('img').css('border','1px solid #ffaa33');
        })
    }

    $('.js_checkbox').live('mousedown',function(){
        var selected = $(this).find('i');
        var flag = selected.is(':visible');
        if(flag){
            selected.hide();
            $(this).find('input[name='+$(this).attr("data-name")+']').removeAttr('checked');
        }else{
            selected.show();
            $(this).find('input[name='+$(this).attr("data-name")+']').attr('checked','checked');
        }
        return false;
    });
    $('.js_checkBox').live('mousedown',function(){
        var selected = $(this).find('input');
        var flag = selected.attr("checked");
        if(flag){
            selected.removeAttr('checked');
        }else{
            selected.attr('checked','checked');
        }
        return false;
    });

    /*
     评论交互
     */
    $('.Js_noneinput').live('click',function(){
        var that = $(this);
        that.hide();
        that.parent().next().show().find(".Js_subtextarea").show().find(".sub-text").focus();
        that.parent().next().show().find(".Js_subtextarea").prev().hide();
        container.masonry();
    })

    $(".Js_hinput").live("click",function(){
        var that = $(this);
        that.parents(".post-comment").find(".Js_subtextarea").show().find(".sub-text").focus();
        that.parent().hide();
        that.val('');
        container.masonry();
    })

    $(".Js_topinput").bind("click",function(){
        var that = $(this);
        var tempInput = '	<div class="post-repeat-wrap">'
            +'		<div class="post-repeat-box Js_subtextarea">'
            +'			<div class="p-c-list ">'
            +'				<img class="p-c-avatar" src="assets/temp/7.png" alt="">'
            +'				<div class="p-c-con">'
            +'					<textarea class="sub-text" name="" cols="30" rows="10"></textarea>'
            +'					<div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle_top">取消</span></div>'
            +'				</div>'
            +'			</div>'
            +'		</div>'
            +'	</div>';
        that.parents(".psot-like").next(".post-comment").show();
        that.parents(".psot-like").next(".post-comment").html(tempInput).find(".Js_subtextarea").show();
        that.hide();
        $(".Js_c_cancle_top").unbind('click', cancleTextarea).bind('click',{me:$(this)},cancleTextarea);
        container.masonry();
    })

    $('.Js_c_cancle').live('click',function(){
        if($(this).parents('.post-repeat-wrap').prev().find('.Js_itemlist').children().length>0){
            $(this).parents(".post-comment").find(".post-repeat-text").show();
            $(this).parents(".Js_subtextarea").hide();
        }else{
            $(this).parents(".post-comment").hide();
            $(this).parents(".post-comment").prev().find('.Js_noneinput').show();
        }
        container.masonry()
    })

    function cancleTextarea(event){
        var obj = event.data.me;
        obj.parents(".posts-box").find(".post-repeat-wrap").remove();
        obj.parents(".posts-box").find(".post-comment").hide();
        obj.show();
        container.masonry()
    }

    $(".p-c-title").live("click",function(){
        if($(this).attr('data-type')=="true"){
            $(this).next().children().filter(':gt(2)').hide();
            $(this).attr('data-type',false);
        }else if($(this).attr('data-type')=="false" || !$(this).attr('data-type')){
            $(this).next().children().show();
            $(this).attr('data-type',true);
        }
        container.masonry()
        //$(this).attr('data-type',true);
    })

    $(".Js_sub_comment").live("click",function(){
        var that = $(this);
        var valobj = that.parents(".p-c-con").find(".sub-text");
        var ccommon = that.parents(".post-comment");
        var tdata = '<li>'
            +'	<div class="p-c-list">'
            +'		<img class="p-c-avatar" src="assets/temp/7.png" alt="">'
            +'		<div class="p-c-con">'
            +'			<p class="top"><a class="cname Js_visitCard" href="#">刘兆宇</a><span class="ctime">2:27PM</span></p>'
            +'			<p class="con">'+valobj.val()+'</p>'
            +'		</div>'
            +'	</div>'
            +'</li>';
        if(valobj.val()!==""){
            if(!ccommon.find(".post-c-con").length>0){
                var listwrap = '<div class="post-c-con">'
                    +'		<div class="p-c-title"><b>1</b> 条评论</div>'
                    +'		<ul class="Js_itemlist clearfix">'
                    +'		</ul>'
                    +'	</div>';
                ccommon.prepend(listwrap);

                var subcon = '<div class="post-repeat-wrap">'
                    +'		<div class="post-repeat-text" style="display: none;">'
                    +'			<input class="Js_hinput" type="text" placeholder="添加您的评论...">'
                    +'		</div>'
                    +'		<div class="post-repeat-box Js_subtextarea" style="display: block;">'
                    +'			<div class="p-c-list ">'
                    +'				<img class="p-c-avatar" src="assets/temp/7.png" alt="">'
                    +'				<div class="p-c-con">'
                    +'					<textarea class="sub-text" name="" cols="30" rows="10"></textarea>'
                    +'					<div class="btn-bar clearfix"><span class="s-cBtn Js_sub_comment">发表评论</span><span class="c-cBtn ml10 Js_c_cancle">取消</span></div>'
                    +'				</div>'
                    +'			</div>'
                    +'		</div>'
                    +'	</div>';
                ccommon.append(subcon);
                that.parents(".post-repeat-wrap").remove();
            }
            ccommon.find(".Js_itemlist").append(tdata);
            ccommon.find('.p-c-title b').text(parseInt(ccommon.find('.p-c-title b').text(),10)+1)
            valobj.val("");
            ccommon.find(".post-repeat-text").show();
            ccommon.find(".Js_subtextarea").hide();
            container.masonry();
        }
    })


    /*
     城市联动点击事件
     */
    var x = 2;/*
    $('.Js_state').click(function(){
        x++;
        $('.Js_state').find('ul').hide();
        $(this).find('ul').show();
        $(this).css("z-index",x);
        $(document).one('click',function(){
            $('.Js_state ul').hide();
        });
        return false;
    });*/
    $('.Js_state').live('click',function(){
        x++;
        $('.Js_state').find('ul').hide();
        $(this).find('ul').show();
        $(this).css("z-index",x);
        $(document).one('click',function(){
            $('.Js_state ul').hide();
        });
        return false;
    });
    /*
    $('.Js_state ul li').click(function(){
        var index = $(this).parent().children().index($(this));
        $(this).parent().prev().find('span').text($(this).text());
        $(this).parent().hide();
        //$(this).parent().next().children().eq(index)[0].selected = true;
        return false;
    });*/
    $('.Js_state ul li').live('click',function(){
        var index = $(this).parent().children().index($(this));
        $(this).parent().prev().find('span').text($(this).text());
        $(this).parent().hide();
        //$(this).parent().next().children().eq(index)[0].selected = true;
        return false;
    });

})
$(function(){
    $('.Js_enterOnlyNum').keyup(function(e){
        if(e.keyCode<37 || (e.keyCode>40&&e.keyCode<48) || (e.keyCode>57&&e.keyCode<96) || e.keyCode > 105){
            if(e.keyCode!=8)
                return false;
        }
    }).keydown(function(e){
            if(e.keyCode<37 || (e.keyCode>40&&e.keyCode<48) || (e.keyCode>57&&e.keyCode<96) || e.keyCode > 105){
                if(e.keyCode!=8)
                    return false;
            }
        })
});

function birthday(){
    $('#birthdayDateBox').datepicker({ picker: "<img class='picker' align='middle' alt=''/>",applyrule:bir,onloaded:onloaderCallBack});
    function onloaderCallBack(obj){
        //点击后的回调
    }
    function bir(id) {
        //before start time picker drops down
        var today =new Date();
        var aM =today.getMonth(),
            aD =today.getDate(),
            aY =today.getFullYear();
        var customDate = new Date(aY,aM,aD);//可以随便自定义日期
        var v = aY + '/' + aM + '/' + aD;
        var d = v.match(/^(\d{1,4})(-|\/|.)(\d{1,2})\2(\d{1,2})$/);
        var nd = new Date(parseInt(d[1], 10), parseInt(d[3], 10) , parseInt(d[4], 10));
        return { enddate: nd };
    }
}


function tDataControl(date1,date2){
    if($('#'+date1).length>0){
        if(date2){
            $('#'+date1).datepicker({ picker: "<img class='picker' align='middle' alt=''/>",applyrule:rule,onloaded:onloaderCallBack});
            $("#"+date2).datepicker({ picker: "<img class='picker' align='middle' alt=''/>",applyrule:rule,onloaded:onloaderCallBack});
        }else{
            $("#"+date1).datepicker({ picker: "<img class='picker' align='middle' alt=''/>",applyrule:rule,onloaded:onloaderCallBack});
        }
    }
    function onloaderCallBack(obj){
        //点击后的回调
    }

    function rule(id) {
        //before start time picker drops down
        var today =new Date();
        var aM =today.getMonth(),
            aD =today.getDate(),
            aY =today.getFullYear();
        var customDate = new Date(aY,aM,aD);//可以随便自定义日期
        if (id == date1) {
            var v = $("#"+date2).val();
            if (!v || v == "") {
                //return { startdate: customDate };
            }else {
                //To get end time. All dates after end time will be disabled.
                var d = v.match(/^(\d{1,4})(-|\/|.)(\d{1,2})\2(\d{1,2})$/);
                if (d != null) {
                    var nd = new Date(parseInt(d[1], 10), parseInt(d[3], 10) - 1, parseInt(d[4], 10));
                    return { enddate: nd, startdate: customDate };
                }else {
                    return { startdate: customDate };
                }
            }
        }else {
            var v = $("#"+date1).val();
            if (!v || v == "") {
                return { startdate: customDate };
            }else {
                //To get start time. All dates before start time will be disabled.
                var d = v.match(/^(\d{1,4})(-|\/|.)(\d{1,2})\2(\d{1,2})$/);
                if (d != null) {
                    var nd = new Date(parseInt(d[1], 10), parseInt(d[3], 10) - 1, parseInt(d[4], 10));
                    return { startdate: nd };
                }else {
                    return { startdate: customDate }
                }
            }
        }
    }
}

function dataControl(date1,date2){
    $('.datebox').click(function(){
        $('.Js_cityList').find('s').show().next().css('margin-left','55px');
        $('.Js_cityList').find('input').next().css('left','-43px');
        $('.Js_cityList').find('input').next().hide();
    })
    if($('#'+date1).length>0){
        if(date2){
            if($('#'+date1).attr('data-type')=="history"&&$('#'+date2).attr('data-type')=="history"){
                $('#'+date1+",#"+date2).datepicker({ picker: "<img class='picker' align='middle' src='assets/images/cal.gif' alt=''/>",applyrule:rule1,onloaded:onloaderCallBack1});
            }else{
                $('#'+date1).datepicker({ picker: "<img class='picker' align='middle' src='assets/images/cal.gif' alt=''/>",applyrule:rule,onloaded:onloaderCallBack});
                $("#"+date2).datepicker({ picker: "<img class='picker' align='middle' src='assets/images/cal.gif' alt=''/>",applyrule:rule,onloaded:onloaderCallBack});
            }
        }else{
            $("#"+date1).datepicker({ picker: "<img class='picker' align='middle' src='assets/images/cal.gif' alt=''/>",applyrule:rule,onloaded:onloaderCallBack});
        }
    }
    function onloaderCallBack(obj){
        //点击后的回调
    }


    function rule(id) {
        //before start time picker drops down
        var today =new Date();
        var aM =today.getMonth(),
            aD =today.getDate()+1,
            aY =today.getFullYear();
        var customDate = new Date(aY,aM,aD);//可以随便自定义日期

        if (id == date1) {
            var v = $("#"+date2).val();
            if (!v || v == "") {
                return { startdate: customDate };
            }else {
                //To get end time. All dates after end time will be disabled.
                var d = v.match(/^(\d{1,4})(-|\/|.)(\d{1,2})\2(\d{1,2})$/);
                if (d != null) {
                    var nd = new Date(parseInt(d[1], 10), parseInt(d[3], 10) - 1, parseInt(d[4], 10));
                    return { enddate: nd, startdate: customDate };
                }else {
                    return { startdate: customDate };
                }
            }
        }else {
            var v = $("#"+date1).val();
            if (!v || v == "") {
                return { startdate: customDate };
            }else {
                //To get start time. All dates before start time will be disabled.
                var d = v.match(/^(\d{1,4})(-|\/|.)(\d{1,2})\2(\d{1,2})$/);
                if (d != null) {
                    var nd = new Date(parseInt(d[1], 10), parseInt(d[3], 10) - 1, parseInt(d[4], 10));
                    return { startdate: nd };
                }else {
                    return { startdate: customDate }
                }
            }
        }
    };

    function rule1(id) {
        //before start time picker drops down
        var customDate = new Date();//可以随便自定义日期
        customDate.setTime(0);
        if (id == date1) {
            var v = $("#"+date2).val();
            var d = v.match(/^(\d{1,4})(-|\/|.)(\d{1,2})\2(\d{1,2})$/);
            var nd = new Date(parseInt(d[1], 10), parseInt(d[3], 10) - 1, parseInt(d[4], 10));
            return { enddate: nd, startdate: customDate};
        }else {
            var v = $("#"+date1).val();
            var d = v.match(/^(\d{1,4})(-|\/|.)(\d{1,2})\2(\d{1,2})$/);
            var cn = new Date(parseInt(d[1], 10), parseInt(d[3], 10) - 1, parseInt(d[4], 10));
            var nd = new Date();
            return { enddate: nd, startdate: cn };
        }
    }

    function onloaderCallBack1(obj){
        //点击后的回调
    }

    /**/
}

var JS_GMAP_ADDRESS_SEARCH = {
    init : function(){
        this.domEvent();
    },

    menuListHide : function(o){
        o.find('s').show().next().css('margin-left','55px');
        o.find('input').next().css('left','-43px').hide();
    },

    menuListShow : function(o){
        var _this = this;
        var input = o.find('input')
        o.find('s').hide().next().css('margin-left','15px');
        input.next().css('left','-13px');
        input.next().show();
        $(document).one('click.map_search',function(){
            _this.menuListHide($('.Js_cityList'));
        });
    },


    domEvent : function(){
        var _this = this;
        $('.Js_cityList input').bind('keyup',function(e){
            _this.mapInputSearch($(this),e);
        });

        $('.Js_cityList').mousedown(function(){
            _this.menuListShow($(this));
            _this.hideDateControl();
            $(this).find('input').focus();
        }).click(function(){
                return false;
            });

        $('.Js_loc_ad').click(function(){
            locFn($(this).parents('.Js_cityList').find('input'));
        });

        $('.Js_world_ad').click(function(){
            $(this).parents('.Js_cityList').find('input').val('全世界').next().hide();
            $(document).trigger('click.map_search');
            $(this).parents('.Js_cityList').find('s').css('background','url(assets/images/icon-match.png) 12px -117px no-repeat')
            return false;
        })
    },

    mapInputSearch : function(o,event){
        var _this = this;
        var val = o.val();
        if(o.val()!==""){
            var pla = new google.maps.places.AutocompleteService();
            pla.getPlacePredictions({input:val},function(a,b){
                o.next().find('ul').children(':gt(2)').remove();
                if(a){
                    for(var i=0; i<a.length; i++){
                        var tp = '<li><a href="javascript:;" class="abcdefg">'+a[i].description+'</a></li>';
                        o.next().find('ul').append(tp);
                    }
                    o.next().find('ul').children(':gt(2)').unbind('click').bind('click',function(){
                        var val = $(this).text();
                        $(this).parents('.Js_cityList').find('input').val(val);
                        $(this).parents('.Js_cityList').find('s').css('background','url(assets/images/dw.png) no-repeat center center');
                        $(document).trigger('click.map_search');
                        return false;
                    })
                    o.next().show();
                }else{
                    o.next().find('ul').children(':gt(2)').remove();
                }
            })
        }else{
            o.next().find('ul').children(':gt(2)').remove();
        }
        return false;
    },

    hideDateControl : function(){
        if($(".bbit-dp").length>0){
            $(".bbit-dp").css({"visibility":"hidden","display":"none"});
        }
    }

}



var JS_FINDFRIEND_COMMEN = {
    cardObj : null,
    init :function(){
        this.DomEvent();
        this.bindData();
    },

    createBox : function(){
        var chtml = '<div class="Js_commen_add_friendbox addFriend-box">'
            +'		<div class="addFriendDiv">'
            +'		<ul id="Js_commen_circleList">'
            +'			<li class="clearfix">'
            +'				<s><b></b></s>'
            +'				<input type="checkbox" />'
            +'				<span data-type="py">朋友</span>'
            +'				<i>8</i>'
            +'			</li>'
            +'			<li class="clearfix">'
            +'				<s><b></b></s>'
            +'				<input type="checkbox" />'
            +'				<span data-type="jt">家庭</span>'
            +'				<i>8</i>'
            +'			</li>'
            +'			<li class="clearfix">'
            +'				<s><b></b></s>'
            +'				<input type="checkbox" />'
            +'				<span data-type="rs">认识的人</span>'
            +'				<i>8</i>'
            +'			</li>'
            +'			<li class="clearfix">'
            +'				<s><b></b></s>'
            +'				<input type="checkbox" />'
            +'				<span data-type="gz">关注对象</span>'
            +'				<i>8</i>'
            +'			</li>'
            +'			<li class="clearfix">'
            +'				<s><b></b></s>'
            +'				<input type="checkbox" />'
            +'				<span>摄影和美术</span>'
            +'				<i>8</i>'
            +'			</li>'
            +'		</ul>'
            +'		<div class="addformDiv">'
            +'			<a href="javascript:;" class="lys Js_comment_add_circle">创建新的圈子</a>'
            +'			<div style="display:none;">'
            +'				<input type="text" />'
            +'				<a href="javascript:;" class="addBtn Js_comment_addBtn">创建</a>'
            +'			</div>'
            +'		</div>'
            +'		</div>'
            +'	</div>'
        $('body').append(chtml);
    },

    bindData : function(){
        $('.Js_visitCard').each(function(){
            $(this)[0].arr = [];
            $(this)[0].addItem = [];
        })
    },

    createCircleBoxColor : {
        initBoxColor : ['#c0c0c0','#7bab73','#3b86c4','#abae8f','#cf6e6e'],
        randomColor : function(){
            var color = '#' + (function(color){
                return (color +=  '0123456789abcdef'[Math.floor(Math.random()*16)])
                    && (color.length == 6) ?  color : arguments.callee(color);
            })('');

            if($.inArray(color,this.initBoxColor)<0){
                this.initBoxColor.push(color);
                return color;
            }else{
                this.randomColor();
            }
        }
    },

    createCircleEvent : function(circleId){
        var val = $('.Js_comment_addBtn').prev().val();
        var color = this.createCircleBoxColor.randomColor();
        var chtml = '<div class="circle-itemDiv"><div class="circle-item normal Js_add" style="background:'+color+'">'
            +'		<div class="item-wrap">'
            +'			<div class="item-td">'
            +'				<span class="c-name" data-type="'+circleId+'">'+val+'</span>'
            +'				<span class="c-number">0</span>'
            +'			</div>'
            +'		</div>'
            +'	</div></div>'
        $('.Js_circle_item_wrap').append(chtml);
    },

    DomEvent : function(){
        var _this = this;
        if($('.Js_commen_add_friendbox').length>0){
            $(document).mousemove(function(e){
                if(e.pageX > ($('.Js_commen_add_friendbox').position().left + $('.Js_commen_add_friendbox').outerWidth()) || e.pageX < $('.Js_commen_add_friendbox').position().left){
                    $('.Js_commen_add_friendbox').remove();
                }
                if(e.pageY > ($('.Js_commen_add_friendbox').position().top + $('.Js_commen_add_friendbox').outerHeight()) || e.pageY < $('.Js_commen_add_friendbox').position().top){
                    $('.Js_commen_add_friendbox').remove();
                }
            })
        }

        $('.Js_commen_friend_add').live('mouseenter',function(){
            _this.mouseEnter(_this,$(this));
        })

        $('.Js_commen_add_friendbox').live('mouseleave',function(e){
            if($(e.relatedTarget).parents('.Js_vc_txt').length <= 0){
                $('.Js_vc_txt').mouseleave();
            }
            $(this).remove();
        })

        $('.Js_comment_add_circle').live('click',function(){
            $(this).hide();
            $(this).next().show();
        });

        $('.Js_comment_addBtn').live('click',function(){
            _this.addCircleItem(_this,$(this));
        });

        $('#Js_commen_circleList li').live('click',function(){
            var o = $(this).find('b');
            if(o.is(':hidden')){
                o.show().parent().next().attr('checked',true);
                $(this).find('i').text(parseInt($(this).find('i').text(),10)+1);
                _this.box[0].arr.push($(this));
                if(!_this.box.attr('data-type')){
                    _this.box.attr('data-type',$(this).find('span').attr('data-type'));
                }else{
                    var wb = _this.box.attr('data-type');
                    _this.box.attr('data-type',wb + "|" + $(this).find('span').attr('data-type'));
                }
            }else{
                o.hide().parent().next().attr('checked',false);
                $(this).find('i').text(parseInt($(this).find('i').text(),10)-1);
                for(var i=0; i<_this.box[0].arr.length; i++){
                    if(_this.box[0].arr[i].find('span').text()===$(this).find('span').text()){
                        var del = _this.box[0].arr[i].find('span').attr('data-type');
                        _this.box[0].arr.splice(i,1);
                        //var strIndex = _this.box.attr('data-type').indexOf(del);
                        var arrStr = _this.box.attr('data-type').split("|");
                        arrStr.splice($.inArray(del,arrStr),1);
                        if(arrStr.length>0){
                            var delStr = arrStr.join("|");
                            _this.box.attr('data-type',delStr)
                        }else{
                            _this.box.removeAttr('data-type');
                        }
                        /*if(_this.box.attr('data-type').substr(strIndex-1,1)=="|"){
                         var delStr = _this.box.attr('data-type').replace('|'+del,'');
                         _this.box.attr('data-type',delStr)
                         }else{
                         _this.box.removeAttr('data-type');
                         }*/
                    }
                }
            }
            if(_this.box[0].arr.length>0){
                if(_this.box[0].arr.length==1){
                    _this.sBox.css({'background':'#7bab73','border-bottom':'1px solid #7bab73'}).children().text(_this.box[0].arr[0].find('span').text()).addClass('circle-addtoed');
                }else{
                    _this.sBox.css({'background':'#7bab73','border':'1px solid #7bab73'}).children().text(_this.box[0].arr.length + "个圈子").addClass('circle-addtoed');
                }
            }else{
                _this.sBox.css({'background':'#fff','border':'1px solid #d9d9d9'}).children().text('添加').removeClass('circle-addtoed');
            }
        })
    },

    sCheckBox : function(){
        for(var i=0; i<this.box[0].arr.length; i++){
            var o = this.box[0].arr[i];
            $('#Js_commen_circleList li').each(function(){
                if($(this).find('span').text()==o.find('span').text()){
                    $(this).find('b').show();
                    $(this).find('i').text(o.find('i').text());
                }
            })
        }
    },

    mouseEnter : function(self,me){
        if($('.Js_commen_add_friendbox ').length>0) $('.Js_commen_add_friendbox').remove();
        var h = document.documentElement.clientHeight + document.body.scrollTop;
        var offset = me.offset();
        self.box = self.cardObj;
        self.sBox = me;
        self.createBox();
        if(h<$('.Js_commen_add_friendbox').height() + offset.top){
            var y = offset.top-$('.Js_commen_add_friendbox').height()+me.outerHeight();
            $('.Js_commen_add_friendbox').css({left:offset.left,top:y});
        }else{
            $('.Js_commen_add_friendbox').css({left:offset.left,top:offset.top});
        }
        this.sCheckBox();
        for(var i=0; i<self.box[0].addItem.length; i++){
            $('#Js_commen_circleList').append(self.box[0].addItem[i]);
        }
    },

    addCircleItem : function(self,me){
        var o = me.prev();
        var isHaveCircle = true;
        if(o.val()=="") return;
        $('#Js_commen_circleList li').each(function(){
            if($(this).find('span').text()==o.val()){
                alert('已有该圈子');
                isHaveCircle = false;
                return false;
            }
        })
        if(!isHaveCircle) return;
        var newCircleId = "ncr"; //新圈子的id号
        if($('.circle-item-list').length>0){
            self.createCircleEvent(newCircleId);
        }
        var chtml = $('<li class="clearfix">'
            +'				<s><b></b></s>'
            +'				<input type="checkbox" />'
            +'				<span data-type="'+newCircleId+'">'+o.val()+'</span>'
            +'				<i>1</i>'
            +'			</li>');
        chtml.find('b').show();
        chtml.find('input').attr('checked',true);
        o.val('').parent().hide().prev().show();
        $('#Js_commen_circleList').append(chtml);
        this.box[0].addItem.push(chtml);
        this.box[0].arr.push(chtml);
        if(!this.box.attr('data-type')){
            this.box.attr('data-type',newCircleId);
        }else{
            var wb = this.box.attr('data-type');
            this.box.attr('data-type',wb + "|" + newCircleId);
        }
        if(self.box[0].arr.length>0){
            if(self.box[0].arr.length==1){
                self.sBox.css({'background':'#7bab73','border':'1px solid #7bab73'}).children().text(self.box[0].arr[0].find('span').text()).addClass('circle-addtoed');
            }else{
                self.sBox.css({'background':'#7bab73','border':'1px solid #7bab73'}).children().text(self.box[0].arr.length + "个圈子").addClass('circle-addtoed');
            }
        }else{
            self.sBox.css({'background':'#fff','border':'1px solid #d9d9d9'}).children().text('添加').removeClass('circle-addtoed');
        }
    }
}

function locFn(o){
    if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(function(position){
            var coords = position.coords;
            //var latlng = new google.maps.LatLng(30.657358499999994, 104.049977);
            var latlng = new google.maps.LatLng(coords.latitude, coords.longitude);
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == 'OK') {
                    var flag = false;
                    for(var i=0; i<results.length; i++){
                        var address = results[i];
                        for(var j=0; j<address.types.length; j++){
                            if(address.types[j]=="sublocality"){
                                flag = true;
                            }
                        }

                        if(flag){
                            var s = address.formatted_address;
                            break;
                        }
                    }
                    o.val(s);
                    o.next().hide();
                    o.parents('.Js_cityList').find('s').css('background','url(assets/images/dw.png) no-repeat center center');
                    $(document).trigger('click.map_search');
                    /*if (results[1]) {
                     var s = "";
                     for(var i=results[1].address_components.length-1; i>=0; i--){
                     var o = results[1].address_components[i];
                     var flag = true;
                     for(var j=0; j<o.types.length; j++){
                     if(o.types[j]=="postal_code"){
                     flag = false;
                     break;
                     }
                     }

                     if(flag){
                     s+=o.long_name;
                     }
                     }
                     o.val(s);
                     o.next().hide();
                     o.parents('.Js_cityList').find('s').css('background','url(assets/images/dw.png) no-repeat center center');
                     $(document).trigger('click.map_search');
                     }*/
                } else {
                }
            });
        },function(error){
            var errorTypes = {
                1 : '位置服务被拒绝',
                2 : '获取不到的位置信息',
                3 : '获取信息超时'
            }
        });
    }else{
        alert('浏览器不支持该功能');
    }
}

function pageChange(obj,leftName,rightName){
    var o = obj;
    var oUl = obj.find('ul');
    var oLi = oUl.children();
    var timer = null;
    var num = 3;

    oUl.css('position','relative');
    oLi.each(function(a,b){
        if(a>=3){
            $(this).css({'position':'absolute','top':0,'left':o.outerWidth()})
        }else{
            $(this).css({'position':'absolute','top':0,'left':a*($(this).width()+14)})
        }
    })

    var leftBtn = $('<span style="width:31px; height:57px; background:url(assets/images/icon_dir.png); position:absolute; left:-60px; cursor:pointer" id="'+leftName+'"></span>');
    var rightBtn = $('<span style="width:31px; height:57px; background:url(assets/images/icon_dir.png) no-repeat 0 -57px; position:absolute; right:-58px; cursor:pointer" id="'+rightName+'"></span>');
    leftBtn.css('top',(o.outerHeight()-leftBtn.height())/2);
    rightBtn.css('top',(o.outerHeight()-rightBtn.height())/2);
    if(oLi.length>3){
        o.prepend(leftBtn);
        o.prepend(rightBtn);
    }else{
        if($('#'+leftName).length>0){
            $('#'+leftName).remove();
        }
        if($('#'+rightName).length>0){
            $('#'+rightName).remove();
        }
    }
    $('#'+rightName).die('click').live('click',function(){
        var sn = 0;
        if(!timer){
            timer = window.setInterval(function(){
                var speed = Math.ceil((954-sn)/5)
                sn += speed;
                oLi.eq(0).css('left',oLi.eq(0).position().left - speed);
                for(var i=1; i<4; i++){
                    oLi.eq(i).css('left',oLi.eq(i-1).position().left+oLi.eq(i-1).width()+14);
                }

                if(oLi.eq(0).position().left<=-318){
                    var s = oLi.eq(0).clone(true);
                    oLi.eq(0).remove();
                    oUl.append(s);
                    oLi = oUl.children();
                }
                if(speed==0){
                    window.clearInterval(timer);
                    timer = null;
                }
            },33);
        }
    });

    $('#' + leftName).die('click').live('click',function(){
        var sn = 0;
        if(!timer){
            timer = window.setInterval(function(){
                var speed = Math.floor((-954-sn)/5)
                sn += speed;
                oLi.eq(2).css('left',oLi.eq(2).position().left - speed);
                for(var i=1; i>=-1; i--){
                    oLi.eq((oLi.length+i)%oLi.length).css('left',oLi.eq(i+1).position().left-oLi.eq(i+1).width()-14);
                }

                if(oLi.eq(2).position().left>=o.width() ){
                    var s = oLi.eq(oLi.length-1).clone(true);
                    oLi.eq(oLi.length-1).remove();
                    oUl.prepend(s);
                    oLi = oUl.children();
                }

                if(speed==0){
                    window.clearInterval(timer);
                    timer = null;
                }
            },33);
        }
    })
}

function userlogin(){
    $('#button_login').click(function(){/*
     var step = '';
     if($('#Js_user_name').val()==''){
     $('#Js_user_name').parents('.tools').addClass('error').next().show();
     step = 'one';
     }else{
     $('#Js_user_name').parents('.tools').removeClass('error').next().hide();
     }

     if($('#Js_user_pwd').val()==''){
     $('#Js_user_pwd').parents('.tools').addClass('error').next().show();
     step = 'one';
     }else{
     $('#Js_user_pwd').parents('.tools').removeClass('error').next().hide();
     }*/
        //if(step==''){
        var data = $('#form_login').serialize();
        $.ajax({
            url:'test_login.php',
            dataType : 'json',
            data:data,
            type : 'post',
            success : function(data){
                if(data.status == 0){
                    window.location.href = 'index.php?isLogin=1';
                }else{
                    var fields = ['login_name', 'password'];
                    checkErrorCommon(fields, data.error);
                }
            }
        });
        //}
    })
}


function user_register(){
    $('.Js_do_register').click(function(){/*
     var step = '';
     if($('#Js_register_email').val()==''){
     $('#Js_register_email').parents('.tools').addClass('error').next().show();
     step = 'one';
     }else{
     $('#Js_register_email').parents('.tools').removeClass('error').next().hide();
     }

     if($('#Js_register_name').val()==''){
     $('#Js_register_name').parents('.tools').addClass('error').next().show();
     step = 'one';
     }else{
     $('#Js_register_name').parents('.tools').removeClass('error').next().hide();
     }

     if($('#Js_register_pwd').val()==''){
     $('#Js_register_pwd').parents('.tools').addClass('error').next().show();
     step = 'one';
     }else{
     $('#Js_register_pwd').parents('.tools').removeClass('error').next().hide();
     }

     if($('#Js_register_again').val()==''){
     $('#Js_register_again').parents('.tools').addClass('error').next().show();
     step = 'one';
     }else{
     $('#Js_register_again').parents('.tools').removeClass('error').next().hide();
     }

     if($('#Js_register_code').val()==''){
     $('#Js_register_code').parents('.tools').addClass('error').next().next().show();
     step = 'one';
     }else{
     $('#Js_register_code').parents('.tools').removeClass('error').next().next().hide();
     }

     if(!$('#Js_register_chk').is(':checked')){
     $('#Js_register_checkerr').show();
     step = 'one';
     }else{
     $('#Js_register_checkerr').hide();
     }*/
        var email = $("input[name=email]").val();
        if(!checkTextForm.email(email)){
            $('#email .tools').addClass('error');
            $('#email .errorMsg').show().html("请输入正确格式的邮件");
        }else{
            var data = $('#form_register').serialize();
            $.ajax({
                url:'test_register.php',
                dataType : 'json',
                data:data,
                type : 'post',
                success : function(data){
                    if(data.status == 0){
                        window.location.href = 'index.php?isLogin=1';
                    }else{
                        var fields = ['user_name', 'password', 'email', 'password_again', 'verify_code', 'is_agree_protocol'];
                        checkErrorCommon(fields, data.error);
                    }
                }
            })
        }
        
        /*if(step==''){
        }*/
    })

}
var checkTextForm = {/*文本格式检测*/
    email:function(value){
        var regexp = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
        if(regexp.test(value)){
            return true;
        }else{
            return false;
        }
    },
    cellphone:function(value){
        var regexp = /^((13[0-9]{1})|159|153)+\d{8}$/;
        if(regexp.test(value)){
            return true;
        }else{
            return false;
        }
    },
    telphone:function(value){
        var regexp = /^(([0\+]\d{2,3}-)?(0\d{2,3}))(\d{7,8})(-(\d{3,}))?$/;
        if(regexp.test(value)){
            return true;
        }else{
            return false;
        }
    },
    zipCode:function(value){
        var regexp = /^[1-9][0-9]{5}$/;
        if(regexp.test(value)){
            return true;
        }else{
            return false;
        }
    },
    nonZeroBegin:function(value){
        var regexp = /^[1-9][0-9]*$/;
        if(regexp.test(value)){
            return true;
        }else{
            return false;
        }
    },
    idCard:function(value){
        var Wi = [ 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1 ];// 加权因子   
        var ValideCode = [ 1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2 ];// 身份证验证位值.10代表X
        if (value.length == 15) {
            return isValidityBrithBy15IdCard(value);
        } else if (value.length == 18) {
            var a_value = value.split("");// 得到身份证数组
            if(isValidityBrithBy18IdCard(value)&&isTrueValidateCodeBy18IdCard(a_value)){
                return true;
            }else {
                return false;
            }
        } else {
            return false;
        }
        function isValidityBrithBy15IdCard(idCard15){
            var year =  idCard15.substring(6,8);
            var month = idCard15.substring(8,10);
            var day = idCard15.substring(10,12);
            var temp_date = new Date(year,parseFloat(month)-1,parseFloat(day));
            // 对于老身份证中的你年龄则不需考虑千年虫问题而使用getYear()方法
            if(temp_date.getYear()!=parseFloat(year)
                ||temp_date.getMonth()!=parseFloat(month)-1
                ||temp_date.getDate()!=parseFloat(day)){
                return false;
            }else{
                return true;
            }
        }
        function isValidityBrithBy18IdCard(idCard18){
            var year =  idCard18.substring(6,10);
            var month = idCard18.substring(10,12);
            var day = idCard18.substring(12,14);
            var temp_date = new Date(year,parseFloat(month)-1,parseFloat(day));
            // 这里用getFullYear()获取年份，避免千年虫问题
            if(temp_date.getFullYear()!=parseFloat(year)
                ||temp_date.getMonth()!=parseFloat(month)-1
                ||temp_date.getDate()!=parseFloat(day)){
                return false;
            }else{
                return true;
            }
        }
        function isTrueValidateCodeBy18IdCard(a_idCard) {
            var sum = 0; // 声明加权求和变量
            if (a_idCard[17].toLowerCase() == 'x') {
                a_idCard[17] = 10;// 将最后位为x的验证码替换为10方便后续操作
            }
            for ( var i = 0; i < 17; i++) {
                sum += Wi[i] * a_idCard[i];// 加权求和
            }
            var valCodePosition = sum % 11;// 得到验证码所位置
            if (a_idCard[17] == ValideCode[valCodePosition]) {
                return true;
            } else {
                return false;
            }
        }
    }
};

function checkErrorCommon(fields, error) {
    for (var i in fields) {
        id = fields[i];
        if (error.hasOwnProperty(id)) {
            $('#' + id + ' .tools').addClass('error');
            $('#' + id + ' .errorMsg').show().html(error[id]);
        } else {
            $('#' + id + ' .tools').removeClass('error');
            $('#' + id + ' .errorMsg').show().html("");
        }
    }
}
$(function(){
    $('.Js_l_xy').click(function(){
        var chtml = '<div style="margin:20px; font-size:12px; font-family:simsun; line-height:16px;">'
            +'		<p>1 注册</p>'
            +'		<p>1.1 注册协议</p>'
            +'		<p>1.1.1 简介</p>'
            +'		<br />'
            +'		<p>尊敬的用户，欢迎您阅读本协议：?</p>'
            +'		<br />'
            +'		<p>创意世界网站（www.csoow.com）和四川创意世界科技有限公司（以下简称创意世界）基于本协议的规定为用户提供服务，本协议具有合同效力。请您在使用创意世界网站（www.csoow.com）或四川创意世界科技有限公司提供的服务（以下简称创意世界的服务）前仔细阅读并审阅接受或不接受《注册协议》（以下简称协议）的所有条款（未成年人需在法定监护人的陪同下审阅）。除非用户接受本《协议》的条款，否则无权使用创意世界的服务。</p>'
            +'		<br />'
            +'		<p>一旦您开始使用创意世界的服务（包括但不限于：参观，浏览，注册，提交内容），则表示您完全同意遵守本协议的全部内容，届时您不应以未阅读本协议的内容为理由，主张本协议无效，或要求撤销本协议。</p>'
            +'		<br />'
            +'		<p>创意世界网站和四川创意世界科技有限公司（以下简称创意世界）有权根据需要不时地制订、修改本协议及/或各类规则，并以网站公示的方式进行公告，不再单独通知您。变更后的协议和规则一经在网站公布后，立即自动生效。如您不同意相关变更，应当立即停止使用创意世界的服务。您继续使用创意世界的服务，即表示您接受经修订的协议和规则。</p>'
            +'		<br />'
            +'		<p>1.1.2 用户资格</p>'
            +'		<br />'
            +'		<p>只有符合下列条件条件之一的人员或实体才能申请成为本网站用户，可以使用本网站的服务：</p>'
            +'		<br />'
            +'		<p>年满十八岁，并具有民事权利能力和民事行为能力的自然人；</p>'
            +'		<p>无民事行为能力人或限制民事行为能力人应经过其监护人的同意；</p>'
            +'		<p>根据中国法律、法规、行政规章成立并合法存在的机关、企事业单位、社团组织和其他组织。</p>'
            +'		<br />'
            +'		<p>若您不具备前述主体资格，则您及您的监护人应承担因此而导致的一切后果，且创意世界有权注销或永久冻结您的账户。</p>'
            +'		<br />'
            +'		<p>1.1.3 使用限制</p>'
            +'		<br />'
            +'		<p>用户必须在遵守中华人民共和国相关法律法规的前提下使用创意世界的服务。用户必须为自己账号的行为承担责任，不得允许任何第三方使用您的账户。用户保证不会利用创意世界的服务进行任何违法或不正当的活动，包括但不限于下列行为∶</p>'
            +'		<br />'
            +'		<p>煽动抗拒、破坏宪法和法律、行政法规实施的；</p>'
            +'		<p>煽动颠覆国家政权，推翻社会主义制度的；</p>'
            +'		<p>煽动分裂国家、破坏国家统一的；</p>'
            +'		<p>煽动民族仇恨、民族歧视，破坏民族团结的；</p>'
            +'		<p>捏造或者歪曲事实，散布谣言，扰乱社会秩序的；</p>'
            +'		<p>宣扬封建迷信、淫秽、色情、赌博、暴力、凶杀、恐怖、教唆犯罪的；</p>'
            +'		<p>公然侮辱他人或者捏造事实诽谤他人的，或者进行其他恶意攻击的；</p>'
            +'		<p>损害国家机关信誉的；</p>'
            +'		<p>其他违反宪法和法律行政法规的；</p>'
            +'		<p>以任何机器人软件、蜘蛛软件、爬虫软件、刷屏软件或其它自动方式扰乱或破坏本网站；</p>'
            +'		<p>通过任何方式对本网站内部结构造成或可能造成不合理或不合比例的重大负荷的行为；</p>'
            +'		<p>通过任何方式干扰或试图干扰网站的正常工作或网站上进行的任何活动；</p>'
            +'		<br />'
            +'		<p>1.1.4 服务的变更、中断或终止</p>'
            +'		<br />'
            +'		<p>用户在接受创意世界的服务的同时，同意接受创意世界提供的各类信息服务。用户在此授权创意世界可以向其电子邮件、手机、通信地址等发送商业信息。</p>'
            +'		<br />'
            +'		<p>用户有权选择不接受创意世界提供的各类信息服务，并进入创意世界网站相关页面进行更改。</p>'
            +'		<br />'
            +'		<p>创意世界保留随时修改或中断服务而不需通知用户的权利。创意世界有权行使修改或中断服务的权利，不需对用户或任何无直接关系的第三方负责。</p>'
            +'		<br />'
            +'		<p>用户对本协议的修改有异议，或对创意世界的服务不满，可以行使如下权利：</p>'
            +'		<br />'
            +'		<p>停止使用创意世界的服务；</p>'
            +'		<p>通过客服等渠道告知创意世界停止对其服务。 结束服务后，用户使用创意世界的服务的权利立即终止。在此情况下，创意世界没有义务传送任何未处理的信息或未完成的服务给用户或任何无直接关系的第三方。</p>'
            +'		<br />'
            +'		<p>1.1.5 创意世界的责任范围</p>'
            +'		<br />'
            +'		<p>当用户接受本协议时，应当明确了解并同意∶</p>'
            +'		<br />'
            +'		<p>本网站不能随时预见到任何技术上的问题或其他困难。</p>'
            +'		<br />'
            +'		<p>该等困难可能会导致数据损失或其他服务中断。本网站是在现有技术基础上提供的服务。本网站不保证以下事项∶ </p>'
            +'		<br />'
            +'		<p>本网站将符合所有用户的要求；</p>'
            +'		<p>本网站不受干扰、能够及时提供、安全可靠或免于出错；</p>'
            +'		<p>本服务使用权的取得结果是正确或可靠的。</p>'
            +'		<br />'
            +'		<p>是否经由本网站下载或取得任何资料，由用户自行考虑、衡量并且自负风险，因下载任何资料而导致用户电脑系统的任何损坏或资料流失，用户应负完全责任。希望用户在使用本网站时，小心谨慎并运用常识；</p>'
            +'		<br />'
            +'		<p>用户经由本网站取得的建议和资讯，无论其形式或表现，绝不构成本协议未明示规定的任何保证；</p>'
            +'		<br />'
            +'		<p>基于以下原因而造成的利润、商誉、使用、资料损失或其它无形损失，本网站不承担任何直接、间接、附带、特别、衍生性或惩罚性赔偿（即使本网站已被告知前款赔偿的可能性）：</p>'
            +'		<br />'
            +'		<p>本网站的使用或无法使用；</p>'
            +'		<p>用户的传输或资料遭到未获授权的存取或变更；</p>'
            +'		<p>本网站中任何第三方之声明或行为；</p>'
            +'		<p>本网站只是为用户提供一个服务交易的平台，对于用户所发布的内容的合法性、真实性及其品质，以及用户履行交易的能力等，本网站一律不负任何担保责任；</p>'
            +'		<br />'
            +'		<p>本网站提供与其它互联网上的网站或资源的链接，用户可能会因此连结至其它运营商经营的网站，但不表示本网站与这些运营商有任何关系。其它运营商经营的网 站均由各经营者自行负责，不属于本网站控制及负责范围之内。对于存在或来源于此类网站或资源的任何内容、广告、物品或其它资料，本网站亦不予保证或负责。因使用或依赖任何此类网站或资源发布的或经由此类网站或资源获得的任何内容、物品或服务所产生的任何损害或损失，本网站不负任何直接或间接的责任。</p>'
            +'		<br />'
            +'		<p>1.1.6 隐私权政策</p>'
            +'		<br />'
            +'		<p>适用范围：</p>'
            +'		<br />'
            +'		<p>在用户注册本网站账户时，用户根据本网站要求提供的个人注册信息；</p>'
            +'		<p>在用户使用本网站服务，参与本网站活动，或访问本网站网页时，本网站自动接收并记录的用户浏览器上的服务器数值，包括但不限于IP地址等数据及用户要求取用的网页记录；</p>'
            +'		<p>本网站收集到的用户在本网站进行交易的有关数据，包括但不限于出价、购买、及违规记录；</p>'
            +'		<p>本网站通过合法途径从商业伙伴处取得的用户个人数据。</p>'
            +'		<br />'
            +'		<p>信息使用：</p>'
            +'		<br />'
            +'		<p>本网站不会向任何人出售或出借用户的个人信息，除非事先得到用户的许可；</p>'
            +'		<p>本网站亦不允许任何第三方以任何手段收集、编辑、出售或者无偿传播用户的个人信息。任何用户如从事上述活动，一经发现，本网站有权立即终止与该用户的服务协议，查封其账号。</p>'
            +'		<p>为服务用户的目的，本网站可能通过使用用户的个人信息，向用户提供服务，包括但不限于向用户发出产品和服务信息，或者与本网站合作伙伴共享信息以便他们向用户发送有关其产品和服务的信息（后者需要用户的事先同意）。</p>'
            +'		<br />'
            +'		<p>信息披露：</p>'
            +'		<br />'
            +'		<p>用户的个人信息将在下述情况下部分或全部被披露：</p>'
            +'		<br />'
            +'		<p>经用户同意，向第三方披露；</p>'
            +'		<p>如用户是合资格的知识产权投诉人并已提起投诉，应被投诉人要求，向被投诉人披露，以便双方处理可能的权利纠纷；</p>'
            +'		<p>根据法律的有关规定，或者行政或司法机构的要求，向第三方或者行政、司法机构披露；</p>'
            +'		<p>如果用户出现违反中国有关法律或者网站政策的情况，需要向第三方披露；</p>'
            +'		<p>为提供所要求的产品和服务，而必须和第三方分享用户的个人信息；</p>'
            +'		<p>其它本网站根据法律或者网站政策认为合适的披露；</p>'
            +'		<br />'
            +'		<p>信息安全：</p>'
            +'		<br />'
            +'		<p>本网站账户有密码保护功能，请妥善保管用户的账户及密码信息；</p>'
            +'		<p>如果用户发现自己的个人信息泄密，尤其是本网站账户及密码发生泄露，请用户立即与本网站联络，以便本网站采取相应措施。</p>'
            +'		<br />'
            +'		<p>Cookie的使用：</p>'
            +'		<br />'
            +'		<p>通过本网站所设Cookie所取得的有关信息，将适用本政策；</p>'
            +'		<p>在本网站上发布广告的公司通过广告在用户计算机上设定的Cookies，将按其自己的隐私权政策使用。</p>'
            +'		<p>编辑和删除个人信息的权限： 用户可以点击"个人账户"对用户的个人信息进行编辑，删除个人信息可以通知本网站帮助删除。</p>'
            +'		<p>政策修改：</p>'
            +'		<br />'
            +'		<p>本网站保留对本政策做出不时修改的权利。</p>'
            +'		<br />'
            +'		<p>问题及建议：</p>'
            +'		<br />'
            +'		<p>如果您有任何问题及建议，请联系我们。</p>'
            +'		<br />'
            +'		<p>1.1.7 保密</p>'
            +'		<br />'
            +'		<p>双方保证在对讨论、签订、执行本协议中所获悉的属于对方的且无法自公开渠道获得的文件及资料（包括但不限于商业秘密、公司计划、运营活动、财务信息、技术信息、经营信息及其他商业秘密）予以保密。未经该资料和文件的原提供方同意，另一方不得向第三方泄露该商业秘密的全部或者部分内容。但法律、法规、行政规章另有规定或者双方另有约定的除外。</p>'
            +'		<br />'
            +'		<p>1.1.8 争议解决方式</p>'
            +'		<br />'
            +'		<p>本协议及其修订本的有效性、履行和与本协议及其修订本效力有关的所有事宜，将受中华人民共和国法律管辖，任何争议仅适用中华人民共和国法律。?</p>'
            +'		<br />'
            +'		<p>本协议签订地为中国四川省成都市。因本协议所引起的用户与本网站的任何纠纷或争议，首先应友好协商解决，协商不成的，用户在此完全同意将纠纷或争议提交本网站所在地即成都市有管辖权的人民法院诉讼解决。?</p>'
            +'		<br />'
            +'		<p>1.1.9 不可抗力</p>'
            +'		<br />'
            +'		<p>因不可抗力或者其他意外事件，使得本协议的履行不可能、不必要或者无意义的，双方均不承担责任。本合同所称之不可抗力意指不能预见、不能避免并不能克服的客观情况，包括但不限于战争、台风、水灾、火灾、雷击或地震、罢工、暴动、法定疾病、黑客攻击、网络病毒、电信部门技术管制、政府行为或任何其它自然或人为造成的灾难等客观情况。</p>'
            +'	</div>'
        $.fancybox({
            fitToView	: false,
            autoHeight	: false,
            autoSize	: false,
            width		: 930,
            height		: 600,
            content     : chtml,
            padding		: 0,
            beforeShow 	: function () {

            }
        })
    });
    $('.Js_l_sm').click(function(){
        var chtml = '<div style="margin:20px; font-size:12px; font-family:simsun; line-height:16px;">'
            +'		<p>1.2 版权声明</p>'
            +'		<br />'
            +'		<p>创意世界网站由四川创意世界科技有限公司运营及所有。创意世界网站上的内容受中华人民共和国法律及相关国际公约、条约、协议、及其他对中国有效地国际法律规则保护。未经四川创意世界科技有限公司或相关权利人事先的书面许可，任何人不得以任何方式，包括但不限于复制、再造、转载、上载、出版、改编、链接、张贴或传播本网站上的任何内容，也不得在非创意世界所属的服务器上做镜像。已经本网协议授权的媒体、网站在下载使用本网站内容时必须注明"稿件来源：创意世界"，并保证在使用本网站内容时的准确性、完整性。任何未经授权使用本网站的行为都将违反《中华人民共和国著作权法》和其他法律法规以及有关国际公约的规定，本网将依法追究法律责任。</p>'
            +'		<br />'
            +'		<p>特别声明：www.csoow.com的域名为四川创意世界科技有限公司所有。未经四川创意世界科技有限公司书面授权，任何单位或个人不得使用。</p>'
            +'		<br />'
            +'		<p>本网站及本网站所使用的任何相关软件、程序、内容，包括但不限于作品、图片、档案、资料、网站构架、网站版面的安排、网页设计、经由本网站或广告商向用户呈现的广告或资讯，均由本网站或其它权利人依法享有相应的知识产权，包括但不限于著作权、商标权、专利权或其它专属权利等，受到相关法律的保护。未经本网站或相关权利人明示授权，用户保证不修改、出租、出借、出售、散布本网站及本网站所使用的上述任何资料和资源，或根据上述资料和资源制作成任何种类物品。</p>'
            +'		<br />'
            +'		<p>本网站授予用户不可转移及非专属的使用权，使用户可以通过单机计算机使用本网站的目标代码（以下简称"软件"），但用户不得且不得允许任何第三方，复制、修改、创作衍生作品、进行还原工程、反向组译，或以其它方式破译或试图破译源代码，或出售、转让"软件"或对"软件"进行再授权，或以其它方式移转"软件"之任何权利。用户同意不以任何方式修改"软件"，或使用修改后的"软件"。</p>'
            +'		<br />'
            +'		<p>用户不得经由非本网站所提供的界面使用本网站。</p>'
            +'		<br />'
            +'		<p>本网站上出现的创意世界LOGO，商号，企业名称，产品、服务名称，产品、服务、标志和标识等都为四川创意世界科技有限公司拥有，未经书面许可，任何人不得以任何方式使用，违者本网站将严格追究法律责任。</p>'
            +'		<br />'
            +'		<p>如果您向本网站提供了任何作品，除非与本网站另有协议，均将视为您已经永久地、无偿地、不可撤销地授权本网站使用、转授权您提供的作品。如果这些作品侵犯任何单位或个人的在先权利，或者违反法律，您需要承担相应的法律责任。</p>'
            +'		<br />'
            +'		<p>如任何单位或个人认为本网站内容侵犯您的在先权利，请即与本网站联系，并提供相应的证明文件，本网站将依法予以处理。</p>'
            +'	</div>'
        $.fancybox({
            fitToView	: false,
            autoHeight	: false,
            autoSize	: false,
            width		: 930,
            height		: 600,
            content     : chtml,
            padding		: 0,
            beforeShow 	: function () {

            }
        })
    });

});


function date_gap(date,gap){
    var y = date.getFullYear();
    var m = date.getMonth();
    var days = 0;
    for(var i=0; i<gap; i++){
        days += get_month_item_days(y,m-i);
    }
    var olddate = (date.getTime() - days*24*60*60*1000);
    var d = new Date();
    d.setTime(olddate);
    return d;
}

function check_year_leapyear(y){
    if(y % 4 == 0 || y % 400 == 0){
        return 29;
    }else{
        return 28;
    }
}

function get_month_item_days(y,m){
    switch(m){
        case 0 :
            return 31;
            break;
        case 1 :
            return check_year_leapyear(y);
            break;
        case 2 :
            return 31;
            break;
        case 3 :
            return 30;
            break;
        case 4 :
            return 31;
            break;
        case 5 :
            return 30;
            break;
        case 6 :
            return 31;
            break;
        case 7 :
            return 31;
            break;
        case 8 :
            return 30;
            break;
        case 9 :
            return 31;
            break;
        case 10 :
            return 30;
            break;
        case 11 :
            return 31;
            break;
    }
}

function strAdd(srcA, srcB) {
    var i, temp, tempA, tempB, len, lenA, lenB, carry = 0;
    var res = [],
        arrA = [],
        arrB = [],
        cloneArr = [];
    arrA = srcA.split('');
    arrB = srcB.split('');
    arrA.reverse();
    arrB.reverse();
    lenA = arrA.length;
    lenB = arrB.length;
    len = lenA > lenB ? lenB : lenA;
    for (i = 0; i < len; i++) {
        tempA = parseInt(arrA[i], 10);
        tempB = parseInt(arrB[i], 10);
        temp = tempA + tempB + carry;
        if (temp > 9) {
            res.push(temp - 10);
            carry = 1;
        } else {
            res.push(temp);
            carry = 0;
        }
    }
    cloneArr = lenA > lenB ? arrA : arrB;
    for (; i < cloneArr.length; i++) {
        tempA = parseInt(cloneArr[i], 10);
        temp = tempA + carry;
        if (temp > 9) {
            res.push(temp - 10);
            carry = 1;
        } else {
            res.push(temp);
            carry = 0;
        }
    }
    return (res.reverse()).join('');
};


function subtract(str1,str2){
    //alert(a+"-"+b+"="+(a-b));
    //减法结果
    var c="";
    //借位标志
    var flog=true;
    //被减数和减数长度
    var i=str1.length-1;
    var j=str2.length-1;
    if(j>i){
        var c='-'+subtract(str2,str1);
        return c;
    }
    if(j==i&&str1<str2){
        var c='-'+subtract(str2,str1);
        return c;
    }
    for(;i>=0;i--,j--){
        //获取减数低位
        var charb=0;
        if(j>=0){
            charb=str2.charAt(j);
        }

        //获取被减数低位
        var chara=str1.charAt(i);
        //如果借位
        if(flog!=true){
            //如果被借位为0
            if(chara==0){
                //再借一然后减一
                flog=false;
                chara=9;
            }else if((chara-1)<charb){
                //如果不够减借一
                flog=false;
                chara=1+chara-1;
            }else{
                chara=chara-1;

                flog=true;
            }
        }
        //低位相减
        if(chara>=charb){
            c=''+(chara-charb)+c;
            //  alert('够减'+c);
        }else{
            //如果不够减借位
            var x=1+chara-charb;
            // alert(x);
            c=''+x+c;
            flog=false;
            //alert('不够减'+c);
        }
    }
    //字符串
    return c;
}