$(function() {
	//编辑器配置
	$('#tz_content').redactor({ 
		imageUpload: '../editor_upload_pic.php', 
		lang: 'zh_cn' , 
		buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' , 'video' , 'link'], 
		toolbarExternal: '#tz_toolbar'
	});	
	$('#js_content').redactor({ 
		imageUpload: '../editor_upload_pic.php', 
		lang: 'zh_cn' , 
		buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' , 'video' , 'link'], 
		toolbarExternal: '#js_toolbar'
	});
	$('#js_context_update').redactor({ 
		imageUpload: '../editor_upload_pic.php', 
		lang: 'zh_cn' , 
		buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' ,'video' , 'link'], 
		toolbarExternal: '#js_context_toolbar'
	});
	$('#xb_content').redactor({ 
		imageUpload: '../editor_upload_pic.php', 
		lang: 'zh_cn' , 
		buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' ,'video' , 'link'], 
		toolbarExternal: '#xb_toolbar'
	});
	$('#bz_content').redactor({ 
		imageUpload: '../editor_upload_pic.php', 
		lang: 'zh_cn' , 
		buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' ,'video' , 'link'], 
		toolbarExternal: '#bz_toolbar'
	});
	$('#post_content').redactor({ 
		imageUpload: '../editor_upload_pic.php', 
		lang: 'zh_cn' , 
		buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' ,'video' , 'link'], 
		toolbarExternal: '#post_toolbar'
	});
	$('#ctb_content').redactor({ 
		imageUpload: '../editor_upload_pic.php', 
		lang: 'zh_cn' , 
		buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' ,'video' , 'link'], 
		toolbarExternal: '#ctb_toolbar'
	});
	$('#dtb_content').redactor({ 
		imageUpload: '../editor_upload_pic.php', 
		lang: 'zh_cn' , 
		buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' ,'video' , 'link'], 
		toolbarExternal: '#dtb_toolbar'
	});
    $('#dtb_content1').redactor({
        imageUpload: '../editor_upload_pic.php',
        lang: 'zh_cn' ,
        buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' ,'video' , 'link'],
        toolbarExternal: '#dtb_toolbar1'
    });
    $('#dtb_content2').redactor({
        imageUpload: '../editor_upload_pic.php',
        lang: 'zh_cn' ,
        buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' ,'video' , 'link'],
        toolbarExternal: '#dtb_toolbar2'
    });
});