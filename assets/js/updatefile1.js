$(function(){
	$(".file_uploadbox").html5Uploader({
		name: 'userfile',
		postUrl:'./upload.php',
		onClientAbort: function(){
		},
		onClientLoadStart: function(e, file, xhr) {
			
		},
		onClientLoad: function(e, file, xhr) {
			
		},
		onServerLoadStart: function(e, file, xhr) {
			
		},
		onServerProgress: function(e, file, xhr) {
			
		},
		onServerLoad: function(e, file) {
			
		},
		onSuccess:function(e, file, xhr, data){
			var data = eval ('('+ data +')');
			var status = data.status;
			var path = data.content;
			if(status== 0){
				switch (data.ext){
				   	case 'doc':
				     	break
				   	case 'pdf':
				     	break
				   	default:
						var chtml = '<div class="tit">请选取头像显示区域</div>'
									+'	<div class="clearfix">'
									+'   <div class="Js_uploadArea j-crop">'
									+'		<img id="faceImg" src="/'+path+'" alt="" style="z-index:10" />'
									+'</div>'
									+'<div class="Js_facepreview w100 simg" style=""><img src="" alt=""></div>'
									+'</div>'
									+'<div style="height:25px;"></div>'
									+'<div class="p-warp"><a href="#" class="btn upfile h8 mr10 Js_reUpload">取消</a><a href="#" class="btn upfile b73 Js_okUpload">确认</a></div><div class="height_8"></div>'
					
					$(".Js_user_head").html(chtml);
					$(".Js_facepreview.w100").html('<img src="/'+path+'" id="preview100" alt="Preview" class="jcrop-preview" />');
					$('#faceImg').load(function(){
						$('.Js_userfile_box').show().width($('#faceImg').width()+180);
						$('#preview100').parent().css('margin-top',($('#faceImg').height()-100)/2);
						initJcrop();
					})
					
					$('.Js_reUpload').click(function(){
						$(".Js_userfile_box").hide();
					})
					
					$('.Js_okUpload').click(function(){
						if (parseInt($('#w').val())){
							var data ={file_name:$("#file_path").val(),start_x:$("#x").val(),start_y:$("#y").val(),end_x:(Number($("#x").val())+Number($("#w").val())),end_y:(Number($("#y").val())+Number($("#h").val()))}
							//ajax_(resize_user_avatar_url,data,resize_user_avatar_c);这个是以前的ajax提交函数，现在的用request函数
							//data提交的是选取图片的4点坐标
							//ajax函数
							//alert($("#file_path").val())
							//alert($("#x").val())
							//alert($("#y").val())
							//alert((Number($("#x").val())+Number($("#w").val())))
							//alert((Number($("#y").val())+Number($("#h").val())))
							return true;
						}else{
							alert('请选择头像区域');
							return false;
						} 
					})
					
				}
			}else{
				if(data.error){
					alert(data.error);//这里出要弹出意外信息，比如不支持的上传文件格式
				}else{
					alert("上传文件格式不正确，请从新上传")
				}
			}
		}
	})
	$("#uploadbox").click(function(){
		$(".file_uploadbox").click();
	});
})

function initJcrop(){
		$(".ava-title .ava1").text("裁剪新头像").width(310);
		$(".ava-title .ava2").text("预览新头像");
		$(".Js_avaUploadBtnArea").show();


		var jcrop_api, boundx, boundy;
		
		$('#faceImg').Jcrop({
			onChange: updatePreview,
			onSelect: updatePreview,
			setSelect: [ 0, 0, 100, 100 ],
			aspectRatio: 1
		},function(){
			// Use the API to get the real image size
			var bounds = this.getBounds();
			boundx = bounds[0];
			boundy = bounds[1];
			// Store the API in the jcrop_api variable
			jcrop_api = this;
			updatePreview({h:100,w:100,x:0,x2:100,y:0,y2:100});
			//this.release()
		});

		function updatePreview(c)
		{
			if (parseInt(c.w) > 0)
			{
				var rx2 = 100 / c.w;
				var ry2 = 100 / c.h;

				$('#preview100').css({
					width: Math.round(rx2 * boundx) + 'px',
					height: Math.round(ry2 * boundy) + 'px',
					marginLeft: '-' + Math.round(rx2 * c.x) + 'px',
					marginTop: '-' + Math.round(ry2 * c.y) + 'px'
				});
			};

			$('#x').val(c.x);
			$('#y').val(c.y);
			$('#w').val(c.w);
			$('#h').val(c.h);

		};
	};