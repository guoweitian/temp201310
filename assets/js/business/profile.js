$('.Js_password').val('');
$('.Js_receive_bonus').click(receiveBonus)

function receiveBonus(){
	$.ajax({
		url:base_url+'profile/receive-creative-index-bonus',
		dataType : 'json',
		type : 'get',
		success : function(res){
			$('#Js_createNum').text(0);	
		}
	})
}

$('#Js_buy_coin').click(buyCoin)

function buyCoin(){
	var obj = $(':checked');
	var len = obj.parent().next().find('input[type=text]').length;
	var num = len=='0' ? obj.attr('m-num') : obj.parent().next().find('input[type=text]').val();
	var url = 'profile/buy-coin';
	var data = {amount:num};
	ajax(url, data, buyCoinOk);	
}
function buyCoinOk(){}

$('#Js_buy_rmb').click(buyRmb);

function buyRmb(){
	var obj = $(':checked');
	var len = obj.parent().next().find('input[type=text]').length;
	var num = len=='0' ? obj.attr('m-num') : obj.parent().next().find('input[type=text]').val();
	var url = 'profile/refill-request';
	var data = {amount:num};
	ajax(url, data, buyRmbOk);
}

function buyRmbOk(data) {
	if (data.status == 0) {
		window.location.href = data.content;
	}
}

$('.Js_update_personal').click(updatePersonal);

function updatePersonal(){
	var url = 'profile/personal';
	var data = $('#Js_personal_form').serialize();
	ajax(url, data, updatePersonalFun);	
}

function updatePersonalFun(){}

$('.Js_createindex_type').click(createIndexRecord);

function createIndexRecord(){
	$('.Js_createindex_type').removeClass('current');
	$(this).addClass('current');
	var type = $(this).attr('type');
	var url = 'profile/' + manAddr;
	var data = {start_time:$('#dateone').val(),end_time:$('#datetwo').val(),type:type,page:1};
	ajax(url,data,typecallfun);
}

function typecallfun(data){
	if(data.status=='0'){
		
	}	
}


var page_list_btnnum = 0;
$('.Js_page_list_next').click(function(){
	if(page_list_btnnum==$(this).prev().children().length-1){
		return;	
	}
	if(page_list_btnnum==$(this).prev().children().length-2){
		$('.Js_page_list_next').each(function(){
			$(this).css('background-position','0px -18px')
		})
	}
	page_list_btnnum++;
	$('.Js_page_list_prev').css('background-position','0 -36px');
	$('.Js_page_list_next').each(function(){
		$(this).prev().children().removeClass('current');
		$(this).prev().children().eq(page_list_btnnum).addClass('current');
	})
	var url = 'profile/' + manAddr;
	var data = {start_time:$('#dateone').val(),end_time:$('#datetwo').val(),type:$('.Js_createindex_type').filter('.current').attr('type'),page:page_list_btnnum};
	ajax(url,data,pagecallfun);
}).mouseenter(function(){
	if(page_list_btnnum==$(this).prev().children().length-1){
		return;	
	}
	$(this).css('background-position','0 -90px');	
}).mouseleave(function(){
	if(page_list_btnnum==$(this).prev().children().length-1){
		return;	
	}
	$(this).css('background-position','0 -54px');	
})

$('.Js_page_list_prev').click(function(){
	if(page_list_btnnum==0){
		return;	
	}
	if(page_list_btnnum==1){
		$('.Js_page_list_prev').each(function(){
			$(this).css('background-position','0px 0')
		})
	}
	page_list_btnnum--;
	$('.Js_page_list_next').css('background-position','0 -54px');
	$('.Js_page_list_prev').each(function(){
		$(this).next().children().removeClass('current');
		$(this).next().children().eq(page_list_btnnum).addClass('current');
	})
	var url = 'profile/' + manAddr;
	var data = {start_time:$('#dateone').val(),end_time:$('#datetwo').val(),type:$('.Js_createindex_type').filter('.current').attr('type'),page:page_list_btnnum};
	ajax(url,data,pagecallfun);
}).mouseenter(function(){
	if(page_list_btnnum==0){
		return;	
	}
	$(this).css('background-position','0 -72px');	
}).mouseleave(function(){
	if(page_list_btnnum==0){
		return;	
	}
	$(this).css('background-position','0 0');	
})

$('.Js_page_list_itemNum').children().click(function(){
	var l_index = $(this).parent().children().index($(this));
	page_list_btnnum = l_index;
	$('.Js_page_list_itemNum').each(function(){
		$(this).children().removeClass('current');
		$(this).children().eq(page_list_btnnum).addClass('current');
		if(page_list_btnnum==0){
			$('.Js_page_list_next').css('background-position','0 -54px');
			$('.Js_page_list_prev').css('background-position','0 0');
		}else if(page_list_btnnum==$(this).children().length-1){
			$('.Js_page_list_next').css('background-position','0 -18px');
			$('.Js_page_list_prev').css('background-position','0 -36px');	
		}else{
			$('.Js_page_list_next').css('background-position','0 -54px');
			$('.Js_page_list_prev').css('background-position','0 -36px');
		}
	})
	var url = 'profile/' + manAddr;
	var data = {start_time:$('#dateone').val(),end_time:$('#datetwo').val(),type:$('.Js_createindex_type').filter('.current').attr('type'),page:page_list_btnnum};
	ajax(url,data,pagecallfun);
})

function pagecallfun(data){
	if(data.status=='0'){
		
	}
}

$('#Js_pwd_update').click(updatePassword)

function updatePassword(){
	var data = $('#Js_pwd_form').serialize();
	var url = 'profile/password';
	ajax(url,data,updatepwdfun);
}

function updatepwdfun(data){
	if(data.status=='0'){
		$('.Js_password').val('');
	}	
}

/*确认支付订单*/

$('.Js_confirm_pay').bind('click', function() {
	var order_id = $(this).attr('data');
	var is_use_balance = $('#Js_is_use_balance').val();
	var balance = $('#Js_pay_balance').val();
	var data = {is_use_balance:is_use_balance, balance:balance};
	var url = 'profile/order-pay/' + order_id;
	ajax (url, data , payOrderSuccess);
});

function payOrderSuccess(data) {
	if (data.status == 0) {
		window.location.href = data.content;
	}
} 

/*确认发货*/

$('.Js_confirm_delivery').bind('click', function() {
	var order_id = $(this).attr('data');
	var data = {order_id:order_id};
	var url  = 'profile/delivery';
	ajax(url, data, deliverySuccess);
})

function deliverySuccess(data) {
	if (data.status == 0) {
		var url = $('.Js_order_url').val();
		window.location.href = url;
	}
}

/*确认收货*/

$('.Js_confirm_received').bind('click', function() {
	var order_id = $(this).attr('data');
	var data = {order_id:order_id};
	var url  = 'profile/received';
	ajax(url, data, receivedSuccess); 
});

function receivedSuccess(data) {
	if (data.status == 0) {
		var url = $('.Js_order_url').val();
		window.location.href = url;
	}
}


/*提现*/
$('#Js_withdraw').bind('click',function(){
    var amount = $("input[name='amount']").val();
    var bank_id = $("input:checked").val();
    var data = {bank_id:bank_id, amount:amount};
    var url = 'profile/withdraw';
    ajax(url, data,withdraw);
});

function withdraw(){}