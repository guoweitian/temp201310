function ajax(businessInterface, data, successFunction, property, object) {
	request = {
		url : base_url + businessInterface,
		data : data,
		dataType : "json",
		type : "POST",
		success : function(data) {
			successFunction(data, object)
		}
	};

	$.extend(request, property);
	$.ajax(request);
};

function escapeHtml(text) {
	  return text
	      .replace(/&/g, "&amp;")
	      .replace(/</g, "&lt;")
	      .replace(/>/g, "&gt;")
	      .replace(/"/g, "&quot;")
	      .replace(/'/g, "&#039;");
}

function formatRichEditContent(text) {
	return text.replace('<p>', '').replace('</p>', '');
}

function checkErrorCommon(fields, error) {
	for (var i in fields) {
		id = fields[i];
		if (error.hasOwnProperty(id)) {
			$('#' + id + ' .tools').addClass('error');
			$('#' + id + ' .errorMsg').show().html(error[id]);
		} else {
			$('#' + id + ' .tools').removeClass('error');
			$('#' + id + ' .errorMsg').show().html("");
		}
	}
}

function getRemainDay(date) {
	  date = date.replace('-', '/').replace('-', '/');
	  var time = Date.parse(date) / 1000;
	  
	  if (time == 0) {
	    return '长期';
	  }
	  
	  var remainSeconds = time - (new Date()).getTime() / 1000;
	  
	  if (remainSeconds <= 0) {
	    return '已结束';
	  }
	  
	  return '剩余' + Math.floor(remainSeconds / 86400) + '天';
	}

function fmoney(s, n)   
{   
   n = n >= 0 && n <= 20 ? n : 2;   
   s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";   
   var l = s.split(".")[0].split("").reverse(),   
   r = s.split(".")[1];   
   t = "";   
   for(i = 0; i < l.length; i ++ )   
   {   
      t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");   
   }   
   if (n == 0) {
	   return t.split("").reverse().join("");
   }
   return t.split("").reverse().join("") + "." + r;   
}

function formatNumber(num) {
  num += '';
  x = num.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + x2;
}

function getAvatar(avatar) {
	return base_url + (avatar ? avatar : 'assets/images/default.png');
}

function checkError(error, previousStep, steps, stepPage) {
    var hasError = false;
    for (var i in steps) {
        id = steps[i];
        if (error.hasOwnProperty(id)) {
            $('#' + id + ' .tools').addClass('error');
            $('#' + id + ' .errorMsg').show().html(error[id]);
            hasError = true;
        } else {
            $('#' + id + ' .tools').removeClass('error');
            $('#' + id + ' .errorMsg').hide().html("");
        }
    }

    if (previousStep != '') {
        return previousStep;
    }

    return hasError ? stepPage : '';
}

function formatTimestamp(timestamp) {
    var second = 1000;
    var minutes = second * 60;
    var hours = minutes * 60;
    var days = hours * 24;
    var months = days * 30;
    
    var now = new Date();
	var date = new Date(timestamp * 1000);
	
    var longtime = now.getTime() - date.getTime();
    
	if ( longtime > months * 2 )
    {
        var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var seconds = date.getSeconds();

		if (hours < 10) 
		 hours = '0' + hours;

		if (minutes < 10) 
		 minutes = '0' + minutes;

		if (seconds < 10) 
		 seconds = '0' + seconds;

		return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
    }
    else if (longtime > months)
    {    
        return "1个月前";
    }
    else if (longtime > days*7)
    {    
        return ("1周前");
    }
    else if (longtime > days)
    {    
        return(Math.floor(longtime / days) + "天前");
    }
    else if ( longtime > hours)
    {    
        return(Math.floor(longtime / hours) + "小时前");
    }
    else if (longtime > minutes)
    {    
        return(Math.floor(longtime / minutes) + "分钟前");
    }
    else if (longtime > second)
    {    
        return(Math.floor(longtime/second) + "秒前");
    } else {
    	return '1秒以前';
    }
}


function formatDate(timestamp) {
    var second = 1000;
    var minutes = second * 60;
    var hours = minutes * 60;
    var days = hours * 24;
    var months = days * 30;
    
    var now = new Date();
	var date = new Date(timestamp * 1000);
	
    var longtime = now.getTime() - date.getTime();
    

    var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();

	if (hours < 10) 
	 hours = '0' + hours;

	if (minutes < 10) 
	 minutes = '0' + minutes;

	if (seconds < 10) 
	 seconds = '0' + seconds;

	return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
}

function getImage(image) {
  var lastPathPosition = image.lastIndexOf('/');
  var path = image.substring(0, lastPathPosition + 1);
  var fileFullName = image.substring(lastPathPosition + 1);
  
  var extPosition = fileFullName.lastIndexOf('.');
  var fileMainName = fileFullName.substring(0, extPosition);
  var extName = fileFullName.substring(extPosition);
  
  var fileName = fileMainName.substring(0, fileMainName.lastIndexOf('_'));
  
  return path + fileName + extName;
}

function htmlspecialchars(str)  
{  
	   str = str.replace(/&/g, '&amp;');
	    str = str.replace(/</g, '&lt;');
	    str = str.replace(/>/g, '&gt;');
	    str = str.replace(/"/g, '&quot;');
	   str = str.replace(/'/g, '&#039;');
	   return str;
}


$(function(){
//setInterval(getUnreadNotification,10000);
function checkExternalExpertError(fields, error) {
	for (var i in fields) {
		id = fields[i];
		if (error.hasOwnProperty(id)) {
			$('#' + id + ' .tools').addClass('error');
			$('#' + id + ' .errorMsg').html(error[id]);
		} else {
			$('#' + id + ' .tools').removeClass('error');
			$('#' + id + ' .errorMsg').html("");
		}
	}
}
});
//邀请专家点评 站外 end

/*发布帖子*/
$('.button_publish_topic').live('click', function (){
   // var data = $('#form_publish_topic').serialize();
   var err;
   if(!$('#community_topic_title_input').val()){
       $('#community_topic_title_input').parents('.tools').addClass('error');
       $('#community_topic_title_input').parents('.tools').next().show();
		err = 'one';
	}else{
       $('#community_topic_title_input').parents('.tools').removeClass('error');
       $('#community_topic_title_input').parents('.tools').next().hide();
	}
	
	if(!$('#post_content').val()){
		$('#post_content').parents('.toolbar-content').next().show();
		err = 'one';
	}else{
		$('#post_content').parents('.toolbar-content').next().hide();
	}
	
	if(err=='one') return;
	var txt = $('#community_topic_title_input').val();
	var content = $('#post_content').prev().html();
	var data = 'community_topic_title='+txt+'&community_topic_content=' + content;
	var self = $(this);
	if(contest_type!='thinktank'){
		data += "&contest_id=" + contest_id;
	}
    var url = contest_type + '/topic';
    ajax(url, data, $.proxy(topicSuccess,self));
});

function topicSuccess(data) {
    if (data.status == 0) {
        var thtml = '<li>'
            +'  <div class="clearfix communityDiv Js_theme_list" data="' + data.content.id + '">'
            +'	<div class="nr">'
            +'	  <a href="javascript:;">'+ escapeHtml(data.content.title) +'</a>'
            +'	  <span>' + data.content.user.name + ' 刚刚发布</span>'
            +'	</div>'
            +'	<div class="thread">'
            +'	  <p>0</p>'
            +'	  <a href="#">跟帖</a>'
            +'	</div>'
            +'	<div class="conTxt">'
            +'	  <p>0</p>'
            +'	  <a href="#">查看</a>'
            +'	</div>'
            +'	<div class="ftr">'
            +'	</div>'
            +' </div>'
            +'</li>'
		
		if($('.Js_discuss_area').children().length>0){
        	$('.Js_theme_list').eq(0).before(thtml);
		}else{
			$('.Js_discuss_area').append(thtml);
		}
        $('.Js_tsubpost_topics').click();
        $(this).parents('#form_publish_topic').find('#post_content').val('').prev().html('<p>&#8203</p>');
        $(this).parents('#form_publish_topic').find('#community_topic_title_input').val('');
    } else if(data.status == 10001){
        var fields = ['community_topic_title', 'community_topic_content'];
        checkErrorCommon(fields, data.error);
    } else if (data.status == 30001) {
		window.location.href = base_url + "login?url=" + base_url + data.error;
	}
}

/*点击帖子*/
$(".Js_theme_list").live("click",function(){
    var topic_id = $(this).attr('data');
    var data = {topic_id:topic_id};
    var url = contest_type + '/topic-posts';
    ajax(url, data, topicPostsSuccess, '', $(this));
});

function topicPostsSuccess(data, obj) {
    if (data.status == 0) {
        var lindex = $(".Js_theme_list").index(obj);
        var that = obj;
        var themeH = '<div class="theme-box" style="display:none;">'
            +'	<div class="theme-wrap">'
            +'		<div class="theme-item">'
            +'			<div class="theme-avatar"><a href="' + base_url + 'home/index/' + data.content.topic.user.id +'"><img class="avatar" src="' + getAvatar(data.content.topic.user.avatar) + '" style="width:44px"></a></div>'
            +'			<div class="theme-con">'
            +'				<p class="uname"><a href="' + base_url + 'home/index/' + data.content.topic.user.id +'">' + data.content.topic.user.name + '</a></p>'
            +'				<div>' + escapeHtml(formatRichEditContent(data.content.topic.content)) + '</div>'
            +'				<div class="time-bar">' + data.content.topic.created_at + '</div>'
            +'			</div>'
            +'		</div>'
        $.each(data.content.posts, function() {
   themeH   +='		<div class="theme-item">'
            +'			<div class="theme-avatar"><a href="'+base_url+'home/index/'+this.user.id +'"><img class="avatar" src=" ' + getAvatar(this.user.avatar) +'" style="width:44px"></a></div>'
            +'			<div class="theme-con">'
            +'				<p class="uname"><a href="' + base_url + 'home/index/' + data.content.topic.user.id +'">' + this.user.name + '</a></p>'
            +'				<div>' + escapeHtml(formatRichEditContent(this.content)) + '</div>'
            +'				<div class="time-bar">' + this.created_at + '</div>'
            +'			</div>'
            +'		</div>'
        });
   themeH   +='		<div class="theme-item theme-postbox clearfix">'
            +'			<div class="theme-avatar"><a href="' + base_url + 'home/index/' + login_user_id +'"><img class="avatar" src="' + getAvatar(login_avatar) +'" style="width:44px"></a></div>'
            +'			<div class="toolbar-content left" style="width: 590px; padding-left: 17px;">'
            +'				<textarea id="reply_content'+lindex+'" name="community_reply_content" class="Js_reply_content"></textarea>'
            +'			</div>'
			+'			<div class="errorMsg left mt8" style="display:none;">请输入评论内容</div>'
            +'			<div class="height_20 clearfix"></div>'
            +'			<div class="theme-post-bar">'
            +'				<a class="sub-sbtn left Js_AddReply" style=" text-align: center; width: 100px; margin-left: 60px;" href="javascript:;" data="' + data.content.topic.id + '">回复</a>'
            +'			</div>'
            +'			<div class="height_10"></div>'
            +'		</div>'
            +'	</div>'
            +'	<div style=" margin:0 auto; padding-left:90px; width:937px;">'
            +'		<div class="sp-line" style="top:0">'
            +'			<div class="line-left"></div>'
            +'			<div class="line-right"></div>'
            +'		</div>'
            +'	</div>'
            +'</div>';


        if(that.hasClass("open")){
            that.next().slideUp(300,function(){
                that.removeClass("open");
                that.next(".theme-box").remove();
                that.find('i').hide();
            });
        }else{
            that.addClass("open");
            that.after(themeH);
            that.find('i').show();
            that.find('.conTxt p').text(parseInt(that.find('.conTxt p').text(),10)+1)
            that.next().slideDown(300,function(){
                that.next().removeAttr('style');
            });
        }
        $('#reply_content'+lindex).redactor({
            imageUpload: '../editor_upload_pic.php',
            lang: 'zh_cn' ,
            buttons: ['bold', 'italic' , '|' , 'unorderedlist' , 'orderedlist' , 'formatting' , '|' ,'video' , 'link'],
            toolbarExternal: '#reply_toolbar'+lindex
        });
    }
}

/*帖子回复*/
$('.Js_AddReply').live('click', function(){
	if(!$(this).parents('.theme-item').find('textarea').val()){
		$(this).parents('.theme-item').find('.errorMsg').show();
		return;
	}else{
		$(this).parents('.theme-item').find('.errorMsg').hide();	
	}
    var community_reply_content = $(this).parents('.theme-item').find('textarea').prev().text();
    var topic_id = $(this).attr('data');
    var data = {community_reply_content:community_reply_content, topic_id:topic_id};
    var url = contest_type + '/post';
    ajax(url, data, postSuccess, '', $(this));
});

function postSuccess(data, obj) {
    if (data.status == 0) {
        var scope = obj;
        content = scope.parents('.theme-item').find('textarea').prev().html();
        var themeH = '<div class="theme-item">'
            +'			<div class="theme-avatar"><a href="' + base_url + 'home/index/' + data.content.user.id + '"><img class="avatar" src="' + getAvatar(data.content.user.avatar) + '" style="width:44px;"></a></div>'
            +'			<div class="theme-con">'
            +'				<p class="uname"><a href="' + base_url + 'home/index/' + data.content.user.id + '">' + data.content.user.name + '</a></p>'
            +'				<div>'+content+'</div>'
            +'				<div class="time-bar">刚刚</div>'
            +'			</div>'
            +'		</div>';

        scope.parents('.theme-item').before(themeH);
        scope.parents('.theme-item').find('textarea').val('').prev().html('<p>&#8203</p>');
		scope.parents('.theme-box').prev().find('.thread p').text(parseInt(scope.parents('.theme-box').prev().find('.thread p').text())+1);
    } else if (data.status == 30001) {
		window.location.href = base_url + "login?url=" + base_url + data.error;
	}
}

/*帖子分页*/
var m_discuss_page = 1;
$('.Js_discuss_prev').live('click',function(){
	m_discuss_page--;
	$('.Js_discuss_next').css('background-position','0 -54px');
	if(m_discuss_page<=1){
		$(this).css('background-position','0 0');	
		m_discuss_page = 1;
	}else{
		$(this).css('background-position','0 -72px');
	}
	$('.Js_discuss_page').removeClass('current');
	$('.Js_discuss_page').eq(m_discuss_page-1).addClass('current');
	var target_id = contest_id;
	var page = m_discuss_page;
	if(contest_type=='thinktank'){
		var data = {page:page};
	}else if(contest_type=='project'){
		var data = {project_id:target_id,page:page};	
	}else if(contest_type=='task'){
		var data = {task_id:target_id,page:page};	
	}else if(contest_type=='hire'){
		var data = {hire_id:target_id,page:page};	
	}else if(contest_type=='area'){
		var data = {area_id:target_id,page:page};	
	}
	var url = contest_type + '/topics';
	ajax(url, data, changePagelist);
	
}).live('mouseenter',function(){
	if(m_discuss_page==1) return;
	else $(this).css('background-position','0 -72px');
}).live('mouseleave',function(){
	if(m_discuss_page==1) return;
	else $(this).css('background-position','0 -36px');	
})

$('.Js_discuss_next').live('click',function(){
	m_discuss_page++;
	$('.Js_discuss_prev').css('background-position','0 -36px');
	if(m_discuss_page>=$('.Js_discuss_page').length){
		$(this).css('background-position','0 -18px');	
		m_discuss_page = $('.Js_discuss_page').length;
	}else{
		$(this).css('background-position','0 -89px');
	}
	$('.Js_discuss_page').removeClass('current');
	$('.Js_discuss_page').eq(m_discuss_page-1).addClass('current');
	var target_id = contest_id;
	var page = m_discuss_page;
	if(contest_type=='thinktank'){
		var data = {page:page};
	}else if(contest_type=='project'){
		var data = {project_id:target_id,page:page};	
	}else if(contest_type=='task'){
		var data = {task_id:target_id,page:page};	
	}else if(contest_type=='hire'){
		var data = {hire_id:target_id,page:page};	
	}else if(contest_type=='area'){
		var data = {area_id:target_id,page:page};	
	}
	var url = contest_type + '/topics';
	ajax(url, data, changePagelist);
}).live('mouseenter',function(){
	if(m_discuss_page==$('.Js_discuss_page').length) return;
	else $(this).css('background-position','0 -90px');
}).live('mouseleave',function(){
	if(m_discuss_page==$('.Js_discuss_page').length) return;
	else $(this).css('background-position','0 -54px');	
})

$('.Js_discuss_page').live('click',function(){
	var num = $('.Js_discuss_page').index($(this));
	m_discuss_page = num+1;
	$('.Js_discuss_page').removeClass('current');
	$(this).addClass('current');
	if(m_discuss_page==1){
		$('.Js_discuss_prev').css('background-position','0 0');	
	}else{
		$('.Js_discuss_prev').css('background-position','0 -36px');		
	}
	
	if(m_discuss_page==$('.Js_discuss_page').length){
		$('.Js_discuss_next').css('background-position','0 -18px');	
	}else{
		$('.Js_discuss_next').css('background-position','0 -54px');		
	}
	
	var target_id = contest_id;
	var page = m_discuss_page;
	if(contest_type=='thinktank'){
		var data = {page:page};
	}else if(contest_type=='project'){
		var data = {project_id:target_id,page:page};	
	}else if(contest_type=='task'){
		var data = {task_id:target_id,page:page};	
	}else if(contest_type=='hire'){
		var data = {hire_id:target_id,page:page};	
	}else if(contest_type=='area'){
		var data = {area_id:target_id,page:page};	
	}
	var url = contest_type + '/topics';
	ajax(url, data, changePagelist);
})

function changePagelist(data){
	//alert(data)
	$('.Js_discuss_area').html('');
	for(var i = 0; i<data.content.length; i++){
		var chtml = '<li>'
				+'	  <div class="clearfix communityDiv Js_theme_list" data="3">'
				+'		<div class="nr">'
				+'		  <a href="javascript:;">' + escapeHtml(data.content[i].title) + '</a>'
				+'		  <span>' + data.content[i].user.name + ' ' + data.content[i].created_at + '</span>'
				+'		</div>'
				+'		<div class="thread">'
				+'		  <p>' + data.content[i].num_reply + '</p>'
				+'		  <a href="javascript:;">跟帖</a>'
				+'		</div>'
				+'		<div class="conTxt">'
				+'		  <p style="">' + data.content[i].num_visit + '</p>'
				+'		  <a href="javascript:;">查看</a>'
				+'		</div>'
				+'		<div class="ftr">';
		if (data.content[i].last_reply_user_id > 0) {
			chtml += '<p>' + data.content[i].last_reply_user.name + '</p>'
					+ '<span href="#">' + data.content[i].updated_at + ' 最后回复</span>';
		}
			chtml += '					</div>'
				+'	  </div>'
				+'	</li>';
			$('.Js_discuss_area').append(chtml);
	}
}


/*发布明星交易*/
$('.Js_trade_btn .Js_label_ok').live('click',function(){
	var n = $('.Js_trade_btn .Js_label_ok').index($(this));
	if(n==0){
		$('#Js_trade_jp').show();
		$('#Js_trade_gd').hide();	
	}else if(n==1){
		$('#Js_trade_gd').show();
		$('#Js_trade_jp').hide();	
	}
})

$('#Js_send_bisilish').live('click',function(){
	var form = $('#Js_hiring_business');
	var data = form.serialize()+"&hire_id=" + contest_id;
	var url = 'exchange/publish';
	ajax(url, data, sendPublicActivity);
})

function sendPublicActivity(data){
	if(data.status=='0'){
		window.location.reload();
	}
}

/*订单支付页面点击使用账户余额支付*/
$('.Js_label_ok').live('click', function(){
	var obj = $('.Js_bank_pay');

	if (!!$(this).attr('checked')) {
		$('#Js_is_use_balance').val('1');
		obj.hide();
	} else {
		$('#Js_is_use_balance').val('0');
		obj.show();
	}
})

