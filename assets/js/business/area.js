$('.Js_organization').click(organization);
$('.Js_industry').click(industry);
$('#Js_thinktank_ok').click(thinktanSubmit)

function organization(){
	$('.Js_organization').removeClass('on');
	$(this).addClass('on');
	var org = $(this).attr('organization_id');
	var ind = $('.Js_industry').filter('.on').attr('industry_id');
	ind = ind ? ind : 0;
	var url = contest_type == 'area' ? 'area/experts' : 'thinktank/experts';
	var data = contest_type == 'area' ? {area_id:contest_id,organization_id:org,industry_id:ind} : {organization_id:org,industry_id:ind};
	ajax(url, data, expertFilter);
}

function industry(){
	$('.Js_industry').removeClass('on');
	$(this).addClass('on');
	var ind = $(this).attr('industry_id');
	var org = $('.Js_organization').filter('.on').attr('organization_id');
	org = org ? org: 0;
	var url = contest_type == 'area' ? 'area/experts' : 'thinktank/experts';
	var data = contest_type == 'area' ? {area_id:contest_id,organization_id:org,industry_id:ind} : {organization_id:org,industry_id:ind};
	ajax(url, data, expertFilter);
}

function expertFilter(data){
	if(data.status == 0) {
		var chtml = '';
		for (var i in data.content) {
			chtml =   '<li>'
					+ '  <a href="#"><img style="width:220px;height:140px" alt="" src="' + getAvatar(data.content[i].expert_pic) + '"></a>'
					+ '  <dl>'
					+ '	<dt><a class="Js_visitCard" href="#">' + data.content[i].expert_name + '</a></dt>'
					+ '	<dd><a href="#">' + data.content[i].expert_intro + '</a></dd>'
					+ '  </dl>'
					+ '</li>';
		}
	 	$('.Js_expert_list_ul').html(chtml);
	}
}

$('.Js_contests').click(contests);

function contests(){
	$('.Js_contests').removeClass('on');
	$(this).addClass('on');	
	var contest_type = $(this).attr("contest_type");
	var url = 'area/contests';
	var data = {area_id:contest_id,contest_type:contest_type};
	ajax(url, data, expertContests);	
}

function expertContests(data){
	if(data.status == 0) {
		var chtml = '';
		for (var i in data.content) {
			chtml =     '<li class="p-match">'
					  + '  <div class="match-box">'
					  + '    <div class="listItemImg">'
					  + '      <div class="rightBox mark"></div>'
					  + '      <s class="mark"></s>'
					  + '      <div class="bottomBox"></div>'
					  + '      <img src="' + base_url + data.content[i].cover + '" alt="">'
					  + '    </div>'
					  + '    <div class="contextDiv">'
					  + '      <h3>' + data.content[i].contest_title + '</h3>'
					  + '      <div class="progress"></div>'
					  + '      <p class="promulgatorName"><a href="#" class="Js_visitCard">发布者名字</a></p>'
					  + '      <p class="promulgatorMoney">$10,000元</p>'
					  + '      <span class="status">所需投资金额</span>'
					  + '    </div>'
					  + '    <div class="bottomDiv clearfix">'
					  + '      <span class="left">长期</span>'
					  + '      <span class="right tk">有3位用户参与投资</span>'
					  + '    </div>'
					  + '  </div>'
					  + '</li>'
		}
	 	$('.Js_contests_list_ul').html(chtml);
	}	
}

function thinktanSubmit(){
	var cs = $('#Js_thinktank_form input[type=hidden]');
	var covers = [];
	cs.each(function(){
		covers.push($(this).val());
	})
	var data = $('#Js_thinktank_form').serialize()+'&cover='+covers+'';
	console.log(data);
	var url = 'thinktank/apply-candidate';
	ajax(url,data,function(res){
		if(res=='0'){
			$('#Js_thinktank_name,#js_content,#Js_thinktank_email,#Js_phone_number,#Js_contact_info').val('');
			$('#js_content').prev().html('&#8203;');
			$('.upfileDiv ul').html('');
			$('#Js_thinktank_industry_name').html('请您选择推荐专家行业');
			$('#Js_thinktank_organization_id').html('选择您申请加入的组织');
			$('#Js_thinktank_role_id').html('选择申请职务');
		}	
	})
}
