/**
 * Created with JetBrains PhpStorm.
 * User: user
 * Date: 13-10-24
 * Time: 下午3:33
 * To change this template use File | Settings | File Templates.
 */
/*发布*/

$(function(){
	birthday();
    pro_type_change();
})

function pro_type_change(){
    $('.Js_forever').click(function(){
        $('.Js_limit_content').hide();
        $('.Js_invest_time_end_type').val('1');
    })

    $('.Js_limit_time').click(function(){
        $('.Js_limit_content').show();
        $('.Js_invest_time_end_type').val('2');
    })
}


$('.Js_send_project').click(function() {
    var data = $('#form_hire_publish').serialize();
    var url  = 'hire/publish';
    ajax(url, data, hirePublishSuccess);
})

function hirePublishSuccess(data) {
    if (data.status == 0) {
        window.location.href= data.content;
    } else if (data.status == 30001) {
        window.location.href= base_url + 'login';
    }else {
        var step1 = ['title', 'area', 'end_time',  'cover'];
        var step2 = ['award_money','awards','intro'];

        var step4 = ['is_agree_protocol', 'signature'];

        var step = '';
        step = checkError(data.error, step, step1, 'one');
        step = checkError(data.error, step, step2, 'two');
        step = checkError(data.error, step, step4, 'four');

        if (step != '') {
            window.location.href = '#step-' + step;
            $('#tab-t .' + step + ' a').click();
        }
    }
}

/*参赛*/
$('#Js_join_agreement').live('click', function() {
    var birthday = $('.Js_apply_birthday').val();
    var height   = $('#Js_your_height').html();
    var weight   = $('#Js_your_weight').html();
    var bust     = $('.Js_bust').html();
    var waist    = $('.Js_waist').html();
    var hips     = $('.Js_hips').html();
    var intro    = $('#tz_content').val();
    var signature= $('#Js_signature').val();
    var hire_id  = $(this).attr('data');

    height = parseInt(height);
    weight = parseInt(weight);
    bust   = parseInt(bust);
    waist  = parseInt(waist);
    hips   = parseInt(hips);

    var data = {birthday:birthday,height:height,weight:weight,bust:bust,waist:waist,hips:hips,intro:intro,signature:signature,hire_id:hire_id};
    var url  = 'hire/apply';
    ajax(url, data, applySuccess);
});

function applySuccess(data) {
    if (data.status == 0) {
        window.location.href = base_url + 'hire/detail/' + data.content;
    } else if (data.status == 10001) {
        var fields = ['birthday', 'height', 'weight', 'bust', 'waist' ,'hips'];
        var error_html = '';
        for (var i in fields) {
            if (data.error.hasOwnProperty(fields[i])) {
                error_html += data.error[fields[i]];
                window.location.href = '#step-one';
            }
        }

        $('.Js_profile_error').html(error_html);

        var fields = ['intro', 'signature'];
        checkErrorCommon(fields, data.error);
    }
}


/*选取获胜明星*/
$('.Js_setName_ck').live('mousedown',function(e){
    if(e.which==3) return false;
    if($(this).find('input').attr('checked')){
        var o = $(this).parents('.selectHiring').remove();
        $('#Js_search_hName').append(o);
    }else{
        var o = $(this).parents('.selectHiring').remove();
        $('#Js_set_hName').append(o);
    }
});


$('.Js_do_select').bind('click', function() {
    var data = $('#form_hire_winner').serialize();
})

$('#Js_select_hiring').bind('click',function(){
	var winner_celebrity_ids = [];
	var winner_name = $('#winner_name').val();
	var oArr = $('#Js_set_hName').children();
	oArr.each(function(){
		winner_celebrity_ids.push($(this).attr('celebrity_id'));
	})
	var data = {hire_id:contest_id,winner_user_ids:winner_celebrity_ids,winner_name:winner_name};
	var url  = 'hire/select-winner';
    ajax(url, data, selectWinner);
})

function selectWinner(data){
	if (data.status == 0) {
        window.location.reload();
	}
}

/*发布公益活动*/
$('#Js_send_public_activity').click(function(){
	var form = $('#Js_public_activity');
	var data = form.serialize()+"&hire_id=" + contest_id;;
	var url = 'hire/publish-boai';
	ajax(url, data, sendPublicActivity);
})

function sendPublicActivity(data){
	if(data.status=='0'){
		//window.location.reload();
	}
}

/*明星比赛图片上传*/
$('#Js_photo_upload').click(function(){
	var coverArr = $(this).parents('.js_tsubpost_txt').find('input[name="cover"]');
	var arr = [];
	coverArr.each(function(){
		arr.push($(this).val());	
	})
	var data = {photos:arr,hire_id:contest_id};
	var url = 'hire/submit-photo';
	ajax(url, data, uploadPhotoArr);
})

function uploadPhotoArr(data){
	if(data.status=='0'){
		window.location.reload();
	}
}