/*发布*/
$(function(){
    trade_type_change();
})

function trade_type_change(){
    $('#Js_check_jp').parent().click(function(){
        $('.Js_setting_gd').hide();
        $('.Js_setting_jp').show();
        $('#trade_type_input').val('bid');
    })

    $('#Js_check_gd').parent().click(function(){
        $('.Js_setting_gd').show();
        $('.Js_setting_jp').hide();
        $('#trade_type_input').val('buy');
    })
}

$('.Js_send_project').click(function() {
    var data = $('#form_exchange_publish').serialize();
    var url  = 'exchange/publish';
    ajax(url, data, exchangePublishSuccess);
})

function exchangePublishSuccess(data) {
    if (data.status == 0) {
        window.location.href=data.content;
    } else if (data.status == 30001) {
        window.location.href= base_url + 'login';
    }else {
        var step1 = ['title', 'area', 'end_time','buy_now_price','num_for_trade','current_price' ,'end_time', 'cover'];
        var step2 = [,'intro'];

        var step3 = ['is_agree_protocol', 'signature'];

        var step = '';
        step = checkError(data.error, step, step1, 'one');
        step = checkError(data.error, step, step2, 'two');
        step = checkError(data.error, step, step3, 'three');

        if (step != '') {
            window.location.href = '#step-' + step;
            $('#tab-t .' + step + ' a').click();
        }
    }
}

/*出价*/
$('#Js_bid_agreement').bind('click', function() {
   var data = $('#form_bid').serialize();
   var url = 'exchange/quote';
   ajax(url,data, quoteSuccess);
});

function quoteSuccess(data) {
    if (data.status == 0) {
        window.location.href = base_url + "exchange/detail/" + data.content;
    } else if (data.status == 10001) {
        var step1 = ['bid_price'];
        var step2 = ['signature'];

        var step = '';
        step = checkError(data.error, step, step1, 'one');
        step = checkError(data.error, step, step2, 'two');

        if (step != '') {
            window.location.href = '#step-' + step;
            $('#tab-t .' + step + ' a').click();
        }
    }
}

/*购买*/
$('#Js_ex_send_agreement').bind('click', function() {
   var data = $('#form_buy').serialize();
   var url  = 'exchange/buy';
   ajax(url, data, buySuccess);
});

function buySuccess(data) {
    if (data.status == 0) {
        window.location.href = base_url + 'exchange/detail/' + data.content;
    } else if (data.status == 10001) {
        var step1 = ['number'];
        var step2 = ['signature'];

        var step = '';
        step = checkError(data.error, step, step1, 'one');
        step = checkError(data.error, step, step2, 'two');

        if (step != '') {
            window.location.href = '#step-' + step;
            $('#tab-t .' + step + ' a').click();
        }
    }

}
