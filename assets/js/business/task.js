/**
 * Created with JetBrains PhpStorm.
 * User: user
 * Date: 13-10-24
 * Time: 下午3:05
 * To change this template use File | Settings | File Templates.
 */
/*发布*/
$(function(){
    pro_type_change();
})

function pro_type_change(){
    $('.Js_forever').click(function(){
        $('.Js_limit_content').hide();
        $('.Js_is_limit_end_time').val('0');
    })

    $('.Js_limit_time').click(function(){
        $('.Js_limit_content').show();
        $('.Js_is_limit_end_time').val('1');
    })
}

$('.Js_send_project').click(function() {
    var data = $('#form_task_publish').serialize();
    var url  = 'task/publish';
    ajax(url, data, taskPublishSuccess);
})

function taskPublishSuccess(data) {
    if (data.status == 0) {
        window.location.href= data.content;
    } else if (data.status == 30001) {
        window.location.href= base_url + 'login';
    }else {
        var step1 = ['title', 'area', 'end_time', 'award_money', 'cover'];
        var step2 = ['intro'];

        var step3 = ['is_agree_protocol', 'signature'];

        var step = '';
        step = checkError(data.error, step, step1, 'one');
        step = checkError(data.error, step, step2, 'two');
        step = checkError(data.error, step, step3, 'three');

        if (step != '') {
            window.location.href = '#step-' + step;
            $('#tab-t .' + step + ' a').click();
        }
    }
}

/*提交作品(参赛)*/
$('#Js_task_send_agreement').bind('click', function(){
    var data = $('#form_submit_work').serialize();
    var url  = 'task/add-work';
    ajax(url, data, addWorkSuccess);
});

function addWorkSuccess(data) {
    if (data.status == 0) {
        window.location.href = data.content;
    } else {
        var step1 = ['is_agree_protocol','signature'];
        var step2 = ['intro', 'work_pics'];

        var step = '';
        step = checkError(data.error, step, step1, 'one');
        step = checkError(data.error, step, step2, 'two');

        if (step != '') {
            window.location.href = '#step-' + step;
            $('#tab-t .' + step + ' a').click();
        }
    }
}

/*撤回/取消撤回作品*/
$('.Js_zp_cancel').live('click', function() {
	var obj = $(this);
    var work_id = $(this).attr('data');
    var data = {work_id:work_id};
    var status = $(this).attr('data-status');
    if (status == 1) {
    	obj.prev('.Js_winner_btn').hide();
    	obj.next('.Js_withdrawed').show();
        obj.text('取消撤回').css('background','#888888').attr('data-status', 2);
        var url  = 'task/withdraw-work';
    } else {
    	obj.prev('.Js_winner_btn').show();
    	obj.next('.Js_withdrawed').hide();
        obj.text('撤回作品').css('background','#abae8f').attr('data-status', 1);
        var url = 'task/cancel-withdraw-work';
    }
    ajax(url, data, withdrawWorkSuccess,'', $(this));
});

function withdrawWorkSuccess(data, obj) {
    if (data.status == 0) {

    }
}

/*淘汰/取消淘汰作品*/
$('.Js_eliminate').live('click', function() {
	var obj = $(this);
    var work_id = $(this).attr('data');
    var data = {work_id:work_id};
    var status = $(this).attr('data-status');
    if (status == 1) {
    	obj.prev('.Js_winner_btn').hide();
    	obj.next('.Js_fired').show();
        obj.text('取消淘汰').attr('data-status', 2);
        var url  = 'task/fire-work';
    } else {
    	obj.prev('.Js_winner_btn').show();
    	obj.next('.Js_fired').hide();
        obj.text('淘汰该作品').attr('data-status', 1);
        var url = 'task/cancel-fire-work';
    }
    ajax(url, data, fireWorkSuccess,'', $(this));
});
function fireWorkSuccess(data, obj) {
    if (data.status == 0) {
        if (obj.attr('data-status') == 1) {
            obj;
        } else {
            obj;
        }
    }
}

/*选为获胜作品*/
$('.Js_confirm_winner').bind('click', function() {
    var work_id = $(this).attr('data');
    var data = {work_id:work_id};
    var url  = 'task/select-winner';
    ajax(url, data, selectWinnerSuccess);
});

function selectWinnerSuccess(data) {
    if (data.status == 0) {
        window.location.reload();
    }
}

/*签署装让协议*/
$('.Js_sign_zr').bind('click', function() {
/*    var task_id = $(this).attr('data');
    var signature = $('#zr_signature').val();*/

    var data = $('#form_sign_transfor_agreement').serialize();
    var url  = 'task/sign-transfer-agreement';
    ajax(url, data, signTransferSuccess);
});

function signTransferSuccess(data) {
    if (data.status == 0) {
        window.location.reload();
    } else if (data.status == 10001) {
        var fields = ['signature','is_agree_protocol'];
        checkErrorCommon(fields, data.error);
    }
}

/*确认任务成果*/
$('.Js_confirm_achievement').bind('click', function(){
    var task_id = $(this).attr('data');
    var data = {task_id:task_id};
    var url  = 'task/confirm-achievement';
    ajax(url, data, confirmAchievementSuccess);
});

function confirmAchievementSuccess(data) {
    if (data.status == 0) {
        window.location.reload();
    }
}

$('.rateit-range').live('click',function(){
	var score = $(this).find('.rateit-selected').width()/24;
	var url = 'task/score';
	var data={work_id:$(this).parent().attr('target_id'),score:score}
	ajax(url,data,function(){})	
})
