/**
 * Created with JetBrains PhpStorm.
 * User: user
 * Date: 13-10-24
 * Time: 上午10:28
 * To change this template use File | Settings | File Templates.
 */
/*发布*/
$(function(){
    pro_type_change();
})

function pro_type_change(){
    $('.Js_forever').click(function(){
        $('.Js_limit_content').hide();
        $('.Js_invest_time_end_type').val('1');
    })

    $('.Js_limit_time').click(function(){
        $('.Js_limit_content').show();
        $('.Js_invest_time_end_type').val('2');
    })
}

$('.Js_send_project').click(function() {
    var data = $('#form_project_publish').serialize();
    var url  = 'project/publish';
    ajax(url, data, projectPublishSuccess);
})

function projectPublishSuccess(data) {
    if (data.status == 0) {
        window.location.href= data.content;
    } else if (data.status == 30001) {
        window.location.href= base_url + 'login';
    }else {
        var step1 = ['title', 'area', 'end_time', 'investment_goal_amount', 'cover'];
        var step2 = ['content', 'investment_return_delivery_time', 'investment_return_pics'];
        var step3 = ['intro'];
        var step4 = ['is_agree_protocol', 'signature'];

        var step = '';
        step = checkError(data.error, step, step1, 'one');
        step = checkError(data.error, step, step2, 'two');
        step = checkError(data.error, step, step3, 'three');
        step = checkError(data.error, step, step4, 'four');

        if (step != '') {
            window.location.href = '#step-' + step;
            $('#tab-t .' + step + ' a').click();
        }
    }
}

/*我要投资*/
$('.Js_pay_alls').click(function(){
    $('.Js_pay_fq_div').hide();
})

$('.Js_pay_fq').click(function(){
    $('.Js_pay_fq_div').show();
})

$('#Js_pro_send_agreement').bind('click', function(){
    var data = $('#form_project_invest').serialize();
    var url  = 'project/investment';
    ajax(url, data, projectInvestSuccess);
});

function projectInvestSuccess(data) {
    if (data.status == 0) {
        window.location.href = data.content;
    }  else if (data.status == 30001) {
        window.location.href= base_url + 'login';
    } else {
        var step1 = ['payment_method', 'invest_intro', 'invest_money', 'deposit_percentage', 'final_percentage', 'final_payment_pay_time'];
        var step2 = ['address', 'zip_code', 'phone_number'];
        var step3 = ['signature','is_agree_protocol'];

        var step = '';
        step = checkError(data.error, step, step1, 'one');
        step = checkError(data.error, step, step2, 'two');
        step = checkError(data.error, step, step3, 'three');

        if (step != '') {
            window.location.href = '#step-' + step;
            $('#tab-t .' + step + ' a').click();
        }
    }
}

/*立即支付全款*/
$('#Js_just_payallmoney').click(function(){
	var project_id = $(this).attr('project-id');
	var data = {project_id:project_id};	
 	var url = 'project/pay-total';
   	ajax(url, data, payallMoney);
})

function payallMoney(data){
	if (data.status == 0) {
        window.location.reload();
    }	
}

/*立即支付尾款*/
$('#Js_just_payfinalmoney').click(function(){
	var project_id = $(this).attr('project-id');
	var data = {project_id:project_id};	
 	var url = 'project/pay-final';
   	ajax(url, data, payFinalMoney);
})

function payFinalMoney(data){
	if (data.status == 0) {
        window.location.reload();
    }	
}

/*立即发放回报*/
$('#Js_just_payment').click(function(){
	var project_id = $(this).attr('project-id');
	var data = {project_id:project_id};	
 	var url = 'project/provide-return';
   	ajax(url, data, payMent);
})

function payMent(data){
	if (data.status == 0) {
        window.location.href = data.content;
    }	
}

/*确认收到回报*/
$('#Js_get_payment').click(function(){
	var project_id = $(this).attr('project-id');
	var data = {project_id:project_id};	
 	var url = 'project/confirm-return';
   	ajax(url, data, getPayMent);
})

function getPayMent(data){
	if (data.status == 0) {
        window.location.href = data.content;
    }	
}

/*支付定金*/
/*$('#Js_just_paydeposit').click(function(){
	var project_id = $(this).attr('project-id');
	var data = {project_id:project_id};	
 	var url = 'project/pay-deposit';
   	ajax(url, data, payDeposit);
})

function payDeposit(data){
	if (data.status == 0) {
        window.location.reload();
    }	
}*/

/*发布更新*/
$('.Js_do_update').bind('click', function(){
    var data = $('#form_project_update').serialize();
	console.log(data);
    var url  = 'project/update';
    ajax(url, data, updatePublishSuccess);
});

function updatePublishSuccess(data) {
    if (data.status == 0) {
        window.location.href = base_url + "project/detail/" + data.content;
    } else if(data.status == 30001) {
        window.location.href= base_url + 'login';
    } else {
        var fields = ['update_title', 'update_content', 'pics'];
        checkErrorCommon(fields, data.error);
    }
}

/*接受投资*/
$('.Js_accept_invest').live('click', function(){
   var invest_id = $(this).attr('data');
   var data = {investment_id:invest_id,project_id:project_id};
   var url = 'project/confirm-investment';
   ajax(url, data, acceptInvestSuccess);
});

function acceptInvestSuccess(data) {
    if (data.status == 0) {
        window.location.reload();
    }
}

/*撤回投资*/
$('.Js_withdraw_invest').live('click', function() {
	var invest_id = $(this).attr('data');
	var data = {investment_id:invest_id, project_id:project_id}
	var url  = 'project/withdraw-investment';
	ajax(url, data, withdrawInvestSuccess);
});

function withdrawInvestSuccess(data) {
	if (data.status == 0) {
		window.location.reload();
	} else if (data.status == 10050) {
		alert('发布者已经接受了您的投资，无法进行撤回操作。');
	}
}

/*取消投资*/
$('.Js_cancel_invest').live('click', function() {
    var invest_id = $(this).attr('data');
    var data = {investment_id:invest_id,project_id:project_id};
    var url = 'project/cancel-investment';
    ajax(url, data, cancelInvestSuccess);
});

function cancelInvestSuccess(data) {
    if (data.status == 0) {
        window.location.reload();
    }
}