$(function(){
	ex_type_change();
	msg_error_check();
})

/*竞拍价格和固定价格类别选择*/
function ex_type_change(){
	$('#Js_check_jp').parent().click(function(){
		$('.Js_setting_gd').hide();
		$('.Js_setting_jp').show();
	})
	
	$('#Js_check_gd').parent().click(function(){
		$('.Js_setting_gd').show();
		$('.Js_setting_jp').hide();
	})
}

function msg_error_check(){
	$('.Js_send_project').click(function(){
		var step = '';

        var form_name = $('#form-send-trade');
        var form_data = form_name.serialize();
        var form_post_url = form_name.attr('action');
		
		if(!$('#Js_readed_agreement').is(':checked')){
			$('#Js_readed_agreement').parents('p').next().show();
			step = 'three';
		}else{
			$('#Js_readed_agreement').parents('p').next().hide();	
		}
		
		if(!$('#Js_signature').val()){
			$('#Js_signature').parent().parent().next().show();
			step = 'three';
		}else{
			$('#Js_signature').parent().parent().next().hide();	
		}
		
		if($('#js_content').val()==""){
			$('#js_content').parents('.toolbar-content').next().show();
			$('#js_content').parents('.toolbar-content').find('.redactor_box').addClass('error');
			step = 'two';
		}else{
			$('#js_content').parents('.toolbar-content').next().hide();
			$('#js_content').parents('.toolbar-content').find('.redactor_box').removeClass('error');
		}
		
		if($('#Js_task_name_input').val()==""){
			$('#Js_task_name_input').parents('.tools').addClass('error').next().show();
			step = 'one';
		}else{
			$('#Js_task_name_input').parents('.tools').removeClass('error').next().hide();
		}
		
		if($('#Js_loc_input').val()==""){
			$('#Js_loc_input').parents('.tools').addClass('error').next().show();
			step = 'one';
		}else{
			$('#Js_loc_input').parents('.tools').removeClass('error').next().hide();
		}
		
		if($('#Js_check_jp').is(':checked')){
			var obj = $('.Js_setting_jp').find('.Js_input_text');
			if(!checkTextForm.nonZeroBegin(obj.val())){
				obj.parents('.tools').addClass('error').next().show();
				step = 'one';	
			}else{
				obj.parents('.tools').removeClass('error').next().hide();	
			}
			
			if($('#date1').val()==""){
				$('#date1').parent().addClass('error').next().show();
				step = 'one';
			}else{
				$('#date1').parent().removeClass('error').next().hide();
			}
		}
		
		if($('#Js_check_gd').is(':checked')){
			var $price_gd = $('.Js_setting_gd').find('.Js_input_text[name=buy_now_price]');
			if(!checkTextForm.nonZeroBegin($price_gd.val())){
                $price_gd.parents('.tools').addClass('error').next().show();
				step = 'one';	
			}else{
                $price_gd.parents('.tools').removeClass('error').next().hide();
			}
			var $trade_num = $('.Js_setting_gd').find('.Js_input_text[name=num_for_trade]');
			if(!checkTextForm.nonZeroBegin($trade_num.val())){
                $trade_num.parent().addClass('error').next().show();
				step = 'one';
			}else{
                $trade_num.parent().removeClass('error').next().hide();
			}
		}
		
		if($('.Js_fm_cover_div').children().length<=0){
			$('.Js_fm_cover_div').parent().find('.errorMsg').show();
			step = 'one';
		}else{
			$('.Js_fm_cover_div').parent().find('.errorMsg').hide();
		}
		
		if(step=='one'){
			$("#tab-t .one a").click();
		}else if(step=='two'){
			$("#tab-t .two a").click();
		}else if(step=='three'){
			$("#tab-t .three a").click();
		}else if(step==''){
            $.ajax({
                url:"test.php",
                dataType : 'json',
                type : 'post',
                //data : form_data,
                success : function(responds){
                    if(responds.status == '1'){
                        window.location.href="trade-bid.php";
                    }
                    else{
                        alert('ERROR_CODE:' + responds);
                    }
                }
            })
		}
	})	
}