$(function(){
	pro_type_change();
	msg_error_check();
})

/*永久和限制时间类别选择*/
function pro_type_change(){
	$('.Js_forever').click(function(){
		$('.Js_limit_content').hide();
	})
	$('.Js_limit_time').click(function(){
		$('.Js_limit_content').show();	
	})
}

function msg_error_check(){
	$('.Js_send_project').click(function(){
		var step = "";

        var form_name = $('#form-send-project');
        var form_data = form_name.serialize();
        var form_post_url = form_name.attr('action');

		if(!$('#Js_readed_agreement').is(':checked')){
			$('#Js_readed_agreement').parents('p').next().show();
			step = 'four';
		}else{
			$('#Js_readed_agreement').parents('p').next().hide();	
		}
		
		if(!$('#Js_signature').val()){
			$('#Js_signature').parent().parent().next().show();
			step = 'four';
		}else{
			$('#Js_signature').parent().parent().next().hide();	
		}
		
		if($('#js_content').val()==""){
			$('#js_content').parents('.toolbar-content').next().show();
			$('#js_content').parents('.toolbar-content').find('.redactor_box').addClass('error');
			step = 'three';
		}else{
			$('#js_content').parents('.toolbar-content').next().hide();
			$('#js_content').parents('.toolbar-content').find('.redactor_box').removeClass('error');
		}
		
		if($('#tz_content').val()==""){
			$('#tz_content').parents('.toolbar-content').next().show();
			$('#tz_content').parents('.toolbar-content').find('.redactor_box').addClass('error');
			step = 'two';
		}else{
			$('#tz_content').parents('.toolbar-content').next().hide();
			$('#tz_content').parents('.toolbar-content').find('.redactor_box').removeClass('error');
		}
		
		if($('#date2').val()==""){//投资回报兑现时间
			$('#date2').parent().addClass('error').next().show();
			step = 'two';
		}else{
			$('#date2').parent().removeClass('error').next().hide();
		}
		
		if($('.Js_upload_more').children().length<=0){
			$('.Js_upload_more').parent().find('.errorMsg').show();
			step = 'two';
		}else{
			$('.Js_upload_more').parent().find('.errorMsg').hide();
		}
		
		if($('#Js_project_name_input').val()==""){
			$('#Js_project_name_input').parents('.tools').addClass('error').next().show();
			step = 'one';
		}else{
			$('#Js_project_name_input').parents('.tools').removeClass('error').next().hide();
		}
		if($('#Js_loc_input').val()==""){
			$('#Js_loc_input').parents('.tools').addClass('error').next().show();
			step = 'one';
		}else{
			$('#Js_loc_input').parents('.tools').removeClass('error').next().hide();
		}
		
		if($('.Js_limit_time').find('input').is(':checked')){
			if($('#date1').val()==""){
				$('#date1').parent().addClass('error').next().show();
				step = 'one';
			}else{
				$('#date1').parent().removeClass('error').next().hide();
			}
		}
		if(!checkTextForm.nonZeroBegin($('#Js_find_money').val())){
			$('#Js_find_money').parents('.tools').addClass('error').next().show();
			step = 'one';
		}else{
            $('#Js_find_money').parents('.tools').removeClass('error').next().hide();
		}
		if($('.Js_fm_cover_div').children().length<=0){
			$('.Js_fm_cover_div').parent().find('.errorMsg').show();
			step = 'one';
		}else{
			$('.Js_fm_cover_div').parent().find('.errorMsg').hide();
		}
		if(step=='one'){
			$("#tab-t .one a").click();
		}else if(step=='two'){
			$("#tab-t .two a").click();
		}else if(step=='three'){
			$("#tab-t .three a").click();	
		}else if(step=='four'){
			$("#tab-t .four a").click();	
		}else if(step==''){
            $.ajax({
                url:form_post_url,
                dataType : 'json',
                type : 'post',
                data : form_data,
                success : function(responds){
                    if(responds == '0'){
                        window.location.href="project-going.php";
                    }
                    else{
                        alert('ERROR_CODE:' + responds);
                    }
                }
            })
		}
	})	
}