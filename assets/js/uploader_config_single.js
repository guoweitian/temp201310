$(function() {
	// var fileTemplate = "<div class=\"fbox\" id=\"{{id}}\">";
	// fileTemplate += "<div class=\"progressbar\"><span>0%</span></div>";
	// fileTemplate += "<div class=\"preview\"></div>";
	// fileTemplate += "<div class=\"filename\"></div>";
	// fileTemplate += "</div>";
	var fileTemplate= '<div class="Js_updateImg"><div class="updateImgLoadDiv">'
					+'		<a href="javascript:;" class="close Js_upload_delete"></a>'
					+'		<div class="updateImgLoad">'
					+'			<span class="sc">图片上传中</span><span class="scName"></span>'
					+'			<div class="processDiv">'
					+'				<div class="opacityBorder"></div>'
					+'				<div class="process" style="width:0%">'
					+'					<span class="process-text">0%</span>'
					+'				</div>'
					+'			</div>'
					+'		</div>'
					+'	</div>'
					+'	<div class="height_10"></div></div>';
	//uploaderFun($('#upload-single'));
	var upload;
	$(".uploadWrap-single , .file_uploadbox-single").html5Uploader({
		name: 'userfile',
		postUrl:'./upload.php',//base_url+"/post_file.php",
		onClientAbort: function(){
			alert("上传中断");
		},
		onClientLoadStart: function(e, file, xhr) {
			for(var i=0; i<xhr_arr.length; i++){
				if(xhr===xhr_arr[i]){
					var xhrIndex = 	i;
				}	
			}
			if(file.type.indexOf('image')<0){
				xhr.abort();
				xhr_arr.splice(xhrIndex,1);
				if(typeof FileReader == 'undefined') fileName.splice(xhrIndex,1);
				alert('上传格式不正确');
			}else{
				var obj = $(fileTemplate)
				if(upload.find('.updateSucImg').length>0){
					upload.find('.updateSucImg').eq(0).before(obj);
				}else{
					upload.append(obj);
					upload.parents('.rowblock').find('.uploadWrap-single').hide();
				}
				uploadObjArr.push(obj);
			}
		},
		onClientLoad: function(e, file, xhr) {
			for(var i=0; i<xhr_arr.length; i++){
				if(xhr===xhr_arr[i]){
					var xhrIndex = 	i;
				}	
			}
			
			if(file.type.indexOf('image')>=0){
				uploadObjArr[xhrIndex].find('.scName').text(file.name+'(' + parseInt(file.size / 1024) + 'KB)');
			}
		},
		onServerLoadStart: function(e, file, xhr) {
			if(typeof FileReader == 'undefined'){
				var obj = $(fileTemplate)
				if(upload.find('.updateSucImg').length>0){
					upload.find('.updateSucImg').eq(0).before(obj);
				}else{
					upload.append(obj);
					upload.parents('.rowblock').find('.uploadWrap-single').hide();
				}
				uploadObjArr.push(obj);
			}
		},
		onServerProgress: function(e, file, xhr) {
			for(var i=0; i<xhr_arr.length; i++){
				if(xhr===xhr_arr[i]){
					var xhrIndex = 	i;
				}	
			}
			
			var percentComplete = e.loaded / e.total;
			var precentCompleteNum = parseInt((parseInt(e.loaded) / parseInt(e.total))*100);
			uploadObjArr[xhrIndex].find(".process").css("width",precentCompleteNum.toString()+"%");
			uploadObjArr[xhrIndex].find(".process-text").text(precentCompleteNum.toString()+"%");
			if(typeof FileReader == 'undefined'){
				uploadObjArr[xhrIndex].find('.scName').text(file.name+'(' + parseInt(e.total / 1024) + 'KB)');
			}
		},
		onServerLoad: function(e, file) {
			
		},
		onSuccess:function(e, file, xhr, data){
			var data = eval ('('+ data +')')
			var status = data.status;
			if(status== 0){
				switch (data.ext){
				   	case 'doc':
				     	break
				   	case 'pdf':
				     	break
				   	default:
						for(var i=0; i<xhr_arr.length; i++){
							if(xhr===xhr_arr[i]){
								var xhrIndex = 	i;
							}	
						}
						uploadObjArr[xhrIndex].remove();
						xhr_arr.splice(xhrIndex,1);
						uploadObjArr.splice(xhrIndex,1);
						if(typeof FileReader == 'undefined') fileName.splice(xhrIndex,1);
						var path = data.content;
						var tpic ='<div class="updateSucImg">'
								+'	<img src="./'+path+'">'
								+'	<a href="javascript:;" title="删除" class="close Js_file_delete"></a>'
								+'	<input name="cover" class="upload_success_cover" type="hidden" value="'+path+'" />'
								+'</div>';
						upload.append(tpic);
				}
			}else{
				if(data.error){
					alert(data.error);//这里出要弹出意外信息，比如不支持的上传文件格式
				}else{
					alert("上传文件格式不正确，请从新上传")
				}
				upload.parents('.rowblock').find('.uploadWrap-single').show();
			}
		}
	});
	
	$('.Js_upload_delete').live('click',function(){
		for(var i=0; i<uploadObjArr.length; i++){
			if($(this).parents('.Js_updateImg').get(0)===uploadObjArr[i].get(0)){
				var objIndex = 	i;
			}	
		}
		xhr_arr[objIndex].abort();
		xhr_arr.splice(objIndex,1);
		uploadObjArr.splice(objIndex,1);
		if(typeof FileReader == 'undefined') fileName.splice(objIndex,1);
		$(this).parent().next().remove();
		$(this).parents('.rowblock').find('.uploadWrap-single').show();
		$(this).parent().remove();
	})
	
	$(".uploadWrap-single .uploadbox").live('click',function(){
		upload = $(this).parents('.rowblock').find('.upload-single');
		$(this).parent().next().click();
	});
    $(".tuploadbox").click(function(){
      $(".file_uploadbox-single").click();
    });

	
	$(".upload-single .Js_file_delete").die().live("click",function(){
		var that = $(this);
		var file_id = null;//文件id
		var match_id = null//比赛id
		var fdata = {task_id:file_id,match_id:match_id}
		//ajax_(base_url+'task/removeFinalWork',fdata,delete_file_c,that);  //发送Ajax数据
		delete_file_c(that)//删除成功回调
	})
	//删除文件成功回调
	function delete_file_c(obj){
		obj.parents('.rowblock').find('.uploadWrap-single').show();
		$(".uploadWrap-single").parents(".updateSucImg").remove();
		var ie = (navigator.appVersion.indexOf("MSIE")!=-1);//IE
		if(ie){
			//$(".file_uploadbox-single").parent()[0].reset();
		}else{
			$(".file_uploadbox-single").val("");
		}
		obj.parent().remove();
		return false
	}
});