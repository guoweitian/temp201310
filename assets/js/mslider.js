﻿
function overall(root){
    var root = root || document;
    var re = /j_([\w_]+)/;
    var funcs = {};
    $(".js",root).each(function(i) {
        var m = re.exec(this.className);
        if (m) {
            var f = funcs[m[1]];        
            if (!f) {
                f = eval('CF.' + m[1].replace(/\_/gi,'.'));
                funcs[m[1]] = f;
            }           
            f && f(this);
        }
    });
}

var CF = new Object();
//首页
CF.index = {
    carouseProduct: function(obj){ 
        var obj = $(obj);
        var prevChild = obj.prev();
        var visible = 3;
        var liNums = $('li', obj).length;  
        var pageNum = Math.ceil( liNums/visible );
        var pageStr = '';
        prevChild.append('<ul class="jcarouseLiteNav"></ul>');
        var jcarouseLiteNav = $('>ul.jcarouseLiteNav', prevChild);      
        for(var i = 0; i < pageNum; i++){
            pageStr += '<li><a class="' + (i + 1) + '"><span>' + (i + 1) + '</span></a></li>';
        }
        //带有当前点点
        //jcarouseLiteNav.prepend('<li class="prev"><a><span>previous</span></a></li>' + pageStr + ' <li class="next"><a><span>next</span></a></li>');
        jcarouseLiteNav.prepend('<li class="prev"><a><span>previous</span></a></li><li class="next"><a><span>next</span></a></li>');
        $('>li .1', jcarouseLiteNav).parent().addClass('current');

        if(liNums <=visible){
            jcarouseLiteNav.hide();
        }
        obj.jCarouselLite({
            btnNext: $('>li.next ', jcarouseLiteNav),
            btnPrev: $('>li.prev a', jcarouseLiteNav),
            visible: visible,
            scroll: visible,
            speed: 1200,
            afterEnd: function(a){

            },
            btnGo: $('>li:not([class*=previous]):not([class*=next]) a', jcarouseLiteNav)
        })
        var width = obj.width();
        obj.width( width - 3 );
    }
}


$(function() {
    overall();
	if($('.j_index_carouseProduct').find('li').length==3){
		$('.j_index_carouseProduct').find('ul').css('left','-640px')	
	}
})
