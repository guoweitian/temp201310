<?php

/*
 *  Define page vars
 */

$load_css = array(
        'style.1.0.3',
        'jquery.fancybox',
);

$load_js = array(
        'jquery',
        'jquery.pstrength-min.1.2',
        'jquery.html5uploader',
        'uploader_config_single',
        'jquery.fancybox.min',
        'common',
        'detail',
        'login',
);

$title_text = '创意世界';

$special_text = '<li class="listItem"><a href="javascript:;" class="Js_user_select">我的动态</a></li>';

$navigation_text = array(
        '0' => '<a class="on" href="javascript:;">全部</a>',
        '1' => '<a href="javascript:;">用户</a>',
        '2' => '<a href="javascript:;">帖子</a>',
        '3' => '<a href="javascript:;">照片</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation.php');
include('modules/sidebar.php');

include('blocks/set-new-password.php');

include('modules/footer.php');
?>