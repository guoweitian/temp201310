<?php

/*
 *  Define Navigation Text
 */

$load_css = array(
        'style.1.0.3',
        'jquery.fancybox',
);

$load_js = array(
        'jquery',
        'jquery.html5uploader',
        'uploader_config_single',
        'jquery.fancybox.min',
        'common',
);

$title_text = '创意世界交易所';

$navigation_text = array(
        '0' => '<a href="trade-list.php">正在进行的交易</a>',
        '1' => '<a class="on" href="trade-list-over.php">已经完成的交易</a>',
        '2' => '<a href="trade-publish.php">发布交易</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-trade.php');
include('modules/sidebar.php');

include('blocks/trade-list-over.php');

include('modules/footer.php');
?>