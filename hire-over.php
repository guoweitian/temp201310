<?php

/*
 *  Define Navigation Text
 */
$scroll = 1;
$load_css = array(
        'style.1.0.3',
        'redactor',
        'rateit',
        'jquery.fancybox',
        'jquery.fancybox-thumbs',
        'dp',
);

$load_js = array(
        'jquery',
        'redactor.min',
        'redactor_zh_cn',
        'redactor_config',
        'jquery.html5uploader',
        'uploader_config_single',
        'uploader_config_more',
        'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'tabs',
        'jquery.rateit.min',
        'jquery.datepicker',
        'datepicker_lang_CN',
        'publish',
        'common',
        'detail',
        'theme',
);

$title_text = '创意公益明星比赛';

$navigation_text = array(
        '0' => '<a class="on" href="javascript:;">比赛详情</a>',
        '1' => '<a href="javascript:;">明星排行</a>',
        '2' => '<a href="javascript:;">比赛评委</a>',
        '3' => '<a href="javascript:;">讨论区</a>',
        '4' => '<a href="javascript:;">竞猜</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-hire.php');
include('modules/sidebar.php');

include('blocks/hire-over.php');

include('modules/footer.php');
?>