<?php

/*
 *  Define Navigation Text
 */
$scroll = 1;
$load_css = array(
        'style.1.0.3',
        'redactor',
        'dp',
        'jquery.fancybox',
        'jquery.fancybox-thumbs'
);

$load_js = array(
        'jquery',
        'markerclusterer',
        'jquery.html5uploader',
        'uploader_config_single',
        'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'common',
        'detail',
        'map',
        'redactor',
        'redactor_zh_cn',
        'redactor_config',
        'theme',
);

$title_text = '创意世界地区';

$navigation_text = array(
        '0' => '<a class="on" href="javascript:;">地区资讯</a>',
        '1' => '<a href="javascript:;">地区智库</a>',
        '2' => '<a href="javascript:;">地区大赛</a>',
        '3' => '<a href="javascript:;">地区交易所</a>',
        '4' => '<a href="javascript:;">交流区</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-area.php');
include('modules/sidebar.php');

include('blocks/area.php');

include('modules/footer.php');
?>