<?php

/*
 *  Define Navigation Text
 */
$scroll = 1;
$load_css = array(
        'style.1.0.3',
        'redactor',
        'jquery-ui',
        'dp',
        'jquery.fancybox',
        'jquery.fancybox-thumbs',
);

$load_js = array(
        'jquery',
        'jquery.html5uploader',
        'uploader_config_single',
        'uploader_config_more',
        'jquery.fancybox.min',
        'jquery.fancybox-thumbs',
        'jquery.datepicker',
        'datepicker_lang_CN',
        'tabs',
        'jquery.rateit.min',
        'linkage_slider',
        'jquery-ui',
        'tCity',
        'city_select',
        'detail',
        'publish',
        'theme',
        'common',
        'redactor',
        'redactor_zh_cn',
        'redactor_config',
        'jquery.fancybox',
);

$title_text = '投资项目比赛';

$navigation_text = array(
        '0' => '<a class="on" href="javascript:;">项目详情</a>',
        '1' => '<a href="javascript:;">项目更新</a>',
        '2' => '<a href="javascript:;">投资者</a>',
        '3' => '<a href="javascript:;">专家点评</a>',
        '4' => '<a href="javascript:;">讨论区</a>',
        '5' => '<a href="javascript:;">竞猜</a>',
);

include('modules/header.php');
include('modules/ccz.php');
include('modules/navigation-project.php');
include('modules/sidebar.php');

include('blocks/project-waiting-for-pay-deposit-publisher.php');

include('modules/footer.php');
?>