<?php

/*
 *  Define page vars
 */
$scroll = 1;
$load_css = array(
    'style.1.0.3',
    'jquery.fancybox',
    'jquery.fancybox-thumbs'
);

$load_js = array(
    'jquery',
    'common',
    'detail',
    'imagesloaded.min',
    'jquery.fancybox.min',
    'jquery.fancybox-thumbs',
    'foundation.min',
    'jquery.html5uploader',
    'uploader_config_single',
    'uploader_config_more',
    'tCity',
    'city_select',
    'vendor/custom.modernizr',
    'masonry.min',
    'imagesloaded.min'
);

$title_text = '';

$special_text = '<li class="listItem"><a href="javascript:;" class="Js_user_select">我的动态</a></li>';

$navigation_text = array(
    '0' => '<a class="on" href="javascript:;">热门资讯</a>',
    '1' => '<a href="javascript:;">创意世界智库</a>',
    '2' => '<a href="javascript:;">创意世界地区</a>',
    '3' => '<a href="javascript:;">朋友圈动态</a>'
);

if(isset($_GET["isLogin"])){
    $login_flag = true;
}else{
    $login_flag = false;
}
include('modules/header.php');

if($login_flag){
    include('modules/ccz.php');
}else{
    include('modules/ccz-logout.php');
}
include('modules/navigation.php');
include('modules/sidebar.php');

include('blocks/index.php');

include('modules/footer.php');
?>

